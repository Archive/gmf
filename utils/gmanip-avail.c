/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "gmanip.h"

static void gmanip_avail_list_populate(AvailFilterList *list);
static void gmanip_avail_drag_begin(GtkWidget *widget, GdkDragContext *context,
				    AvailFilterList *list);
static void gmanip_avail_drag_data_get(GtkWidget *widget,
				       GdkDragContext *context,
				       GtkSelectionData *data,
				       guint info,
				       guint time,
				       AvailFilterList *list);
static void gmanip_avail_drag_begin(GtkWidget *widget,
				    GdkDragContext *context,
				    AvailFilterList *list);

static const GtkTargetEntry drag_targets[] = {
  {"application/x-gmffilter-oafiid", 0, 0},
  {"application/x-gnome-oafiid", 0, 1}
};

AvailFilterList *
gmanip_avail_create(void)
{
  static const char *column_titles[] = {
    "Name",
    "Description",
    "Input Types",
    "Output Types"
  };

  AvailFilterList *retval;

  retval = g_new0(AvailFilterList, 1);

  retval->widget = GTK_CLIST( gtk_clist_new_with_titles (ELEMENTS(column_titles),
			      				 (char **)column_titles));

  gmanip_avail_list_populate (retval);

  gtk_clist_columns_autosize (GTK_CLIST(retval->widget));

  /* XXX TODO - nice icon for filter drags */
  gtk_drag_source_set(GTK_WIDGET(retval->widget),
		      GDK_BUTTON1_MASK, drag_targets,
		      ELEMENTS(drag_targets),
		      GDK_ACTION_COPY);

  gtk_signal_connect(GTK_OBJECT(retval->widget), "drag_data_get",
		     GTK_SIGNAL_FUNC(gmanip_avail_drag_data_get), retval);
  gtk_signal_connect(GTK_OBJECT(retval->widget), "drag_begin",
		     GTK_SIGNAL_FUNC(gmanip_avail_drag_begin), retval);

  return retval;
}

static GSList *
get_lang_list (void)
{
        GSList *retval;
        char *lang;
        char * equal_char;

        retval = NULL;

        lang = g_getenv ("LANGUAGE");

        if (!lang) {
                lang = g_getenv ("LANG");
        }


        if (lang) {
                equal_char = strchr (lang, '=');
                if (equal_char != NULL) {
                        lang = equal_char + 1;
                }

                retval = g_slist_prepend (retval, lang);
        }
        
        return retval;
}

static void
gmanip_avail_list_populate (AvailFilterList *list)
{
	CORBA_Environment ev;
	char *query;
	OAF_ServerInfoList *oaf_result;
	int index, row_idx;
	gchar *cols[4];

	CORBA_exception_init(&ev);

	/* Query OAF for all filters that can handle some type of protocol */
	query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')");

	oaf_result = oaf_query (query, NULL, &ev);	
	if (oaf_result == NULL) {
		g_message ("gmf_filter_activate_for_url: oaf_query no results");
	}

	g_free (query);

	/* Check and see if we found any filters */
 	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL) {
		GSList *langs;

 	        langs = get_lang_list ();
		
		for (index = 0; index < oaf_result->_length; index++) {			
			OAF_ServerInfo *server;

                        server = &oaf_result->_buffer[index];

			cols[0] = (char *)oaf_server_info_prop_lookup (server, "name", langs);
			cols[1] = (char *)oaf_server_info_prop_lookup (server, "description", langs);
			cols[2] = (char *)oaf_server_info_prop_lookup (server, "gmf:input_types", langs);
			cols[3] = (char *)oaf_server_info_prop_lookup (server, "gmf:output_types", langs);

    			row_idx = gtk_clist_append(GTK_CLIST(list->widget), cols);

    			gtk_clist_set_row_data (GTK_CLIST(list->widget), row_idx, server->iid);
		}
		g_slist_free (langs);
	} 

	/* Clean up query result */
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}

	CORBA_exception_free(&ev);

}

void
gmanip_avail_destroy(AvailFilterList *list)
{
	g_free(list);
}

static void
gmanip_avail_drag_begin(GtkWidget *widget,
			GdkDragContext *context,
			AvailFilterList *list)
{
  list->dragged_row = GTK_CLIST(widget)->focus_row;
}

static void
gmanip_avail_drag_data_get(GtkWidget *widget,
			   GdkDragContext *context,
			   GtkSelectionData *data,
			   guint info,
			   guint time,
			   AvailFilterList *list)
{
 const char *iid;

  iid = gtk_clist_get_row_data(list->widget, list->dragged_row);

  gtk_selection_data_set(data,
			 gdk_atom_intern(drag_targets[info].target,
					 FALSE),
			 8, iid,
			 strlen(iid));
}
