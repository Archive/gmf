/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMANIP_H
#define GMANIP_H 1

#include <liboaf/oaf.h>
#include <gmf.h>

#include "../libgmf/gmf-object-directory.h"


#define ELEMENTS(x) sizeof(x)/sizeof(x[0])

typedef struct {
  GtkCList *widget;
  GList *slist;
  gint dragged_row;
} AvailFilterList;
AvailFilterList *gmanip_avail_create(void);
void gmanip_avail_destroy(AvailFilterList *list);

typedef struct {
  GMF_Filter corba_obj;
} GraphFilter;

typedef struct {
  GMF_Pipe corba_obj;
} GraphPipe;

typedef struct _GraphInfo GraphInfo;

typedef struct {
  GraphInfo *up;
  GtkWidget *widget, *clist;

  GtkWidget *cnx_dialog, *cnx_f1, *cnx_f2;
} GraphFilterList;

typedef struct {
  GraphInfo *up;
  GtkWidget *widget, *clist;
} GraphPipeList;

struct _GraphInfo {
  GtkWidget *widget;

  GraphFilterList *filtlist;
  GraphPipeList *pipelist;
  GMF_FilterGraph corba_obj;
  GMFFilterGraph *gtk_obj;
};

typedef struct {
  CORBA_Environment *appev;
  GnomeApp *mainwin;
  GtkWidget *vbox;
  AvailFilterList *availlist;
  GraphInfo *curgraph;
} GraphManipApp;

GraphFilterList *gmanip_filterlist_create(GraphInfo *ginfo);
void gmanip_filterlist_destroy(GraphFilterList *flist);
void gmanip_filterlist_update(GraphFilterList *flist);
void gmanip_filterlist_connect(GtkWidget *widget, GraphManipApp *appinfo);

GraphPipeList *gmanip_pipelist_create(GraphInfo *ginfo);
void gmanip_pipelist_destroy(GraphPipeList *plist);
void gmanip_pipelist_update(GraphPipeList *plist);

GraphInfo *gmanip_graph_create(GraphManipApp *appinfo);
void gmanip_graph_destroy(GraphInfo *ginfo);

#endif
