/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "gmanip.h"

static void gmanip_filterlist_populate(GraphFilterList *flist);
static void gmanip_filterlist_drop(GtkWidget          *widget,
				   GdkDragContext     *context,
				   gint                x,
				   gint                y,
				   GtkSelectionData   *selection_data,
				   guint               info,
				   guint               time,
				   GraphFilterList *flist);

static const GtkTargetEntry drag_targets[] = {
  {"application/x-gmffilter-goadid", 0, 0}
};

GraphFilterList *
gmanip_filterlist_create(GraphInfo *graph)
{
  static const char *column_titles[] = {
    "Name",
    "Type",
    "GOAD ID"
  };
  GraphFilterList *retval;

  retval = g_new0(GraphFilterList, 1);
  retval->up = graph;

  retval->widget = gtk_scrolled_window_new(NULL, NULL);
  retval->clist = gtk_clist_new_with_titles(ELEMENTS(column_titles),
					    (char **)column_titles);
  gtk_container_add(GTK_CONTAINER(retval->widget), retval->clist);

  gtk_clist_freeze(GTK_CLIST(retval->clist));
  gtk_clist_set_selection_mode(GTK_CLIST(retval->clist),
			       GTK_SELECTION_BROWSE);

  gmanip_filterlist_populate(retval);

  gtk_widget_set_usize(GTK_WIDGET(retval->widget), 400, 200);
  gtk_clist_columns_autosize(GTK_CLIST(retval->clist));
  gtk_clist_thaw(GTK_CLIST(retval->clist));

  gtk_drag_dest_set(GTK_WIDGET(retval->clist),
		    GTK_DEST_DEFAULT_ALL,
		    drag_targets,
		    ELEMENTS(drag_targets),
		    GDK_ACTION_COPY);
  gtk_signal_connect(GTK_OBJECT(retval->clist), "drag_data_received",
		     GTK_SIGNAL_FUNC(gmanip_filterlist_drop), retval);

  return retval;
}

static void
gmanip_filterlist_populate(GraphFilterList *flist)
{
  GMF_Filter_RecordList *filters;
  int i;
  CORBA_Environment ev;

  CORBA_exception_init(&ev);

  filters = GMF_FilterGraph__get_graphFilters(flist->up->corba_obj, &ev);
  if(ev._major != CORBA_NO_EXCEPTION) {
    g_warning("Can't access filter graph!");
    return;
  }

  for(i = 0; i < filters->_length; i++) {
    char *col[3];

    col[0] = filters->_buffer[i].name;
    switch(filters->_buffer[i].filterType) {
    case GMF_Filter_TRANSFORM:
      col[1] = "Transform"; break;
    case GMF_Filter_SOURCE:
      col[1] = "Source"; break;
    case GMF_Filter_RENDERER:
      col[1] = "Renderer"; break;
    case GMF_Filter_OTHER:
    default:
      col[1] = "Other"; break;
    }

    {
      CORBA_any *goadid;

      goadid = GMF_Filter_get_attribute(filters->_buffer[i].obj, "goad ID", &ev);
      if(ev._major != CORBA_NO_EXCEPTION) {
	g_warning("Can't access filter %s!", filters->_buffer[i].name);
	continue;
      }

      if(CORBA_TypeCode_equal(goadid->_type, (CORBA_TypeCode)TC_string, &ev)) {
	col[2] = *(char **)goadid->_value;
      } else
	col[2] = "?";

      CORBA_free(goadid);
    }

    gtk_clist_append(GTK_CLIST(flist->clist), col);
  }

  CORBA_exception_free(&ev);
  CORBA_free(filters);
}

void
gmanip_filterlist_destroy(GraphFilterList *flist)
{
  g_free(flist);
}

void
gmanip_filterlist_update(GraphFilterList *flist)
{
  gtk_clist_freeze(GTK_CLIST(flist->clist));
  gtk_clist_clear(GTK_CLIST(flist->clist));
  gmanip_filterlist_populate(flist);
  gtk_clist_columns_autosize(GTK_CLIST(flist->clist));
  gtk_clist_thaw(GTK_CLIST(flist->clist));
}

static void
gmanip_filterlist_drop(GtkWidget          *widget,
		       GdkDragContext     *context,
		       gint                x,
		       gint                y,
		       GtkSelectionData   *selection_data,
		       guint               info,
		       guint               time,
		       GraphFilterList *flist)
{
  GtkWidget *dlg, *ent;
  int btn;
  g_return_if_fail(selection_data->length > 0);

  dlg = gnome_dialog_new("Add filter", "OK", "Cancel", NULL);
  gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dlg)->vbox),
		    gtk_label_new("Enter the name you wish to assign to this filter in the graph"));
  ent = gtk_entry_new();

  gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(dlg)->vbox), ent);
  gtk_window_set_focus(GTK_WINDOW(dlg), ent);
  gtk_window_set_default(GTK_WINDOW(dlg), GNOME_DIALOG(dlg)->buttons->data);
  gtk_widget_show_all(dlg);
  gnome_dialog_close_hides(GNOME_DIALOG(dlg), TRUE);
  btn = gnome_dialog_run_and_close(GNOME_DIALOG(dlg));

  if(btn == 0) {
    char *myname;
    GMF_Filter obj;
    CORBA_Environment ev;

    myname = gtk_entry_get_text(GTK_ENTRY(ent));
    
    g_message("Name will be %s for %s", myname, selection_data->data);

    obj = gmf_filter_activate (selection_data->data);

    if(CORBA_Object_is_nil(obj, &ev)) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Could not activate filter.",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
      goto out;
    }

    CORBA_exception_init(&ev);
    GMF_FilterGraph_AddFilter(flist->up->corba_obj, myname, obj, &ev);
    if(ev._major != CORBA_NO_EXCEPTION) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Could not add filter to graph.",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
    } else {
      gmanip_filterlist_update(flist);
    }

    CORBA_exception_free(&ev);
  }

 out:
  gtk_widget_destroy(dlg);
}

static void
gmanip_filterlist_connect_destroy(GtkWidget *widget,
				  GraphFilterList *flist)
{
	flist->cnx_dialog = NULL;
}

void
gmanip_filterlist_connect(GtkWidget *widget, GraphManipApp *appinfo)
{
  GraphFilterList *flist;

  g_return_if_fail(appinfo->curgraph);

  flist = appinfo->curgraph->filtlist;
  g_return_if_fail(GTK_CLIST(flist->clist)->selection);

  if(flist->cnx_dialog) {
    GMF_Filter f1, f2;
    GMF_Pipe p1, p2;
    CORBA_Environment ev;
    char *nom1, *nom2;

    CORBA_exception_init(&ev);

    /* Second row selected - do it */
    gtk_label_get(GTK_LABEL(flist->cnx_f1), &nom1);
    f1 = GMF_FilterGraph_FindFilterByName(flist->up->corba_obj, nom1, &ev);

    if(ev._major != CORBA_NO_EXCEPTION) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Filter 1 has disappeared!",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
      
      CORBA_exception_free(&ev);
      gtk_widget_destroy(flist->cnx_dialog);
      return;
    }

    gtk_clist_get_text(GTK_CLIST(flist->clist),
		       GPOINTER_TO_INT(GTK_CLIST(flist->clist)->selection->data),
		       0, &nom2);
    f2 = GMF_FilterGraph_FindFilterByName(flist->up->corba_obj, nom2, &ev);
    if(ev._major != CORBA_NO_EXCEPTION) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Filter 2 has disappeared!",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
      CORBA_Object_release(f1, &ev);
      CORBA_exception_free(&ev);
      gtk_widget_destroy(flist->cnx_dialog);
      return;
    }

    p1 = GMF_Filter_GetPipe(f1, GMF_OUT, &ev);
    if(ev._major != CORBA_NO_EXCEPTION || CORBA_Object_is_nil(p1, &ev)) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Filter 1 wouldn't give us an output pipe!",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
      
      CORBA_Object_release(f1, &ev);
      CORBA_Object_release(f2, &ev);
      CORBA_exception_free(&ev);
      gtk_widget_destroy(flist->cnx_dialog);
      return;
    }

    p2 = GMF_Filter_GetPipe(f2, GMF_IN, &ev);
    if(ev._major != CORBA_NO_EXCEPTION || CORBA_Object_is_nil(p2, &ev)) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Filter 2 wouldn't give us an input pipe!",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
      
      CORBA_Object_release(p1, &ev);
      CORBA_Object_release(f1, &ev);
      CORBA_Object_release(f2, &ev);
      CORBA_exception_free(&ev);
      gtk_widget_destroy(flist->cnx_dialog);
      return;
    }

    GMF_Pipe_ConnectTo(p1, p2, &ev);

    if(ev._major != CORBA_NO_EXCEPTION) {
      gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Connection failed",
							 GNOME_MESSAGE_BOX_ERROR, "Close",
							 NULL)));
      
      CORBA_Object_release(p1, &ev);
      CORBA_Object_release(p2, &ev);
      CORBA_Object_release(f1, &ev);
      CORBA_Object_release(f2, &ev);
      CORBA_exception_free(&ev);
      gtk_widget_destroy(flist->cnx_dialog);
      return;
    }

    CORBA_exception_free(&ev);
    gtk_widget_destroy(flist->cnx_dialog);
    gmanip_pipelist_update(flist->up->pipelist);
  } else {
    char *text;

    flist->cnx_dialog = gnome_dialog_new("Make connection", "Cancel", NULL);

    gtk_clist_get_text(GTK_CLIST(flist->clist),
		       GPOINTER_TO_INT(GTK_CLIST(flist->clist)->selection->data),
		       0, &text);

    flist->cnx_f1 = gtk_label_new(text);
    flist->cnx_f2 = gtk_label_new("Select the receiving filter on the filter list, and click the connection icon again.");
    gtk_label_set_line_wrap(GTK_LABEL(flist->cnx_f2), TRUE);

    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(flist->cnx_dialog)->vbox),
		      flist->cnx_f1);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(flist->cnx_dialog)->vbox),
		      gtk_hseparator_new());
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(flist->cnx_dialog)->vbox),
		      flist->cnx_f2);

    gtk_signal_connect(GTK_OBJECT(flist->cnx_dialog), "destroy",
		       GTK_SIGNAL_FUNC(gmanip_filterlist_connect_destroy),
		       flist);

    gtk_widget_show_all(flist->cnx_dialog);
  }
}
