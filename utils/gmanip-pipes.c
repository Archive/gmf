/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "gmanip.h"

/* This should really be called "connection list" instead of "pipe list",
 * since it lists connections (which are pairs of pipes). Oh well.
 */

static void gmanip_pipelist_populate(GraphPipeList *plist);

GraphPipeList *
gmanip_pipelist_create(GraphInfo *ginfo)
{
  static const char *column_titles[] = {
    "ID",
    "From",
    "To"
  };
  GraphPipeList *retval;

  retval = g_new0(GraphPipeList, 1);
  retval->up = ginfo;

  retval->widget = gtk_scrolled_window_new(NULL, NULL);
  retval->clist = gtk_clist_new_with_titles(ELEMENTS(column_titles),
					    (char **)column_titles);
  gtk_container_add(GTK_CONTAINER(retval->widget), retval->clist);

  gtk_clist_freeze(GTK_CLIST(retval->clist));
  gmanip_pipelist_populate(retval);

  gtk_widget_set_usize(GTK_WIDGET(retval->widget), 400, 200);
  gtk_clist_columns_autosize(GTK_CLIST(retval->clist));
  gtk_clist_thaw(GTK_CLIST(retval->clist));

  return retval;
}

/* Algorithm is:
   For each filter, get a list of all the output pipes.
   For each of them, get from & to filters.
   
   This will be mostly decorative for now.
*/
static void
gmanip_pipelist_populate(GraphPipeList *plist)
{
  GMF_Filter_RecordList *filters;
  int i, j;
  CORBA_Environment ev;

  CORBA_exception_init(&ev);
  filters = GMF_FilterGraph__get_graphFilters(plist->up->corba_obj, &ev);
  if(ev._major != CORBA_NO_EXCEPTION) {
    g_warning("Can't access filter graph!");
    return;
  }

  for(i = 0; i < filters->_length; i++) {
    GMF_PipeList *pipelist;

    pipelist = GMF_Filter__get_outputPipes(filters->_buffer[i].obj, &ev);

    if(ev._major != CORBA_NO_EXCEPTION) {
      g_warning("Can't access filter %s!", filters->_buffer[i].name);
      continue;
    }

    for(j = 0; j < pipelist->_length; j++) {
      char *col[3];
      GMF_Filter outto;
      GMF_Pipe_ConnectionInfo *cinfo;
      GMF_Pipe_Info *pinfo;

      col[0] = GMF_Pipe__get_pipeName(pipelist->_buffer[j], &ev);
      if(ev._major != CORBA_NO_EXCEPTION) {
	g_warning("Can't access a pipe for filter %s!",
		  filters->_buffer[i].name);
	continue;
      }

      col[1] = filters->_buffer[i].name;

      cinfo = GMF_Pipe__get_pConnectionInfo(pipelist->_buffer[j], &ev);
      pinfo = GMF_Pipe__get_pipeInfo(cinfo->connectedTo, &ev);
      outto = pinfo->pFilter;
      col[2] = GMF_Filter__get_filterName(outto, &ev);

      gtk_clist_append(GTK_CLIST(plist->clist), col);

      CORBA_free(cinfo);
      CORBA_free(pinfo);
      CORBA_free(col[0]);
      CORBA_free(col[2]);
    }

    CORBA_free(pipelist);
  }

  CORBA_free(filters);
}

void
gmanip_pipelist_destroy(GraphPipeList *plist)
{
  g_free(plist);
}

void
gmanip_pipelist_update(GraphPipeList *plist)
{
  gtk_clist_freeze(GTK_CLIST(plist->clist));
  gtk_clist_clear(GTK_CLIST(plist->clist));
  gmanip_pipelist_populate(plist);
  gtk_clist_columns_autosize(GTK_CLIST(plist->clist));
  gtk_clist_thaw(GTK_CLIST(plist->clist));
}
