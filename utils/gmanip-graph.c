/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "gmanip.h"

GraphInfo *
gmanip_graph_create(GraphManipApp *appinfo)
{
  GraphInfo *retval;
  CORBA_Environment ev;

  retval = g_new0(GraphInfo, 1);

  retval->widget = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
  retval->gtk_obj = GMF_FILTER_GRAPH(gmf_filter_graph_new());
  CORBA_exception_init(&ev);
  retval->corba_obj = CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(retval->gtk_obj)), &ev);
  CORBA_exception_free(&ev);

  retval->filtlist = gmanip_filterlist_create(retval);
  gtk_container_add(GTK_CONTAINER(retval->widget),
		    GTK_WIDGET(retval->filtlist->widget));

  retval->pipelist = gmanip_pipelist_create(retval);
  gtk_container_add(GTK_CONTAINER(retval->widget),
		    GTK_WIDGET(retval->pipelist->widget));

  return retval;
}

void
gmanip_graph_destroy(GraphInfo *ginfo)
{
  CORBA_Environment ev;
  gmanip_filterlist_destroy(ginfo->filtlist);
  gmanip_pipelist_destroy(ginfo->pipelist);
  if(ginfo->gtk_obj)
    gtk_object_unref(GTK_OBJECT(ginfo->gtk_obj));
  CORBA_exception_init(&ev);
  CORBA_Object_release(ginfo->corba_obj, &ev);
  CORBA_exception_free(&ev);
  
  g_free(ginfo);
}
