/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "gmanip.h"
#include <signal.h>
#include <sys/signal.h>

/*        Objects
   Main window:
     Filter list for graph
     Pipe list
     Activatable filters

   Menus:
     Save/load/exit

   Do all manipulation through CORBA to make accessing non-hosted graphs easy.
 */


static GraphManipApp *gmanip_app_create(CORBA_ORB orb, CORBA_Environment *ev);
static void gmanip_app_run(GraphManipApp *app);

static const struct poptOption options[] = {
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

int main(int argc, char *argv[])
{
  	CORBA_Environment myev;
  	GraphManipApp *myapp;
  	CORBA_ORB orb;
  	poptContext popt_context;

  	CORBA_exception_init(&myev);

	gnome_init_with_popt_table ("graph-manip", VERSION,
				    argc, argv, options, 0,
				    &popt_context);

	orb = oaf_init (argc, argv);
  	gmf_init (orb);

  	myapp = gmanip_app_create (orb, &myev);

  	gmanip_app_run (myapp);
  
  	return 0;
}

static void gmanip_app_new(GnomeApp *app, GraphManipApp *appinfo);
static void gmanip_app_open(GnomeApp *app, GraphManipApp *appinfo);
static void gmanip_app_save(GnomeApp *app, GraphManipApp *appinfo);
static void gmanip_app_exit(GnomeApp *app, GraphManipApp *appinfo);
static void gmanip_app_about(GnomeApp *app, GraphManipApp *appinfo);
static gint gmanip_app_delete_event(GnomeApp *app, GdkEventAny *event,
				    GraphManipApp *appinfo);
static void gmanip_filterlist_update_cb(GnomeApp *app, GraphManipApp *appinfo);

static GraphManipApp *
gmanip_app_create(CORBA_ORB orb,
	       CORBA_Environment *ev)
{
  static GnomeUIInfo filemenu[] = {
    GNOMEUIINFO_MENU_NEW_ITEM("_New", "New filter graph", gmanip_app_new, NULL),
    GNOMEUIINFO_MENU_OPEN_ITEM(gmanip_app_open, NULL),
    GNOMEUIINFO_MENU_SAVE_ITEM(gmanip_app_save, NULL),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_MENU_EXIT_ITEM(gmanip_app_exit, NULL),
    GNOMEUIINFO_END
  };
  static GnomeUIInfo filtermenu[] = {
    GNOMEUIINFO_ITEM("_Connect...", "Connect two filters",
		     gmanip_filterlist_connect, NULL),
    GNOMEUIINFO_ITEM_STOCK("_Refresh", "Refresh the list of active filters",
			   gmanip_filterlist_update_cb,
			   GNOME_STOCK_MENU_REFRESH),
    GNOMEUIINFO_END
  };
  static GnomeUIInfo helpmenu[] = {
    GNOMEUIINFO_MENU_ABOUT_ITEM(gmanip_app_about, NULL),
    GNOMEUIINFO_END
  };
  static GnomeUIInfo appmenus[] = {
    GNOMEUIINFO_MENU_FILE_TREE(filemenu),
    GNOMEUIINFO_SUBTREE("Fi_lter", filtermenu),
    GNOMEUIINFO_MENU_HELP_TREE(helpmenu),
    GNOMEUIINFO_END
  };
  GraphManipApp *retval;
  GtkWidget *vbox;

  retval = g_new0(GraphManipApp, 1);

  retval->mainwin = GNOME_APP(gnome_app_new("graph-manip",
					    "GMF Filter Graph Manipulator"));
  gtk_signal_connect(GTK_OBJECT(retval->mainwin), "delete_event",
		     GTK_SIGNAL_FUNC(gmanip_app_delete_event), retval);

  retval->vbox = vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

  retval->availlist = gmanip_avail_create();
  gtk_container_add(GTK_CONTAINER(vbox), GTK_WIDGET(retval->availlist->widget));

  gnome_app_create_menus_with_data(retval->mainwin, appmenus, retval);
  gnome_app_set_contents(retval->mainwin, vbox);

  return retval;
}

static void
gmanip_app_run(GraphManipApp *app)
{
  gtk_widget_show_all(GTK_WIDGET(app->mainwin));
  gtk_main();
}

static void
gmanip_app_new(GnomeApp *app, GraphManipApp *appinfo)
{
  if(appinfo->curgraph) {
    gtk_container_remove(GTK_CONTAINER(appinfo->vbox),
			 GTK_WIDGET(appinfo->curgraph->widget));
    gmanip_graph_destroy(appinfo->curgraph);
  }

  appinfo->curgraph = gmanip_graph_create(appinfo);

  gtk_container_add(GTK_CONTAINER(appinfo->vbox), appinfo->curgraph->widget);
  gtk_widget_show_all(appinfo->curgraph->widget);
}

static void
gmanip_app_open(GnomeApp *app, GraphManipApp *appinfo)
{
  gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Not yet implemented",
						      GNOME_MESSAGE_BOX_ERROR,
						      "Close", NULL)));
}

static void
gmanip_app_save(GnomeApp *app, GraphManipApp *appinfo)
{
  gnome_dialog_run(GNOME_DIALOG(gnome_message_box_new("Not yet implemented",
						      GNOME_MESSAGE_BOX_ERROR,
						      "Close", NULL)));
}

static gint
gmanip_app_delete_event(GnomeApp *app, GdkEventAny *event,
			GraphManipApp *appinfo)
{
  gtk_widget_hide(GTK_WIDGET(app));

  gmanip_app_exit(app, appinfo);

  return FALSE;
}

static void
gmanip_app_exit(GnomeApp *app, GraphManipApp *appinfo)
{
  gtk_main_quit();
}

static void
gmanip_app_about(GnomeApp *app, GraphManipApp *appinfo)
{
  GtkWidget *about;
  const char *authors[] = {"Elliot Lee", NULL};

  about = gnome_about_new("Graph Manip", VERSION,
			  "Copyright (C) 1999 Elliot Lee", authors,
			  "A utility for creating and editing GMF\n"
			  "media filter graphs.", NULL);
  gtk_widget_show(about);
}

static void
gmanip_filterlist_update_cb(GnomeApp *app,
			    GraphManipApp *appinfo)
{
  g_return_if_fail(appinfo->curgraph);

  gmanip_filterlist_update(appinfo->curgraph->filtlist);
}
