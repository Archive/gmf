/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

/* Ugly hacks in here:
 *   Everything related to the MediaInfo display.
 */

#define G_LOG_DOMAIN "media-player"

#include <gnome.h>
#include <gmf.h>
#include <libgnomevfs/gnome-vfs.h>
#include <math.h>

#include "../libgmf/gmf-object-directory.h"

typedef struct {
	GtkObject *fg, *timeref;

	GnomeVFSURI *uri;
	gint ctr;

	GMF_MediaPosition media_pos;
	GMF_MediaInfo media_info;

	GMF_TimeVal pauseStart, duration;
	gint timer_id;
	GMF_Callback_CallbackID cbid;
	GtkObject *vidarea;
	GtkWidget *socket;

	GMF_MediaInfo_Titles *titles;
	GtkWidget *vbox, *curseg_vbox, *curtitle_vbox, *opt_seg;
	int curtitle, curseg;

	int skipdir;
} MPCurfile;

enum MPStatus { REWIND, STOP, PAUSE, PLAY, FF, LAST_STATUS };
const char * const status_names[] = {"Rewind", "Stop", "Pause", "Play", "FF", NULL };

typedef struct {
	GtkWidget *mainwin;

	GtkWidget *slider, *label;

	GtkWidget *rewind, *stop, *play, *ff, *pause;
	GtkWidget *vbox;
	gboolean inside_toggle_handler;

	GtkAdjustment *position;

	enum MPStatus status;

	MPCurfile *curfile;
	CORBA_Environment ev;
	gint poschange;
} MPMainwin;

static MPMainwin *mp_mainwin_new(void);
static void mp_menu_file_open(GtkWidget *widget, MPMainwin *mw);
static void mp_menu_help_about(void);
static void mp_rewind_button_pressed(GtkWidget *btn, MPMainwin *mw);
static void mp_rewind_button_released(GtkWidget *btn, MPMainwin *mw);
static void mp_ff_button_pressed(GtkWidget *btn, MPMainwin *mw);
static void mp_ff_button_released(GtkWidget *btn, MPMainwin *mw);
static void mp_stop_button_toggled(GtkToggleButton *btn, MPMainwin *mw);
static void mp_play_button_toggled(GtkToggleButton *btn, MPMainwin *mw);
static void mp_pause_button_toggled(GtkToggleButton *btn, MPMainwin *mw);
static void mp_set_state(MPMainwin *mw, gint status);
static void mp_file_close(MPMainwin *mw, MPCurfile *curfile);
static void mp_mainwin_destroy (MPMainwin *mw);
static void mp_file_open (MPMainwin *mw, GnomeVFSURI *uri);
static void mp_graph_stop(GMFFilterGraph *fg, MPMainwin *mw);
static void mp_graph_pause(GMFFilterGraph *fg, MPMainwin *mw);
static void mp_graph_run(GMFFilterGraph *fg, MPMainwin *mw);
static void mp_graph_deliver_event(GMFFilterGraph *fg, GMF_TimeVal *timestamp,
				   const char *evName, CORBA_any *evData, MPMainwin *mw);
static gboolean mp_play_update(MPMainwin *mw);
static void mp_play_update_start(MPMainwin *mw);
static void mp_play_update_stop(MPMainwin *mw);
static void mp_update_label(GtkWidget *label, GMF_TimeVal position, GMF_TimeVal duration, MPMainwin *mw);
static void mp_change_position(GtkAdjustment *position, MPMainwin *mw);
static void mp_file_open_display_media_info(MPMainwin *mw, MPCurfile *curfile);
static char *keyval_get_value(CORBA_sequence_GMF_MediaInfo_KeyVal *kv, const char *key);

static const struct poptOption options[] = {
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

int main(int argc, char *argv[])
{
	CORBA_ORB orb;
	MPMainwin *mw;
	poptContext popt_context;
	char *uri_string;
	GnomeVFSURI *uri;
	gnome_init_with_popt_table ("gmf-media-player", VERSION,
				    argc, argv, options, 0,
				    &popt_context);

	orb = oaf_init (argc, argv);
	gmf_init(orb);

	mw = mp_mainwin_new ();
	
	gtk_widget_show_all(mw->mainwin);

	uri_string = poptGetArg(popt_context);
	if (uri_string != NULL) {
		uri = gnome_vfs_uri_new (uri_string);
		if (uri != NULL) {
			mp_file_open (mw, uri);
			gnome_vfs_uri_unref (uri);
		}
	}
	poptFreeContext(popt_context);

	bonobo_main ();

	mp_mainwin_destroy (mw);

	return 0;
}

static GtkWidget *
make_stock_button(const char *name, gboolean is_toggle)
{
  GtkWidget *retval;
  char tmp[256];
  char *ctmp;

  g_snprintf(tmp, sizeof(tmp), "media-player/%s.xpm", name);
  ctmp = gnome_pixmap_file(tmp);

  if(is_toggle)
    retval = gtk_toggle_button_new();
  else
    retval = gtk_button_new();
  gtk_container_add(GTK_CONTAINER(retval),
		    gnome_pixmap_new_from_file(ctmp));
  g_free(ctmp);

  return retval;
}

static MPMainwin *
mp_mainwin_new(void)
{
	static GnomeUIInfo filemenu[] = {
		GNOMEUIINFO_MENU_OPEN_ITEM(mp_menu_file_open, NULL),
		GNOMEUIINFO_MENU_EXIT_ITEM(gtk_main_quit, NULL),
		GNOMEUIINFO_END
		};
	static GnomeUIInfo helpmenu[] = {
		GNOMEUIINFO_HELP("gmf-media-player"),
		GNOMEUIINFO_MENU_ABOUT_ITEM(mp_menu_help_about, NULL),
		GNOMEUIINFO_END
	};
	static GnomeUIInfo appmenus[] = {
		GNOMEUIINFO_SUBTREE("_File", filemenu),
		GNOMEUIINFO_SUBTREE("_Help", helpmenu),
		GNOMEUIINFO_END
	};

	GtkWidget *vbox, *hbox;
	MPMainwin *mw;

	mw = g_new0(MPMainwin, 1);

	/* Set up window CORBA environemnt */
	CORBA_exception_init (&mw->ev);

	mw->mainwin = gnome_app_new("gmf-media-player", "Media Player");
	mw->status = STOP;

	gnome_app_create_menus_with_data(GNOME_APP(mw->mainwin), appmenus, mw);
	gtk_signal_connect(GTK_OBJECT(mw->mainwin), "delete_event",
		     GTK_SIGNAL_FUNC(gtk_main_quit), NULL);

	mw->vbox = vbox = gtk_vbox_new(FALSE, GNOME_PAD);
	gnome_app_set_contents(GNOME_APP(mw->mainwin), vbox);

	mw->position = GTK_ADJUSTMENT(gtk_adjustment_new(0, 0, 0, 0, 0, 0));
	gtk_signal_connect(GTK_OBJECT(mw->position), "value_changed", mp_change_position, mw);

	hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(vbox), hbox);

	mw->slider = gtk_hscale_new(mw->position);
	gtk_scale_set_digits(GTK_SCALE(mw->slider), 2);
	gtk_widget_set_usize(mw->slider, 300, -1);
	gtk_container_add(GTK_CONTAINER(hbox), mw->slider);

	mw->label = gtk_label_new("0:00 / 0:00");
	gtk_container_add(GTK_CONTAINER(hbox), mw->label);

	hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(vbox), hbox);

	mw->rewind = make_stock_button("rw", FALSE);
	gtk_signal_connect(GTK_OBJECT(mw->rewind), "pressed",
		     GTK_SIGNAL_FUNC(mp_rewind_button_pressed), mw);
	gtk_signal_connect(GTK_OBJECT(mw->rewind), "released",
		     GTK_SIGNAL_FUNC(mp_rewind_button_released), mw);
	gtk_container_add(GTK_CONTAINER(hbox), mw->rewind);

	mw->stop = make_stock_button("stop", TRUE);
	gtk_signal_connect(GTK_OBJECT(mw->stop), "toggled",
		     GTK_SIGNAL_FUNC(mp_stop_button_toggled), mw);
	gtk_container_add(GTK_CONTAINER(hbox), mw->stop);

  	mw->pause = make_stock_button("pause", TRUE);
	gtk_signal_connect(GTK_OBJECT(mw->pause), "toggled",
		     GTK_SIGNAL_FUNC(mp_pause_button_toggled), mw);
	gtk_container_add(GTK_CONTAINER(hbox), mw->pause);

	mw->play = make_stock_button("play", TRUE);
	gtk_signal_connect(GTK_OBJECT(mw->play), "toggled",
		     GTK_SIGNAL_FUNC(mp_play_button_toggled), mw);
	gtk_container_add(GTK_CONTAINER(hbox), mw->play);

	mw->ff = make_stock_button("ff", FALSE);
  	gtk_signal_connect(GTK_OBJECT(mw->ff), "pressed",
		     GTK_SIGNAL_FUNC(mp_ff_button_pressed), mw);
	gtk_signal_connect(GTK_OBJECT(mw->ff), "released",
		     GTK_SIGNAL_FUNC(mp_ff_button_released), mw);
	gtk_container_add(GTK_CONTAINER(hbox), mw->ff);

	return mw;
}

static void
mp_mainwin_destroy (MPMainwin *mw)
{
	if (mw->curfile) {
		mp_file_close (mw, mw->curfile);
    	}

	CORBA_exception_free (&mw->ev);
	
	gtk_widget_destroy(mw->mainwin);
}

static void
file_open_ok (GtkWidget *w, GtkWidget *filesel)
{
	MPMainwin *mw;
	char *filename;
	GnomeVFSURI *uri;
	GtkWidget **dlgptr;

	mw = gtk_object_get_data (GTK_OBJECT (filesel), "MPMainwin");
	filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (filesel));
	uri = gnome_vfs_uri_new (filename);
	
	//g_message ("URI: %s", gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE));
	//g_message ("Path: %s", gnome_vfs_uri_get_path (uri));
	//g_message ("Scheme: %s", gnome_vfs_uri_get_scheme (uri));
	
	mp_file_open (mw, uri);
	gnome_vfs_uri_unref (uri);
	
	dlgptr = gtk_object_get_data (GTK_OBJECT (filesel), "dlgptr");
	gtk_widget_destroy (filesel);
	*dlgptr = NULL;
}

static void
file_open_cancel(GtkWidget *w, GtkWidget *filesel)
{
  GtkWidget **dlgptr;

  dlgptr = gtk_object_get_data(GTK_OBJECT(filesel), "dlgptr");
  gtk_widget_destroy(filesel);
  *dlgptr = NULL;
}

static void
mp_menu_file_open(GtkWidget *w, MPMainwin *mw)
{
  static GtkWidget *dlg = NULL;

  if(!dlg) {
    dlg = gtk_file_selection_new("Load file");
    
    gtk_object_set_data(GTK_OBJECT(dlg), "MPMainwin", mw);
    gtk_object_set_data(GTK_OBJECT(dlg), "dlgptr", &dlg);
    
    gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(dlg)->ok_button), "clicked",
		       GTK_SIGNAL_FUNC(file_open_ok), dlg);
    gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(dlg)->cancel_button), "clicked",
		       GTK_SIGNAL_FUNC(file_open_cancel), dlg);
    
    gtk_widget_show(dlg);
  } else
    gdk_window_raise(dlg->window);
}

static void
mp_menu_help_about(void)
{
  const char * const authors[] = {"Elliot Lee <sopwith@redhat.com>", NULL};
  GtkWidget *wtmp;

  wtmp = gnome_about_new("GMF Media Player", VERSION,
			 "Copyright (C) 1999 Elliot Lee",
			 (const gchar **)authors,
			 "A media player for the GNOME Media Framework.",
			 NULL);

  gtk_widget_show(wtmp);
}

static void
mp_start_speeding(MPMainwin *mw, int dir)
{
  gtk_adjustment_set_value(mw->position, mw->position->value + dir);
}

static void
mp_stop_speeding(MPMainwin *mw, int dir)
{
}

static void
mp_rewind_button_pressed(GtkWidget *btn, MPMainwin *mw)
{
	if(!mw->curfile) 
		return;

  	mp_start_speeding(mw, -1);
}

static void
mp_rewind_button_released(GtkWidget *btn, MPMainwin *mw)
{
  if(!mw->curfile) return;

  mp_stop_speeding(mw, -1);
}

static void
mp_ff_button_pressed(GtkWidget *btn, MPMainwin *mw)
{
  if(!mw->curfile) return;

  mp_start_speeding(mw, 1);
}

static void
mp_ff_button_released(GtkWidget *btn, MPMainwin *mw)
{
  if(!mw->curfile) return;

  mp_stop_speeding(mw, 1);
}

static void
mp_stop_button_toggled(GtkToggleButton *btn, MPMainwin *mw)
{
  mp_set_state(mw, STOP);
}

static void
mp_play_button_toggled(GtkToggleButton *btn, MPMainwin *mw)
{
  if(btn->active)
    mp_set_state(mw, PLAY);
  else
    mp_set_state(mw, STOP);
}

static void
mp_pause_button_toggled(GtkToggleButton *btn, MPMainwin *mw)
{
  if(btn->active)
    mp_set_state(mw, PAUSE);
  else
    mp_set_state(mw, PLAY);
}

static void
mp_set_state(MPMainwin *mw, gint status)
{
  g_return_if_fail(mw->curfile);

  if(mw->inside_toggle_handler) return;
  mw->inside_toggle_handler = TRUE;

  switch(status) {
  case STOP:
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->rewind), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->ff), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->play), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->stop), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->pause), FALSE);
    break;
  case PAUSE:
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->stop), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->play), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->ff), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->pause), TRUE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->rewind), FALSE);
    break;
  case REWIND:
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->stop), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->play), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->ff), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->pause), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->rewind), TRUE);
    break;
  case PLAY:
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->rewind), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->stop), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->ff), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->pause), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->play), TRUE);
    break;
  case FF:
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->rewind), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->stop), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->play), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->pause), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mw->ff), TRUE);
    break;
  default:
    g_error("Unknown state %d.", status);
    break;
  }

  if(mw->status == status)
    goto out;

  switch(status) {
  case STOP:
    GMF_StateChange_Stop(bonobo_object_corba_objref(BONOBO_OBJECT(mw->curfile->fg)), &mw->ev);
    mp_play_update_stop(mw);
    break;
  case REWIND:
    g_warning("Rewind NYI");
    break;
  case PLAY:
    {
      GMF_TimeVal timeBase, halfsec = {0,500000};

      timeBase = gmf_time_reference_current_time(GMF_TIME_REFERENCE(mw->curfile->timeref));
      g_message("now is [%d, %d], pauseStart is [%d, %d]", timeBase.tvSec, timeBase.tvUsec,
		mw->curfile->pauseStart.tvSec,
		mw->curfile->pauseStart.tvUsec);

      if(mw->curfile->pauseStart.tvSec
	 && mw->curfile->pauseStart.tvUsec) {
	timeBase = gmf_time_subtract(timeBase, mw->curfile->pauseStart);
	mw->curfile->pauseStart.tvSec =
	  mw->curfile->pauseStart.tvUsec = 0;
      } else
	timeBase = gmf_time_add(timeBase, halfsec);

      g_message("timeBase is [%d, %d]", timeBase.tvSec, timeBase.tvUsec);
      GMF_StateChange_SetTimeBase(bonobo_object_corba_objref(BONOBO_OBJECT(mw->curfile->fg)), &timeBase, &mw->ev);
      GMF_StateChange_Run(bonobo_object_corba_objref(BONOBO_OBJECT(mw->curfile->fg)), &mw->ev);

      mp_play_update_start(mw);
    }
    break;
  case PAUSE:
    g_return_if_fail(mw->curfile);
    GMF_StateChange_Pause(bonobo_object_corba_objref(BONOBO_OBJECT(mw->curfile->fg)), &mw->ev);
    mp_play_update_stop(mw);
    break;
  case FF:
    g_warning("FF NYI");
    break;
  }

  mw->status = status;

 out:
  mw->inside_toggle_handler = FALSE;
}

static void
mp_file_close(MPMainwin *mw, MPCurfile *curfile)
{
	if(curfile->timer_id >= 0)
		gtk_timeout_remove(curfile->timer_id);

	gnome_vfs_uri_unref (curfile->uri);

	bonobo_object_unref (BONOBO_OBJECT (curfile->fg));

	gtk_container_remove(GTK_CONTAINER(mw->vbox), curfile->vbox);
	bonobo_object_unref (BONOBO_OBJECT (curfile->vidarea));

	CORBA_Object_release (curfile->media_pos, &mw->ev);
	CORBA_Object_release (curfile->media_info, &mw->ev);

	CORBA_free (curfile->titles);

	g_free (curfile);
}

static gint
mp_render_filter(GMF_Filter filt, GMF_Pipe infrom, MPMainwin *mw)
{
	GMF_PipeList *plist = NULL;
	int i;
	gint retval = -1;

	if (CORBA_Object_is_nil(mw->curfile->media_pos, &mw->ev)) {
		mw->curfile->media_pos = Bonobo_Unknown_queryInterface (filt, "IDL:GMF/MediaPosition:1.0", &mw->ev);
	}
	
	if (CORBA_Object_is_nil (mw->curfile->media_info, &mw->ev)) {
		mw->curfile->media_info = Bonobo_Unknown_queryInterface (filt, "IDL:GMF/MediaInfo:1.0", &mw->ev);
	}
	
	if (!CORBA_Object_is_nil (infrom, &mw->ev)) {
		/* Connect 'infrom' into 'filt' */
		plist = GMF_Filter__get_inputPipes (filt, &mw->ev);
		if(mw->ev._major != CORBA_NO_EXCEPTION) 
			return -1;
		
		for(i = 0; i < plist->_length; i++) {
			CORBA_boolean use_me;

			use_me = !GMF_OutputPipe__get_isConnected (plist->_buffer[i], &mw->ev);
			if(mw->ev._major != CORBA_NO_EXCEPTION) 
				goto out;

			if(use_me) {
				GMF_Pipe_ConnectTo (infrom, plist->_buffer[i], &mw->ev);
				if(mw->ev._major == CORBA_NO_EXCEPTION)
					break;
			}
		}

		if(i >= plist->_length) { 
			/* Nothing in the list of pipes - ask for a new one */
			GMF_Pipe thepipe;

			thepipe = GMF_Filter_GetPipe (filt, GMF_IN, &mw->ev);
			if(mw->ev._major != CORBA_NO_EXCEPTION) {
				goto out;
			}

			GMF_Pipe_ConnectTo (infrom, thepipe, &mw->ev);
			if(mw->ev._major != CORBA_NO_EXCEPTION) {
				goto out;
      			}
    		}

		CORBA_free(plist); plist = NULL;
  	}

	plist = GMF_Filter__get_outputPipes (filt, &mw->ev);
  	if(mw->ev._major != CORBA_NO_EXCEPTION)
  		return -1;

	for(i = 0; i < plist->_length; i++) {
    		CORBA_boolean use_me;
    		GMF_Filter nextfilter = CORBA_OBJECT_NIL;
    		GMF_MediaTypeInfoList *mtil;
    		int j;

    		use_me = GMF_OutputPipe__get_autoRender (plist->_buffer[i], &mw->ev);
    		if (mw->ev._major != CORBA_NO_EXCEPTION) {
    			goto out;
    		}
    	
    		if (!use_me) {
      			continue;
      		}

    		use_me = !GMF_OutputPipe__get_isConnected (plist->_buffer[i], &mw->ev);
    		if (mw->ev._major != CORBA_NO_EXCEPTION) {
    			goto out;
    		}

    		if (!use_me) {
      			continue;
		}
		
    		/* We need to render this output pipe. */
    		mtil = GMF_OutputPipe__get_processableTypes (plist->_buffer[i], &mw->ev);
    		if (mw->ev._major != CORBA_NO_EXCEPTION) {
    			goto out;
    		}

    		for (j = 0; j < mtil->_length && CORBA_Object_is_nil (nextfilter, &mw->ev); j++) {
      			nextfilter = gmf_filter_activate_for_types (mtil->_buffer[j].minorType, "*");
    		}

    		if (!CORBA_Object_is_nil (nextfilter, &mw->ev)) {
      			char buf[32];

      			g_snprintf (buf, sizeof(buf), "Filter%d", mw->curfile->ctr++);

      			GMF_FilterGraph_AddFilter (bonobo_object_corba_objref (BONOBO_OBJECT (mw->curfile->fg)), buf, nextfilter, &mw->ev);
      			if (mw->ev._major == CORBA_NO_EXCEPTION)
				mp_render_filter (nextfilter, plist->_buffer[i], mw);
    			}

    		CORBA_free (mtil);
  	}

	retval = 0;

 	out:
  	CORBA_free(plist);

  	return retval;
}


static void
mp_file_open (MPMainwin *mw, GnomeVFSURI *uri)
{
	MPCurfile *curfile, *oldfile;
  	GMF_Filter curfilter;

  	oldfile = mw->curfile;
  	if (oldfile != NULL) {
    		mp_set_state (mw, STOP);
    	}

  	mw->curfile = curfile = g_new0 (MPCurfile, 1);

  	curfile->media_pos = CORBA_OBJECT_NIL;
  	curfile->media_info = CORBA_OBJECT_NIL;
  	curfile->timer_id = -1;
  	curfile->uri 	  = gnome_vfs_uri_ref (uri);

  	curfile->fg 	  = gmf_filter_graph_new ();
  	gtk_object_ref (curfile->fg);
  	gtk_object_sink (curfile->fg);
  	
  	curfile->timeref = GTK_OBJECT (GMF_FILTER_GRAPH (curfile->fg)->timeref);

  	curfile->vbox = gtk_vbox_new (FALSE, GNOME_PAD);
  	gtk_container_add (GTK_CONTAINER (mw->vbox), curfile->vbox);

  	curfile->vidarea = gmf_video_area_new ();
  	GMF_FilterGraph_SetApplicationObject (bonobo_object_corba_objref (BONOBO_OBJECT (curfile->fg)),
				       bonobo_object_corba_objref (BONOBO_OBJECT (curfile->vidarea)),
				       &mw->ev);
  	curfile->socket = gmf_video_area_get_socket (GMF_VIDEO_AREA (curfile->vidarea));
  	gtk_container_add (GTK_CONTAINER (curfile->vbox), curfile->socket);

  	gtk_signal_connect(curfile->fg, "stop", mp_graph_stop, mw);
  	gtk_signal_connect(curfile->fg, "pause", mp_graph_pause, mw);
  	gtk_signal_connect(curfile->fg, "run", mp_graph_run, mw);
  	gtk_signal_connect(curfile->fg, "deliver_event", mp_graph_deliver_event, mw);

  	curfilter = gmf_filter_activate_for_uri (uri);
  	if (CORBA_Object_is_nil (curfilter, &mw->ev)) {
  		mp_file_close (mw, curfile);
  		mw->curfile = oldfile;

  		gtk_widget_show (gnome_message_box_new ("An error occurred setting up the "
					      		"playback components for this file.",
					      		GNOME_MESSAGE_BOX_ERROR,
					    		"Close", NULL));
		return;
  	}

  	GMF_FilterGraph_AddFilter (bonobo_object_corba_objref (BONOBO_OBJECT (curfile->fg)), 
  				   "Source", curfilter, &mw->ev);

  	mp_render_filter (curfilter, CORBA_OBJECT_NIL, mw);

  	{
    		char buf[32];

    		g_snprintf(buf, sizeof(buf), "Media Player: %s", gnome_vfs_uri_get_basename (uri));
    		gtk_window_set_title (GTK_WINDOW (mw->mainwin), buf);
  	}

  	/* We do this here instead of at the beginning because (a) it makes error recovery nicer (b) it speeds things up
     	   a little bit WRT filter factory refcounts not hitting zero when we make use of them in the new graph */
  	if (oldfile != NULL) {
    		mp_file_close (mw, oldfile);
    	}

  	if (!CORBA_Object_is_nil (curfile->media_info, &mw->ev)) {
    		mp_file_open_display_media_info (mw, curfile);
    	}

  	if(!CORBA_Object_is_nil (curfile->media_pos, &mw->ev)) {
    		GMF_TimeVal duration, zero = {0, 0};
    		GMF_MediaPosition_Value tmpval;

    		tmpval = GMF_MediaPosition_get_duration (mw->curfile->media_pos, GMF_MediaPosition_TIME, &mw->ev);
    		g_assert (tmpval._d == GMF_MediaPosition_TIME);

    		duration = mw->curfile->duration = tmpval._u.value_time;
    		if(mw->ev._major != CORBA_NO_EXCEPTION) {
      			g_warning("Got exception %s for get_duration", CORBA_exception_id(&mw->ev));
      		}
    		else {
      			mw->position->upper = ((gfloat)duration.tvSec) + (((gfloat)duration.tvUsec)/1000000);
      		}

    		gtk_adjustment_changed (mw->position);

    		mp_update_label (mw->label, zero, duration, mw);
    		tmpval._d = GMF_MediaPosition_TIME;
    		tmpval._u.value_time = zero;
    		GMF_MediaPosition_set_position (mw->curfile->media_pos, &tmpval, &mw->ev);
  	}

  	gtk_widget_show_all (curfile->vbox);
}

static void
mp_update_segment (int n, MPMainwin *mw)
{
	char buf[256], *ctmp;
	GMF_MediaInfo_Segment *curseg;
	int i;
	GtkWidget *wtmp, *wframe, *wvbox;

	if (!(mw->curfile && mw->curfile->titles)) {
		return;
	}

	if (mw->curfile->curseg_vbox != NULL) {
		gtk_container_remove (GTK_CONTAINER (mw->curfile->curtitle_vbox), mw->curfile->curseg_vbox);
	}

	mw->curfile->curseg = n;
	mw->curfile->curseg_vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (mw->curfile->curtitle_vbox), mw->curfile->curseg_vbox);

  	curseg = &(mw->curfile->titles->_buffer[mw->curfile->curtitle].segments._buffer[n]);
  	ctmp = keyval_get_value(&curseg->attrs, "title");
  	if (!ctmp) { 
  		keyval_get_value(&curseg->attrs, "name");
  	}
  	
  	if (!ctmp) { 
  		ctmp = buf; 
  		g_snprintf (buf, sizeof(buf), "Segment %d", n + 1); 
	}

  	wframe = gtk_frame_new (ctmp);
  	gtk_container_add (GTK_CONTAINER (mw->curfile->curseg_vbox), wframe);
  	wvbox = gtk_vbox_new (FALSE, GNOME_PAD);
  	gtk_container_add (GTK_CONTAINER (wframe), wvbox);

  	g_snprintf(buf, sizeof(buf), "%d:%.2d - %d:%.2d",
	     curseg->startTime.tvSec/60,
	     curseg->startTime.tvSec%60,
	     curseg->stopTime.tvSec/60,
	     curseg->stopTime.tvSec%60);
  	wtmp = gtk_label_new (buf);
  	gtk_container_add (GTK_CONTAINER (wvbox), wtmp);

  	for (i = 0; i < curseg->attrs._length; i++) {
    		g_snprintf (buf, sizeof(buf), "%s: %s",
	       		    curseg->attrs._buffer[i].key,
	       		    curseg->attrs._buffer[i].value);
      
    		wtmp = gtk_label_new (buf);
    		gtk_container_add (GTK_CONTAINER (wvbox), wtmp);
	}

	gtk_widget_show_all (mw->curfile->curseg_vbox);
}

static void
mp_segment_activate (GtkWidget *widget, MPMainwin *mw)
{
	GMF_MediaInfo_Segment *curseg;
	int n;

	n = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (widget), "segment_num"));

	mp_update_segment (n, mw);
  	curseg = &(mw->curfile->titles->_buffer[mw->curfile->curtitle].segments._buffer[n]);

	gtk_adjustment_set_value (mw->position, (gfloat)curseg->startTime.tvSec + ((gfloat)curseg->startTime.tvUsec)/1000000.0);
}

static void
mp_update_title (GtkWidget *widget, MPMainwin *mw)
{
	int i, n;
	GMF_MediaInfo_Title *title;
	char buf[256], *ctmp;
	GtkWidget *wtmp, *wtmp2;
	GtkWidget *wtmp3, *wtmp4;
	GMF_MediaInfo_Segment *curseg;

	g_return_if_fail (widget);

	if (!(mw->curfile && mw->curfile->titles)) {
		return;
	}

	if (mw->curfile->curtitle_vbox) {
		gtk_container_remove (GTK_CONTAINER (mw->vbox), mw->curfile->curtitle_vbox);
		mw->curfile->curseg_vbox = NULL;
	}

	mw->curfile->curtitle = n = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT(widget), "title_num"));
	mw->curfile->curtitle_vbox = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (mw->vbox), mw->curfile->curtitle_vbox);

	title = &mw->curfile->titles->_buffer[n];

	ctmp = keyval_get_value(&title->attrs, "title");
	if(!ctmp) {
		keyval_get_value(&title->attrs, "name");
  		ctmp = buf; 
  		g_snprintf(buf, sizeof(buf), "Title %d", n); 
	}

	if (title->attrs._length > 0) {
		wtmp = gtk_frame_new(ctmp);

		gtk_container_add (GTK_CONTAINER (mw->curfile->curtitle_vbox), wtmp);
		gtk_container_add (GTK_CONTAINER (wtmp), wtmp2 = gtk_vbox_new (FALSE, GNOME_PAD));

		for (i = 0; i < title->attrs._length; i++) {
			g_snprintf (buf, sizeof(buf), "%s: %s",
				    title->attrs._buffer[i].key,
				    title->attrs._buffer[i].value);
      
			gtk_container_add (GTK_CONTAINER (wtmp2), gtk_label_new (buf));
		}
	}

  	if (title->segments._length < 2) {
    		gtk_widget_show_all (mw->curfile->curtitle_vbox);
		return;
	}

	mw->curfile->opt_seg = wtmp2 = gtk_option_menu_new ();
	wtmp3 = gtk_menu_new ();

	for (i = 0; i < title->segments._length; i++) {
		char *ctmp, buf2[256];
		GMF_TimeVal len;
		curseg = &title->segments._buffer[i];
      
		ctmp = keyval_get_value (&curseg->attrs, "title");
		if (!ctmp) {
			keyval_get_value (&curseg->attrs, "name");
		}
		len = gmf_time_subtract (curseg->stopTime, curseg->startTime);

		g_snprintf (buf2, sizeof(buf2), "%d %s%s%d:%.2d", i + 1, ctmp?ctmp:"", ctmp?" ":"",
	       		    len.tvSec/60, len.tvSec%60);

		wtmp4 = gtk_menu_item_new_with_label (buf2);
		gtk_container_add (GTK_CONTAINER (wtmp3), wtmp4);
		gtk_widget_show (wtmp4);
		gtk_object_set_data (GTK_OBJECT (wtmp4), "segment_num", GUINT_TO_POINTER (i));
		gtk_signal_connect (GTK_OBJECT (wtmp4), "activate", GTK_SIGNAL_FUNC (mp_segment_activate), mw);
	}

	gtk_option_menu_set_menu (GTK_OPTION_MENU (wtmp2), wtmp3);
	gtk_container_add (GTK_CONTAINER (mw->curfile->curtitle_vbox), wtmp2);

	gtk_option_menu_set_history (GTK_OPTION_MENU (wtmp2), 0);

	mp_update_segment (0, mw);

	gtk_widget_show_all (mw->curfile->curtitle_vbox);
}

static void
mp_file_open_display_media_info(MPMainwin *mw, MPCurfile *curfile)
{
	GMF_MediaInfo_Titles *titles;
	GtkWidget *myvbox;
	int i;
	GMF_MediaInfo_Title *curtitle;

	curfile->titles = titles = GMF_MediaInfo__get_mediaTitles (curfile->media_info, &mw->ev);
	if (mw->ev._major != CORBA_NO_EXCEPTION) {
		curfile->titles = NULL;
		return;
	}

	if (titles->_length == 0) {
		return;
	}

	/* Turn the information into a nice displayable thing for the user */
	myvbox = gtk_vbox_new (FALSE, 0);
  	gtk_container_add (GTK_CONTAINER (curfile->vbox), myvbox);

	if (titles->_length > 1) {
		GtkWidget *wtmp2, *wtmp3, *wtmp4;
    wtmp2 = gtk_option_menu_new();
    wtmp3 = gtk_menu_new();

    for(i = 0; i < titles->_length; i++) {
      char *ctmp, buf[16];
      curtitle = &titles->_buffer[i];

      ctmp = keyval_get_value(&curtitle->attrs, "title");
      if(!ctmp) keyval_get_value(&curtitle->attrs, "name");
      if(!ctmp) { ctmp = buf; g_snprintf(buf, sizeof(buf), "Title %d", i); }

      wtmp4 = gtk_menu_item_new_with_label(ctmp);
      gtk_container_add(GTK_CONTAINER(wtmp3), wtmp4);
      gtk_widget_show(wtmp4);
      gtk_object_set_data(GTK_OBJECT(wtmp4), "title_num", GUINT_TO_POINTER(i));
      gtk_signal_connect(GTK_OBJECT(wtmp4), "activate", GTK_SIGNAL_FUNC(mp_update_title), mw);
    }

    gtk_container_add(GTK_CONTAINER(myvbox), wtmp2);
    gtk_option_menu_set_history(GTK_OPTION_MENU(wtmp2),
				curfile->curtitle = GMF_MediaInfo__get_currentTitle(curfile->media_info, &mw->ev));
    gtk_option_menu_set_menu(GTK_OPTION_MENU(wtmp2), wtmp3);
  }

  gtk_object_set_data(GTK_OBJECT(myvbox), "title_num", GUINT_TO_POINTER(curfile->curtitle));
  mp_update_title(myvbox, mw);
}

static char *
keyval_get_value(CORBA_sequence_GMF_MediaInfo_KeyVal *kv, const char *key)
{
  int i;
  for(i = 0; i < kv->_length; i++) {
    if(!strcasecmp(key, kv->_buffer[i].key))
      return kv->_buffer[i].value;
  }

  return NULL;
}

static void
mp_graph_stop(GMFFilterGraph *fg, MPMainwin *mw)
{
  g_return_if_fail(mw->curfile);

  mw->poschange++;
  gtk_adjustment_set_value(mw->position, 0.0);
  mw->poschange--;
  mw->curfile->pauseStart.tvSec = mw->curfile->pauseStart.tvUsec = 0;
}

static void
mp_graph_pause(GMFFilterGraph *fg, MPMainwin *mw)
{
  g_return_if_fail(mw->curfile);

  if(!GMF_FILTER_GRAPH(mw->curfile->fg)->timeBase.tvSec
     && !GMF_FILTER_GRAPH(mw->curfile->fg)->timeBase.tvUsec) {
    mw->curfile->pauseStart.tvSec =
      mw->curfile->pauseStart.tvUsec = 0;
  } else
    mw->curfile->pauseStart = gmf_time_subtract(gmf_time_reference_current_time(GMF_TIME_REFERENCE(mw->curfile->timeref)),
						GMF_FILTER_GRAPH(mw->curfile->fg)->timeBase);
}

static void
mp_graph_run(GMFFilterGraph *fg, MPMainwin *mw)
{
}

static gboolean
mp_graph_end_of_stream(MPMainwin *mw)
{
  mp_set_state(mw, STOP);

  return FALSE;
}

static void
mp_graph_deliver_event(GMFFilterGraph *fg, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData, MPMainwin *mw)
{
  if(!strcmp(evName, "EndOfStream")) {
    mw->curfile->cbid =
      gmf_time_reference_request_alarm(GMF_TIME_REFERENCE(mw->curfile->timeref), &GMF_FILTER_GRAPH(mw->curfile->fg)->timeBase,
				       timestamp, (GSourceFunc)mp_graph_end_of_stream, mw);
  }
}

/* assumptions - media_pos !nil */
static gboolean
mp_play_update(MPMainwin *mw)
{
  GMF_TimeVal position;
  GMF_MediaPosition_Value tmpval;


  tmpval = GMF_MediaPosition_get_position(mw->curfile->media_pos, GMF_MediaPosition_TIME, &mw->ev);
  position = tmpval._u.value_time;

  mp_update_label(mw->label, position, mw->curfile->duration, mw);

  mw->poschange++;
  gtk_adjustment_set_value(mw->position, (gfloat)position.tvSec + ((gfloat)position.tvUsec)/1000000.0);
  mw->poschange--;

  return TRUE;
}

static void
mp_play_update_start(MPMainwin *mw)
{
  g_return_if_fail(mw->curfile);

  if(!CORBA_Object_is_nil(mw->curfile->media_pos, &mw->ev)) {
    mw->curfile->timer_id = gtk_timeout_add(250, (GtkFunction)mp_play_update, mw);
  }
}

static void
mp_play_update_stop(MPMainwin *mw)
{
  g_return_if_fail(mw->curfile);

  if(mw->curfile->timer_id >= 0) {
    mp_play_update(mw);
    gtk_timeout_remove(mw->curfile->timer_id);
  }
}

static void
mp_update_label(GtkWidget *label, GMF_TimeVal position, GMF_TimeVal duration, MPMainwin *mw)
{
  char buf[64];
  int i;
  CORBA_sequence_GMF_MediaInfo_Segment *curseg;

  g_snprintf(buf, sizeof(buf), "%u:%2.2u.%3.3u / %u:%2.2u.%3.3u",
	     position.tvSec/60, position.tvSec % 60,
	     position.tvUsec / (1000000/100),
	     duration.tvSec/60, duration.tvSec % 60,
	     duration.tvUsec / (1000000/100));
  gtk_label_set(GTK_LABEL(label), buf);

  if(mw->curfile->titles && mw->curfile->titles->_length) {
    /* wheeeeee! */
    curseg = &(mw->curfile->titles->_buffer[mw->curfile->curtitle].segments);

    for(i = 0; i < curseg->_length; i++) {
      if(gmf_time_compare(curseg->_buffer[i].startTime, position) >= 0
	 && gmf_time_compare(curseg->_buffer[i].stopTime, position) < 0) {
	if(mw->curfile->curseg != i) {
	  gtk_option_menu_set_history(GTK_OPTION_MENU(mw->curfile->opt_seg), i);
	  mp_update_segment(i, mw);
	}
	break;
      }
    }
  }
}

static void
mp_change_position(GtkAdjustment *position, MPMainwin *mw)
{
  GMF_MediaPosition_Value newpos;
  double newsec;

  if(mw->poschange > 0)
    return;

  mw->poschange++;

  newpos._d = GMF_MediaPosition_TIME;
  newpos._u.value_time.tvUsec = modf(position->value, &newsec) * 1000000.0;
  newpos._u.value_time.tvSec = floor(newsec);

  GMF_MediaPosition_set_position(mw->curfile->media_pos, &newpos, &mw->ev);
  
  mw->poschange--;
}

/* Three sources of position changes:
 *   Outside (normal playback):
 *     Update track & slider.
 *   Track changes:
 *     Update slider & outside.
 *   Slider changes:
 *     Update track & outside;
 */
