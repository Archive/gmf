/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */
/* We could implement this filter two different ways:
 *   1. Just send commands to the drive and have it make the noise.
 *   2. Get the audio data out of the drive and send it on an output pipe.
 *
 * #2 would be nicer, but it uses more CPU and supposedly won't work on many CD-ROM drives, so we do #1 for now.
 * The skeleton code is here if we need to switch, though.
 */

#include <gmf.h>
#include <libgmf/gmf-digital-audio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>

#include <sys/stat.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/cdrom.h>



#define USEC_PER_SAMPLE 20000

typedef struct {
  CORBA_ORB orb;
  CORBA_Environment *ev, evdat;

  GMFFilter *filter;
  GMFMediaInfo *mi;
  GMFTimeReference *timeref;
  GMF_Callback_CallbackID cbid;

  GMF_TimeVal presend;
  GMF_TimeVal last_sample;
  GMF_TimeVal playback_interval;
  struct cdrom_msf duration;
  struct cdrom_msf playtime;

  gboolean isDiscontinuity, sent_eos;
  GMF_Filter_State last_state;

  char *curfile;
  int fd;
  struct cdrom_tochdr toch;
  struct cdrom_tocentry *toc;
  int ntocents;
} FilterInfo;

typedef struct {
  FilterInfo *fi;
  GMFPipe *apipe;
} PipeInfo;

static CORBA_Object cdaudio_create_filter(PortableServer_POA poa, const char *oaf_id,
					  gpointer impl_ptr, CORBA_Environment *ev);
static void cdaudio_unref_factory(GtkObject *filter, FilterInfo *fi);
static void cdaudio_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void cdaudio_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi);
static void cdaudio_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi);
static void cdaudio_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, CORBA_Environment *ev, FilterInfo *fi);
static void cdaudio_stop(GMFFilter *filter, FilterInfo *fi);
static void cdaudio_pause(GMFFilter *filter, FilterInfo *fi);
static void cdaudio_run(GMFFilter *filter, FilterInfo *fi);
static void cdaudio_can_seek(GMFFilter *filter, gboolean *retval, FilterInfo *fi);
static void cdaudio_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType,
				 GMF_MediaPosition_Value *value, FilterInfo *fi);
static void cdaudio_get_position(GMFMediaPosition *mp, GMF_MediaPosition_Type posType,
				 GMF_MediaPosition_Value *value, FilterInfo *fi);
static void cdaudio_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void cdaudio_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType,
				  GMF_MediaPosition_Value *value, FilterInfo *fi);
static void cdaudio_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void cdaudio_get_media_titles(GMFMediaInfo *mi, GMF_MediaInfo_Titles *retval, FilterInfo *fi);

static void cdaudio_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void cdaudio_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void cdaudio_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void cdaudio_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			      GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);
static void cdaudio_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi);
#if 0
static gboolean cdaudio_send_sample(FilterInfo *fi);
#endif

static const char iid[] = "OAFIID:gmf-filter-cdaudio:32eb5b97-6d8d-4531-a0eb-e647addd2526";

static const OAFPluginObject plugin_list[] = {
	{
  		iid,
		cdaudio_create_filter,
	},
	
  	{
  		NULL
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"CA Audio Stuff"
};

static CORBA_Object
cdaudio_create_filter(PortableServer_POA poa, const char *iid,
		      gpointer impl_ptr, CORBA_Environment *ev)
{
  GtkObject *filter, *mp, *mi;
  FilterInfo *fi;

  if(strcmp(iid, "gmf-filter-cdaudio"))
    return CORBA_OBJECT_NIL;

  filter = gtk_object_new(gmf_filter_get_type(), "filter_type", GMF_Filter_PARSER, NULL);
  mp = gtk_object_new(gmf_media_position_get_type(), NULL);
  mi = gtk_object_new(gmf_media_info_get_type(), NULL);

  oaf_plugin_use (poa, impl_ptr);
  bonobo_object_add_interface(BONOBO_OBJECT(filter), BONOBO_OBJECT(mp));
  bonobo_object_add_interface(BONOBO_OBJECT(filter), BONOBO_OBJECT(mi));

  fi = g_new0(FilterInfo, 1);
  fi->ev = &fi->evdat;
  CORBA_exception_init(fi->ev);

  gtk_signal_connect(filter, "get_pipe", GTK_SIGNAL_FUNC(cdaudio_get_pipe), fi);
  gtk_signal_connect(filter, "destroy", GTK_SIGNAL_FUNC(cdaudio_unref_factory), fi);

  gtk_signal_connect(filter, "get_attributes", GTK_SIGNAL_FUNC(cdaudio_get_attributes), fi);
  gtk_signal_connect(filter, "get_attribute", GTK_SIGNAL_FUNC(cdaudio_get_attribute), fi);
  gtk_signal_connect(filter, "set_attribute", GTK_SIGNAL_FUNC(cdaudio_set_attribute), fi);

  gtk_signal_connect(filter, "stop", GTK_SIGNAL_FUNC(cdaudio_stop), fi);
  gtk_signal_connect(filter, "pause", GTK_SIGNAL_FUNC(cdaudio_pause), fi);
  gtk_signal_connect(filter, "run", GTK_SIGNAL_FUNC(cdaudio_run), fi);

  gtk_signal_connect(mp, "can_seek_forward", GTK_SIGNAL_FUNC(cdaudio_can_seek), fi);
  gtk_signal_connect(mp, "can_seek_backward", GTK_SIGNAL_FUNC(cdaudio_can_seek), fi);
  gtk_signal_connect(mp, "get_duration", GTK_SIGNAL_FUNC(cdaudio_get_duration), fi);
  gtk_signal_connect(mp, "get_position", GTK_SIGNAL_FUNC(cdaudio_get_position), fi);
  gtk_signal_connect(mp, "set_position", GTK_SIGNAL_FUNC(cdaudio_set_position), fi);
  gtk_signal_connect(mp, "get_stop_time", GTK_SIGNAL_FUNC(cdaudio_get_stop_time), fi);
  gtk_signal_connect(mp, "set_stop_time", GTK_SIGNAL_FUNC(cdaudio_set_stop_time), fi);

  gtk_signal_connect(mi, "get_media_titles", GTK_SIGNAL_FUNC(cdaudio_get_media_titles), fi);

  fi->filter = GMF_FILTER(filter);
  fi->timeref = fi->filter->timeref;
  fi->cbid = 0;
  fi->playback_interval.tvSec = 0;
  fi->playback_interval.tvUsec = USEC_PER_SAMPLE;
  fi->last_state = GMF_Filter_STOPPED;

  if (0) { /* Right now we don't have any outputs */
    GMFPipe *default_pipe;

    cdaudio_get_pipe(fi->filter, GMF_OUT, &default_pipe, fi);
    gtk_object_set(GTK_OBJECT(default_pipe), "autorender", TRUE, NULL);
  }

  impl_ptr = filter;

  return CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(filter)), fi->ev);
}

static void
cdaudio_unref_factory(GtkObject *filter, FilterInfo *fi)
{
  CORBA_exception_free(fi->ev);
  if(fi->curfile) {
    g_free(fi->curfile);
    ioctl(fi->fd, CDROMSTOP);
    close(fi->fd);
    g_free(fi->toc);
  }
  g_free(fi);

  oaf_plugin_unuse(filter);
}

static void
cdaudio_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  GtkObject *apipe;
  PipeInfo *pi;

  g_return_if_fail(pDirection == GMF_OUT);

  apipe = gtk_object_new(gmf_output_pipe_get_type(),
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 NULL);

  pi = g_new0(PipeInfo, 1);
  pi->fi = fi;
  pi->apipe = GMF_PIPE(apipe);

  gtk_object_set_data(apipe, "PipeInfo", pi);

  gtk_signal_connect(apipe, "can_process_type", cdaudio_out_can_process_type, pi);
  gtk_signal_connect(apipe, "get_processable_types", cdaudio_out_get_processable_types, pi);
  gtk_signal_connect(apipe, "connect", cdaudio_out_connect, pi);
  gtk_signal_connect(apipe, "fixup_media_format", cdaudio_out_fixup, pi);
  gtk_signal_connect(apipe, "quality_notify", cdaudio_out_qos, pi);

  gmf_filter_add_pipe(fi->filter, pi->apipe);

  *out_pipe = pi->apipe;
}

static void
cdaudio_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi)
{
  const char * const attr_names[] = {
    "URL",
    NULL
  };
  int i;

  for(i = 0; attr_names[i]; i++);

  retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(i);
  for(i = 0; attr_names[i]; i++)
    retval->_buffer[i] = CORBA_string_dup((char *)attr_names[i]);
}

static void
cdaudio_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi)
{
  if(!strcmp(attr_name, "URL") && fi->curfile) {
    ret_attr_val->_type = (CORBA_TypeCode)TC_string;
    ret_attr_val->_value = &fi->curfile;
    CORBA_any_set_release(ret_attr_val, CORBA_FALSE);
  }
}

static void
cdaudio_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, CORBA_Environment *ev, FilterInfo *fi)
{
  if(!strcmp(attr_name, "URL")) {
    struct cdrom_tocentry *curent;
    char **strptr, *fname;
    int newfd, i;
    struct cdrom_tochdr toch;

    g_return_if_fail(CORBA_TypeCode_equal(attr_val->_type, (CORBA_TypeCode)TC_string, fi->ev));


    strptr = attr_val->_value;

    fname = *strptr;

    if(!strncmp(fname, "file:", strlen("file:"))) {
      fname += strlen("file:");
      if(!strncmp(fname, "//", 2)) {
	char *ctmp;

	ctmp = strchr(fname + 2, '/');
	if(ctmp)
	  fname = ctmp;
	else
	  fname = "";
      }
    }

    newfd = open(fname, O_RDONLY | O_NONBLOCK);
    if(newfd < 0)
      goto out;

    if(ioctl(newfd, CDROMREADTOCHDR, &toch))
      goto out;

    ioctl(newfd, CDROMSTOP);

    if(fi->curfile) {
      ioctl(fi->fd, CDROMSTOP);
      close(fi->fd);
      g_free(fi->curfile);
      g_free(fi->toc);
    }

    fi->curfile = g_strdup(fname);
    fi->fd = newfd;
    fi->toch = toch;
    fi->toc = g_new0(struct cdrom_tocentry, toch.cdth_trk1 + 1);
    fi->ntocents = toch.cdth_trk1 + 1;

    curent = &fi->toc[0];
    curent->cdte_track = CDROM_LEADOUT;
    curent->cdte_format = CDROM_MSF;
    if(ioctl(fi->fd, CDROMREADTOCENTRY, curent)) {
      g_warning("TOC entry for %d failed", (int)curent->cdte_track);
    }
    for(i = toch.cdth_trk0; i <= toch.cdth_trk1; i++) {

      curent = &fi->toc[i];

      curent->cdte_track = i;
      curent->cdte_format = CDROM_MSF;
      if(ioctl(fi->fd, CDROMREADTOCENTRY, curent)) {
	g_warning("TOC entry for %d failed", i);
	memset(curent, '\0', sizeof(*curent));
	continue;
      }

    }

    curent = &fi->toc[0];
    memset(&fi->duration, '\0', sizeof(fi->duration));
    fi->duration.cdmsf_frame1 += curent->cdte_addr.msf.frame;
    if(fi->duration.cdmsf_frame1 > CD_FRAMES) {
      fi->duration.cdmsf_sec1 += (fi->duration.cdmsf_frame1 / CD_FRAMES);
      fi->duration.cdmsf_frame1 = (fi->duration.cdmsf_frame1 % CD_FRAMES);
    }
    fi->duration.cdmsf_sec1 += curent->cdte_addr.msf.second;
    if(fi->duration.cdmsf_sec1 > CD_SECS) {
      fi->duration.cdmsf_min1 += (fi->duration.cdmsf_sec1 / CD_SECS);
      fi->duration.cdmsf_sec1 = (fi->duration.cdmsf_sec1 % CD_SECS);
    }
    fi->duration.cdmsf_min1 += curent->cdte_addr.msf.minute;

    fi->playtime = fi->duration;
  }

  return;

 out:
	CORBA_exception_set(ev, CORBA_USER_EXCEPTION, ex_GMF_AttributeAccess_SetFailed, NULL);
}

static void
cdaudio_stop(GMFFilter *filter, FilterInfo *fi)
{
  g_return_if_fail(fi->curfile);

  ioctl(fi->fd, CDROMSTOP);

  fi->playtime.cdmsf_min0 =
    fi->playtime.cdmsf_sec0 =
    fi->playtime.cdmsf_frame0 = 0;

  fi->last_state = GMF_Filter_STOPPED;
}

static void
cdaudio_pause(GMFFilter *filter, FilterInfo *fi)
{
  struct cdrom_subchnl sub;
  g_return_if_fail(fi->curfile);

  switch(fi->last_state) {
  case GMF_Filter_RUNNING:
    sub.cdsc_format = CDROM_MSF;
    ioctl(fi->fd, CDROMSUBCHNL, &sub);
    fi->playtime.cdmsf_min0 = sub.cdsc_absaddr.msf.minute;
    fi->playtime.cdmsf_sec0 = sub.cdsc_absaddr.msf.second;
    fi->playtime.cdmsf_frame0 = sub.cdsc_absaddr.msf.frame;
    ioctl(fi->fd, CDROMSTOP);
    fi->last_state = GMF_Filter_PAUSED;
    break;
  case GMF_Filter_STOPPED:
  default:
    ioctl(fi->fd, CDROMSTOP);
    fi->last_state = GMF_Filter_STOPPED; /* Lame, lame */
    break;
  }
}

static void
cdaudio_run(GMFFilter *filter, FilterInfo *fi)
{
  g_return_if_fail(fi->curfile);

  ioctl(fi->fd, CDROMPLAYMSF, &fi->playtime);

  fi->last_state = GMF_Filter_RUNNING;
  fi->sent_eos = FALSE;
}

static void
cdaudio_can_seek(GMFFilter *filter, gboolean *retval, FilterInfo *fi)
{
  *retval = TRUE; 
}

static void
cdaudio_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType,
		     GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    value->_d = posType;
    value->_u.value_samples = ((fi->duration.cdmsf_min1 * CD_SECS) + fi->duration.cdmsf_sec1) * CD_FRAMES + fi->duration.cdmsf_frame1;
    break;
  case GMF_MediaPosition_TIME:
    value->_d = posType;
    value->_u.value_time.tvSec = ((fi->duration.cdmsf_min1 * CD_SECS) + fi->duration.cdmsf_sec1);
    value->_u.value_time.tvUsec = ((double)fi->duration.cdmsf_frame1)/((double)CD_FRAMES) * 1000000.0;
    break;
  }
}

static void
cdaudio_get_position(GMFMediaPosition *mp, GMF_MediaPosition_Type posType,
		     GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  struct cdrom_subchnl sub;

  sub.cdsc_format = CDROM_MSF;
  ioctl(fi->fd, CDROMSUBCHNL, &sub);

  if(value) {
    switch(posType) {
    case GMF_MediaPosition_SAMPLES:
      value->_d = posType;
      value->_u.value_samples = ((sub.cdsc_absaddr.msf.minute * CD_SECS) + sub.cdsc_absaddr.msf.second) * CD_FRAMES
	+ sub.cdsc_absaddr.msf.frame;
      break;
    case GMF_MediaPosition_TIME:
      value->_d = posType;
      value->_u.value_time.tvSec = (((int)sub.cdsc_absaddr.msf.minute * CD_SECS) + (int)sub.cdsc_absaddr.msf.second);
      value->_u.value_time.tvUsec = ((double)sub.cdsc_absaddr.msf.frame)/((double)CD_FRAMES) * 1000000.0;
      break;
    }
  }

  if((sub.cdsc_audiostatus == CDROM_AUDIO_COMPLETED) && !fi->sent_eos) {
    GMF_FilterGraph fg;
    GMF_TimeVal now;
    CORBA_any foo;

    foo._type = (CORBA_TypeCode)TC_null;
    foo._value = NULL;
    CORBA_any_set_release(&foo, CORBA_TRUE);

    now = gmf_time_subtract(gmf_time_reference_current_time(fi->timeref), fi->filter->timeBase);
    fg = GMF_Filter__get_graph(bonobo_object_corba_objref(BONOBO_OBJECT(fi->filter)), fi->ev);
    GMF_EventReceiver_deliver(fg, &now, "EndOfStream", &foo, fi->ev);
    fi->sent_eos = TRUE;
  }
}

static void
cdaudio_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  struct cdrom_msf msf;
  int i;

  msf = fi->playtime;

  switch(value->_d) {
  case GMF_MediaPosition_SAMPLES:
    i = value->_u.value_samples;
    msf.cdmsf_frame0 = i % CD_FRAMES;
    i -= msf.cdmsf_frame0;
    i /= CD_FRAMES;
    msf.cdmsf_sec0 = i % CD_SECS;
    i -= msf.cdmsf_sec0;
    i /= CD_SECS;
    msf.cdmsf_min0 = i;
    break;
  case GMF_MediaPosition_TIME:
    msf.cdmsf_frame0 = ((double)value->_u.value_time.tvUsec) * ((double)CD_FRAMES) / 1000000.0;
    msf.cdmsf_sec0 = value->_u.value_time.tvSec % CD_SECS;
    msf.cdmsf_min0 = value->_u.value_time.tvSec / CD_SECS;
    break;
  }

  fi->playtime = msf;

  if(fi->last_state == GMF_Filter_RUNNING)
    cdaudio_run(fi->filter, fi);
}

static void
cdaudio_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType,
		      GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    value->_d = posType;
    value->_u.value_samples = ((fi->playtime.cdmsf_min1 * CD_SECS) + fi->playtime.cdmsf_sec1) * CD_FRAMES
      + fi->playtime.cdmsf_frame1;
    break;
  case GMF_MediaPosition_TIME:
    value->_d = posType;
    value->_u.value_time.tvSec = ((fi->playtime.cdmsf_min1 * CD_SECS) + fi->playtime.cdmsf_sec1);
    value->_u.value_time.tvUsec = ((double)fi->playtime.cdmsf_frame1)/((double)CD_FRAMES) * 1000000.0;
    break;
  }
}

static void
cdaudio_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  struct cdrom_msf msf;
  int i;

  msf = fi->playtime;
  switch(value->_d) {
  case GMF_MediaPosition_SAMPLES:
    i = value->_u.value_samples;
    msf.cdmsf_frame1 = i % CD_FRAMES;
    i -= msf.cdmsf_frame1;
    i /= CD_FRAMES;
    msf.cdmsf_sec1 = i % CD_SECS;
    i -= msf.cdmsf_sec1;
    i /= CD_SECS;
    msf.cdmsf_min1 = i;
    break;
  case GMF_MediaPosition_TIME:
    msf.cdmsf_frame1 = ((double)value->_u.value_time.tvUsec) * ((double)CD_FRAMES) / 1000000.0;
    msf.cdmsf_sec1 = value->_u.value_time.tvSec % CD_SECS;
    msf.cdmsf_min1 = value->_u.value_time.tvSec / CD_SECS;
    break;
  }

  fi->playtime = msf;
}

static void
cdaudio_get_media_titles(GMFMediaInfo *mi, GMF_MediaInfo_Titles *retval, FilterInfo *fi)
{
  int i;
  struct cdrom_tocentry *curent, *nextent;
  GMF_MediaInfo_Title *cd;
  static const CORBA_char *stream_dims[] = {"audio"};
  static const GMF_MediaInfo_Stream audio_stream = { /* XXX hardcode hack */
    {GMF_MEDIA_AUDIO_ANALOG, "audio/x-cd-audio-out", CORBA_FALSE, CORBA_FALSE, 0,
    {(CORBA_TypeCode)TC_null, NULL, CORBA_FALSE},
    {(CORBA_TypeCode)TC_null, NULL, CORBA_FALSE}},
    {1, 1, (char **)stream_dims, CORBA_FALSE}
  };
  GMF_MediaInfo_Segment *curseg;

  if(!fi->curfile) return;

  retval->_length = 1;
  retval->_buffer = cd = CORBA_sequence_GMF_MediaInfo_Title_allocbuf(1);
  CORBA_sequence_set_release(retval, CORBA_TRUE);

  cd->attrs._length = 0; /* XXX todo - fill in title & artist from CDDB */
  cd->attrs._buffer = NULL;
  CORBA_sequence_set_release(&cd->attrs, CORBA_FALSE);

  cd->streams._length = 1;
  cd->streams._buffer = (GMF_MediaInfo_Stream *)&audio_stream;
  CORBA_sequence_set_release(&cd->streams, CORBA_FALSE);

  cd->segments._length = fi->ntocents - 1;
  cd->segments._buffer = CORBA_sequence_GMF_MediaInfo_Segment_allocbuf(cd->segments._length);

  for(i = 0; i < cd->segments._length; i++) {
    curseg = &(cd->segments._buffer[i]);

    curent = &(fi->toc[i + fi->toch.cdth_trk0]);
    if(i >= (cd->segments._length - 1))
      nextent = &(fi->toc[0]);
    else
      nextent = &(fi->toc[i + fi->toch.cdth_trk0 + 1]);

    curseg->attrs._length = 0;
    curseg->attrs._buffer = NULL;
    CORBA_sequence_set_release(&curseg->attrs, CORBA_FALSE);

    curseg->startTime.tvSec = (curent->cdte_addr.msf.minute * CD_SECS) + curent->cdte_addr.msf.second;
    curseg->startTime.tvUsec = ((double)curent->cdte_addr.msf.frame) / ((double)CD_FRAMES) * 1000000.0;

    curseg->stopTime.tvSec = (nextent->cdte_addr.msf.minute * CD_SECS) + nextent->cdte_addr.msf.second;
    curseg->stopTime.tvUsec = ((double)nextent->cdte_addr.msf.frame) / ((double)CD_FRAMES) * 1000000.0;
  }
}

/* Unused below here */
static void
cdaudio_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			     gboolean *retval, PipeInfo *pi)
{
}

static void
cdaudio_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
}

static void
cdaudio_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
}

static void
cdaudio_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		  GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
}

static void
cdaudio_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi)
{
}

#if 0
static gboolean
cdaudio_send_sample(FilterInfo *fi)
{
  return FALSE;
}
#endif
