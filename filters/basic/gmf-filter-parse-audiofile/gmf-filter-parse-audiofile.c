/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */
/* How it works:
 *  When we start running, we send out samples incrementing from the current position.
 *  Using the QOS thing, we always catch up if we are late, and drop a certain % of sample packets.
 */

#define G_LOG_DOMAIN "parseaf"

#include <gmf.h>
#include <libgmf/gmf-digital-audio.h>
#include <limits.h>
#include <endian.h>
#include <math.h>
#include <sched.h>
#include <audiofile.h>
#include <af_vfs.h>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netinet/in.h> /* byte conversion */

#define USEC_PER_SAMPLE 50000

typedef struct {
  CORBA_Environment *ev, evdat;
  CORBA_ORB orb;

  GMFFilter *filter;
  GMFTimeReference *timeref;

  GMFPipe *inpipe;
  GMF_DataReader cobj;

  AFfilesetup mysetup;

  AFfilehandle file_handle;
  int tracknum;

  GMF_TimeVal presend;
  GMF_TimeVal last_sample;
  GMF_TimeVal stopTime;
  GMF_TimeVal playback_interval;

  long stopframe;

  GMF_Callback_CallbackID cbid;
  gboolean isDiscontinuity;

  GMF_Media_AudioDigitalFormat adf;
} FilterInfo;

typedef struct {
  FilterInfo *fi;

  GMFPipe *apipe;
} PipeInfo;

static void parseaf_unref_factory(GtkObject *filter, FilterInfo *fi);
static void parseaf_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void parseaf_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi);
static void parseaf_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi);
static void parseaf_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, FilterInfo *fi);
static gint parseaf_read_af_header(FilterInfo *fi);
static void parseaf_stop(GMFFilter *filter, FilterInfo *fi);
static void parseaf_pause(GMFFilter *filter, FilterInfo *fi);
static void parseaf_run(GMFFilter *filter, FilterInfo *fi);
static void parseaf_deliver_event(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName,
				  CORBA_any *evData, FilterInfo *fi) G_GNUC_UNUSED;
static void parseaf_can_seek(GMFMediaPosition *mp, gboolean *retval, FilterInfo *fi);
static void parseaf_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseaf_get_position(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseaf_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseaf_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseaf_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);

static void parseaf_in_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void parseaf_in_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void parseaf_in_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi);
static void parseaf_in_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void parseaf_in_disconnect(GMFPipe *apipe, PipeInfo *pi);
static void parseaf_in_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			     GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);

static void parseaf_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void parseaf_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void parseaf_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void parseaf_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			      GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);
static void parseaf_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi);
static gboolean parseaf_play(FilterInfo *fi);
static CORBA_Object parseaf_create_filter(PortableServer_POA poa,
					  const char *iid,
					  gpointer impl_ptr,
					  CORBA_Environment *ev);

static const char filter_iid[] = "OAFIID:gmf-filter-parse-audiofile:06cd08e0-9b30-4642-a0c5-7781cf564727";

static const OAFPluginObject plugin_list[] = {
	{
  		filter_iid,
		parseaf_create_filter
	},
	
  	{
  		NULL,
  		NULL
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"Audio File Filter Stuff"
};


static CORBA_Object
parseaf_create_filter (PortableServer_POA poa,
		       const char *iid,
		       gpointer impl_ptr,
		       CORBA_Environment *ev)
{
	GtkObject *filter, *mp;
	FilterInfo *fi;
	GMFPipe *default_pipe;

	if ( strcmp(iid, filter_iid) != 0) {
    		return CORBA_OBJECT_NIL;
    	}

	filter = gtk_object_new (gmf_filter_get_type(), "filter_type", GMF_Filter_PARSER, NULL);
	mp = gtk_object_new (gmf_media_position_get_type(), NULL);
	oaf_plugin_use (poa, impl_ptr);
	bonobo_object_add_interface (BONOBO_OBJECT (filter), BONOBO_OBJECT (mp));
	g_assert (filter && mp);

	fi = g_new0 (FilterInfo, 1);
	fi->ev = &fi->evdat;
	CORBA_exception_init (fi->ev);
	gtk_signal_connect (filter, "get_pipe", GTK_SIGNAL_FUNC (parseaf_get_pipe), fi);
	gtk_signal_connect (filter, "destroy", GTK_SIGNAL_FUNC (parseaf_unref_factory), fi);
	gtk_signal_connect (filter, "get_attributes", GTK_SIGNAL_FUNC (parseaf_get_attributes), fi);
	gtk_signal_connect (filter, "get_attribute", GTK_SIGNAL_FUNC (parseaf_get_attribute), fi);
	gtk_signal_connect (filter, "set_attribute", GTK_SIGNAL_FUNC (parseaf_set_attribute), fi);
	gtk_signal_connect (filter, "stop", GTK_SIGNAL_FUNC (parseaf_stop), fi);
	gtk_signal_connect (filter, "pause", GTK_SIGNAL_FUNC (parseaf_pause), fi);
	gtk_signal_connect (filter, "run", GTK_SIGNAL_FUNC (parseaf_run), fi);
#if 0
	gtk_signal_connect (filter, "deliver_event", GTK_SIGNAL_FUNC (parseaf_deliver_event), fi);
#endif
	gtk_signal_connect (mp, "can_seek_forward", GTK_SIGNAL_FUNC (parseaf_can_seek), fi);
	gtk_signal_connect (mp, "can_seek_backward", GTK_SIGNAL_FUNC (parseaf_can_seek), fi);
	gtk_signal_connect (mp, "get_duration", GTK_SIGNAL_FUNC (parseaf_get_duration), fi);
	gtk_signal_connect (mp, "get_position", GTK_SIGNAL_FUNC (parseaf_get_position), fi);
	gtk_signal_connect (mp, "set_position", GTK_SIGNAL_FUNC (parseaf_set_position), fi);
	gtk_signal_connect (mp, "get_stop_time", GTK_SIGNAL_FUNC (parseaf_get_stop_time), fi);
	gtk_signal_connect (mp, "set_stop_time", GTK_SIGNAL_FUNC (parseaf_set_stop_time), fi);

	fi->filter = (GMFFilter *) filter;
	fi->timeref = fi->filter->timeref;
	fi->cbid = 0;
	fi->playback_interval.tvSec = 0;
	fi->playback_interval.tvUsec = USEC_PER_SAMPLE;
	fi->presend.tvSec = 0;
	fi->presend.tvUsec = USEC_PER_SAMPLE;
	fi->mysetup = afNewFileSetup();
	fi->tracknum = AF_DEFAULT_TRACK;

	parseaf_get_pipe (fi->filter, GMF_OUT, &default_pipe, fi);
	gtk_object_set (GTK_OBJECT (default_pipe), "autorender", TRUE, NULL);

	impl_ptr = filter;

	return CORBA_Object_duplicate (bonobo_object_corba_objref (BONOBO_OBJECT (filter)), fi->ev);
}

static void parseaf_unref_factory(GtkObject *filter, FilterInfo *fi)
{
  afFreeFileSetup(fi->mysetup);

  CORBA_exception_free(fi->ev);
  g_free(fi);

  oaf_plugin_unuse(filter);
}

static void
parseaf_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi)
{
  const char * const attr_names[] = {
    NULL
  };
  int i;

  for(i = 0; attr_names[i]; i++);

  retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(i);
  for(i = 0; attr_names[i]; i++)
    retval->_buffer[i] = CORBA_string_dup((char *)attr_names[i]);
}

static void
parseaf_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi)
{  
}

static void
parseaf_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, FilterInfo *fi)
{
}

static void
parseaf_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  	GtkObject *apipe;
  	PipeInfo *pi;
  	GtkType ptype = GMF_OUT;

  	switch(pDirection) {
  		case GMF_IN:
    			if(fi->inpipe) {
      				return; /* We can only have one input pipe - any more would be totally confusing */
      			}
    			ptype = gmf_input_pipe_get_type();
    			break;

  		case GMF_OUT:
    			ptype = gmf_output_pipe_get_type();
    			break;
  	}

  	apipe = gtk_object_new(ptype,
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 NULL);
  	g_assert(apipe);

  	pi = g_new0(PipeInfo, 1);

  	pi->fi = fi;
  	pi->apipe = (GMFPipe *)apipe;

  	switch(pDirection) {
  		case GMF_IN:
    			gtk_signal_connect(apipe, "can_process_type", parseaf_in_can_process_type, pi);
    			gtk_signal_connect(apipe, "get_processable_types", parseaf_in_get_processable_types, pi);
    			gtk_signal_connect(apipe, "receive_sample", parseaf_in_receive_sample, pi);
    			gtk_signal_connect(apipe, "connect", parseaf_in_connect, pi);
    			gtk_signal_connect(apipe, "disconnect", parseaf_in_disconnect, pi);
    			gtk_signal_connect(apipe, "fixup_media_format", parseaf_in_fixup, pi);
    			fi->inpipe = (GMFPipe *)apipe;
    			break;
    			
  		case GMF_OUT:
    			gtk_signal_connect(apipe, "can_process_type", parseaf_out_can_process_type, pi);
    			gtk_signal_connect(apipe, "get_processable_types", parseaf_out_get_processable_types, pi);
    			gtk_signal_connect(apipe, "connect", parseaf_out_connect, pi);
    			gtk_signal_connect(apipe, "fixup_media_format", parseaf_out_fixup, pi);
    			gtk_signal_connect(apipe, "quality_notify", parseaf_out_qos, pi);
    			break;
  	}

  	gtk_object_set_data(apipe, "PipeInfo", pi);

  	gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  	*out_pipe = (GMFPipe *)apipe;
}

static void
parseaf_stop(GMFFilter *filter, FilterInfo *fi)
{
  GMF_MediaPosition_Value tmpval;

  parseaf_pause(filter, fi);

  fi->last_sample.tvSec = fi->last_sample.tvUsec = 0;
  tmpval._d = GMF_MediaPosition_TIME;
  tmpval._u.value_time.tvSec =
    tmpval._u.value_time.tvUsec = 0;

  parseaf_set_position(NULL, &tmpval, fi);
}

static void
parseaf_pause(GMFFilter *filter, FilterInfo *fi)
{
  if(fi->cbid > 0) {
    gmf_time_reference_remove_request(fi->timeref, fi->cbid);
    fi->cbid = 0;
  }

  fi->isDiscontinuity = TRUE;
}

static long
parseaf_time_to_samples(FilterInfo *fi, GMF_TimeVal tv)
{
	gdouble retval;

	retval = ((double)tv.tvSec) + ((double)tv.tvUsec)/1000000.0;

	retval *= afGetRate (fi->file_handle, fi->tracknum);

	return (long)retval;
}

static GMF_TimeVal
parseaf_samples_to_time(FilterInfo *fi, long nsamples)
{
	GMF_TimeVal tv;
	gdouble interval, ip;

	interval = ((double)nsamples) / afGetRate (fi->file_handle, fi->tracknum);
    
	tv.tvUsec = (modf(interval, &ip) * 1000000.0);
	tv.tvSec = floor(ip);

	return tv;
}

static gboolean parseaf_play(FilterInfo *fi)
{
  GMF_Sample samp;
  long nsamples, datalen, out_samples;
  int sample_size;
  void *track;
  gboolean do_eos = FALSE;

  g_return_val_if_fail(fi->filter->state == GMF_Filter_RUNNING, FALSE);

  samp.sInfo.streamStartTime = fi->last_sample;
  samp.sInfo.streamEndTime = gmf_time_add(samp.sInfo.streamStartTime, fi->playback_interval);
  fi->last_sample = samp.sInfo.streamEndTime;

  samp.sInfo.mediaStartTime.tvUsec = afTellFrame(fi->file_handle, fi->tracknum);
  samp.sInfo.mediaStartTime.tvSec = samp.sInfo.mediaEndTime.tvSec = 0;
  samp.sInfo.isSyncPoint = CORBA_TRUE;
  samp.sInfo.isPreroll = CORBA_FALSE;
  samp.sInfo.isDiscontinuity = fi->isDiscontinuity; fi->isDiscontinuity = FALSE;

  CORBA_sequence_set_release(&samp.sData, CORBA_FALSE);
  nsamples = parseaf_time_to_samples(fi, fi->playback_interval);
  sample_size = afGetFrameSize(fi->file_handle, fi->tracknum, TRUE);

  datalen = sample_size * nsamples;

  samp.sData._buffer = track = alloca(datalen);
  out_samples = afReadFrames(fi->file_handle, fi->tracknum, track, nsamples);
  if(out_samples < nsamples) {
    datalen = sample_size * out_samples;
    samp.sInfo.streamEndTime = gmf_time_add(samp.sInfo.streamStartTime, parseaf_samples_to_time(fi, out_samples));
    do_eos = TRUE;
  }

  samp.sInfo.actualDataLength = samp.sData._length = datalen;

  samp.sInfo.mediaEndTime.tvUsec = afTellFrame(fi->file_handle, fi->tracknum);

  g_list_foreach(fi->filter->outputPipes, (GFunc)gmf_output_pipe_send_sample, &samp);

  {
    GMF_TimeVal nextplay;

    if((samp.sInfo.mediaEndTime.tvUsec >= afGetFrameCount(fi->file_handle, fi->tracknum))
       || (fi->stopTime.tvUsec && fi->stopTime.tvSec && (samp.sInfo.mediaEndTime.tvUsec >= fi->stopframe)))
      do_eos = TRUE;
    else {
      nextplay = gmf_time_add(fi->last_sample, fi->presend);
      fi->cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase, &nextplay,
						  (GSourceFunc)parseaf_play, fi);
    }
  }

  if(do_eos)
    g_list_foreach(fi->filter->outputPipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &samp.sInfo.streamEndTime);

  return FALSE;
}

static void
parseaf_run(GMFFilter *filter, FilterInfo *fi)
{
	parseaf_pause (filter, fi);

	if (fi->file_handle != NULL) {
    		fi->cbid = gmf_time_reference_request_alarm (fi->timeref, &fi->filter->timeBase,
							     &fi->last_sample, (GSourceFunc)parseaf_play, fi);
	}
}

static void
parseaf_deliver_event(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData, FilterInfo *fi)
{
}

static void
parseaf_can_seek(GMFMediaPosition *mp, gboolean *retval, FilterInfo *fi)
{
  *retval = TRUE;
}

static void
parseaf_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
	double interval;
  	double ip;

  	g_return_if_fail (fi->file_handle);

  	switch(posType) {
  		case GMF_MediaPosition_SAMPLES:
    			value->_d = posType;
    			value->_u.value_samples = afGetFrameCount(fi->file_handle, fi->tracknum);
    			break;
    			
  		case GMF_MediaPosition_TIME:
  		default:
    			value->_d = GMF_MediaPosition_TIME;
    			interval = ((double)afGetFrameCount(fi->file_handle, fi->tracknum))/afGetRate(fi->file_handle, fi->tracknum);
    
    			value->_u.value_time.tvUsec = (modf(interval, &ip) * 1000000.0);
    			value->_u.value_time.tvSec = floor(ip);
    			break;
  	}
}

static void
parseaf_get_position (GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
	double interval;
	double ip;

	g_return_if_fail (fi->file_handle);

	switch(posType) {
		case GMF_MediaPosition_SAMPLES:
			value->_d = posType;
			value->_u.value_samples = afTellFrame(fi->file_handle, fi->tracknum);
			break;
			
		case GMF_MediaPosition_TIME:
		default:
			value->_d = GMF_MediaPosition_TIME;
			interval = ((double)afTellFrame(fi->file_handle, fi->tracknum)) / afGetRate (fi->file_handle, fi->tracknum);
			value->_u.value_time.tvUsec = (modf(interval, &ip) * 1000000.0);
			value->_u.value_time.tvSec = floor(ip);
			break;
	}
}

static void
parseaf_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
	GMF_TimeVal ttmp;
	gdouble interval;
	int rv;

	switch(value->_d) {
		case GMF_MediaPosition_SAMPLES:
			rv = afSeekFrame (fi->file_handle, fi->tracknum, value->_u.value_samples);
			break;
		
  		case GMF_MediaPosition_TIME:
    			ttmp = value->_u.value_time;
    			interval = ((gdouble)ttmp.tvSec) + ((gdouble)ttmp.tvUsec)/1000000.0;
    			rv = afSeekFrame(fi->file_handle, fi->tracknum, interval * afGetRate(fi->file_handle, fi->tracknum));
    			break;
    			
    		default:
    			break;
	}

	if (rv == -1) {
    		g_list_foreach (fi->filter->outputPipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &fi->stopTime);
    	}

	fi->isDiscontinuity = TRUE;
}

static void
parseaf_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  if(!(fi->stopTime.tvSec || fi->stopTime.tvUsec)) {
    parseaf_get_duration(mp, posType, value, fi);
    return;
  }
    
  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    {
      gdouble interval;

      value->_d = posType;
      interval = ((gdouble)fi->stopTime.tvSec) + ((gdouble)fi->stopTime.tvUsec)/1000000.0;
      value->_u.value_samples = interval * afGetRate(fi->file_handle, fi->tracknum);
    }
    break;
  case GMF_MediaPosition_TIME:
  default:
    value->_d = GMF_MediaPosition_TIME;
    value->_u.value_time = fi->stopTime;
    break;
  }
}

static void
parseaf_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  switch(value->_d) {
  case GMF_MediaPosition_SAMPLES:
    {
      gdouble interval, ip;

      interval = afGetRate(fi->file_handle, fi->tracknum) * ((gdouble)value->_u.value_samples);

      fi->stopTime.tvUsec = (modf(interval, &ip) * 1000000.0);
      fi->stopTime.tvSec = floor(ip);
      fi->stopframe = value->_u.value_samples;
    }
    break;
  case GMF_MediaPosition_TIME:
    value->_d = GMF_MediaPosition_TIME;
    fi->stopTime = value->_u.value_time;
    fi->stopframe = parseaf_time_to_samples(fi, fi->stopTime);
    break;
  default:
    g_assert_not_reached();
    break;
  }
}

static void
parseaf_in_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			    gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_DATA)
    return;

  if(strcmp(type_info->minorType, "audio/basic")
     && strcmp(type_info->minorType, "audio/x-ulaw")
     && strcmp(type_info->minorType, "audio/x-wav"))
    return;

  *retval = TRUE;
}

static void
parseaf_in_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
	GMF_MediaTypeInfoList *list;
	GMF_MediaTypeInfo *ti;
	int i;

	list = GMF_MediaTypeInfoList__alloc();
	list->_length = 4;
	list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

	i = 0;

	ti = &list->_buffer[i++];

	// audio/basic
	{
		memset (ti, 0, sizeof(*ti));
		ti->majorType = GMF_MEDIA_DATA;
		ti->minorType = CORBA_string_dup("audio/basic");
		ti->typeData._type = (CORBA_TypeCode) CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->typeData._value = NULL;
		CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
		ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->formatData._value = NULL;
		CORBA_any_set_release(&ti->formatData, CORBA_FALSE);
	}

	// audio/x-wav
	ti = &list->_buffer[i++];
	{
		memset(ti, 0, sizeof(*ti));
		ti->majorType = GMF_MEDIA_DATA;
		ti->minorType = CORBA_string_dup("audio/x-wav");
		ti->typeData._type = (CORBA_TypeCode) CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->typeData._value = NULL;
		CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
		ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->formatData._value = NULL;
		CORBA_any_set_release(&ti->formatData, CORBA_FALSE);
	}

	// audio/x-riff
	ti = &list->_buffer[i++];
	{
		memset(ti, 0, sizeof(*ti));
		ti->majorType = GMF_MEDIA_DATA;
		ti->minorType = CORBA_string_dup("audio/x-riff");
		ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate ((CORBA_Object) TC_null, pi->fi->ev);
		ti->typeData._value = NULL;
		CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
		ti->formatData._type = (CORBA_TypeCode) CORBA_Object_duplicate ((CORBA_Object) TC_null, pi->fi->ev);
		ti->formatData._value = NULL;
		CORBA_any_set_release (&ti->formatData, CORBA_FALSE);
	}

	// audio/x-ulaw
	ti = &list->_buffer[i++];
	{
		memset(ti, 0, sizeof(*ti));
		ti->majorType = GMF_MEDIA_DATA;
		ti->minorType = CORBA_string_dup("audio/x-ulaw");
		ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->typeData._value = NULL;
		CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
		ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->formatData._value = NULL;
		CORBA_any_set_release(&ti->formatData, CORBA_FALSE);
  	}

  	*out_typelist = list;
}

static void
parseaf_in_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
	pi->fi->cobj = Bonobo_Unknown_queryInterface(cnxinfo->connectedTo, "IDL:GMF/DataReader:1.0", pi->fi->ev);
  	if(pi->fi->ev->_major != CORBA_NO_EXCEPTION) {
  		pi->fi->cobj = CORBA_OBJECT_NIL;
  	}

  	parseaf_read_af_header(pi->fi);
}

static void
parseaf_in_disconnect(GMFPipe *apipe, PipeInfo *pi)
{
	afCloseFile (pi->fi->file_handle); 
	pi->fi->file_handle = NULL;

	/* FIXME we should unref the object here */
	CORBA_Object_release (pi->fi->cobj, pi->fi->ev); pi->fi->cobj = CORBA_OBJECT_NIL;
}

static void
parseaf_in_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
	switch(other_end) {
		case GMF_Pipe_CANNOT_HANDLE:
			/* Second opinion? */
			*retval = GMF_Pipe_CANNOT_HANDLE;
			break;
			
  		case GMF_Pipe_UNKNOWN:
			/* Fill in the blanks */
			*retval = GMF_Pipe_ACCEPTED;
			break;
			
		case GMF_Pipe_TRY_MINE:
			/* Can we handle this? I guess so. */
			*retval = GMF_Pipe_ACCEPTED;
			break;
			
		case GMF_Pipe_ACCEPTED:
			*retval = GMF_Pipe_ACCEPTED;
			break;
	}
}

static void
parseaf_in_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi)
{
  /* And what do I do with this? */
  g_warning("We got receive_sample in a parse filter. This makes no sense.");
}

static int
parseaf_vfile_read(AF_VirtualFile *source, unsigned char *data, int nbytes)
{
	GMF_Data *out;
	int retval;
	FilterInfo *fi = source->closure;

	GMF_DataReader_read (fi->cobj, nbytes, &out, fi->ev);
	if(fi->ev->_major == CORBA_NO_EXCEPTION) {
		memcpy(data, out->_buffer, MIN(nbytes, out->_length));
		retval = MIN(nbytes, out->_length);
		CORBA_free(out);
	} else {
		retval = 0;
	}
	
	return retval;
}

static long
parseaf_vfile_length(AF_VirtualFile *source)
{
  long retval;
  FilterInfo *fi = source->closure;

  retval = GMF_DataReader__get_length(fi->cobj, fi->ev);

  if(fi->ev->_major != CORBA_NO_EXCEPTION)
    retval = 0;

  return retval;
}

static long
parseaf_vfile_seek(AF_VirtualFile *source, long offset, int is_relative)
{
  FilterInfo *fi = source->closure;
  return GMF_DataReader_set_position(fi->cobj, offset, is_relative?GMF_DataReader_RELATIVE:GMF_DataReader_ABSOLUTE, fi->ev);
}

static long
parseaf_vfile_tell(AF_VirtualFile *source)
{
  FilterInfo *fi = source->closure;
  return GMF_DataReader_get_position(fi->cobj, fi->ev);
}

static gint
parseaf_read_af_header (FilterInfo *fi)
{
	gint retval = -1;
	AF_VirtualFile *vf;

	if (fi->file_handle != NULL) {
		return 0;
	}

	if (CORBA_Object_is_nil (fi->cobj, fi->ev) || fi->ev->_major != CORBA_NO_EXCEPTION) {
    		goto out;
    	}

	vf = af_virtual_file_new();

	vf->read = parseaf_vfile_read;
	vf->length = parseaf_vfile_length;
	vf->seek = parseaf_vfile_seek;
	vf->tell = parseaf_vfile_tell;
	vf->closure = fi;

	g_message ("parseaf_read_af_header");
	fi->file_handle = afOpenVirtualFile (vf, "r", fi->mysetup);	
	if(fi->file_handle != NULL) {
		int m, n;
		fi->tracknum = AF_DEFAULT_TRACK;
		fi->adf.sample_rate = afGetRate(fi->file_handle, fi->tracknum);
		fi->adf.nchannels = afGetChannels(fi->file_handle, fi->tracknum);
		afGetSampleFormat(fi->file_handle, fi->tracknum, &m, &n);
		fi->adf.nbits = n;
		afSetVirtualByteOrder(fi->file_handle, fi->tracknum, AF_BYTEORDER_LITTLEENDIAN);
		fi->adf.byte_order_be = CORBA_FALSE;
  	} else {
  		g_message ("parseaf_read_af_header: Open failed!!!");
  	}

	retval = 0;

	out:
		return retval;
}

static void
parseaf_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_AUDIO_DIGITAL)
    return;

  if(strcmp(type_info->minorType, "audio/raw"))
    return;

  *retval = TRUE;
}

static void
parseaf_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  ti = &list->_buffer[0];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_AUDIO_DIGITAL;
    ti->minorType = CORBA_string_dup("audio/raw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  *out_typelist = list;
}

static void
parseaf_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
}

static GMF_Pipe_FormatAcceptance
parseaf_out_can_handle(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  if(CORBA_TypeCode_equal(typeinfo->formatData._type, (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat, pi->fi->ev)) {
    GMF_Media_AudioDigitalFormat *adf;
    
    adf = typeinfo->formatData._value;
    
    if((adf->sample_rate == pi->fi->adf.sample_rate)
       && (adf->nchannels == pi->fi->adf.nchannels)
       && (adf->nbits == (pi->fi->adf.nbits))
       && (adf->byte_order_be == CORBA_FALSE))
      return GMF_Pipe_ACCEPTED;
    else
     return GMF_Pipe_CANNOT_HANDLE;
  } else
    return GMF_Pipe_CANNOT_HANDLE;
}

static void
parseaf_out_fill(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  GMF_Media_AudioDigitalFormat *adf;

  if(CORBA_any_get_release(&typeinfo->formatData))
    CORBA_free(typeinfo->formatData._value);
  CORBA_Object_release((CORBA_Object)typeinfo->formatData._type, pi->fi->ev);
  typeinfo->formatData._type = (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat;
  adf = typeinfo->formatData._value = GMF_Media_AudioDigitalFormat__alloc();
  CORBA_any_set_release(&typeinfo->formatData, CORBA_TRUE);

 *adf = pi->fi->adf;
}

static void
parseaf_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		  GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
	if(pi->fi->file_handle == NULL) {
		*retval = GMF_Pipe_CANNOT_HANDLE;
		return;
	}

  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    parseaf_out_fill(typeinfo, pi);
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? */
    if(parseaf_out_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else
      *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_ACCEPTED:
    /* Can we handle this? */
    if(parseaf_out_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else {
      parseaf_out_fill(typeinfo, pi);
      *retval = GMF_Pipe_TRY_MINE;
    }
    break;
  }
}

static void
parseaf_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi)
{
  g_return_if_fail(pi->fi->filter->state == GMF_Filter_RUNNING);

  if(gmf_time_compare(pi->fi->presend, qosinfo->lag) < 0) {
    pi->fi->presend = qosinfo->lag;
  } else {
    GList *ltmp;
    GMF_TimeVal ttmp = qosinfo->lag;

    for(ltmp = pi->fi->filter->outputPipes; ltmp; ltmp = g_list_next(ltmp)) {
      if(gmf_time_compare(ttmp, ((GMFOutputPipe *)(ltmp->data))->last_qosinfo.lag) < 0)
	ttmp = ((GMFOutputPipe *)(ltmp->data))->last_qosinfo.lag;
    }

    pi->fi->presend = ttmp;
  }

  g_message("presend at %g is %g",
	    ((double)pi->fi->last_sample.tvSec) + ((double)pi->fi->last_sample.tvUsec)/1000000.0,
	    ((double)pi->fi->presend.tvSec) + ((double)pi->fi->presend.tvUsec)/1000000.0);

  /* For now we just ignore rate_proportion */

#if 0
  if(gmf_time_compare(pi->fi->presend, zero) > 0) {
    gdouble sample_size, interval;
    /* Try and get into the green again */

    interval = ((gdouble)pi->fi->playback_interval.tvSec) + ((gdouble)pi->fi->playback_interval.tvUsec)/1000000.0;

    sample_size = (interval * (gdouble)pi->fi->au_header.sample_rate * (gdouble)pi->fi->au_header.in_bytes_per_sample);

    pi->fi->au_header.data_position += (sample_size * 3);

    pi->fi->last_sample = gmf_time_add(pi->fi->last_sample, pi->fi->playback_interval);
    pi->fi->last_sample = gmf_time_add(pi->fi->last_sample, pi->fi->playback_interval);
    pi->fi->last_sample = gmf_time_add(pi->fi->last_sample, pi->fi->playback_interval);
  }
#endif
}
