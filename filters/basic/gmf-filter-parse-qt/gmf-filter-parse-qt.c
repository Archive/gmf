/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#define G_LOG_DOMAIN "parseqt"

#include <gmf.h>
#include <libgmf/gmf-digital-audio.h>
#include <libgmf/gmf-digital-video.h>
#include <limits.h>
#include <endian.h>
#include <math.h>
#include <sched.h>

#include <quicktime/quicktime.h>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netinet/in.h> /* byte conversion */

#define AU_HEADER_LEN 24
#define AU_SIZE_UNKNOWN 0xFFFFFFFF

typedef struct {
  CORBA_Environment *ev, evdat;
  CORBA_ORB orb;

  GMFFilter *filter;
  GMFTimeReference *timeref;

  GMFPipe *inpipe;
  GMF_DataReader cobj;

  gboolean read_qt_header;

  struct {
    gboolean out;
    GMF_TimeVal playback_interval;
    GMF_TimeVal presend;
    GMF_TimeVal last_sample;
    GMF_Callback_CallbackID cbid;
    GList *pipes;

    GMF_Media_AudioDigitalFormat adf;
  } audio;

  struct {
    gboolean out;
    GMF_TimeVal frame_interval;
    GMF_TimeVal presend;
    GMF_TimeVal last_sample;
    GMF_Callback_CallbackID cbid;
    GList *pipes;

    GMF_Media_VideoDigitalFormat vdf;
    gdouble fps;
  } video;

  GMF_TimeVal stopTime;

  gboolean isDiscontinuity;
  quicktime_t *qth;
} FilterInfo;

typedef struct {
  FilterInfo *fi;

  GMFPipe *apipe;
  gboolean is_video; /* FALSE means is_audio */
  gboolean is_auto;
} PipeInfo;

static void parseqt_unref_factory(GtkObject *filter, FilterInfo *fi);
static void parseqt_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void parseqt_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi);
static void parseqt_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi);
static void parseqt_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, FilterInfo *fi);
static gint parseqt_read_qt_header(FilterInfo *fi);
static void parseqt_stop(GMFFilter *filter, FilterInfo *fi);
static void parseqt_pause(GMFFilter *filter, FilterInfo *fi);
static void parseqt_run(GMFFilter *filter, FilterInfo *fi);
static void parseqt_deliver_event(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName,
				  CORBA_any *evData, FilterInfo *fi) G_GNUC_UNUSED;
static void parseqt_can_seek(GMFMediaPosition *mp, gboolean *retval, FilterInfo *fi);
static void parseqt_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseqt_get_position(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseqt_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseqt_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseqt_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);

static void parseqt_in_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void parseqt_in_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void parseqt_in_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi);
static void parseqt_in_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void parseqt_in_disconnect(GMFPipe *apipe, PipeInfo *pi);
static void parseqt_in_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			     GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);

static void parseqt_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void parseqt_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void parseqt_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void parseqt_out_disconnect(GMFPipe *apipe, PipeInfo *pi);
static void parseqt_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			      GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);
static void parseqt_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi);
static gboolean parseqt_play_audio(FilterInfo *fi);
static gboolean parseqt_play_video(FilterInfo *fi);
static long parseqt_time_to_frames(FilterInfo *fi, GMF_TimeVal *tval);
static long parseqt_time_to_samples(FilterInfo *fi, GMF_TimeVal *tval);
static GMF_TimeVal parseqt_frames_to_time(FilterInfo *fi, long nframes);
static GMF_TimeVal parseqt_samples_to_time(FilterInfo *fi, long nsamples);
static CORBA_Object parseqt_create_filter(PortableServer_POA poa,
					  const char *iid,
					  gpointer impl_ptr,
					  CORBA_Environment *ev);


static const char filter_iid[] = "OAFIID:gmf-filter-parse-qt:d4fe2618-bc07-44b4-8d85-2724497876a5";

static const OAFPluginObject plugin_list[] = {
	{
  		filter_iid,
		parseqt_create_filter
	},
	
  	{
  		NULL,
  		NULL
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"QuickTime File Filter Stuff"
};


static CORBA_Object
parseqt_create_filter(PortableServer_POA poa,
		      const char *iid,
		      gpointer impl_ptr,
		      CORBA_Environment *ev)
{
  GtkObject *filter, *mp;
  FilterInfo *fi;

  if(strcmp(iid, filter_iid))
    return CORBA_OBJECT_NIL;

  filter = gtk_object_new(gmf_filter_get_type(), "filter_type", GMF_Filter_PARSER, NULL);
  mp = gtk_object_new(gmf_media_position_get_type(), NULL);
  oaf_plugin_unuse(filter);
  bonobo_object_add_interface(BONOBO_OBJECT(filter), BONOBO_OBJECT(mp));
  g_assert(filter && mp);

  fi = g_new0(FilterInfo, 1);
  fi->ev = &fi->evdat;
  CORBA_exception_init(fi->ev);
  gtk_signal_connect(filter, "get_pipe", GTK_SIGNAL_FUNC(parseqt_get_pipe), fi);
  gtk_signal_connect(filter, "destroy", GTK_SIGNAL_FUNC(parseqt_unref_factory), fi);
  gtk_signal_connect(filter, "get_attributes", GTK_SIGNAL_FUNC(parseqt_get_attributes), fi);
  gtk_signal_connect(filter, "get_attribute", GTK_SIGNAL_FUNC(parseqt_get_attribute), fi);
  gtk_signal_connect(filter, "set_attribute", GTK_SIGNAL_FUNC(parseqt_set_attribute), fi);
  gtk_signal_connect(filter, "stop", GTK_SIGNAL_FUNC(parseqt_stop), fi);
  gtk_signal_connect(filter, "pause", GTK_SIGNAL_FUNC(parseqt_pause), fi);
  gtk_signal_connect(filter, "run", GTK_SIGNAL_FUNC(parseqt_run), fi);

  gtk_signal_connect(mp, "can_seek_forward", GTK_SIGNAL_FUNC(parseqt_can_seek), fi);
  gtk_signal_connect(mp, "can_seek_backward", GTK_SIGNAL_FUNC(parseqt_can_seek), fi);
  gtk_signal_connect(mp, "get_duration", GTK_SIGNAL_FUNC(parseqt_get_duration), fi);
  gtk_signal_connect(mp, "get_position", GTK_SIGNAL_FUNC(parseqt_get_position), fi);
  gtk_signal_connect(mp, "set_position", GTK_SIGNAL_FUNC(parseqt_set_position), fi);
  gtk_signal_connect(mp, "get_stop_time", GTK_SIGNAL_FUNC(parseqt_get_stop_time), fi);
  gtk_signal_connect(mp, "set_stop_time", GTK_SIGNAL_FUNC(parseqt_set_stop_time), fi);

  fi->filter = (GMFFilter *)filter;
  fi->timeref = fi->filter->timeref;
  fi->audio.cbid = fi->video.cbid = 0;
  fi->audio.playback_interval.tvSec = 0;
  fi->audio.playback_interval.tvUsec = 100000;
  fi->read_qt_header = FALSE;

  {
    GMFPipe *default_pipe;
    PipeInfo *pi;

    parseqt_get_pipe(fi->filter, GMF_OUT, &default_pipe, fi);
    gtk_object_set(GTK_OBJECT(default_pipe), "autorender", TRUE, NULL);
    pi = gtk_object_get_data(GTK_OBJECT(default_pipe), "PipeInfo");
    pi->is_auto = TRUE;
    pi->is_video = FALSE;

    parseqt_get_pipe(fi->filter, GMF_OUT, &default_pipe, fi);
    gtk_object_set(GTK_OBJECT(default_pipe), "autorender", TRUE, NULL);
    pi = gtk_object_get_data(GTK_OBJECT(default_pipe), "PipeInfo");
    pi->is_auto = TRUE;
    pi->is_video = TRUE;
  }

  impl_ptr = filter;

  return CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(filter)), fi->ev);
}

static void parseqt_unref_factory(GtkObject *filter, FilterInfo *fi)
{
  if(fi->qth)
    quicktime_close(fi->qth);

  CORBA_exception_free(fi->ev);
  g_free(fi);

  oaf_plugin_unuse(filter);
}

static void
parseqt_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi)
{
  const char * const attr_names[] = {
    NULL
  };
  int i;

  for(i = 0; attr_names[i]; i++);

  retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(i);
  for(i = 0; attr_names[i]; i++)
    retval->_buffer[i] = CORBA_string_dup((char *)attr_names[i]);
}

static void
parseqt_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi)
{  
}

static void
parseqt_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, FilterInfo *fi)
{
}

static void
parseqt_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  GtkObject *apipe;
  PipeInfo *pi;
  GtkType ptype = GMF_OUT;

  switch(pDirection) {
  case GMF_IN:
    if(fi->inpipe)
      return; /* We can only have one input pipe - any more would be totally confusing */
    ptype = gmf_input_pipe_get_type();
    break;
  case GMF_OUT:
    ptype = gmf_output_pipe_get_type();
    break;
  }

  apipe = gtk_object_new(ptype,
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 NULL);
  g_assert(apipe);

  pi = g_new0(PipeInfo, 1);

  pi->fi = fi;
  pi->apipe = (GMFPipe *)apipe;

  switch(pDirection) {
  case GMF_IN:
    gtk_signal_connect(apipe, "can_process_type", parseqt_in_can_process_type, pi);
    gtk_signal_connect(apipe, "get_processable_types", parseqt_in_get_processable_types, pi);
    gtk_signal_connect(apipe, "receive_sample", parseqt_in_receive_sample, pi);
    gtk_signal_connect(apipe, "connect", parseqt_in_connect, pi);
    gtk_signal_connect(apipe, "disconnect", parseqt_in_disconnect, pi);
    gtk_signal_connect(apipe, "fixup_media_format", parseqt_in_fixup, pi);
    fi->inpipe = (GMFPipe *)apipe;
    break;
  case GMF_OUT:
    gtk_signal_connect(apipe, "can_process_type", parseqt_out_can_process_type, pi);
    gtk_signal_connect(apipe, "get_processable_types", parseqt_out_get_processable_types, pi);
    gtk_signal_connect(apipe, "connect", parseqt_out_connect, pi);
    gtk_signal_connect(apipe, "disconnect", parseqt_out_disconnect, pi);
    gtk_signal_connect(apipe, "fixup_media_format", parseqt_out_fixup, pi);
    gtk_signal_connect(apipe, "quality_notify", parseqt_out_qos, pi);
    break;
  }

  gtk_object_set_data(apipe, "PipeInfo", pi);

  gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  *out_pipe = (GMFPipe *)apipe;
}

static void
parseqt_stop(GMFFilter *filter, FilterInfo *fi)
{
  parseqt_pause(filter, fi);

  fi->audio.last_sample.tvSec = fi->audio.last_sample.tvUsec =
    fi->video.last_sample.tvSec = fi->video.last_sample.tvUsec = 0;
}

static void
parseqt_pause(GMFFilter *filter, FilterInfo *fi)
{
  if(fi->audio.cbid > 0) {
    gmf_time_reference_remove_request(fi->timeref, fi->audio.cbid);
    fi->audio.cbid = 0;
  }

  if(fi->video.cbid > 0) {
    gmf_time_reference_remove_request(fi->timeref, fi->video.cbid);
    fi->video.cbid = 0;
  }

  fi->isDiscontinuity = TRUE;
}

static gboolean parseqt_play_audio(FilterInfo *fi)
{
  GMF_Sample samp;
  long nsamples, datalen, wholelen;
  QUICKTIME_INT16 *track;
  unsigned char *merged_buffer, *bufptr;
  int i;
  int sample_size;

  samp.sInfo.streamStartTime = fi->audio.last_sample;
  samp.sInfo.streamEndTime = gmf_time_add(samp.sInfo.streamStartTime, fi->audio.playback_interval);
  fi->audio.last_sample = samp.sInfo.streamEndTime;

  samp.sInfo.mediaStartTime.tvUsec = quicktime_audio_position(fi->qth, 0);
  samp.sInfo.mediaStartTime.tvSec = samp.sInfo.mediaEndTime.tvSec = 0;
  samp.sInfo.isSyncPoint = CORBA_TRUE;
  samp.sInfo.isPreroll = CORBA_FALSE;
  samp.sInfo.isDiscontinuity = fi->isDiscontinuity; fi->isDiscontinuity = FALSE;

  CORBA_sequence_set_release(&samp.sData, CORBA_FALSE);
  nsamples = parseqt_time_to_samples(fi, &fi->audio.playback_interval);
  sample_size = (fi->audio.adf.nbits / 8);
  datalen = nsamples * sizeof(QUICKTIME_INT16);

  track = alloca(datalen);
  if(quicktime_decode_audio(fi->qth, track, NULL, nsamples, 0))
    goto out;

  switch(sample_size) {
  case 2:
    samp.sData._buffer = (CORBA_octet *)track;
    wholelen = datalen;
    break;
  case 1:
    wholelen = nsamples * (fi->audio.adf.nbits / 8) /* * fi->audio.adf.nchannels */;
    samp.sData._buffer = merged_buffer = alloca(wholelen);
    for(i = 0, bufptr = merged_buffer; i < nsamples; i++) {
      *bufptr = track[i] >> 8;
    }
    break;
  default:
    g_assert_not_reached();
    break;
  }

  samp.sInfo.actualDataLength = samp.sData._length = wholelen;

  samp.sInfo.mediaEndTime.tvUsec = quicktime_audio_position(fi->qth, 0);

  g_list_foreach(fi->audio.pipes, (GFunc)gmf_output_pipe_send_sample, &samp);

  {
    GMF_TimeVal nextplay;

    if(samp.sInfo.mediaEndTime.tvUsec >= quicktime_audio_length(fi->qth, 0))
      goto out;

    nextplay = gmf_time_add(fi->audio.last_sample, fi->audio.presend);
    fi->audio.cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase, &nextplay,
						      (GSourceFunc)parseqt_play_audio, fi);
  }

  return FALSE;

 out:
  g_list_foreach(fi->audio.pipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &samp.sInfo.streamEndTime);
  return FALSE;
}

static gboolean parseqt_play_video(FilterInfo *fi)
{
  GMF_Sample samp;
  long wholelen;
  int i;
  guchar **rowptrs;

  samp.sInfo.streamStartTime = fi->video.last_sample;
  samp.sInfo.streamEndTime = gmf_time_add(samp.sInfo.streamStartTime, fi->video.frame_interval);

  if(fi->filter->state == GMF_Filter_RUNNING)
    fi->video.last_sample = samp.sInfo.streamEndTime;

  samp.sInfo.mediaStartTime.tvUsec = quicktime_video_position(fi->qth, 0);
  samp.sInfo.mediaStartTime.tvSec = samp.sInfo.mediaEndTime.tvSec = 0;
  samp.sInfo.isSyncPoint = CORBA_TRUE;
  samp.sInfo.isPreroll = CORBA_FALSE;
  samp.sInfo.isDiscontinuity = fi->isDiscontinuity; fi->isDiscontinuity = FALSE;

  wholelen = fi->video.vdf.frame_width * fi->video.vdf.frame_height * 3;
  samp.sData._buffer = alloca(wholelen);
  CORBA_sequence_set_release(&samp.sData, CORBA_FALSE);
  rowptrs = alloca(fi->video.vdf.frame_height * sizeof(gpointer));
  for(i = 0; i < fi->video.vdf.frame_height; i++)
    rowptrs[i] = &samp.sData._buffer[i * fi->video.vdf.frame_width * 3];

  if(quicktime_decode_video(fi->qth, rowptrs, 0))
    goto out;

  samp.sInfo.actualDataLength = wholelen;
  samp.sData._length = wholelen;

  samp.sInfo.mediaEndTime.tvUsec = quicktime_video_position(fi->qth, 0);

  g_list_foreach(fi->video.pipes, (GFunc)gmf_output_pipe_send_sample, &samp);


  if(fi->filter->state == GMF_Filter_RUNNING)
    {
      GMF_TimeVal nextplay;

      if(samp.sInfo.mediaStartTime.tvUsec >= quicktime_video_length(fi->qth, 0))
	goto out;

      nextplay = gmf_time_add(fi->video.last_sample, fi->video.presend);
      fi->video.cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase, &nextplay,
							(GSourceFunc)parseqt_play_video, fi);
    }
  else
    {
      quicktime_set_video_position(fi->qth, samp.sInfo.mediaStartTime.tvUsec, 0);
    }

  return FALSE;

 out:
  if(fi->filter->state == GMF_Filter_RUNNING)
    g_list_foreach(fi->video.pipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &samp.sInfo.streamEndTime);

  return FALSE;
}

static void
parseqt_run(GMFFilter *filter, FilterInfo *fi)
{
  parseqt_pause(filter, fi);

  if(fi->read_qt_header) {
    if(fi->audio.out)
      fi->audio.cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase,
							&fi->audio.last_sample, (GSourceFunc)parseqt_play_audio, fi);
    if(fi->video.out)
      fi->video.cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase,
							&fi->video.last_sample, (GSourceFunc)parseqt_play_video, fi);
  }
}

static void
parseqt_deliver_event(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData, FilterInfo *fi)
{
}

static void
parseqt_can_seek(GMFMediaPosition *mp, gboolean *retval, FilterInfo *fi)
{
  *retval = TRUE;
}

static GMF_TimeVal
parseqt_frames_to_time(FilterInfo *fi, long nframes)
{
  GMF_TimeVal retval;
  gdouble interval, ip;

  interval = ((gdouble)nframes)/fi->video.fps;

  retval.tvUsec = modf(interval, &ip) * 1000000.0;
  retval.tvSec = floor(ip);

  return retval;
}

static GMF_TimeVal
parseqt_samples_to_time(FilterInfo *fi, long nsamples)
{
  GMF_TimeVal retval;
  gdouble interval, ip;

  interval = ((gdouble)nsamples)/fi->audio.adf.sample_rate;
  retval.tvUsec = modf(interval, &ip) * 1000000.0;
  retval.tvSec = floor(ip);

  return retval;
}

static long
parseqt_time_to_frames(FilterInfo *fi, GMF_TimeVal *tval)
{
  gdouble interval;

  interval = tval->tvSec + (((gdouble)tval->tvUsec) / 1000000.0);

  return interval * fi->video.fps;
}

static long
parseqt_time_to_samples(FilterInfo *fi, GMF_TimeVal *tval)
{
  gdouble interval;

  interval = tval->tvSec + (((gdouble)tval->tvUsec) / 1000000.0);

  return interval * fi->audio.adf.sample_rate;
}

static void
parseqt_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  g_return_if_fail(fi->read_qt_header);

  /* This is icky - we could be returning two different types of sample positions based on what we are outputting... */

  if(fi->video.out) {
    switch(posType) {
    case GMF_MediaPosition_SAMPLES:
      value->_d = GMF_MediaPosition_SAMPLES;
      value->_u.value_samples = quicktime_video_length(fi->qth, 0);
      break;
    case GMF_MediaPosition_TIME:
    default:
      value->_d = GMF_MediaPosition_TIME;
      value->_u.value_time = parseqt_frames_to_time(fi, quicktime_video_length(fi->qth, 0));
      break;
    }
  } else {
    switch(posType) {
    case GMF_MediaPosition_SAMPLES:
      value->_d = GMF_MediaPosition_SAMPLES;
      value->_u.value_samples = quicktime_audio_length(fi->qth, 0);
      break;
    case GMF_MediaPosition_TIME:
    default:
      {
	value->_d = GMF_MediaPosition_TIME;
	value->_u.value_time = parseqt_samples_to_time(fi, quicktime_audio_length(fi->qth, 0));
      }
      break;
    }
  }
}

static void
parseqt_get_position(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  g_return_if_fail(fi->read_qt_header);

  switch(posType) {
  case GMF_MediaPosition_TIME:
    value->_d = posType;
    if(fi->video.out)
      value->_u.value_time = parseqt_frames_to_time(fi, quicktime_video_position(fi->qth, 0));
    else
      value->_u.value_time = parseqt_samples_to_time(fi, quicktime_audio_position(fi->qth, 0));
    break;
  case GMF_MediaPosition_SAMPLES:
    value->_d = posType;
    if(fi->video.out)
      value->_u.value_samples = quicktime_video_position(fi->qth, 0);
    else
      value->_u.value_samples = quicktime_audio_position(fi->qth, 0);
    break;
  }

}

static void
parseqt_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  long itmp;
  gboolean past_end = FALSE;

  if(fi->video.out) {
    switch(value->_d) {
    case GMF_MediaPosition_TIME:
      itmp = parseqt_time_to_frames(fi, &value->_u.value_time);
      break;
    case GMF_MediaPosition_SAMPLES:
      itmp = value->_u.value_samples;
      break;
    }

    quicktime_set_video_position(fi->qth, itmp, 0);
    past_end = past_end || (itmp > quicktime_video_length(fi->qth, 0));
  }

  if(fi->audio.out) {
    switch(value->_d) {
    case GMF_MediaPosition_TIME:
      itmp = parseqt_time_to_samples(fi, &value->_u.value_time);
      break;
    case GMF_MediaPosition_SAMPLES:
      itmp = value->_u.value_samples;
      break;
    }
    quicktime_set_audio_position(fi->qth, itmp, 0);
    past_end = past_end || (itmp > quicktime_audio_length(fi->qth, 0));
  }

  if(past_end)
    g_list_foreach(fi->filter->outputPipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &fi->stopTime);

  fi->isDiscontinuity = TRUE;

  if(fi->filter->state == GMF_Filter_PAUSED)
    parseqt_play_video(fi);
}

static void
parseqt_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    value->_d = posType;
    value->_u.value_samples = parseqt_time_to_frames(fi, &fi->stopTime);
    break;
  case GMF_MediaPosition_TIME:
    value->_d = posType;
    value->_u.value_time = fi->stopTime;
    break;
  }
}

static void
parseqt_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  switch(value->_d) {
  case GMF_MediaPosition_SAMPLES:
    fi->stopTime = parseqt_frames_to_time(fi, value->_u.value_samples);
    break;
  case GMF_MediaPosition_TIME:
    fi->stopTime = value->_u.value_time;
    break;
  }

  /* XXX TODO - if we're past stopTime, stop things */
}

static void
parseqt_in_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			    gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_DATA)
    return;

  if(strcmp(type_info->minorType, "video/quicktime")
     && strcmp(type_info->minorType, "*"))
    return;

  *retval = TRUE;
}

static void
parseqt_in_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;
  int i;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  i = 0;

  ti = &list->_buffer[i++];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_DATA;
    ti->minorType = CORBA_string_dup("video/quicktime");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->typeData._value = NULL;
    CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._value = NULL;
    CORBA_any_set_release(&ti->formatData, CORBA_FALSE);
  }

  *out_typelist = list;
}

static void
parseqt_in_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
  pi->fi->cobj = Bonobo_Unknown_queryInterface(cnxinfo->connectedTo, "IDL:GMF/DataReader:1.0", pi->fi->ev);
  if(pi->fi->ev->_major != CORBA_NO_EXCEPTION) pi->fi->cobj = CORBA_OBJECT_NIL;

  if(parseqt_read_qt_header(pi->fi))
    g_warning("Header read failed!");
}

static void
parseqt_in_disconnect(GMFPipe *apipe, PipeInfo *pi)
{
  pi->fi->read_qt_header = FALSE;
  CORBA_Object_release(pi->fi->cobj, pi->fi->ev); pi->fi->cobj = CORBA_OBJECT_NIL;
}

static void
parseqt_in_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? I guess so. */
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_ACCEPTED:
    *retval = GMF_Pipe_ACCEPTED;
    break;
  }
}

static void
parseqt_in_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi)
{
  /* And what do I do with this? */
  g_warning("We got receive_sample in a parse filter. This makes no sense.");
}

#if 0
static int
parseqt_src_read(quicktime_t *source, unsigned char *data, int nbytes, FilterInfo *fi)
{
  GMF_Data *out;
  int retval;
  
  GMF_DataReader_read(fi->cobj, nbytes, &out, fi->ev);
  if(fi->ev->_major == CORBA_NO_EXCEPTION) {
    memcpy(data, out->_buffer, MIN(nbytes, out->_length));
    retval = MIN(nbytes, out->_length);
    CORBA_free(out);
  } else
    retval = 0;
    
  return retval;
}

static long
parseqt_src_length(quicktime_t *source, FilterInfo *fi)
{
  long retval;

  retval = GMF_DataReader__get_length(fi->cobj, fi->ev);

  if(fi->ev->_major != CORBA_NO_EXCEPTION)
    retval = 0;

  return retval;
}

static long
parseqt_src_seek(quicktime_t *source, long offset, int is_relative, FilterInfo *fi)
{
  return GMF_DataReader_set_position(fi->cobj, offset, is_relative?GMF_DataReader_RELATIVE:GMF_DataReader_ABSOLUTE, fi->ev);
}

static long
parseqt_src_tell(quicktime_t *source, FilterInfo *fi)
{
  return GMF_DataReader_get_position(fi->cobj, fi->ev);
}
#endif

static gint
parseqt_read_qt_header(FilterInfo *fi)
{
  gint nouts;

  if(fi->read_qt_header)
    return 0;

  if(CORBA_Object_is_nil(fi->cobj, fi->ev) || fi->ev->_major != CORBA_NO_EXCEPTION) {
    g_message("fi->cobj is_nil");
    return -1;
  }


/*
 * FIXME: Find out what API changed...  
  quicktime_t *src;
  src = quicktime_source_new();
  src->closure = fi;
  src->read = (gpointer)&parseqt_src_read;
  src->length = (gpointer)&parseqt_src_length;
  src->seek = (gpointer)&parseqt_src_seek;
  src->tell = (gpointer)&parseqt_src_tell;

  fi->qth = quicktime_open(src, 1, 0);
  */
  if(!fi->qth) {
    g_message("quicktime_open failed");
    return -1;
  }

  nouts = 0;
  fi->audio.out = fi->video.out = FALSE;
  if(quicktime_has_audio(fi->qth)) {
    nouts++;
    if(!quicktime_supported_audio(fi->qth, 0))
      nouts--;
    else
      fi->audio.out = TRUE;
  }

  if(quicktime_has_video(fi->qth)) {
    nouts++;
    if(!quicktime_supported_video(fi->qth, 0))
      nouts--;
    else
      fi->video.out = TRUE;
  }

  if(nouts == 0) {
    g_message("header read failed - nouts = 0");
    quicktime_close(fi->qth);
    fi->qth = NULL;
    return -1;
  }

  fi->read_qt_header = TRUE;

  g_message("We have read the header: video %d audio %d",
	    fi->video.out, fi->audio.out);
  /* We only support one video track for now */
  if(fi->video.out) {
    g_assert(quicktime_video_tracks(fi->qth) == 1);
    
    fi->video.vdf.fmt = GMF_Media_RGB_888;
    fi->video.vdf.frame_width = quicktime_video_width(fi->qth, 0);
    fi->video.vdf.frame_height = quicktime_video_height(fi->qth, 0);
    fi->video.fps = quicktime_frame_rate(fi->qth, 0);
    {
      double ip;
      
      fi->video.frame_interval.tvUsec = modf(1.0/((double)fi->video.fps), &ip) * 1000000.0;
      fi->video.frame_interval.tvSec = floor(ip);
    }
    fi->stopTime = parseqt_frames_to_time(fi, quicktime_video_length(fi->qth, 0));
    g_message("Video is %dx%d fps %g duration [%d, %d]",
	      fi->video.vdf.frame_width, fi->video.vdf.frame_height,
	      fi->video.fps,
	      fi->stopTime.tvSec,
	      fi->stopTime.tvUsec);
  }

  if(fi->audio.out) {
    g_assert(quicktime_audio_tracks(fi->qth) == 1);

    fi->audio.adf.sample_rate = quicktime_sample_rate(fi->qth, 0);
    fi->audio.adf.nbits = quicktime_audio_bits(fi->qth, 0);
    fi->audio.adf.byte_order_be = FALSE;
    fi->audio.adf.nchannels = MIN(quicktime_track_channels(fi->qth, 0), 1);
    g_assert(fi->audio.adf.nchannels < 3);

    fi->stopTime = parseqt_samples_to_time(fi, quicktime_audio_length(fi->qth, 0));
    g_message("Audio is %d/%d @ %d",
	      fi->audio.adf.nbits,
	      fi->audio.adf.nchannels,
	      fi->audio.adf.sample_rate);
  }

  return 0;
}

static void
parseqt_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  switch(type_info->majorType) {
  case GMF_MEDIA_AUDIO_DIGITAL:
    if(pi->fi->audio.out && !strcmp(type_info->minorType, "audio/raw")
       && (!pi->is_auto || !pi->is_video))
      *retval = TRUE;
    break;
  case GMF_MEDIA_VIDEO_DIGITAL:
    if(pi->fi->video.out && !strcmp(type_info->minorType, "video/raw")
	&& (!pi->is_auto || pi->is_video))
      *retval = TRUE;
    break;
  default:
    break;
  }
}

static void
parseqt_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;
  int i;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 0;
  if(pi->fi->audio.out
     && (!pi->is_auto || !pi->is_video))
    list->_length++;
  if(pi->fi->video.out
     && (!pi->is_auto || pi->is_video))
    list->_length++;

  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  i = 0;
  if(pi->fi->audio.out
     && (!pi->is_auto || !pi->is_video)) {
    ti = &list->_buffer[i++];

    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_AUDIO_DIGITAL;
    ti->minorType = CORBA_string_dup("audio/raw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  if(pi->fi->video.out
     && (!pi->is_auto || pi->is_video)) {
    ti = &list->_buffer[i++];

    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_VIDEO_DIGITAL;
    ti->minorType = CORBA_string_dup("video/raw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  *out_typelist = list;
}

static void
parseqt_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
  pi->is_video = (cnxinfo->connectionMediaInfo.majorType == GMF_MEDIA_VIDEO_DIGITAL);

  if(pi->is_video)
    pi->fi->video.pipes = g_list_append(pi->fi->video.pipes, apipe);
  else
    pi->fi->audio.pipes = g_list_append(pi->fi->audio.pipes, apipe);
}

static void
parseqt_out_disconnect(GMFPipe *apipe, PipeInfo *pi)
{
  if(pi->is_video)
    pi->fi->audio.pipes = g_list_remove(pi->fi->audio.pipes, apipe);
  else
    pi->fi->video.pipes = g_list_remove(pi->fi->video.pipes, apipe);
}

static GMF_Pipe_FormatAcceptance
parseqt_out_can_handle(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  if(CORBA_TypeCode_equal(typeinfo->formatData._type, (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat, pi->fi->ev)) {
    GMF_Media_AudioDigitalFormat *adf;
    
    adf = typeinfo->formatData._value;
    
    if((adf->sample_rate == pi->fi->audio.adf.sample_rate)
       && (adf->nchannels == pi->fi->audio.adf.nchannels)
       && (adf->nbits == (pi->fi->audio.adf.nbits))
       && (adf->byte_order_be == CORBA_FALSE))
      return GMF_Pipe_ACCEPTED;
    else
     return GMF_Pipe_CANNOT_HANDLE;
  } else if(CORBA_TypeCode_equal(typeinfo->formatData._type, (CORBA_TypeCode)TC_GMF_Media_VideoDigitalFormat, pi->fi->ev)) {
    GMF_Media_VideoDigitalFormat *vdf;
    
    vdf = typeinfo->formatData._value;

    if((vdf->fmt == pi->fi->video.vdf.fmt)
       && (vdf->frame_width == pi->fi->video.vdf.frame_width)
       && (vdf->frame_height == pi->fi->video.vdf.frame_height))
      return GMF_Pipe_ACCEPTED;
    else
     return GMF_Pipe_CANNOT_HANDLE;
    
  } else
    return GMF_Pipe_CANNOT_HANDLE;
}

static void
parseqt_out_fill(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  if(typeinfo->majorType == GMF_MEDIA_AUDIO_DIGITAL) {
    if(CORBA_any_get_release(&typeinfo->formatData))
      CORBA_free(typeinfo->formatData._value);
    CORBA_Object_release((CORBA_Object)typeinfo->formatData._type, pi->fi->ev);
    typeinfo->formatData._type = (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat;
    typeinfo->formatData._value = &pi->fi->audio.adf;
    CORBA_any_set_release(&typeinfo->formatData, CORBA_FALSE);
  } else if(typeinfo->majorType == GMF_MEDIA_VIDEO_DIGITAL) {
    if(CORBA_any_get_release(&typeinfo->formatData))
      CORBA_free(typeinfo->formatData._value);
    CORBA_Object_release((CORBA_Object)typeinfo->formatData._type, pi->fi->ev);
    typeinfo->formatData._type = (CORBA_TypeCode)TC_GMF_Media_VideoDigitalFormat;
    typeinfo->formatData._value = &pi->fi->video.vdf;
    CORBA_any_set_release(&typeinfo->formatData, CORBA_FALSE);
  }
}

static void
parseqt_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		  GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
  if(!pi->fi->read_qt_header) {
    *retval = GMF_Pipe_CANNOT_HANDLE;
    return;
  }

  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    parseqt_out_fill(typeinfo, pi);
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? */
    if(parseqt_out_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else
      *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_ACCEPTED:
    /* Can we handle this? */
    if(parseqt_out_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else {
      parseqt_out_fill(typeinfo, pi);
      *retval = GMF_Pipe_TRY_MINE;
    }
    break;
  }
}

static void
parseqt_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi)
{
  GMF_TimeVal *tvptr, ttmp;

#if 0
  if(pi->is_video)
    g_message("qos on video %d says lag is now [%d, %d]", pi->is_video, qosinfo->lag.tvSec, qosinfo->lag.tvUsec);
#endif

  if(pi->is_video)
    tvptr = &pi->fi->video.presend;
  else
    tvptr = &pi->fi->audio.presend;

  if(gmf_time_compare(*tvptr, qosinfo->lag) < 0) {
    *tvptr = qosinfo->lag;
  } else {
    GList *ltmp;
    GMF_TimeVal ttmp = qosinfo->lag;

    for(ltmp = pi->fi->filter->outputPipes; ltmp; ltmp = g_list_next(ltmp)) {
      if(gmf_time_compare(ttmp, ((GMFOutputPipe *)(ltmp->data))->last_qosinfo.lag) < 0)
	ttmp = ((GMFOutputPipe *)(ltmp->data))->last_qosinfo.lag;
    }

    *tvptr = ttmp;
  }

  if(pi->is_video) {
    /* Try skipping frames to catch up */
    ttmp = pi->fi->video.frame_interval;
    if(ttmp.tvSec) ttmp.tvSec *= -1;
    else ttmp.tvUsec *= -1;

    if(gmf_time_compare(*tvptr, ttmp) < 0) {
      *tvptr = gmf_time_add(*tvptr, pi->fi->video.frame_interval);
      pi->fi->video.last_sample = gmf_time_add(pi->fi->video.last_sample, pi->fi->video.frame_interval);

      quicktime_set_video_position(pi->fi->qth, quicktime_video_position(pi->fi->qth, 0) + 1, 0);
    }
  }
}
