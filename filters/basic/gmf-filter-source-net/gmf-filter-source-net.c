/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#define G_LOG_DOMAIN "netin"
#define CACHE_SIZE 65536

#include <gmf.h>

#undef VERSION
#include <WWWLib.h>
#include <WWWStream.h>
#include <WWWInit.h>
#undef VERSION

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

typedef struct {
  CORBA_Environment *ev, evdat;
  CORBA_ORB orb;

  GMFFilter *filter;
  GMFTimeReference *timeref;

  HTRequest *reqfile;
  char *curfile;

  struct {
    CORBA_long base;
    guchar *buf;
    gboolean setup;
  } cache;
} FilterInfo;

typedef struct {
  FilterInfo *fi;

  GMFPipe *apipe;

  CORBA_long cur_position;
} PipeInfo;

static CORBA_Object netin_create_filter(PortableServer_POA poa,
					 const char *iid,
					 gpointer impl_ptr,
					 CORBA_Environment *ev);
static void netin_filter_destroy(GtkObject *filter, FilterInfo *fi);
static void netin_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void netin_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void netin_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void netin_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi);
static void netin_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi);
static void netin_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, CORBA_Environment *ev, FilterInfo *fi);
static void netin_read(GMFDataReader *apipe, CORBA_long nbytes, GMF_Data **rv, PipeInfo *pi);
static void netin_get_position(GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi);
static void netin_set_position(GMFDataReader *apipe, CORBA_long new_pos, GMF_DataReader_PositionRelation prel, CORBA_long *retval, PipeInfo *pi);
static void netin_get_length(GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi);

/* w3c-libwww integration */
static int netin_register_cb(SOCKET s, HTEventType et, HTEvent *e);
static int netin_unregister_cb(SOCKET, HTEventType et);
static BOOL netin_register_timer(HTTimer * timer);
static BOOL netin_unregister_timer(HTTimer * timer);

static int netin_stream_put_character (HTStream * me, char c);
static int netin_stream_put_string (HTStream * me, const char * s);;
static int netin_stream_write (HTStream * me, const char * s, int l);
static int netin_stream_flush (HTStream * me);
static int netin_stream_free (HTStream * me);
static int netin_stream_abort (HTStream * me, HTList * e);
static HTResponse *netin_request_response(HTRequest *request);
static HTStream *netin_stream_new (FilterInfo *fi);

static void netin_cache_setup(FilterInfo *fi);
static gboolean netin_cache_add(FilterInfo *fi, const char *data, int len);
static void *netin_cache_get(FilterInfo *fi, int offset, CORBA_long *len);

static const char filter_iid[] = "OAFIID:gmf-filter-source-net:9cbeaad9-f7a9-4eb5-9b66-c2ecc8af3a16";

static const OAFPluginObject plugin_list[] = {
	{
  		filter_iid,
		netin_create_filter
	},
	
  	{
  		NULL,
  		NULL
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"Network Source Stuff"
};


static int did_init = 0;
static GHashTable *fd_to_sockp;

static CORBA_Object
netin_create_filter(PortableServer_POA poa,
		     const char *iid,
		     gpointer impl_ptr,
		     CORBA_Environment *ev)
{
  GtkObject *filter;
  FilterInfo *fi;

  if(strcmp(iid, filter_iid))
    return CORBA_OBJECT_NIL;

  if(!did_init) {
    fd_to_sockp = g_hash_table_new(g_direct_hash, g_direct_equal);

    HTProfile_newRobot("gmf-filter-source-net", FILTER_VERSION);
    HTEvent_setRegisterCallback(netin_register_cb);
    HTEvent_setUnregisterCallback(netin_unregister_cb);
    HTTimer_registerSetTimerCallback(netin_register_timer);
    HTTimer_registerDeleteTimerCallback(netin_unregister_timer);
  }

  did_init++;

  filter = gtk_object_new(gmf_filter_get_type(), "filter_type", GMF_Filter_SOURCE, NULL);
  g_assert(filter);

  oaf_plugin_use (poa, filter);
  
  fi = g_new0(FilterInfo, 1);
  fi->orb = oaf_orb_get ();
  CORBA_exception_init(&fi->evdat);
  fi->ev = &fi->evdat;
  gtk_signal_connect(filter, "get_pipe", GTK_SIGNAL_FUNC(netin_get_pipe), fi);
  gtk_signal_connect(filter, "destroy", GTK_SIGNAL_FUNC(netin_filter_destroy), fi);
  gtk_signal_connect(filter, "get_attributes", GTK_SIGNAL_FUNC(netin_get_attributes), fi);
  gtk_signal_connect(filter, "get_attribute", GTK_SIGNAL_FUNC(netin_get_attribute), fi);
  gtk_signal_connect(filter, "set_attribute", GTK_SIGNAL_FUNC(netin_set_attribute), fi);
  gtk_object_set_data(filter, "FilterInfo", fi);

  fi->filter = (GMFFilter *)filter;
  fi->timeref = (GMFTimeReference *)gmf_time_reference_new();
  gtk_object_ref(GTK_OBJECT(fi->timeref));
  gtk_object_sink(GTK_OBJECT(fi->timeref));

  {
    GMFPipe *default_pipe;

    netin_get_pipe(fi->filter, GMF_OUT, &default_pipe, fi);
    gtk_object_set(GTK_OBJECT(default_pipe), "autorender", TRUE, NULL);
  }

  impl_ptr = filter;

  return CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(filter)), fi->ev);
}

static void netin_filter_destroy(GtkObject *filter, FilterInfo *fi)
{
  if(fi->curfile) {
    HTRequest_delete(fi->reqfile);
    g_free(fi->curfile);
  }

  did_init--;
  if(!did_init) {
    HTProfile_delete();
    g_hash_table_destroy(fd_to_sockp);
  }

  gtk_object_unref(GTK_OBJECT(fi->timeref));
  CORBA_exception_free(fi->ev);
  g_free(fi);

  oaf_plugin_unuse(filter);
}

static void
netin_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi)
{
  const char * const attr_names[] = {
    "URL",
    NULL
  };
  int i;

  for(i = 0; attr_names[i]; i++);

  retval->_length = i;
  retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(retval->_length);
  for(i = 0; attr_names[i]; i++)
    retval->_buffer[i] = CORBA_string_dup((char *)attr_names[i]);
}

static void
netin_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi)
{  
  if(!strcmp(attr_name, "URL") && fi->curfile) {
    ret_attr_val->_type = (CORBA_TypeCode)TC_string;
    ret_attr_val->_value = &fi->curfile;
    CORBA_any_set_release(ret_attr_val, CORBA_FALSE);
  }
}

static void
netin_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, CORBA_Environment *ev, FilterInfo *fi)
{
  if(!strcmp(attr_name, "URL")) {
    HTRequest *newreq;

    if(CORBA_TypeCode_equal(attr_val->_type, (CORBA_TypeCode)TC_string, fi->ev)) {
      char **strptr;
      char *url;
      BOOL status;

      strptr = attr_val->_value;

      url = *strptr;

      newreq = HTRequest_new();
      if(!newreq)
	goto out;

      HTRequest_setOutputFormat(newreq, WWW_SOURCE);

      status = HTLoadToStream(url, netin_stream_new(fi), newreq);

      if(!status) {
	HTRequest_delete(newreq);
	goto out;
      }

      if(fi->curfile) {
	HTRequest_delete(fi->reqfile);
	g_free(fi->curfile);
      }

      fi->reqfile = newreq;
      fi->curfile = g_strdup(url);
    } else {
      goto out;
    }
  }

  return;

 out:
	CORBA_exception_set(ev, CORBA_USER_EXCEPTION, ex_GMF_AttributeAccess_SetFailed, NULL);
}

static void
netin_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  GtkObject *apipe, *dr;
  PipeInfo *pi;

  g_return_if_fail(pDirection == GMF_OUT);

  apipe = gtk_object_new(gmf_output_pipe_get_type(),
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 NULL);

  g_assert(apipe);

  pi = g_new0(PipeInfo, 1);

  pi->fi = fi;
  pi->apipe = (GMFPipe *)apipe;
  pi->cur_position = 0;

  gtk_signal_connect(apipe, "can_process_type", netin_can_process_type, pi);
  gtk_signal_connect(apipe, "get_processable_types", netin_get_processable_types, pi);

  dr = gtk_object_new(gmf_data_reader_get_type(), NULL);
  bonobo_object_add_interface(BONOBO_OBJECT(apipe), BONOBO_OBJECT(dr));
  gtk_signal_connect(dr, "read", GTK_SIGNAL_FUNC(netin_read), pi);
  gtk_signal_connect(dr, "get_length", GTK_SIGNAL_FUNC(netin_get_length), pi);
  gtk_signal_connect(dr, "get_position", GTK_SIGNAL_FUNC(netin_get_position), pi);
  gtk_signal_connect(dr, "set_position", GTK_SIGNAL_FUNC(netin_set_position), pi);

  gtk_object_set_data(apipe, "PipeInfo", pi);

  gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  *out_pipe = (GMFPipe *)apipe;
}

static char *
netin_get_content_type(FilterInfo *fi)
{
  HTFormat fmt;
  char *retval;

  while(!(fmt = HTResponse_format(netin_request_response(fi->reqfile)))
	|| !strcmp(fmt->name, "www/unknown"))
    g_main_iteration(TRUE);

  retval = fmt->name;
  g_message("content type is %s", retval);

  if(!retval) retval = "*";

  return retval;
}

static void
netin_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  /* We can really process anything */
  if(type_info->majorType != GMF_MEDIA_DATA)
    return;

  *retval = TRUE;

  if(pi->fi->curfile) {
    const char *ctmp;

    ctmp = netin_get_content_type(pi->fi);

    if(*ctmp && strcmp(type_info->minorType, ctmp))
      *retval = FALSE;
  }
}

static void
netin_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  ti = &list->_buffer[0];
  {
    memset(ti, 0, sizeof(*ti));

    if(pi->fi->curfile) {
      const char *ctmp;

      ctmp = netin_get_content_type(pi->fi);
      ti->majorType = GMF_MEDIA_DATA;
      ti->minorType = CORBA_string_dup((char *)ctmp);
    } else {
      ti->majorType = GMF_MEDIA_UNKNOWN;
      ti->minorType = CORBA_string_dup("*");
    }
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  *out_typelist = list;
}

static void netin_read(GMFDataReader *apipe, CORBA_long nbytes, GMF_Data **rv, PipeInfo *pi)
{
  CORBA_long thelen;
  GMF_Data *out_data;
  FilterInfo *fi = pi->fi;

  out_data = *rv = GMF_Data__alloc();

  if(!fi->curfile) return;

  thelen = CLAMP(nbytes, 0, HTResponse_length(netin_request_response(fi->reqfile)) - pi->cur_position);

  out_data->_buffer = netin_cache_get(fi, pi->cur_position, &thelen); /* send random garbage */
  out_data->_length = thelen;
  CORBA_sequence_set_release(out_data, CORBA_FALSE);

  pi->cur_position += thelen;
}

static void
netin_get_position(GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi)
{
  *retval = pi->cur_position;
}

static void
netin_set_position(GMFDataReader *apipe, CORBA_long new_pos, GMF_DataReader_PositionRelation prel,
		    CORBA_long *retval, PipeInfo *pi)
{
  FilterInfo *fi = pi->fi;

  switch(prel) {
  case GMF_DataReader_ABSOLUTE:
    pi->cur_position = new_pos;
    break;
  case GMF_DataReader_RELATIVE:
    pi->cur_position += new_pos;
    break;
  }

  pi->cur_position = CLAMP(pi->cur_position, 0, HTResponse_length(netin_request_response(fi->reqfile)));

  *retval = pi->cur_position;
}

static void
netin_get_length(GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi)
{
  FilterInfo *fi = pi->fi;

  *retval = HTResponse_length(netin_request_response(fi->reqfile));
}

/*** w3c-libwww integration ***/
typedef struct {
  int refcount;
  SOCKET      s ;                     /* our socket */
  GIOChannel *ioc;
  guint event_id;
  int read_count, write_count, pri_count;
} SockEvents;

static gboolean
netin_fd_cb(GIOChannel *ioc, GIOCondition cond, SockEvents *sockp)
{
  sockp->refcount++;

  g_message("fd %d got events %d", sockp->s, cond);

  if(cond & G_IO_IN) {
    HTEventList_dispatch(sockp->s, HTEvent_READ, HTGetTimeInMillis());
    HTEventList_dispatch(sockp->s, HTEvent_ACCEPT, HTGetTimeInMillis());
  }

  if(cond & G_IO_OUT) {
    HTEventList_dispatch(sockp->s, HTEvent_WRITE, HTGetTimeInMillis());
    HTEventList_dispatch(sockp->s, HTEvent_CONNECT, HTGetTimeInMillis());
  }

  if(cond & G_IO_PRI) {
    HTEventList_dispatch(sockp->s, HTEvent_OOB, HTGetTimeInMillis());
  }

  sockp->refcount--;

  if(sockp->refcount <= 0) {
    g_hash_table_remove(fd_to_sockp, GUINT_TO_POINTER(sockp->s));
    g_io_channel_unref(sockp->ioc);
    g_free(sockp);
    
    return FALSE;
  } else if(cond & (G_IO_NVAL|G_IO_ERR|G_IO_HUP))
    return FALSE;

  return TRUE;
}

static GIOCondition
netin_get_iocond(SockEvents *sockp)
{
  GIOCondition cond = 0;

#define CHECK_EVENT(x, y) if(sockp->events[HTEvent_INDEX(x)]) cond |= y

  if(sockp->read_count > 0) cond |= G_IO_IN;
  if(sockp->write_count > 0) cond |= G_IO_OUT;
  if(sockp->pri_count > 0) cond |= G_IO_PRI;

#undef CHECK_EVENT

  g_message("fd %d has condition %d", sockp->s, cond);

  return cond;
}

static int
netin_register_cb(SOCKET s, HTEventType et, HTEvent *e)
{
  SockEvents *sockp;

  g_assert(et != HTEvent_TIMEOUT);

  g_message("Registering fd %d for event %s", s, HTEvent_type2str(et));

  sockp = g_hash_table_lookup(fd_to_sockp, GUINT_TO_POINTER(s));

  if(!sockp) {
    sockp = g_new0(SockEvents, 1);
    g_hash_table_insert(fd_to_sockp, GUINT_TO_POINTER(s), sockp);
    sockp->ioc = g_io_channel_unix_new(s);
    sockp->s = s;
  }

  sockp->refcount++;

  switch(et) {
  case HTEvent_READ:
  case HTEvent_ACCEPT:
    sockp->read_count++;
    break;
  case HTEvent_WRITE:
  case HTEvent_CONNECT:
    sockp->write_count++;
    break;
  case HTEvent_OOB:
    sockp->pri_count++;
    break;
  default:
    break;
  }

  HTEventList_register (s, et, e);

  if(sockp->event_id)
    g_source_remove(sockp->event_id);

  sockp->event_id = g_io_add_watch(sockp->ioc, netin_get_iocond(sockp), (GIOFunc)netin_fd_cb, sockp);

  return 0;
}

static int
netin_unregister_cb(SOCKET s, HTEventType et)
{
  SockEvents *sockp;
  GIOCondition cond;

  g_assert(et != HTEvent_TIMEOUT);

  g_message("Unregistering fd %d for event %s", s, HTEvent_type2str(et));

  sockp = g_hash_table_lookup(fd_to_sockp, GUINT_TO_POINTER(s));
  g_return_val_if_fail(sockp, -1);

  g_assert(sockp->s == s);

  g_assert(sockp->event_id);
  g_source_remove(sockp->event_id);
  HTEventList_unregister(s, et);

  sockp->refcount--;

  switch(et) {
  case HTEvent_READ:
  case HTEvent_ACCEPT:
    sockp->read_count--;
    break;
  case HTEvent_WRITE:
  case HTEvent_CONNECT:
    sockp->write_count--;
    break;
  case HTEvent_OOB:
    sockp->pri_count--;
    break;
  default:
    break;
  }

  cond = netin_get_iocond(sockp);
  if(cond) {
    sockp->event_id = g_io_add_watch(sockp->ioc, cond, (GIOFunc)netin_fd_cb, sockp);
  } else if(sockp->refcount <= 0) {
    g_hash_table_remove(fd_to_sockp, GUINT_TO_POINTER(s));
    g_io_channel_unref(sockp->ioc);
    g_free(sockp);
  }

  return 0;
}

typedef struct {
  void *param;
  guint id;
} TimerInfo;

/* Ugly hack */
struct _HTTimer {
    ms_t	millis;		/* Relative value in millis */
    ms_t	expires;	/* Absolute value in millis */
    BOOL	relative;
    BOOL	repetitive;
    void *	param;		/* Client supplied context */
    HTTimerCallback * cbf;
};

static gboolean
netin_timer_cb(HTTimer *timer)
{
  TimerInfo *ti;

  ti = timer->param;

  timer->param = ti->param;
  HTTimer_dispatch(timer);
  timer->param = ti;

  if(timer->repetitive)
    return TRUE;
  else {
    void *p;

    p = ti->param;
    g_free(ti);
    timer->param = p;

    return FALSE;
  }
}

static BOOL
netin_register_timer(HTTimer * timer)
{
  TimerInfo *ti;

  ti = g_new(TimerInfo, 1);
  ti->param = timer->param;
  ti->id = g_timeout_add(HTTimer_getTime(timer), (GSourceFunc)netin_timer_cb, timer);
  timer->param = ti;

  return TRUE;
}

static BOOL
netin_unregister_timer(HTTimer * timer)
{
  void *p;
  TimerInfo *ti;

  ti = timer->param;
  p = ti->param;

  g_source_remove(ti->id);
  g_free(ti);

  timer->param = p;

  return TRUE;
}

/* Lame hack */
struct _HTStream {
  const HTStreamClass *	isa;
  FilterInfo *fi;
};

static void
netin_cache_setup(FilterInfo *fi)
{
  HTResponse_length(netin_request_response(fi->reqfile));

  fi->cache.buf = g_malloc(CACHE_SIZE);
  fi->cache.setup = TRUE;
}

static gboolean
netin_cache_add(FilterInfo *fi, const char *data, int len)
{
	return TRUE;
}

static void *
netin_cache_get(FilterInfo *fi, int offset, CORBA_long *len)
{
  return alloca(*len);
}

static int netin_stream_put_character (HTStream * me, char c)
{
  return netin_stream_write(me, &c, 1);
}

static int netin_stream_put_string (HTStream * me, const char * s)
{
  return netin_stream_write(me, s, strlen(s));
}

static int netin_stream_write (HTStream * me, const char * s, int l)
{
  if(!me->fi->cache.setup)
    netin_cache_setup(me->fi);

  if(netin_cache_add(me->fi, s, l))
    return HT_WOULD_BLOCK;
  else
    return HT_OK;
}

static int netin_stream_flush (HTStream * me)
{
  g_message("%s: flush", me->fi->curfile);

  return HT_OK;
}

static int netin_stream_free (HTStream * me)
{
  g_message("%s: free", me->fi->curfile);

  g_free(me);

  return HT_OK;
}

static int netin_stream_abort (HTStream * me, HTList * e)
{
  g_message("%s: abort", me->fi->curfile);

  return HT_ERROR;
}

static const HTStreamClass netin_stream_class =
{		
    "netin_stream",
    netin_stream_flush,
    netin_stream_free,
    netin_stream_abort,
    netin_stream_put_character,
    netin_stream_put_string,
    netin_stream_write
}; 

static HTStream *
netin_stream_new (FilterInfo *fi)
{
  HTStream *retval;

  retval = g_new0(HTStream, 1);

  retval->isa = &netin_stream_class;
  retval->fi = fi;

  return retval;
}

/* Bad hack */
static HTResponse *
netin_request_response(HTRequest *request)
{
  HTResponse *retval;
  while(!(retval = HTRequest_response(request)))
    g_main_iteration(TRUE);

  return retval;
}
