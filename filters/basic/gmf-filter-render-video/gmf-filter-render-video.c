/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#define G_LOG_DOMAIN "video-out"

#include <gmf.h>
#include <libgmf/gmf-digital-video.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <gdk/gdkx.h>
#include <X11/extensions/XShm.h>
extern int XShmGetEventBase(Display *display);

typedef struct {
  CORBA_Environment *ev, evdat;

  GMFFilter *filter;
  GMFTimeReference *timeref;

  int num_active_pipes;
  GMF_Callback_CallbackID playback_timer_id;

  GtkWidget *outwin, *out;

  GMF_Media_VideoDigitalFormat vdf;
  GdkGC *gc;
  int evnum;

  GMF_TimeVal x_lag;
} FilterInfo;

typedef struct {
  FilterInfo *fi;

  GMFPipe *apipe;

  GMF_TimeVal presend, last_qos;
  GList *samples;
  GMF_Callback_CallbackID queue_id;
} PipeInfo;

static CORBA_Object vidout_create_filter(PortableServer_POA poa,
					 const char *iid,
					 gpointer impl_ptr,
					 CORBA_Environment *ev);
static void vidout_destroy_filter(GtkObject *filter, FilterInfo *fi);
static void vidout_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void vidout_run(GMFFilter *filter, FilterInfo *fi);
static void vidout_pause(GMFFilter *filter, FilterInfo *fi);
static void vidout_stop(GMFFilter *filter, FilterInfo *fi);
static void vidout_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void vidout_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void vidout_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi);
static void vidout_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cinfo, PipeInfo *pi);
static void vidout_disconnect(GMFPipe *apipe, PipeInfo *pi);
static void vidout_add_active_pipe(PipeInfo *pi);
static void vidout_remove_active_pipe(PipeInfo *pi);
static void vidout_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);
static void vidout_handle_completion(GtkWidget *out, GdkEvent *event, FilterInfo *fi) G_GNUC_UNUSED;
static void vidout_send_qos(PipeInfo *pi, GMF_TimeVal *curtime, GMF_TimeVal *actual, GMF_TimeVal *expected);

static void vidout_queue_play(PipeInfo *pi, GMF_Sample *sample, GMF_TimeVal *curtime);
static void vidout_play_sample(PipeInfo *pi, GMF_Sample *sample);
static gboolean vidout_play_queued_sample(PipeInfo *pi);

static void GMF_Sample__copy(GMF_Sample *out, GMF_Sample *in);

static const char filter_iid[] = "OAFIID:gmf-filter-render-video:4702be3c-52c2-4621-af57-8ecf2ac6a74d";

static const OAFPluginObject plugin_list[] = {
	{
  		filter_iid,
		vidout_create_filter
	},
	
  	{
  		NULL,
  		NULL
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"Video Out Stuff"
};


static CORBA_Object
vidout_create_filter(PortableServer_POA poa,
		     const char *iid,
		     gpointer impl_ptr,
		     CORBA_Environment *ev)
{
  GtkObject *filter;
  FilterInfo *fi;

  gdk_rgb_init();

  if(strcmp(iid, filter_iid))
    return CORBA_OBJECT_NIL;

  filter = gtk_object_new(gmf_filter_get_type(), "filter_type", GMF_Filter_RENDERER, NULL);
  g_assert(filter);

  oaf_plugin_use (poa, filter);
  
  fi = g_new0(FilterInfo, 1);
  fi->ev = &fi->evdat;
  CORBA_exception_init(fi->ev);
  fi->filter = (GMFFilter *)filter;
  fi->timeref = fi->filter->timeref;

  fi->playback_timer_id = 0;

  gtk_signal_connect(filter, "get_pipe", GTK_SIGNAL_FUNC(vidout_get_pipe), fi);
  gtk_signal_connect(filter, "destroy", GTK_SIGNAL_FUNC(vidout_destroy_filter), fi);
  gtk_signal_connect(filter, "run", GTK_SIGNAL_FUNC(vidout_run), fi);
  gtk_signal_connect(filter, "pause", GTK_SIGNAL_FUNC(vidout_pause), fi);
  gtk_signal_connect(filter, "stop", GTK_SIGNAL_FUNC(vidout_stop), fi);

  impl_ptr = filter;

  return CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(filter)), fi->ev);
}

static void vidout_destroy_filter(GtkObject *filter, FilterInfo *fi)
{
  CORBA_exception_free(fi->ev);
  g_free(fi);

  oaf_plugin_unuse(filter);
}

static void
vidout_run(GMFFilter *filter, FilterInfo *fi)
{
  GList *ltmp;
  GMF_TimeVal curtime, stime;
  GMF_TimeVal expected;

  curtime = gmf_time_reference_current_time(fi->timeref);
  stime = gmf_time_subtract(curtime, fi->filter->timeBase);

  expected = gmf_time_subtract(stime, fi->x_lag);

  for(ltmp = filter->inputPipes; ltmp; ltmp = g_list_next(ltmp)) {
    GMFPipe *apipe;
    PipeInfo *pi;

    apipe = ltmp->data;
    pi = gtk_object_get_data(GTK_OBJECT(apipe), "PipeInfo");

    vidout_send_qos(pi, &curtime, &stime, &expected);
  }
}

static void
vidout_stop_pipe(GMFPipe *pipe, FilterInfo *fi)
{
  PipeInfo *pi;

  pi = gtk_object_get_data(GTK_OBJECT(pipe), "PipeInfo");

  g_return_if_fail(pi);

  if(pi->samples) {
    g_list_foreach(pi->samples, (GFunc)CORBA_free, NULL);
    g_list_free(pi->samples); pi->samples = NULL;

    gmf_time_reference_remove_request(fi->timeref, pi->queue_id); pi->queue_id = 0;
  }
}

static void
vidout_pause(GMFFilter *filter, FilterInfo *fi)
{
  g_list_foreach(filter->inputPipes, (GFunc)vidout_stop_pipe, fi);
}

static void
vidout_stop(GMFFilter *filter, FilterInfo *fi)
{
  vidout_pause(filter, fi);
}

static void
vidout_add_active_pipe(PipeInfo *pi)
{
  FilterInfo *fi;

  fi = pi->fi;

  fi->num_active_pipes++;
}

static void
vidout_remove_active_pipe(PipeInfo *pi)
{
  FilterInfo *fi;

  fi = pi->fi;

  g_assert(fi->num_active_pipes > 0);
  fi->num_active_pipes--;
}

static void
vidout_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  GtkObject *apipe;
  PipeInfo *pi;

  g_return_if_fail(pDirection == GMF_IN);

  apipe = gtk_object_new(gmf_input_pipe_get_type(),
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS| GMF_Transport_CORBA,
			 NULL);

  g_assert(apipe);

  pi = g_new0(PipeInfo, 1);

  pi->fi = fi;
  pi->apipe = (GMFPipe *)apipe;

  gtk_signal_connect(apipe, "can_process_type", vidout_can_process_type, pi);
  gtk_signal_connect(apipe, "get_processable_types", vidout_get_processable_types, pi);
  gtk_signal_connect(apipe, "receive_sample", vidout_receive_sample, pi);
  gtk_signal_connect(apipe, "connect", vidout_connect, pi);
  gtk_signal_connect(apipe, "disconnect", vidout_disconnect, pi);
  gtk_signal_connect(apipe, "fixup_media_format", vidout_fixup, pi);

  gtk_object_set_data(apipe, "PipeInfo", pi);

  gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  *out_pipe = (GMFPipe *)apipe;
}

static void
vidout_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_VIDEO_DIGITAL)
    return;

  if(strcmp(type_info->minorType, "video/raw"))
    return;

  *retval = TRUE;
}

static void
vidout_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  ti = &list->_buffer[0];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_VIDEO_DIGITAL;
    ti->minorType = CORBA_string_dup("video/raw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  *out_typelist = list;
}

static void
vidout_play_sample(PipeInfo *pi, GMF_Sample *sample)
{
  FilterInfo *fi;

  fi = pi->fi;

  gdk_draw_rgb_image(fi->out->window,
		     fi->gc,
		     0, 0, fi->vdf.frame_width, fi->vdf.frame_height,
		     GDK_RGB_DITHER_NONE, sample->sData._buffer, 3 * fi->vdf.frame_width);
}

static void
vidout_handle_completion(GtkWidget *out, GdkEvent *event, FilterInfo *fi)
{
  if(event->type == fi->evnum)
    g_message("Got completion event");
}

static gboolean
vidout_play_queued_sample(PipeInfo *pi)
{
  GMF_TimeVal curtime, reftime;
  GMF_Sample *samp;

  g_return_val_if_fail(pi->samples, FALSE);

  samp = pi->samples->data;
  pi->samples = g_list_remove(pi->samples, samp);

  vidout_play_sample(pi, samp);

  reftime = gmf_time_reference_current_time(pi->fi->timeref);
  curtime = gmf_time_subtract(reftime, pi->fi->filter->timeBase);

  CORBA_free(samp);

  if(pi->samples)
    vidout_queue_play(pi, pi->samples->data, &reftime);

  return FALSE;
}

static void
vidout_queue_play(PipeInfo *pi, GMF_Sample *sample, GMF_TimeVal *curtime)
{
  GMF_TimeVal offset;

  offset = gmf_time_subtract(sample->sInfo.streamStartTime, pi->fi->x_lag);

  if(pi->queue_id == 0) {
    pi->queue_id = gmf_time_reference_request_alarm(pi->fi->timeref,
						    &pi->fi->filter->timeBase,
						    &offset,
						    (GSourceFunc)vidout_play_queued_sample,
						    pi);
  }

  if(g_list_length(pi->samples) > 5) {
    GMF_Sample *lastsamp;
    GMF_TimeVal actual;

    lastsamp = g_list_last(pi->samples)->data;

    actual = gmf_time_subtract(*curtime, pi->fi->filter->timeBase);

    vidout_send_qos(pi, curtime, &actual, &lastsamp->sInfo.streamStartTime);
  }
}

static void
vidout_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi)
{
  GMF_TimeVal curtime, stime;

  curtime = gmf_time_reference_current_time(pi->fi->timeref);
  stime = gmf_time_subtract(curtime, pi->fi->filter->timeBase);

  if(sample->sInfo.isPreroll)
    return;

  if(gmf_time_compare(stime, sample->sInfo.streamStartTime) <= 0) {
    if(pi->samples) {
      g_list_foreach(pi->samples, (GFunc)CORBA_free, NULL);
      g_list_free(pi->samples); pi->samples = NULL;
      gmf_time_reference_remove_request(pi->fi->timeref, pi->queue_id); pi->queue_id = 0;
    }

    /* It's late - play it now */
    vidout_play_sample(pi, sample);

    vidout_send_qos(pi, &curtime, &stime, &sample->sInfo.streamStartTime);
  } else {
    /* queue it */
    GMF_Sample *queued_sample;

    queued_sample = GMF_Sample__alloc();
    GMF_Sample__copy(queued_sample, sample);
    pi->samples = g_list_append(pi->samples, queued_sample);

    vidout_queue_play(pi, queued_sample, &curtime);
  }
}

static GMF_Pipe_FormatAcceptance
vidout_can_handle(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  if(CORBA_TypeCode_equal(typeinfo->formatData._type, (CORBA_TypeCode)TC_GMF_Media_VideoDigitalFormat, pi->fi->ev)) {
    GMF_Media_VideoDigitalFormat *vdf;
    
    vdf = typeinfo->formatData._value;

    if(pi->fi->num_active_pipes) {
      GMF_Media_VideoDigitalFormat *our_vdf;

      our_vdf = &pi->fi->vdf;
      if(our_vdf->frame_width != vdf->frame_width
	 || our_vdf->frame_height != vdf->frame_height)
	return GMF_Pipe_CANNOT_HANDLE;
    }

    return GMF_Pipe_ACCEPTED;
  } else
    return GMF_Pipe_CANNOT_HANDLE;
}

static void
vidout_filltype(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  GMF_Media_VideoDigitalFormat *vdf;

  if(CORBA_any_get_release(&typeinfo->formatData))
    CORBA_free(typeinfo->formatData._value);
  CORBA_Object_release((CORBA_Object)typeinfo->formatData._type, pi->fi->ev);

  typeinfo->formatData._type = (CORBA_TypeCode)TC_GMF_Media_VideoDigitalFormat;

  if(pi->fi->num_active_pipes) {
    CORBA_any_set_release(&typeinfo->formatData, CORBA_FALSE);
    typeinfo->formatData._value = &pi->fi->vdf;
  } else {
    vdf = typeinfo->formatData._value = GMF_Media_VideoDigitalFormat__alloc();
    vdf->frame_width = 320;
    vdf->frame_height = 200;
    CORBA_any_set_release(&typeinfo->formatData, CORBA_TRUE);
  }
}

static void
vidout_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		  GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    vidout_filltype(typeinfo, pi);
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? */
    if(vidout_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else
      *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_ACCEPTED:
    /* Can we handle this? */
    if(vidout_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else {
      vidout_filltype(typeinfo, pi);
      *retval = GMF_Pipe_TRY_MINE;
    }
    break;
  }
}

static void
vidout_setup(FilterInfo *fi)
{
  if(!fi->outwin)
  {
    Bonobo_Unknown vidarea, fg;
    guint32 sockid;

    fg = GMF_Filter__get_graph(bonobo_object_corba_objref(BONOBO_OBJECT(fi->filter)), fi->ev);

    g_assert(!CORBA_Object_is_nil(fg, fi->ev));

    vidarea = Bonobo_Unknown_query_interface(fg, "IDL:GMF/VideoArea:1.0", fi->ev);

    if(!CORBA_Object_is_nil(vidarea, fi->ev)
       && (sockid = GMF_VideoArea__get_socket_id(vidarea, fi->ev))) {
      fi->outwin = gtk_plug_new(sockid);
    } else
      fi->outwin = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    CORBA_Object_release(vidarea, fi->ev);
    CORBA_Object_release(fg, fi->ev);

    fi->out = gtk_drawing_area_new();
    gtk_drawing_area_size(GTK_DRAWING_AREA(fi->out), fi->vdf.frame_width, fi->vdf.frame_height);

    gtk_container_add(GTK_CONTAINER(fi->outwin), fi->out);

    gtk_widget_show_all(fi->outwin);

    fi->gc = gdk_gc_new(fi->out->window);
  }
}

static void
vidout_teardown(FilterInfo *fi)
{
  gdk_gc_unref(fi->gc); fi->gc = NULL;
  gtk_widget_destroy(fi->outwin); fi->outwin = NULL;
}

static void
vidout_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cinfo, PipeInfo *pi)
{
  g_assert(CORBA_TypeCode_equal(cinfo->connectionMediaInfo.formatData._type,
				(CORBA_TypeCode)TC_GMF_Media_VideoDigitalFormat, pi->fi->ev));

  /* Set up things for playback */
  if(!pi->fi->num_active_pipes) {
    GMF_Media_VideoDigitalFormat *vdf;
    FilterInfo *fi;

    fi = pi->fi;

    vdf = cinfo->connectionMediaInfo.formatData._value;
    fi->vdf = *vdf;

    vidout_setup(pi->fi);
  }

  vidout_add_active_pipe(pi);
}

static void
vidout_disconnect(GMFPipe *apipe, PipeInfo *pi)
{
  if(pi->queue_id != 0) {
    gmf_time_reference_remove_request(pi->fi->timeref, pi->queue_id);

    pi->queue_id = 0;
  }

  vidout_remove_active_pipe(pi);

  if(pi->fi->num_active_pipes <= 0)
    vidout_teardown(pi->fi);
}

static void
GMF_Sample__copy(GMF_Sample *out, GMF_Sample *in)
{
  g_return_if_fail(in->sData._length);
  g_return_if_fail(in->sData._buffer);

  out->sInfo = in->sInfo;
  out->sData._length = in->sData._length;
  out->sData._buffer = CORBA_sequence_CORBA_octet_allocbuf(out->sData._length);
  memcpy(out->sData._buffer, in->sData._buffer, out->sData._length);
}

static void
vidout_send_qos(PipeInfo *pi, GMF_TimeVal *curtime, GMF_TimeVal *actual, GMF_TimeVal *expected)
{
  GMF_QualityControl_Quality qci;
  GMF_TimeVal qosinterval = {0, 50000};

  if(gmf_time_compare(*curtime, pi->last_qos) > 0)
    return;

  qci.timestamp = *actual;
  qci.lag = gmf_time_subtract(gmf_time_subtract(*expected, *actual), pi->fi->x_lag);
  qci.dlag.tvSec = qci.dlag.tvUsec = 0;

  GMF_QualityControl_quality_notify(pi->apipe->attr_pConnectionInfo->connectedTo, &qci, pi->fi->ev);

  pi->last_qos = gmf_time_add(*curtime, qosinterval);
}
