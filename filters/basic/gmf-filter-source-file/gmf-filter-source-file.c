/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#define G_LOG_DOMAIN "filein"

#include <fcntl.h>
#include <gmf.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-uri.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


typedef struct {
	CORBA_Environment *ev, evdat;
	CORBA_ORB orb;

	GMFFilter *filter;
	GMFTimeReference *timeref;

	GnomeVFSURI *uri;
	GnomeVFSHandle *handle;
	
	gulong file_len;
} FilterInfo;

typedef struct {
	FilterInfo *fi;

	GMFPipe *apipe;

	CORBA_long cur_position;
} PipeInfo;

static CORBA_Object filein_create_filter(PortableServer_POA poa, const char *iid, gpointer impl_ptr, CORBA_Environment *ev);
static void filein_filter_destroy(GtkObject *filter, FilterInfo *fi);
static void filein_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void filein_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info, gboolean *retval, PipeInfo *pi);
static void filein_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void filein_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi);
static void filein_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi);
static void filein_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, CORBA_Environment *ev, FilterInfo *fi);
static void filein_read(GMFDataReader *apipe, CORBA_long nbytes, GMF_Data **rv, PipeInfo *pi);
static void filein_get_position(GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi);
static void filein_set_position(GMFDataReader *apipe, CORBA_long new_pos, GMF_DataReader_PositionRelation prel, CORBA_long *retval, PipeInfo *pi);
static void filein_get_length(GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi);

static const char filter_iid[] = "OAFIID:gmf-filter-source-file:4702be3c-52c2-4621-af57-8ecf2ac6a74d";

static const OAFPluginObject plugin_list[] = {
	{
		filter_iid,
		filein_create_filter
	},
	
  	{
  		NULL  	
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"File Source Stuff"
};

static CORBA_Object
filein_create_filter (PortableServer_POA poa, const char *iid, gpointer impl_ptr, CORBA_Environment *ev)
{
	GtkObject *filter;
  	FilterInfo *fi;
	GMFPipe *default_pipe;

	if (strcmp(iid, filter_iid)) {
		return CORBA_OBJECT_NIL;
	}

	filter = gtk_object_new (gmf_filter_get_type(), "filter_type", GMF_Filter_SOURCE, NULL);
	g_assert (filter);

  	oaf_plugin_use (poa, filter);
  
  	fi = g_new0 (FilterInfo, 1);
  	fi->orb = oaf_orb_get ();
  	CORBA_exception_init (&fi->evdat);
  	fi->ev = &fi->evdat;
  	gtk_signal_connect (filter, "get_pipe", GTK_SIGNAL_FUNC (filein_get_pipe), fi);
  	gtk_signal_connect (filter, "destroy", GTK_SIGNAL_FUNC (filein_filter_destroy), fi);
  	gtk_signal_connect (filter, "get_attributes", GTK_SIGNAL_FUNC (filein_get_attributes), fi);
  	gtk_signal_connect (filter, "get_attribute", GTK_SIGNAL_FUNC (filein_get_attribute), fi);
  	gtk_signal_connect (filter, "set_attribute", GTK_SIGNAL_FUNC (filein_set_attribute), fi);
  	gtk_object_set_data (filter, "FilterInfo", fi);

  	fi->filter = (GMFFilter *)filter;
  	fi->timeref = (GMFTimeReference *) gmf_time_reference_new ();
  	gtk_object_ref (GTK_OBJECT (fi->timeref));
  	gtk_object_sink (GTK_OBJECT (fi->timeref));
  	fi->uri = NULL;
  	fi->handle = NULL;
	
	filein_get_pipe (fi->filter, GMF_OUT, &default_pipe, fi);
	gtk_object_set (GTK_OBJECT (default_pipe), "autorender", TRUE, NULL);

  	impl_ptr = filter;

  	return CORBA_Object_duplicate (bonobo_object_corba_objref (BONOBO_OBJECT (filter)), fi->ev);
}

static void filein_filter_destroy (GtkObject *filter, FilterInfo *fi)
{
	if ( fi->uri != NULL) {
		gnome_vfs_uri_unref (fi->uri);
		fi->uri = NULL;
	}

	if (fi->handle != NULL) {
		gnome_vfs_close (fi->handle);
		fi->handle = NULL;
	}

	gtk_object_unref (GTK_OBJECT (fi->timeref));
	CORBA_exception_free (fi->ev);
	g_free (fi);

	oaf_plugin_unuse (filter);
}

static void
filein_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi)
{
	const char * const attr_names[] = {
		"URL",
		NULL
	};
	int i;

	for(i = 0; attr_names[i]; i++);
	retval->_length = i;	
  	retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(retval->_length);

  	for (i = 0; attr_names[i]; i++) {
		retval->_buffer[i] = CORBA_string_dup ((char *)attr_names[i]);
	}
}

static void
filein_get_attribute (GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi)
{  
	if (strcmp (attr_name, "URL") == 0 && fi->uri != NULL) {
		ret_attr_val->_type = (CORBA_TypeCode)TC_string;
		// FIXME
		//ret_attr_val->_value = &fi->curfile;
		CORBA_any_set_release (ret_attr_val, CORBA_FALSE);
	}
}

static void
filein_set_attribute (GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, CORBA_Environment *ev, FilterInfo *fi)
{
	GnomeVFSResult result;
	GnomeVFSHandle *handle;
	GnomeVFSFileInfo info;
	GnomeVFSURI *new_uri;

	if ( strcmp(attr_name, "URL") == 0) {
		if(CORBA_TypeCode_equal(attr_val->_type, (CORBA_TypeCode)TC_string, fi->ev)) {
			char **strptr;

			// Extract URI from CORBA buffer
			strptr = attr_val->_value;

			// Create new URI
			new_uri = gnome_vfs_uri_new (*strptr);
			if (new_uri == NULL) {
				g_message ("filein_set_attribute: Unable to create uri from %s", *strptr);
				goto out;
			}

			// Attempt to open
			result = gnome_vfs_open_uri (&handle, new_uri, GNOME_VFS_OPEN_READ);
			if (result != GNOME_VFS_OK) {
				g_message ("filein_set_attribute: Unable to open uri %s", *strptr);
				gnome_vfs_uri_unref (new_uri);
				goto out;
			}
			
			gnome_vfs_file_info_init (&info);
			result = gnome_vfs_get_file_info_uri (new_uri, &info, (GNOME_VFS_FILE_INFO_FIELDS_SIZE
						          | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
			if (result != GNOME_VFS_OK) {
				g_message ("filein_set_attribute: Unable to get info for uri %s", *strptr);
				gnome_vfs_uri_unref (new_uri);
				goto out;
			}

			// Free old URI and close handle
			if (fi->uri != NULL) {
				gnome_vfs_uri_unref (fi->uri);
				gnome_vfs_close (fi->handle);
			}

			fi->uri = new_uri;
			fi->file_len = info.size;
			fi->handle = handle;
		} else {
			goto out;
		}
	}
	
	return;

	out:
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION, ex_GMF_AttributeAccess_SetFailed, NULL);
}

static void
filein_get_pipe (GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  GtkObject *apipe, *dr;
  PipeInfo *pi;

  g_return_if_fail(pDirection == GMF_OUT);

  apipe = gtk_object_new(gmf_output_pipe_get_type(),
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 NULL);

  g_assert(apipe);

  pi = g_new0(PipeInfo, 1);

  pi->fi = fi;
  pi->apipe = (GMFPipe *)apipe;
  pi->cur_position = 0;

  gtk_signal_connect(apipe, "can_process_type", filein_can_process_type, pi);
  gtk_signal_connect(apipe, "get_processable_types", filein_get_processable_types, pi);

  dr = gtk_object_new(gmf_data_reader_get_type(), NULL);
  bonobo_object_add_interface(BONOBO_OBJECT(apipe), BONOBO_OBJECT(dr));
  gtk_signal_connect(dr, "read", GTK_SIGNAL_FUNC(filein_read), pi);
  gtk_signal_connect(dr, "get_length", GTK_SIGNAL_FUNC(filein_get_length), pi);
  gtk_signal_connect(dr, "get_position", GTK_SIGNAL_FUNC(filein_get_position), pi);
  gtk_signal_connect(dr, "set_position", GTK_SIGNAL_FUNC(filein_set_position), pi);

  gtk_object_set_data(apipe, "PipeInfo", pi);

  gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  *out_pipe = (GMFPipe *)apipe;
}

static void
filein_can_process_type (GMFPipe *apipe, GMF_MediaTypeInfo *type_info, gboolean *retval, PipeInfo *pi)
{
	/* We can really process anything */
	if (type_info->majorType != GMF_MEDIA_DATA) {
		return;
	}

	*retval = TRUE;

	if (pi->fi->uri != NULL) {
		const char *mime_type;
		mime_type = gnome_vfs_get_mime_type_from_file_data (pi->fi->uri);
		//g_message ("filein_can_process_type: %s %s", type_info->minorType, mime_type);
		if (strcmp (type_info->minorType, mime_type) != 0) {
			*retval = FALSE;
		}
	}
}

static void
filein_get_processable_types (GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
	GMF_MediaTypeInfoList *list;
	GMF_MediaTypeInfo *ti;

	list = GMF_MediaTypeInfoList__alloc();
	list->_length = 1;
	list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

	ti = &list->_buffer[0];
	{
		memset(ti, 0, sizeof(*ti));

		if (pi->fi->uri != NULL) {
			const char *mime_type;
			mime_type = gnome_vfs_get_mime_type_from_file_data (pi->fi->uri);
			ti->majorType = GMF_MEDIA_DATA;
			ti->minorType = CORBA_string_dup ((char *) mime_type);
		} else {
			ti->majorType = GMF_MEDIA_UNKNOWN;
			ti->minorType = CORBA_string_dup("*");
		}

		ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
		ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
	}

	*out_typelist = list;
}

static void 
filein_read (GMFDataReader *apipe, CORBA_long nbytes, GMF_Data **rv, PipeInfo *pi)
{
	CORBA_long thelen;
	GMF_Data *out_data;
	FilterInfo *fi = pi->fi;
	GnomeVFSFileSize  bytes_read;
	GnomeVFSResult result;
	
	out_data = *rv = GMF_Data__alloc ();

	if (fi->uri == NULL || fi->handle == NULL) { 
		return;
  	}
	
	thelen = CLAMP (nbytes, 0, fi->file_len - pi->cur_position);
	out_data->_length = thelen;
	
	//out_data->_buffer = (char *)fi->file_mem + pi->cur_position;
	//CORBA_sequence_set_release (out_data, CORBA_FALSE);
	// FIXME: Do we need to seek?

	// Allocate and set up CORBA buffer
	out_data->_buffer = CORBA_sequence_CORBA_octet_allocbuf (thelen);
	out_data->_length = thelen;
	out_data->_maximum = thelen + 1;
	CORBA_sequence_set_release (out_data, CORBA_TRUE);

	// Read data into buffer
	result = gnome_vfs_read (fi->handle, out_data->_buffer, thelen, &bytes_read);
	if (result != GNOME_VFS_OK) {
		g_message ("filein_read failed");
	}
		
	pi->cur_position += thelen;
}

static void
filein_get_position (GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi)
{
	*retval = pi->cur_position;
}

static void
filein_set_position (GMFDataReader *apipe, CORBA_long new_pos, GMF_DataReader_PositionRelation prel,
		    CORBA_long *retval, PipeInfo *pi)
{
	GnomeVFSResult result;
	FilterInfo *fi = pi->fi;

	switch(prel) {
		case GMF_DataReader_ABSOLUTE:
			pi->cur_position = new_pos;
			break;
			
  		case GMF_DataReader_RELATIVE:
			pi->cur_position += new_pos;
			break;

		default:
			break;
	}

	pi->cur_position = CLAMP(pi->cur_position, 0, fi->file_len);

	result = gnome_vfs_seek (fi->handle, GNOME_VFS_SEEK_START, pi->cur_position);
	
	*retval = pi->cur_position;
}

static void
filein_get_length (GMFDataReader *apipe, CORBA_long *retval, PipeInfo *pi)
{
	FilterInfo *fi = pi->fi;

	*retval = fi->file_len;
}
