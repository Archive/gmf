/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#define G_LOG_DOMAIN "esdout"

#include <gmf.h>
#include <libgmf/gmf-digital-audio.h>
#include <esd.h>
#include <errno.h>
#include <math.h>

typedef struct {
  CORBA_ORB orb;
  CORBA_Environment *ev, evdat;

  GMFFilter *filter;
  GMFTimeReference *timeref;
  int num_active_pipes;
  GMF_Callback_CallbackID playback_timer_id;

  GMF_TimeVal esd_latency;
} FilterInfo;

typedef struct {
  FilterInfo *fi;

  GMFPipe *apipe;

  int esdfd;

  GList *samples;

  GMF_TimeVal presend, preplay, esd_preplay;
  GMF_Callback_CallbackID queue_id;

  GMF_TimeVal last_qos;
} PipeInfo;

static void esdout_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void esdout_run(GMFFilter *filter, FilterInfo *fi);
static void esdout_pause(GMFFilter *filter, FilterInfo *fi);
static void esdout_stop(GMFFilter *filter, FilterInfo *fi);
static void esdout_play_sample(PipeInfo *pi, GMF_Sample *sample);
static gboolean esdout_play_queued_sample(PipeInfo *pi);
static void esdout_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval, PipeInfo *pi);
static void esdout_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void esdout_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi);
static void esdout_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cinfo, PipeInfo *pi);
static void esdout_disconnect(GMFPipe *apipe, PipeInfo *pi);
static void esdout_add_active_pipe(PipeInfo *pi);
static void esdout_remove_active_pipe(PipeInfo *pi);
static void esdout_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);
static void GMF_Sample__copy(GMF_Sample *out, GMF_Sample *in);
static void esdout_queue_play(PipeInfo *pi, GMF_Sample *sample, GMF_TimeVal *curtime);
static void esdout_send_qos(PipeInfo *pi, GMF_TimeVal *curtime, GMF_TimeVal *actual, GMF_TimeVal *expected);
static CORBA_Object esdout_create_filter(PortableServer_POA poa,
					 const char *iid,
					 gpointer impl_ptr,
					 CORBA_Environment *ev);
static void esdout_destroy_filter(GtkObject *filter, FilterInfo *fi);

static const char filter_iid[] = "OAFIID:gmf-filter-render-esd:d758f744-e817-474f-bce2-7d411e11d6b9";

static const OAFPluginObject plugin_list[] = {
	{
		filter_iid,
   		esdout_create_filter
	},
	
  	{
  		NULL,
  		NULL
	}
};

const OAFPlugin OAF_Plugin_info = {
  plugin_list,
  "ESD out stuff"
};


static CORBA_Object esdout_create_filter(PortableServer_POA poa,
					 const char *iid,
					 gpointer impl_ptr,
					 CORBA_Environment *ev)
{
  GtkObject *filter;
  FilterInfo *fi;

  if(strcmp(iid, filter_iid))
    return CORBA_OBJECT_NIL;

  filter = gtk_object_new(gmf_filter_get_type(), "filter_type", GMF_Filter_RENDERER, NULL);
  g_assert(filter);

	oaf_plugin_use (poa, filter);
	
  fi = g_new0(FilterInfo, 1);
  fi->ev = &fi->evdat;
  CORBA_exception_init(fi->ev);
  fi->orb = oaf_orb_get ();

  gtk_signal_connect(filter, "get_pipe", GTK_SIGNAL_FUNC(esdout_get_pipe), fi);
  gtk_signal_connect(filter, "destroy", GTK_SIGNAL_FUNC(esdout_destroy_filter), fi);
  gtk_signal_connect(filter, "run", GTK_SIGNAL_FUNC(esdout_run), fi);
  gtk_signal_connect(filter, "pause", GTK_SIGNAL_FUNC(esdout_pause), fi);
  gtk_signal_connect(filter, "stop", GTK_SIGNAL_FUNC(esdout_stop), fi);
  fi->filter = (GMFFilter *)filter;
  fi->timeref = fi->filter->timeref;

  fi->playback_timer_id = 0;

  {
    int esdtmp;
    GMF_TimeVal tend, tstart, fivems = {0, 5000};

    esdtmp = esd_open_sound(NULL); /* Should use the gnome sound stuff */

    tstart = gmf_time_reference_current_time(fi->timeref);
    esd_get_latency(esdtmp);
    tend = gmf_time_reference_current_time(fi->timeref);

    esd_close(esdtmp);

    fi->esd_latency = gmf_time_add(gmf_time_subtract(tend, tstart), fivems);
  }

  impl_ptr = filter;

  return CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(filter)), fi->ev);
}

static void esdout_destroy_filter(GtkObject *filter, FilterInfo *fi)
{
	CORBA_exception_free(fi->ev);
	g_free(fi);

	oaf_plugin_unuse(filter);
}

static void
esdout_send_qos(PipeInfo *pi, GMF_TimeVal *curtime, GMF_TimeVal *actual, GMF_TimeVal *expected)
{
	GMF_QualityControl_Quality qci;
	GMF_TimeVal qosinterval = {0, 50000};

	if(!pi->apipe->attr_pConnectionInfo)
		return;

	g_return_if_fail(pi->fi->filter->state == GMF_Filter_RUNNING);

	if(gmf_time_compare(*curtime, pi->last_qos) > 0)
		return;

	qci.timestamp = *actual;
	qci.lag = gmf_time_subtract(gmf_time_subtract(gmf_time_subtract(*expected, *actual), pi->preplay),
			      pi->esd_preplay);
	qci.dlag.tvSec = qci.dlag.tvUsec = 0;

	GMF_QualityControl_quality_notify(pi->apipe->attr_pConnectionInfo->connectedTo, &qci, pi->fi->ev);

	pi->last_qos = gmf_time_add (*curtime, qosinterval);
}

static void
esdout_run (GMFFilter *filter, FilterInfo *fi)
{
	GList *ltmp;
	GMF_TimeVal curtime, stime;

	curtime = gmf_time_reference_current_time(fi->timeref);
	stime = gmf_time_subtract(curtime, fi->filter->timeBase);

	for (ltmp = filter->inputPipes; ltmp; ltmp = g_list_next(ltmp)) {
		GMFPipe *apipe;
		PipeInfo *pi;
		GMF_TimeVal expected;

		apipe = ltmp->data;
		pi = gtk_object_get_data (GTK_OBJECT (apipe), "PipeInfo");

		expected = gmf_time_subtract (gmf_time_subtract (stime, pi->preplay), pi->esd_preplay);

		esdout_send_qos (pi, &curtime, &stime, &expected);
	}
}

static void
esdout_stop_pipe(GMFPipe *pipe, FilterInfo *fi)
{
  PipeInfo *pi;

  pi = gtk_object_get_data(GTK_OBJECT(pipe), "PipeInfo");

  g_return_if_fail(pi);

  if(pi->samples) {
    g_list_foreach(pi->samples, (GFunc)CORBA_free, NULL);
    g_list_free(pi->samples); pi->samples = NULL;

    gmf_time_reference_remove_request(fi->timeref, pi->queue_id); pi->queue_id = 0;
  }
}

static void
esdout_pause(GMFFilter *filter, FilterInfo *fi)
{
  g_list_foreach(filter->inputPipes, (GFunc)esdout_stop_pipe, fi);
}

static void
esdout_stop(GMFFilter *filter, FilterInfo *fi)
{
  esdout_pause(filter, fi);
}

static void
esdout_add_active_pipe(PipeInfo *pi)
{
  FilterInfo *fi;

  fi = pi->fi;

  fi->num_active_pipes++;
}

static void
esdout_remove_active_pipe(PipeInfo *pi)
{
  FilterInfo *fi;

  fi = pi->fi;

  g_assert(fi->num_active_pipes > 0);
  fi->num_active_pipes--;
}

static void
esdout_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
  GtkObject *apipe;
  PipeInfo *pi;

  g_return_if_fail(pDirection == GMF_IN);

  apipe = gtk_object_new(gmf_input_pipe_get_type(),
			 "filter", filter,
			 "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 NULL);

  g_assert(apipe);

  pi = g_new0(PipeInfo, 1);

  pi->fi = fi;
  pi->apipe = (GMFPipe *)apipe;
  pi->esdfd = -1;
  pi->queue_id = 0;

  gtk_signal_connect(apipe, "can_process_type", esdout_can_process_type, pi);
  gtk_signal_connect(apipe, "get_processable_types", esdout_get_processable_types, pi);
  gtk_signal_connect(apipe, "receive_sample", esdout_receive_sample, pi);
  gtk_signal_connect(apipe, "connect", esdout_connect, pi);
  gtk_signal_connect(apipe, "disconnect", esdout_disconnect, pi);
  gtk_signal_connect(apipe, "fixup_media_format", esdout_fixup, pi);

  gtk_object_set_data(apipe, "PipeInfo", pi);

  gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  *out_pipe = (GMFPipe *)apipe;
}

static void
esdout_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_AUDIO_DIGITAL)
    return;

  if(strcmp(type_info->minorType, "audio/raw"))
    return;

  *retval = TRUE;
}

static void
esdout_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  ti = &list->_buffer[0];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_AUDIO_DIGITAL;
    ti->minorType = CORBA_string_dup("audio/raw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  *out_typelist = list;
}

static void
esdout_play_sample(PipeInfo *pi, GMF_Sample *sample)
{
  g_return_if_fail(pi->esdfd >= 0);

  write(pi->esdfd, sample->sData._buffer, sample->sData._length);
}

static gboolean
esdout_play_queued_sample(PipeInfo *pi)
{
  GMF_TimeVal curtime, reftime;
  GMF_Sample *samp;

  g_return_val_if_fail(pi->samples, FALSE);

  samp = pi->samples->data;
  pi->samples = g_list_remove(pi->samples, samp);

  esdout_play_sample(pi, samp);

  reftime = gmf_time_reference_current_time(pi->fi->timeref);
  curtime = gmf_time_subtract(reftime, pi->fi->filter->timeBase);

  CORBA_free(samp);

  pi->queue_id = 0;

  if(pi->samples)
    esdout_queue_play(pi, pi->samples->data, &reftime);

  return FALSE;
}

static void
esdout_queue_play(PipeInfo *pi, GMF_Sample *sample, GMF_TimeVal *curtime)
{
  GMF_TimeVal offset;

  offset = gmf_time_subtract(gmf_time_subtract(sample->sInfo.streamStartTime, pi->preplay), pi->esd_preplay);

  if(pi->queue_id == 0) {
    pi->queue_id = gmf_time_reference_request_alarm(pi->fi->timeref,
						    &pi->fi->filter->timeBase,
						    &offset,
						    (GSourceFunc)esdout_play_queued_sample,
						    pi);
  }

  if(g_list_length(pi->samples) > 5) {
    GMF_Sample *lastsamp;
    GMF_TimeVal actual;

    lastsamp = g_list_last(pi->samples)->data;

    actual = gmf_time_subtract(*curtime, pi->fi->filter->timeBase);

    esdout_send_qos(pi, curtime, &actual, &lastsamp->sInfo.streamStartTime);
  }
}

static void
esdout_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi)
{
	GMF_TimeVal curtime, stime;

	curtime = gmf_time_reference_current_time(pi->fi->timeref);
	stime = gmf_time_subtract(curtime, pi->fi->filter->timeBase);

	if(sample->sInfo.isPreroll) {
		return;
	}

	if (gmf_time_compare(stime, sample->sInfo.streamStartTime) <= 0) {
		if(pi->samples) {
			g_list_foreach(pi->samples, (GFunc)CORBA_free, NULL);
			g_list_free(pi->samples); pi->samples = NULL;
			gmf_time_reference_remove_request(pi->fi->timeref, pi->queue_id); pi->queue_id = 0;
		}

		/* It's late - play it now */
		esdout_play_sample (pi, sample);

		esdout_send_qos(pi, &curtime, &stime, &sample->sInfo.streamStartTime);
	} else {
    		/* queue it */
    		GMF_Sample *queued_sample;

    		queued_sample = GMF_Sample__alloc();
    		GMF_Sample__copy(queued_sample, sample);
    		pi->samples = g_list_append(pi->samples, queued_sample);

    		esdout_queue_play(pi, queued_sample, &curtime);
	}
}

static GMF_Pipe_FormatAcceptance
esdout_can_handle(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  if(CORBA_TypeCode_equal(typeinfo->formatData._type, (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat, pi->fi->ev)) {
    GMF_Media_AudioDigitalFormat *adf;
    
    adf = typeinfo->formatData._value;

    if((adf->nchannels > 2)
       || (adf->nchannels < 1)
       || (adf->nbits > 16)
       || (adf->byte_order_be))
     return GMF_Pipe_CANNOT_HANDLE;

    return GMF_Pipe_ACCEPTED;
  } else
    return GMF_Pipe_CANNOT_HANDLE;
}

static void
esdout_filltype(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  GMF_Media_AudioDigitalFormat *adf;

  if(CORBA_any_get_release(&typeinfo->formatData))
    CORBA_free(typeinfo->formatData._value);
  CORBA_Object_release((CORBA_Object)typeinfo->formatData._type, pi->fi->ev);

  typeinfo->formatData._type = (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat;
  adf = typeinfo->formatData._value = GMF_Media_AudioDigitalFormat__alloc();
  CORBA_any_set_release(&typeinfo->formatData, CORBA_TRUE);
  
  adf->sample_rate = ESD_DEFAULT_RATE;
  adf->nchannels = 2;
  adf->nbits = 16;
  adf->byte_order_be = CORBA_FALSE;
}

static void
esdout_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		  GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    esdout_filltype(typeinfo, pi);
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? */
    if(esdout_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else
      *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_ACCEPTED:
    /* Can we handle this? */
    if(esdout_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else {
      esdout_filltype(typeinfo, pi);
      *retval = GMF_Pipe_TRY_MINE;
    }
    break;
  }
}

static void
esdout_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cinfo, PipeInfo *pi)
{
  GMF_Media_AudioDigitalFormat *adf;
  esd_format_t fmt;

  g_return_if_fail(pi->esdfd < 0);

  g_assert(CORBA_TypeCode_equal(cinfo->connectionMediaInfo.formatData._type,
				(CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat, pi->fi->ev));
  adf = cinfo->connectionMediaInfo.formatData._value;

  g_message("new connect is %d/%d @ %d",
	    adf->nbits, adf->nchannels, adf->sample_rate);
  fmt = 0;

  switch(adf->nchannels) {
  case 1:
    fmt |= ESD_MONO;
    break;
  case 2:
    fmt |= ESD_STEREO;
    break;
  default:
    g_assert_not_reached();
    break;
  }

  switch(adf->nbits) {
  case 8:
    fmt |= ESD_BITS8;
    break;
  case 16:
    fmt |= ESD_BITS16;
    break;
  default:
    g_assert_not_reached();
    break;
  }

  pi->esdfd = esd_play_stream(fmt, adf->sample_rate, NULL, "gmf-filter-render-esd");

  pi->esd_preplay = pi->fi->esd_latency;

  esdout_add_active_pipe(pi);
}

static void
esdout_disconnect(GMFPipe *apipe, PipeInfo *pi)
{
  g_return_if_fail(pi->esdfd >= 0);

  close(pi->esdfd);
  pi->esdfd = -1;

  g_list_foreach(pi->samples, (GFunc)CORBA_free, NULL);
  g_list_free(pi->samples); pi->samples = NULL;

  gmf_time_reference_remove_request(pi->fi->timeref, pi->queue_id);
  pi->queue_id = 0;

  esdout_remove_active_pipe(pi);
}

static void
GMF_Sample__copy(GMF_Sample *out, GMF_Sample *in)
{
  g_return_if_fail(in->sData._length);
  g_return_if_fail(in->sData._buffer);

  out->sInfo = in->sInfo;
  out->sData._length = in->sData._length;
  out->sData._buffer = CORBA_sequence_CORBA_octet_allocbuf(out->sData._length);
  memcpy(out->sData._buffer, in->sData._buffer, out->sData._length);
}
