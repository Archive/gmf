/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */
/* How it works:
 *  When we start running, we send out samples incrementing from the current position.
 *  Using the QOS thing, we always catch up if we are late, and drop a certain % of sample packets.
 */

#define G_LOG_DOMAIN "parseau"

#include <gmf.h>
#include <libgmf/gmf-digital-audio.h>
#include <limits.h>
#include <endian.h>
#include <math.h>
#include <sched.h>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netinet/in.h> /* byte conversion */

#define AU_HEADER_LEN 24
#define AU_SIZE_UNKNOWN 0xFFFFFFFF
#define USEC_PER_SAMPLE 20000

typedef struct {
  CORBA_Environment 	*ev, evdat;
  CORBA_ORB 		orb;

  GMFFilter 		*filter;
  GMFTimeReference 	*timeref;

  GMFPipe 		*inpipe;
  GMF_DataReader 	data_reader;

  gboolean read_au_header;
  struct {
    guint32 data_offset, data_size;
    enum { ISDN_ULAW_8=1,
	   LINEAR_PCM_8=2,
	   LINEAR_PCM_16=3,
	   LINEAR_PCM_24=4,
	   LINEAR_PCM_32=5,
	   IEEE_FP_32=6,
	   IEEE_FP_64=7,
	   ISDN_ULAW_ADPCM_8=23
    } encoding;
    guint32 sample_rate;
    guint32 nchannels;

    guint bytes_per_sample;
    guint32 data_position;
    guint in_bytes_per_sample;
  } au_header;

  GMF_TimeVal presend;
  GMF_TimeVal last_sample;
  GMF_TimeVal stopTime;
  GMF_TimeVal playback_interval;

  GMF_Callback_CallbackID cbid;
  gboolean isDiscontinuity;
} FilterInfo;

typedef struct {
  FilterInfo *fi;

  GMFPipe *apipe;
} PipeInfo;

static void parseau_unref_factory		(GtkObject *filter, FilterInfo *fi);
static void parseau_get_pipe			(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi);
static void parseau_get_attributes		(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi);
static void parseau_get_attribute		(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi);
static void parseau_set_attribute		(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, FilterInfo *fi);
static gint parseau_read_au_header		(FilterInfo *fi);
static void parseau_stop			(GMFFilter *filter, FilterInfo *fi);
static void parseau_pause			(GMFFilter *filter, FilterInfo *fi);
static void parseau_run				(GMFFilter *filter, FilterInfo *fi);
static void parseau_deliver_event		(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName,
				  		 CORBA_any *evData, FilterInfo *fi);
static void parseau_can_seek			(GMFMediaPosition *mp, gboolean *retval, FilterInfo *fi);
static void parseau_get_duration		(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseau_get_position		(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseau_set_position		(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseau_get_stop_time		(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi);
static void parseau_set_stop_time		(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi);

static void parseau_in_can_process_type		(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    		 gboolean *retval, PipeInfo *pi);
static void parseau_in_get_processable_types	(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void parseau_in_receive_sample		(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi);
static void parseau_in_connect			(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void parseau_in_disconnect		(GMFPipe *apipe, PipeInfo *pi);
static void parseau_in_fixup			(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			     			 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);

static void parseau_out_can_process_type	(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
				    		 gboolean *retval, PipeInfo *pi);
static void parseau_out_get_processable_types	(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi);
static void parseau_out_connect			(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi);
static void parseau_out_fixup			(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
			      			 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi);
static void parseau_out_qos			(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi);
static gboolean parseau_play			(FilterInfo *fi);
static CORBA_Object parseau_create_filter	(PortableServer_POA poa, const char *iid,
					  	 gpointer impl_ptr, CORBA_Environment *ev);

/* Copied from Linux's isdn_audio.c */
/* ulaw -> signed 16-bit */
static const short isdn_audio_ulaw_to_s16[] =
{
	0x8284, 0x8684, 0x8a84, 0x8e84, 0x9284, 0x9684, 0x9a84, 0x9e84,
	0xa284, 0xa684, 0xaa84, 0xae84, 0xb284, 0xb684, 0xba84, 0xbe84,
	0xc184, 0xc384, 0xc584, 0xc784, 0xc984, 0xcb84, 0xcd84, 0xcf84,
	0xd184, 0xd384, 0xd584, 0xd784, 0xd984, 0xdb84, 0xdd84, 0xdf84,
	0xe104, 0xe204, 0xe304, 0xe404, 0xe504, 0xe604, 0xe704, 0xe804,
	0xe904, 0xea04, 0xeb04, 0xec04, 0xed04, 0xee04, 0xef04, 0xf004,
	0xf0c4, 0xf144, 0xf1c4, 0xf244, 0xf2c4, 0xf344, 0xf3c4, 0xf444,
	0xf4c4, 0xf544, 0xf5c4, 0xf644, 0xf6c4, 0xf744, 0xf7c4, 0xf844,
	0xf8a4, 0xf8e4, 0xf924, 0xf964, 0xf9a4, 0xf9e4, 0xfa24, 0xfa64,
	0xfaa4, 0xfae4, 0xfb24, 0xfb64, 0xfba4, 0xfbe4, 0xfc24, 0xfc64,
	0xfc94, 0xfcb4, 0xfcd4, 0xfcf4, 0xfd14, 0xfd34, 0xfd54, 0xfd74,
	0xfd94, 0xfdb4, 0xfdd4, 0xfdf4, 0xfe14, 0xfe34, 0xfe54, 0xfe74,
	0xfe8c, 0xfe9c, 0xfeac, 0xfebc, 0xfecc, 0xfedc, 0xfeec, 0xfefc,
	0xff0c, 0xff1c, 0xff2c, 0xff3c, 0xff4c, 0xff5c, 0xff6c, 0xff7c,
	0xff88, 0xff90, 0xff98, 0xffa0, 0xffa8, 0xffb0, 0xffb8, 0xffc0,
	0xffc8, 0xffd0, 0xffd8, 0xffe0, 0xffe8, 0xfff0, 0xfff8, 0x0000,
	0x7d7c, 0x797c, 0x757c, 0x717c, 0x6d7c, 0x697c, 0x657c, 0x617c,
	0x5d7c, 0x597c, 0x557c, 0x517c, 0x4d7c, 0x497c, 0x457c, 0x417c,
	0x3e7c, 0x3c7c, 0x3a7c, 0x387c, 0x367c, 0x347c, 0x327c, 0x307c,
	0x2e7c, 0x2c7c, 0x2a7c, 0x287c, 0x267c, 0x247c, 0x227c, 0x207c,
	0x1efc, 0x1dfc, 0x1cfc, 0x1bfc, 0x1afc, 0x19fc, 0x18fc, 0x17fc,
	0x16fc, 0x15fc, 0x14fc, 0x13fc, 0x12fc, 0x11fc, 0x10fc, 0x0ffc,
	0x0f3c, 0x0ebc, 0x0e3c, 0x0dbc, 0x0d3c, 0x0cbc, 0x0c3c, 0x0bbc,
	0x0b3c, 0x0abc, 0x0a3c, 0x09bc, 0x093c, 0x08bc, 0x083c, 0x07bc,
	0x075c, 0x071c, 0x06dc, 0x069c, 0x065c, 0x061c, 0x05dc, 0x059c,
	0x055c, 0x051c, 0x04dc, 0x049c, 0x045c, 0x041c, 0x03dc, 0x039c,
	0x036c, 0x034c, 0x032c, 0x030c, 0x02ec, 0x02cc, 0x02ac, 0x028c,
	0x026c, 0x024c, 0x022c, 0x020c, 0x01ec, 0x01cc, 0x01ac, 0x018c,
	0x0174, 0x0164, 0x0154, 0x0144, 0x0134, 0x0124, 0x0114, 0x0104,
	0x00f4, 0x00e4, 0x00d4, 0x00c4, 0x00b4, 0x00a4, 0x0094, 0x0084,
	0x0078, 0x0070, 0x0068, 0x0060, 0x0058, 0x0050, 0x0048, 0x0040,
	0x0038, 0x0030, 0x0028, 0x0020, 0x0018, 0x0010, 0x0008, 0x0000
};


static const char filter_iid[] = "OAFIID:gmf-filter-parse-au:9cbeaad9-f7a9-4eb5-9b66-c2ecc8af3a16";

static const OAFPluginObject plugin_list[] = {
	{
  		filter_iid,
		parseau_create_filter
	},
	
  	{
  		NULL
  	}
};

const OAFPlugin OAF_Plugin_info = {
	plugin_list,
	"AU Filter Stuff"
};


static CORBA_Object
parseau_create_filter(PortableServer_POA poa,
		      const char *iid,
		      gpointer impl_ptr,
		      CORBA_Environment *ev)
{
  GtkObject *filter, *mp;
  FilterInfo *fi;

  if(strcmp(iid, filter_iid))
    return CORBA_OBJECT_NIL;

  filter = gtk_object_new(gmf_filter_get_type(), "filter_type", GMF_Filter_PARSER, NULL);

  oaf_plugin_use (poa, impl_ptr);
  
  mp = gtk_object_new(gmf_media_position_get_type(), NULL);
  bonobo_object_add_interface(BONOBO_OBJECT(filter), BONOBO_OBJECT(mp));
  g_assert(filter && mp);

  fi = g_new0(FilterInfo, 1);
  fi->ev = &fi->evdat;
  CORBA_exception_init(fi->ev);
  gtk_signal_connect(filter, "get_pipe", GTK_SIGNAL_FUNC(parseau_get_pipe), fi);
  gtk_signal_connect(filter, "destroy", GTK_SIGNAL_FUNC(parseau_unref_factory), fi);
  gtk_signal_connect(filter, "get_attributes", GTK_SIGNAL_FUNC(parseau_get_attributes), fi);
  gtk_signal_connect(filter, "get_attribute", GTK_SIGNAL_FUNC(parseau_get_attribute), fi);
  gtk_signal_connect(filter, "set_attribute", GTK_SIGNAL_FUNC(parseau_set_attribute), fi);
  gtk_signal_connect(filter, "stop", GTK_SIGNAL_FUNC(parseau_stop), fi);
  gtk_signal_connect(filter, "pause", GTK_SIGNAL_FUNC(parseau_pause), fi);
  gtk_signal_connect(filter, "run", GTK_SIGNAL_FUNC(parseau_run), fi);
  gtk_signal_connect(filter, "deliver_event", GTK_SIGNAL_FUNC(parseau_deliver_event), fi);

  gtk_signal_connect(mp, "can_seek_forward", GTK_SIGNAL_FUNC(parseau_can_seek), fi);
  gtk_signal_connect(mp, "can_seek_backward", GTK_SIGNAL_FUNC(parseau_can_seek), fi);
  gtk_signal_connect(mp, "get_duration", GTK_SIGNAL_FUNC(parseau_get_duration), fi);
  gtk_signal_connect(mp, "get_position", GTK_SIGNAL_FUNC(parseau_get_position), fi);
  gtk_signal_connect(mp, "set_position", GTK_SIGNAL_FUNC(parseau_set_position), fi);
  gtk_signal_connect(mp, "get_stop_time", GTK_SIGNAL_FUNC(parseau_get_stop_time), fi);
  gtk_signal_connect(mp, "set_stop_time", GTK_SIGNAL_FUNC(parseau_set_stop_time), fi);

  fi->filter = (GMFFilter *)filter;
  fi->timeref = fi->filter->timeref;
  fi->cbid = 0;
  fi->playback_interval.tvSec = 0;
  fi->playback_interval.tvUsec = USEC_PER_SAMPLE;
  fi->presend.tvSec = 0;
  fi->presend.tvUsec = USEC_PER_SAMPLE;

	{
    		GMFPipe *default_pipe;

    		parseau_get_pipe(fi->filter, GMF_OUT, &default_pipe, fi);
    		gtk_object_set(GTK_OBJECT(default_pipe), "autorender", TRUE, NULL);
  	}

  impl_ptr = filter;

  return CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(filter)), fi->ev);
}

static void parseau_unref_factory(GtkObject *filter, FilterInfo *fi)
{
  CORBA_exception_free(fi->ev);
  g_free(fi);

  oaf_plugin_unuse(filter);
}

static void
parseau_get_attributes(GMFFilter *filter, GNOME_stringlist *retval, FilterInfo *fi)
{
  const char * const attr_names[] = {
    NULL
  };
  int i;

  for(i = 0; attr_names[i]; i++);

  retval->_buffer = CORBA_sequence_CORBA_string_allocbuf(i);
  for(i = 0; attr_names[i]; i++)
    retval->_buffer[i] = CORBA_string_dup((char *)attr_names[i]);
}

static void
parseau_get_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *ret_attr_val, FilterInfo *fi)
{  
}

static void
parseau_set_attribute(GMFFilter *filter, const char *attr_name, CORBA_any *attr_val, FilterInfo *fi)
{
}

static void
parseau_get_pipe(GMFFilter *filter, GMF_Direction pDirection, GMFPipe **out_pipe, FilterInfo *fi)
{
	GtkObject *apipe;
	PipeInfo *pi;
	GtkType ptype = GMF_OUT;

	switch(pDirection) {
		case GMF_IN:
			if(fi->inpipe) {
				/* We can only have one input pipe - any more would be totally confusing */				
				return; 
			}
			
    			ptype = gmf_input_pipe_get_type();
    			break;
    			
  		case GMF_OUT:
    			ptype = gmf_output_pipe_get_type();
    			break;
  	}

  	apipe = gtk_object_new (ptype, "filter", filter,
			 	"acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA,
			 	NULL);
  	g_assert(apipe);

  	pi = g_new0(PipeInfo, 1);

  	pi->fi = fi;
  	pi->apipe = (GMFPipe *)apipe;

  	switch(pDirection) {
  		case GMF_IN:
    			gtk_signal_connect(apipe, "can_process_type", parseau_in_can_process_type, pi);
    			gtk_signal_connect(apipe, "get_processable_types", parseau_in_get_processable_types, pi);
    			gtk_signal_connect(apipe, "receive_sample", parseau_in_receive_sample, pi);
    			gtk_signal_connect(apipe, "connect", parseau_in_connect, pi);
    			gtk_signal_connect(apipe, "disconnect", parseau_in_disconnect, pi);
    			gtk_signal_connect(apipe, "fixup_media_format", parseau_in_fixup, pi);
    			fi->inpipe = (GMFPipe *)apipe;
    			break;
    			
  		case GMF_OUT:
    			gtk_signal_connect(apipe, "can_process_type", parseau_out_can_process_type, pi);
    			gtk_signal_connect(apipe, "get_processable_types", parseau_out_get_processable_types, pi);
    			gtk_signal_connect(apipe, "connect", parseau_out_connect, pi);
    			gtk_signal_connect(apipe, "fixup_media_format", parseau_out_fixup, pi);
    			gtk_signal_connect(apipe, "quality_notify", parseau_out_qos, pi);
    			break;
  	}

  	gtk_object_set_data(apipe, "PipeInfo", pi);

  	gmf_filter_add_pipe(fi->filter, (GMFPipe *)apipe);

  	*out_pipe = (GMFPipe *)apipe;
}

static void
parseau_stop(GMFFilter *filter, FilterInfo *fi)
{
  GMF_MediaPosition_Value tmpval;

  parseau_pause(filter, fi);
  fi->last_sample.tvSec = fi->last_sample.tvUsec = 0;
  tmpval._d = GMF_MediaPosition_TIME;
  tmpval._u.value_time.tvSec =
    tmpval._u.value_time.tvUsec = 0;
  parseau_set_position(NULL, &tmpval, fi);
}

static void
parseau_pause(GMFFilter *filter, FilterInfo *fi)
{
  if(fi->cbid > 0) {
    gmf_time_reference_remove_request(fi->timeref, fi->cbid);
    fi->cbid = 0;
  }

  fi->isDiscontinuity = TRUE;
}

static gboolean
parseau_play(FilterInfo *fi)
{
  GMF_Sample samp;
  guint nbytes;
  GMF_Data *thedata;
  guchar *ctmp, *ctmps, *ctmps_end;

  if(fi->filter->state != GMF_Filter_RUNNING)
    return FALSE;

  samp.sInfo.streamStartTime = fi->last_sample;
  samp.sInfo.streamEndTime = fi->last_sample = gmf_time_add(fi->last_sample, fi->playback_interval);

  {
    GMF_TimeVal ttmp;
    gdouble interval;

    ttmp = gmf_time_subtract(samp.sInfo.streamEndTime, samp.sInfo.streamStartTime);

    interval = ((gdouble)ttmp.tvSec) + ((gdouble)ttmp.tvUsec)/1000000;

    nbytes = (guint32)(interval * (gdouble)fi->au_header.sample_rate * (gdouble)fi->au_header.in_bytes_per_sample
		       * (gdouble)fi->au_header.nchannels);

    g_assert(!(nbytes % fi->au_header.in_bytes_per_sample));
    g_assert(nbytes > 0);
  }

  if(
#if 0
     (fi->stopTime.tvSec && fi->stopTime.tvUsec && (gmf_time_compare(fi->stopTime, samp.sInfo.streamStartTime) >= 0)) || 
#endif
     (fi->au_header.data_position >= fi->au_header.data_size)) {
    g_list_foreach(fi->filter->outputPipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &samp.sInfo.streamEndTime);
    return FALSE;
  }

  GMF_DataReader_read(fi->data_reader, nbytes, &thedata, fi->ev);
  if(fi->ev->_major != CORBA_NO_EXCEPTION) {
    g_warning("Couldn't read data");
    return FALSE;
  }

  samp.sInfo.mediaStartTime.tvUsec = fi->au_header.data_position;
  samp.sInfo.mediaEndTime.tvUsec = fi->au_header.data_position + thedata->_length;
  samp.sInfo.mediaStartTime.tvSec = samp.sInfo.mediaEndTime.tvSec = 0;
  samp.sInfo.isSyncPoint = CORBA_TRUE;
  samp.sInfo.isPreroll = CORBA_FALSE;
  samp.sInfo.isDiscontinuity = fi->isDiscontinuity; fi->isDiscontinuity = FALSE;

  samp.sInfo.actualDataLength = samp.sData._length = nbytes
    * fi->au_header.bytes_per_sample
    / fi->au_header.in_bytes_per_sample;

  samp.sData._buffer = alloca(samp.sData._length);
  CORBA_sequence_set_release(&samp.sData, CORBA_FALSE);

  for(ctmp = samp.sData._buffer, ctmps = thedata->_buffer, ctmps_end = ctmps + nbytes; ctmps < ctmps_end;) {
    int j;
      
    for(j = 0; j < fi->au_header.nchannels; j++) {
      switch(fi->au_header.encoding) {
      case ISDN_ULAW_8:
	{
	  *((gint16 *)ctmp) = isdn_audio_ulaw_to_s16[*((guint8 *)ctmps)];
	  ctmp += 2;
	  ctmps++;
	}
	break;
      case LINEAR_PCM_8:
	*((gint8 *)ctmp) = *((gint8 *)ctmps);
	ctmp++;
	ctmps++;
	break;
      case LINEAR_PCM_16:
	*((gint16 *)ctmp) = ntohs(*((gint16 *)ctmps));
	ctmp += 2;
	ctmps += 2;
	break;
      case LINEAR_PCM_24:
	g_error("LINEAR_PCM_24 - strange format");
	break;
      case LINEAR_PCM_32:
	*((gint32 *)ctmp) = ntohl(*((gint32 *)ctmps));
	ctmp += 4;
	ctmps += 4;
	break;
      case IEEE_FP_32:
	{
	  union {
	    gfloat float_val;
	    gint32 int_val;
	  } u;

	  g_assert(sizeof(gfloat) == 4);

	  u.int_val = ntohl(*((gint32 *)ctmps));;
	  *((gint32 *)ctmp) = u.float_val * UINT_MAX;

	  ctmp += 4;
	  ctmps += 4;
	}
	break;
      case IEEE_FP_64:
	g_error("IEEE_FP_64 - strange format.");
	break;
      default:
	g_assert_not_reached();
	break;
      }
    }
  }

  g_list_foreach(fi->filter->outputPipes, (GFunc)gmf_output_pipe_send_sample, &samp);

  fi->au_header.data_position += nbytes;

  CORBA_free(thedata);

  {
    GMF_TimeVal nextplay;

    nextplay = gmf_time_add(fi->last_sample, fi->presend);
    fi->cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase, &nextplay, (GSourceFunc)parseau_play, fi);
  }

  return FALSE;
}

static void
parseau_run(GMFFilter *filter, FilterInfo *fi)
{
  parseau_pause(filter, fi);

  if(fi->read_au_header)
    fi->cbid = gmf_time_reference_request_alarm(fi->timeref, &fi->filter->timeBase,
						&fi->last_sample, (GSourceFunc)parseau_play, fi);
}

static void
parseau_deliver_event(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData, FilterInfo *fi)
{
}

static void
parseau_can_seek(GMFMediaPosition *mp, gboolean *retval, FilterInfo *fi)
{
  *retval = TRUE;
}

static void
parseau_get_duration(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  double interval;
  double ip;

  g_return_if_fail(fi->read_au_header);

  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    value->_d = posType;

    value->_u.value_samples = ((double)fi->au_header.data_size)
      / (((double)fi->au_header.in_bytes_per_sample)
	 * ((double)fi->au_header.nchannels));
    break;
  case GMF_MediaPosition_TIME:
  default:
    value->_d = GMF_MediaPosition_TIME;
    interval = ((double)fi->au_header.data_size)
      / (((double)fi->au_header.in_bytes_per_sample)
	 * ((double)fi->au_header.nchannels)
	 * ((double)fi->au_header.sample_rate));
    
    value->_u.value_time.tvUsec = (modf(interval, &ip) * 1000000.0);
    value->_u.value_time.tvSec = floor(ip);
    break;
  }
}

static void
parseau_get_position(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  double interval;
  double ip;

  g_return_if_fail(fi->read_au_header);

  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    value->_d = posType;

    value->_u.value_samples = ((double)fi->au_header.data_position)
      / (((double)fi->au_header.in_bytes_per_sample)
	 * ((double)fi->au_header.nchannels));
    break;
  case GMF_MediaPosition_TIME:
  default:
    value->_d = GMF_MediaPosition_TIME;
    interval = ((double)fi->au_header.data_position)
      / (((double)fi->au_header.in_bytes_per_sample)
	 * ((double)fi->au_header.nchannels)
	 * ((double)fi->au_header.sample_rate));
    
    value->_u.value_time.tvUsec = (modf(interval, &ip) * 1000000.0);
    value->_u.value_time.tvSec = floor(ip);
    break;
  }
}

static void
parseau_set_position(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  GMF_TimeVal ttmp;
  gdouble interval;

  switch(value->_d) {
  case GMF_MediaPosition_SAMPLES:
    fi->au_header.data_position = value->_u.value_samples;
    fi->au_header.data_position *= fi->au_header.in_bytes_per_sample;
    fi->au_header.data_position *= fi->au_header.nchannels;
    break;
  case GMF_MediaPosition_TIME:
    ttmp = value->_u.value_time;
  
    interval = ((gdouble)ttmp.tvSec) + ((gdouble)ttmp.tvUsec)/1000000.0;
  
    fi->au_header.data_position = (guint32)(interval * (gdouble)fi->au_header.sample_rate);

    fi->au_header.data_position *= fi->au_header.in_bytes_per_sample;
    fi->au_header.data_position *= fi->au_header.nchannels;
    break;
  }

  if(fi->au_header.data_position > fi->au_header.data_size)
    g_list_foreach(fi->filter->outputPipes, (GFunc)gmf_output_pipe_deliver_end_of_stream, &fi->stopTime);

  GMF_DataReader_set_position(fi->data_reader, fi->au_header.data_position + fi->au_header.data_offset,
			      GMF_DataReader_ABSOLUTE, fi->ev);

  fi->isDiscontinuity = TRUE;
}

static void
parseau_get_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Type posType, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  if(!(fi->stopTime.tvSec || fi->stopTime.tvUsec)) {
    parseau_get_duration(mp, posType, value, fi);
    return;
  }
    
  switch(posType) {
  case GMF_MediaPosition_SAMPLES:
    {
      gdouble interval;

      value->_d = posType;
      interval = ((gdouble)fi->stopTime.tvSec) + ((gdouble)fi->stopTime.tvUsec)/1000000.0;
      value->_u.value_samples = interval * ((gdouble)fi->au_header.sample_rate);
    }
    break;
  case GMF_MediaPosition_TIME:
  default:
    value->_d = GMF_MediaPosition_TIME;
    value->_u.value_time = fi->stopTime;
    break;
  }
}

static void
parseau_set_stop_time(GMFMediaPosition *mp, GMF_MediaPosition_Value *value, FilterInfo *fi)
{
  switch(value->_d) {
  case GMF_MediaPosition_SAMPLES:
    {
      gdouble interval, ip;

      interval = ((gdouble)fi->au_header.sample_rate) * ((gdouble)value->_u.value_samples);

      fi->stopTime.tvUsec = (modf(interval, &ip) * 1000000.0);
      fi->stopTime.tvSec = floor(ip);
    }
    break;
  case GMF_MediaPosition_TIME:
    value->_d = GMF_MediaPosition_TIME;
    fi->stopTime = value->_u.value_time;
    break;
  default:
    g_assert_not_reached();
    break;
  }
}

static void
parseau_in_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_DATA)
    return;

  if(strcmp(type_info->minorType, "audio/basic")
     && strcmp(type_info->minorType, "audio/x-ulaw")
     && strcmp(type_info->minorType, "*"))
    return;

  *retval = TRUE;
}

static void
parseau_in_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;
  int i;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 2;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  i = 0;

  ti = &list->_buffer[i++];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_DATA;
    ti->minorType = CORBA_string_dup("audio/basic");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->typeData._value = NULL;
    CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._value = NULL;
    CORBA_any_set_release(&ti->formatData, CORBA_FALSE);
  }

  ti = &list->_buffer[i++];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_DATA;
    ti->minorType = CORBA_string_dup("audio/x-ulaw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->typeData._value = NULL;
    CORBA_any_set_release(&ti->typeData, CORBA_FALSE);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._value = NULL;
    CORBA_any_set_release(&ti->formatData, CORBA_FALSE);
  }

  *out_typelist = list;
}

static void
parseau_in_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
	pi->fi->data_reader = Bonobo_Unknown_query_interface(cnxinfo->connectedTo, "IDL:GMF/DataReader:1.0", pi->fi->ev);
	if(pi->fi->ev->_major != CORBA_NO_EXCEPTION) {
  		pi->fi->data_reader = CORBA_OBJECT_NIL;
  	}
  	parseau_read_au_header(pi->fi);
}

static void
parseau_in_disconnect(GMFPipe *apipe, PipeInfo *pi)
{
	pi->fi->read_au_header = FALSE;

	/* XXX we should unref the object here */
  	CORBA_Object_release(pi->fi->data_reader, pi->fi->ev);
  	pi->fi->data_reader = CORBA_OBJECT_NIL;
}

static void
parseau_in_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		 GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? I guess so. */
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_ACCEPTED:
    *retval = GMF_Pipe_ACCEPTED;
    break;
  }
}

static void
parseau_in_receive_sample(GMFPipe *apipe, GMF_Sample *sample, PipeInfo *pi)
{
  /* And what do I do with this? */
  g_warning("We got receive_sample in a parse filter. This makes no sense.");
}

static gint
parseau_read_au_header(FilterInfo *fi)
{
	GMF_Data *rv = NULL;
	gint retval = -1;
	guint32 *iptmp;

	if(fi->read_au_header)
		return 0;

	if(CORBA_Object_is_nil(fi->data_reader, fi->ev) || fi->ev->_major != CORBA_NO_EXCEPTION)
		goto out;

	GMF_DataReader_read(fi->data_reader, AU_HEADER_LEN, &rv, fi->ev);

	if(fi->ev->_major != CORBA_NO_EXCEPTION) {
		rv = NULL;
		goto out;
	}

	if(rv->_length != AU_HEADER_LEN)
		goto out;

	/* got all data, now parse the header */
	iptmp = (guint32 *)rv->_buffer;
	if (ntohl(*(iptmp + 0)) != 0x2e736e64 /* ".snd" */ )
		goto out;

	fi->au_header.data_offset = ntohl(*(iptmp + 1));
	if(fi->au_header.data_offset < 24)
		goto out;

	fi->au_header.data_size = ntohl(*(iptmp + 2));
	fi->au_header.encoding = ntohl(*(iptmp + 3));
	fi->au_header.sample_rate = ntohl(*(iptmp + 4));
	fi->au_header.nchannels = ntohl(*(iptmp + 5));
	if(fi->au_header.nchannels > 2) /* Don't support more than two channels (stereo) */
		goto out;

	switch(fi->au_header.encoding) {
		case ISDN_ULAW_8:
			fi->au_header.bytes_per_sample = 2;
			fi->au_header.in_bytes_per_sample = 1;
			break;
			
  		case LINEAR_PCM_8:
    			fi->au_header.bytes_per_sample =
      			fi->au_header.in_bytes_per_sample = 1;
    			break;
    			
  		case LINEAR_PCM_16:
    			fi->au_header.in_bytes_per_sample =
      			fi->au_header.bytes_per_sample = 2;
    			break;
    			
  		case LINEAR_PCM_24:
    			fi->au_header.in_bytes_per_sample =
      			fi->au_header.bytes_per_sample = 3;
    			break;
    			
  		case LINEAR_PCM_32:
    			fi->au_header.in_bytes_per_sample =
      			fi->au_header.bytes_per_sample = 4;
    			break;
    			
  		case IEEE_FP_32:
    			fi->au_header.in_bytes_per_sample =
      			fi->au_header.bytes_per_sample = 4;
    			break;
    			
  		case IEEE_FP_64:
    			fi->au_header.in_bytes_per_sample =
      			fi->au_header.bytes_per_sample = 8;
    			break;
    			
  		case ISDN_ULAW_ADPCM_8:
    			/* We don't handle compression (yet). */
    			fi->au_header.in_bytes_per_sample =
      			fi->au_header.bytes_per_sample = 1;
    			break;
    			
  		default:
    			goto out;
    			break;
	}
  
  	fi->au_header.bytes_per_sample *= fi->au_header.nchannels;
  	fi->au_header.in_bytes_per_sample *= fi->au_header.nchannels;
  	fi->au_header.data_position = 0;

  	g_message("AU type: encoding %d, sample_rate %d, nchannels %d, bytes_per_sample %d, in_bytes_per_sample %d",
	    fi->au_header.encoding, fi->au_header.sample_rate, fi->au_header.nchannels, fi->au_header.bytes_per_sample,
	    fi->au_header.in_bytes_per_sample);

	retval = 0; /* Looks OK! */
	fi->read_au_header = TRUE;

	GMF_DataReader_set_position(fi->data_reader, fi->au_header.data_offset, GMF_DataReader_ABSOLUTE, fi->ev);

	out:
		CORBA_free(rv);

  	return retval;
}

static void
parseau_out_can_process_type(GMFPipe *apipe, GMF_MediaTypeInfo *type_info,
			gboolean *retval, PipeInfo *pi)
{
  if(type_info->majorType != GMF_MEDIA_AUDIO_DIGITAL)
    return;

  if(strcmp(type_info->minorType, "audio/raw"))
    return;

  *retval = TRUE;
}

static void
parseau_out_get_processable_types(GMFPipe *apipe, GMF_MediaTypeInfoList **out_typelist, PipeInfo *pi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  ti = &list->_buffer[0];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_AUDIO_DIGITAL;
    ti->minorType = CORBA_string_dup("audio/raw");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, pi->fi->ev);
  }

  *out_typelist = list;
}

static void
parseau_out_connect(GMFPipe *apipe, GMF_Pipe_ConnectionInfo *cnxinfo, PipeInfo *pi)
{
}

static GMF_Pipe_FormatAcceptance
parseau_out_can_handle(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  if(CORBA_TypeCode_equal(typeinfo->formatData._type, (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat, pi->fi->ev)) {
    GMF_Media_AudioDigitalFormat *adf;
    
    adf = typeinfo->formatData._value;
    
    if((adf->sample_rate == pi->fi->au_header.sample_rate)
       && (adf->nchannels == pi->fi->au_header.nchannels)
       && (adf->nbits == (pi->fi->au_header.bytes_per_sample / adf->nchannels))
       && (adf->byte_order_be == CORBA_FALSE))
      return GMF_Pipe_ACCEPTED;
    else
     return GMF_Pipe_CANNOT_HANDLE;
  } else
    return GMF_Pipe_CANNOT_HANDLE;
}

static void
parseau_out_fill(GMF_MediaTypeInfo *typeinfo, PipeInfo *pi)
{
  GMF_Media_AudioDigitalFormat *adf;

  if(CORBA_any_get_release(&typeinfo->formatData))
    CORBA_free(typeinfo->formatData._value);
  CORBA_Object_release((CORBA_Object)typeinfo->formatData._type, pi->fi->ev);
  typeinfo->formatData._type = (CORBA_TypeCode)TC_GMF_Media_AudioDigitalFormat;
  adf = typeinfo->formatData._value = GMF_Media_AudioDigitalFormat__alloc();
  CORBA_any_set_release(&typeinfo->formatData, CORBA_TRUE);
  
  adf->sample_rate = pi->fi->au_header.sample_rate;
  adf->nchannels = pi->fi->au_header.nchannels;
  adf->nbits = (pi->fi->au_header.bytes_per_sample / adf->nchannels) * 8;
  adf->byte_order_be = CORBA_FALSE;
}

static void
parseau_out_fixup(GMFPipe *apipe, GMF_MediaTypeInfo *typeinfo, GMF_Pipe_FormatAcceptance other_end,
		  GMF_Pipe_FormatAcceptance *retval, PipeInfo *pi)
{
  if(!pi->fi->read_au_header) {
    *retval = GMF_Pipe_CANNOT_HANDLE;
    return;
  }

  switch(other_end) {
  case GMF_Pipe_CANNOT_HANDLE:
    /* Second opinion? */
    *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_UNKNOWN:
    /* Fill in the blanks */
    parseau_out_fill(typeinfo, pi);
    *retval = GMF_Pipe_ACCEPTED;
    break;
  case GMF_Pipe_TRY_MINE:
    /* Can we handle this? */
    if(parseau_out_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else
      *retval = GMF_Pipe_CANNOT_HANDLE;
    break;
  case GMF_Pipe_ACCEPTED:
    /* Can we handle this? */
    if(parseau_out_can_handle(typeinfo, pi) == GMF_Pipe_ACCEPTED)
      *retval = GMF_Pipe_ACCEPTED;
    else {
      parseau_out_fill(typeinfo, pi);
      *retval = GMF_Pipe_TRY_MINE;
    }
    break;
  }
}

static void
parseau_out_qos(GMFOutputPipe *apipe, GMF_QualityControl_Quality *qosinfo, PipeInfo *pi)
{
  if(gmf_time_compare(pi->fi->presend, qosinfo->lag) < 0) {
    pi->fi->presend = qosinfo->lag;
  } else {
    GList *ltmp;
    GMF_TimeVal ttmp = qosinfo->lag;

    for(ltmp = pi->fi->filter->outputPipes; ltmp; ltmp = g_list_next(ltmp)) {
      if(gmf_time_compare(ttmp, ((GMFOutputPipe *)(ltmp->data))->last_qosinfo.lag) < 0)
	ttmp = ((GMFOutputPipe *)(ltmp->data))->last_qosinfo.lag;
    }

    pi->fi->presend = ttmp;
  }

  /* For now we just ignore rate_proportion */

#if 0
  if(gmf_time_compare(pi->fi->presend, zero) > 0) {
    gdouble sample_size, interval;
    /* Try and get into the green again */

    interval = ((gdouble)pi->fi->playback_interval.tvSec) + ((gdouble)pi->fi->playback_interval.tvUsec)/1000000.0;

    sample_size = (interval * (gdouble)pi->fi->au_header.sample_rate * (gdouble)pi->fi->au_header.in_bytes_per_sample);

    pi->fi->au_header.data_position += (sample_size * 3);

    pi->fi->last_sample = gmf_time_add(pi->fi->last_sample, pi->fi->playback_interval);
    pi->fi->last_sample = gmf_time_add(pi->fi->last_sample, pi->fi->playback_interval);
    pi->fi->last_sample = gmf_time_add(pi->fi->last_sample, pi->fi->playback_interval);
  }
#endif
}
