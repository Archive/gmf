#include <gnome.h>



#include <gmf.h>
#include <bonobo.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct {
  CORBA_ORB orb;
  CORBA_Environment *ev;
  GMFTimeReference *timeref;
  BonoboGenericFactory *factory;
  CORBA_Object gnumeric_handle;
} StatsInfo;

typedef struct {
  StatsInfo *ai;
  GMFFilter *filter;
  CORBA_Object sheet_handle;
  GList *value_queue;
  GList *velocity_queue;
  GnomeCanvasItem *keys[88];
  gboolean keyon[88];
  GtkWidget *canvas;
} FiltInfo;

static void 
show_key(FiltInfo *info, int key)
{
  gnome_canvas_item_show( info->keys[key] );
}

static void 
hide_key(FiltInfo *info, int key)
{
  gnome_canvas_item_hide( info->keys[key] );
}

static void
create_canvas(FiltInfo *info)
{
  GdkImlibImage *image;
  GtkWidget *app;
  GnomeCanvasItem *item;
  int i;
  gchar *filename;

  info->canvas = gnome_canvas_new();
  gtk_widget_set_usize (info->canvas, 520, 40);
  gnome_canvas_set_scroll_region ( GNOME_CANVAS( info->canvas ),
				   0, 0,
				   520, 40 );

  filename = gnome_pixmap_file( "keyboard.png" );
  g_assert( filename != NULL );
  image = gdk_imlib_load_image( filename );
  g_free( filename );
  g_assert (image != NULL);
  item = gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS(info->canvas) ),
				gnome_canvas_image_get_type (),
				"image", image,
				"x", 0.0,
				"y", 0.0,
				"width", 520.0,
				"height", 40.0,
				"anchor", GTK_ANCHOR_NW,
				NULL );

  for ( i = 0; i < 88; i++ )
    {
      char short_filename[10];/* Check this if you change the next line at all. */
      sprintf( short_filename, "key%02d.png", i );
      filename = gnome_pixmap_file( short_filename );
      g_assert( filename != NULL );
      image = gdk_imlib_load_image( filename );
      g_free( filename );
      g_assert (image != NULL);
      info->keys[i] = gnome_canvas_item_new( gnome_canvas_root( GNOME_CANVAS( info->canvas) ),
				       gnome_canvas_image_get_type(),
				       "image", image,
				       "x", 0.0,
				       "y", 0.0,
				       "width", 520.0,
				       "height", 40.0,
				       "anchor", GTK_ANCHOR_NW,
				       NULL);
      hide_key(info, i);
    }


#if 1
  app = gtk_window_new ( GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title( GTK_WINDOW(app), "Keyboard" );
#else
  app = gnome_app_new( "Keyboard", "Keyboard" );
#endif
  gtk_container_add( GTK_CONTAINER( app ), info->canvas );
  gtk_widget_show_all( app );
}


static BonoboObject *stats_create_filter(BonoboGenericFactory *factory, const char *iid, gpointer data);

static void stats_in_get_pipe(GMFFilter *filter,
				  GMF_Direction pDirection,
				  GMFPipe **pipe,
				  FiltInfo *fi);

int main(int argc, char *argv[])
{
  	CORBA_Environment ev;
  	StatsInfo appinfo;
  	static volatile int barrier = -1;

  	if(barrier == -1) barrier = GPOINTER_TO_INT(getenv("STATISTICS_MAIN_BARRIER"));
  		while(barrier);

  	CORBA_exception_init(&ev);
  
  	appinfo.ev = &ev;

	gnome_init_with_popt_table("keyboard", VERSION, 
				   argc, argv,
				   oaf_popt_options, 0, NULL); 
  	appinfo.orb = oaf_init (argc, argv);
	gmf_init(appinfo.orb);

  	appinfo.timeref = GMF_TIME_REFERENCE(gmf_time_reference_new());
  	appinfo.factory = bonobo_generic_factory_new_multi (("OAFIID:keyboard:1102eeec-6e81-4d5a-8aa2-059471934bf2"),
  				stats_create_filter, NULL);

  	bonobo_main ();

  	return 0;
}

static BonoboObject *
stats_create_filter(BonoboGenericFactory *factory, const char *iid, gpointer data)
{
	GMFFilter *new_filter;
	GMF_Filter_Type ftype;
	FiltInfo *fi;
	int i;
	CORBA_Environment ev;
	BonoboObject *object;

	if(!strcmp(iid, "OAFIID:keyboard:1102eeec-6e81-4d5a-8aa2-059471934bf2")) {
		ftype = GMF_Filter_RENDERER;
	} else {
		return NULL; /* Can't activate anything else */
	}

	CORBA_exception_init (&ev);
	new_filter = GMF_FILTER(gtk_object_new(gmf_filter_get_type(), "filter_type", ftype, NULL));
		g_assert(new_filter);

	fi = g_new0(FiltInfo, 1);
	if(!strcmp(iid, "gmf-filter-keyboard")) {
		gtk_signal_connect(GTK_OBJECT(new_filter), "get_pipe",
				   GTK_SIGNAL_FUNC(stats_in_get_pipe), fi);
  	} else
    		g_assert(!"Not reached!");

  	create_canvas (fi);

  	for ( i = 0; i < 88; i++ )
    	{
      		fi->keyon[i] = FALSE;
    	}

  	fi->filter = new_filter;

	object = CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(new_filter)), &ev);

	CORBA_exception_free (&ev);

	return object;
}

static void stats_pipe_can_process(GMFPipe *pipe, GMF_MediaTypeInfo *type_info,
				 gboolean *retval)
{
  /* We can handle anything */

  *retval = TRUE;
}

static void stats_pipe_processable_types(GMFPipe *pipe,
				       GMF_MediaTypeInfoList **out_typelist,
				       FiltInfo *fi)
{
  GMF_MediaTypeInfoList *list;
  GMF_MediaTypeInfo *ti;

  list = GMF_MediaTypeInfoList__alloc();
  list->_length = 1;
  list->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(list->_length);

  ti = &list->_buffer[0];
  {
    memset(ti, 0, sizeof(*ti));
    ti->majorType = GMF_MEDIA_DATA;
    ti->minorType = CORBA_string_dup("*");
    ti->typeData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, fi->ai->ev);
    ti->formatData._type = (CORBA_TypeCode)CORBA_Object_duplicate((CORBA_Object)TC_null, fi->ai->ev);
  }

  *out_typelist = list;
}

static gint
gmf_midi_event_len(guchar *data, gulong data_len)
{
  guint retval = 0;
  guchar first_byte;

  g_return_val_if_fail(data_len > 0, -1);

  first_byte = data[0];

  switch(first_byte & 0xF0) {
  case 0xF0:
    switch(first_byte) {
    case 0xF7:
    case 0xF8:
    case 0xF9:
    case 0xFA:
    case 0xFB:
    case 0xFC:
    case 0xFE:
    case 0xFF:
      goto onebyteop;
    case 0xF1:
    case 0xF3:
      goto twobyteop;
    case 0xF0:
      { /* handle the dreadful sysex */
	int i;
	for(i = 1;
	    (i < data_len) && (data[i] & 0x80);
	    i++) /**/ ;
	
	if(data[i] & 0x80) {

	  if(data[i] == 0xF7)
	    i++; /* If they put an EOX byte in, then it
		    is part of the current message */

	  return i;
	} else
	  return -1;
      }
    break;
    default:
      break;
    }
  case 0x90:
  case 0x80:
  case 0xA0:
  case 0xB0:
  case 0xD0:
  case 0xE0:
    retval++;
  twobyteop:
  case 0xC0:
    retval++;
  onebyteop:
  default:
    retval++;
  }

  if(retval <= data_len)
    return retval;
  else
    return -1;
}

static void stats_pipe_in_handle_sample(GMFPipe *pipe,
					GMF_Sample *sample,
					gboolean must_copy,
					FiltInfo *fi)
{
  guint command;
  guint lagval, velval;
  guint channel;
  unsigned char *curptr, *endptr;
  unsigned int elen;

  curptr = sample->sData._buffer;
  endptr = curptr + sample->sData._length;
  
  while(curptr < endptr)
    {
      elen = gmf_midi_event_len(curptr, endptr - curptr);

      command = curptr[0] & 0xf0;
      if ( command == 0x90 || command == 0x80 )
	{
	  channel = curptr[0] & 0x0f;
	  lagval = curptr[1] - 21;
	  velval = curptr[2];
	  
	  if ( command == 0x90 && velval != 0 )
	    {
	      show_key( fi, lagval );
	    }
	  else
	    {
	      hide_key( fi, lagval );
	    }
	}
      curptr += elen;
    }

#if 0
  curtime = gmf_time_reference_current_time(fi->ai->timeref);
  thislag = gmf_time_subtract(curtime, sample->sInfo.mediaStartTime);

  lagval = (thislag.tvSec * 1000000) + thislag.tvUsec;
#endif
#if 0
  if ( fi->keyon[lagval] )
    {
      fi->keyon[lagval] = FALSE;
      hide_key( fi, lagval );
    }
  else
    {
      fi->keyon[lagval] = TRUE;
      show_key( fi, lagval );
    }
#endif
#if 0
  if ( velval < 20 )
    {
      hide_key( fi, lagval );
    }
  else
    {
      show_key( fi, lagval );
    }
#endif
}

static void stats_in_get_pipe(GMFFilter *filter,
			      GMF_Direction pDirection,
			      GMFPipe **pipe,
			      FiltInfo *fi)
{
  GMFPipe *newpipe;

  g_return_if_fail(pDirection == GMF_IN);

  newpipe = GMF_PIPE(gtk_object_new(gmf_input_pipe_get_type(),
				    "filter", filter,
				    "acceptable_transports", GMF_Transport_UNIX_SOCKETS|GMF_Transport_CORBA));

  g_assert(newpipe);

  gtk_signal_connect(GTK_OBJECT(newpipe), "can_process_type",
		     GTK_SIGNAL_FUNC(stats_pipe_can_process), fi);
  gtk_signal_connect(GTK_OBJECT(newpipe), "get_processable_types",
		     GTK_SIGNAL_FUNC(stats_pipe_processable_types), fi);

  gtk_signal_connect(GTK_OBJECT(newpipe), "receive_sample",
		     GTK_SIGNAL_FUNC(stats_pipe_in_handle_sample), fi);

  *pipe = newpipe;
}
