/* This example (theoretically) acts as a rendering bridge to esd. */
#include <gmf.h>
#include <esd.h>

typedef struct {
  GMFFilter *filter;
  GMFTimeReference *timeref;
} FEInfo;

static void fe_make_pipe(GMFFilter *filter, GMF_Direction pdir,
			 GMFPipe **retpipe, FEInfo *fei);
static gboolean fe_receive_sample(GMFPipe *pipe, GMF_Sample *sdata,
				  gboolean must_copy,
				  FEInfo *fei);
static gboolean fe_can_process_type(GMFPipe *pipe, GMF_MediaTypeInfo *type_info,
				    gboolean *retval,
				    FEInfo *fei);
static gboolean fe_get_processable_types(GMFPipe *pipe,
					 GMF_MediaTypeInfoList **out_typelist,
					 FEInfo *fei);

static const struct poptOption options[] = {
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

CORBA_Environment ev;

int main(int argc, char *argv[])
{
	CORBA_ORB orb;
	poptContext popt_context;
	GtkObject *filt;
	FEInfo fei;
	
	CORBA_exception_init (&ev);
	
	gnome_init_with_popt_table ("filter-example", VERSION,
				    argc, argv, options, 0,
				    &popt_context);

	orb = oaf_init (argc, argv);
	gmf_init(orb);

	filt = gmf_filter_new (GMF_Filter_RENDERER, "filter-example");
	//goad_server_register (CORBA_OBJECT_NIL, GMF_FILTER(filt)->corba_object,
	//	       "filter-example", "server", &ev);

	fei.filter = GMF_FILTER (filt);
	fei.timeref = GMF_TIME_REFERENCE (gmf_time_reference_new());

	gtk_signal_connect (filt, "get_pipe", GTK_SIGNAL_FUNC (fe_make_pipe), &fei);

	gtk_main ();

	CORBA_exception_free (&ev);

	return 0;
}

static void
fe_make_pipe (GMFFilter *filter, GMF_Direction pdir,
	     GMFPipe **retpipe, FEInfo *fei)
{
	GtkObject *newpipe;
	
	/*FIXME
	 *newpipe = gmf_pipe_new(filter, pdir,
			 GMF_Transport_CORBA|GMF_Transport_UNIX_SOCKETS);
	*/
  	gtk_signal_connect(newpipe, "receive_sample",
		     GTK_SIGNAL_FUNC(fe_receive_sample),
		     fei);
  	gtk_signal_connect(newpipe, "can_process_type",
		     GTK_SIGNAL_FUNC(fe_can_process_type),
		     fei);
  	gtk_signal_connect(newpipe, "get_processable_types",
		     GTK_SIGNAL_FUNC(fe_get_processable_types),
		     fei);

	*retpipe = GMF_PIPE(newpipe);
}

typedef struct {
  GMF_Sample samp;
  GMFPipe *pipe;
} FESample;

static gboolean
fe_play_sample(FESample *samp)
{
  int esd_cnx;

  esd_cnx = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(samp->pipe),
						"esd_cnx"));

  write(esd_cnx, samp->samp.sData._buffer, samp->samp.sData._length);

  CORBA_free(samp->samp.sData._buffer);
  g_free(samp);
  return FALSE;
}

static void
fe_queue_sample(GMFPipe *pipe, GMF_Sample *sdata,
		gboolean must_copy,
		FEInfo *fei)
{
  FESample *fes;
  GMF_TimeVal base = {0,0};

  fes = g_new(FESample, 1);
  fes->pipe = pipe;
  fes->samp = *sdata;

  if(must_copy) {
    fes->samp.sData._buffer =
      CORBA_sequence_CORBA_octet_allocbuf(fes->samp.sData._length);
    memcpy(fes->samp.sData._buffer, sdata->sData._buffer,
	   fes->samp.sData._length);
  } /* else (not needed)
       fes->samp.sData._buffer = sdata->sData._buffer; */

  gmf_time_reference_request_alarm(fei->timeref, &base,
				   &sdata->sInfo.mediaStartTime,
				   (GSourceFunc)fe_play_sample, fes);
}

static gboolean
fe_receive_sample(GMFPipe *pipe, GMF_Sample *sdata, gboolean must_copy,
		  FEInfo *fei)
{
  GMF_TimeVal now, diff;
  int esd_cnx;

  esd_cnx = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(pipe), "esd_cnx"));

  if(!esd_cnx) {
    esd_cnx = esd_play_stream(ESD_BITS16|ESD_STEREO, 44100, NULL,
			      "filter-example");
    gtk_object_set_data(GTK_OBJECT(pipe), "esd_cnx", GINT_TO_POINTER(esd_cnx));
  }

  now = gmf_time_reference_current_time(fei->timeref);
  diff = gmf_time_subtract(sdata->sInfo.mediaStartTime, now);
  if(diff.tvSec > 0) {
    /* queue it for later. Why do we have to bother with queuing?
       Because the source might send something to us ahead of time,
       to help decrease rendering latency.
    */
    fe_queue_sample(pipe, sdata, must_copy, fei);
  } else { /* play it */
    write(esd_cnx, sdata->sData._buffer, sdata->sData._length);
  }

  return TRUE;
}

static gboolean
fe_can_process_type(GMFPipe *pipe, GMF_MediaTypeInfo *type_info,
		    gboolean *retval,
		    FEInfo *fei)
{
  if(type_info->majorType == GMF_MEDIA_AUDIO_DIGITAL
     && !strcmp(type_info->minorType, "audio/wav")
     /* XXX need to check the type & format data :\ */)
    *retval = TRUE;
  
  return TRUE; /* this is of no consequence afaik */
}

static gboolean
fe_get_processable_types(GMFPipe *pipe,
			 GMF_MediaTypeInfoList **out_typelist,
			 FEInfo *fei)
{
	if(!*out_typelist) {
		GMF_MediaTypeInfoList *outval;
		GMF_MediaTypeInfo *theval;

		outval = *out_typelist = GMF_MediaTypeInfoList__alloc();
		outval->_length = 1;
		outval->_buffer = CORBA_sequence_GMF_MediaTypeInfo_allocbuf(outval->_length);

		theval = outval->_buffer;

		theval->majorType = GMF_MEDIA_AUDIO_DIGITAL;
		theval->minorType = CORBA_string_dup("audio/wav");
		theval->fixedSizeSamples = CORBA_TRUE;
		theval->temporalCompression = CORBA_FALSE;
		theval->sampleSize = 2048;

		theval->typeData._type = (CORBA_TypeCode)
			CORBA_Object_duplicate((CORBA_Object)TC_null, &ev);
		theval->typeData._value = NULL;
		CORBA_any_set_release(&theval->typeData, CORBA_FALSE);

		theval->formatData._type = (CORBA_TypeCode)
		CORBA_Object_duplicate((CORBA_Object)TC_null, &ev);
		theval->formatData._value = NULL;
		CORBA_any_set_release(&theval->typeData, CORBA_FALSE);
	}

	return TRUE;
}
