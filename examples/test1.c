#include <gmf.h>

static void print_strv(char **strv)
{
  int i;
  g_print("[");
  for(i = 0; strv[i]; i++) {
    g_print("%s%s", strv[i], strv[i+1]?" ":"");
  }
  g_print("]\n");
}

int main(int argc, char *argv[])
{
  CORBA_Environment ev;
  GMFFilterRegistry *reg;
  GSList *ltmp;
  GMFFilterRegistryEntry *ent;

  CORBA_exception_init(&ev);
  gnome_CORBA_init("test1", "0.0", &argc, argv, 0, &ev);

  reg = gmf_filter_registry_get();

  for(ltmp = reg->list; ltmp; ltmp = g_slist_next(ltmp)) {
    ent = ltmp->data;

    g_print("Filter %s:\n", ent->goad_id);

    g_print("    Inputs: ");
    print_strv(ent->input_types);

    g_print("    Outputs: ");
    print_strv(ent->output_types);

    g_print("\n");
  }

  gmf_filter_activate_for_types(NULL, reg, "audio/midi", "video/avi");

  gmf_filter_registry_free(reg);

  return 0;
}
