/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "config.h"

#include <gmf.h>
#include <bonobo/bonobo-object.h>

#include "gmf-gtk-macros.h"
#include "gmf-object-directory.h"

typedef struct {
  char *name;
  GMF_Filter obj;
  GMF_Filter_Type ftype;
} FG_FilterInfo;

typedef struct {
	POA_GMF_FilterGraph servant;
  
	/* notused */ /* GMF_TimeReference attr_syncSource; */
	GHashTable *filters_byname;
	GList *source_filters, *render_filters;

	gint end_of_stream_recv;

	GMFFilterGraph *gtk_object;

} impl_POA_GMF_FilterGraph;


static void
impl_GMF_EventReceiver_deliver(impl_POA_GMF_FilterGraph * servant, GMF_TimeVal *timestamp,
			       CORBA_char *evName,
			       CORBA_any *evData, CORBA_Environment *ev);

static GMF_TimeReference
impl_GMF_FilterGraph__get_syncSource(impl_POA_GMF_FilterGraph * servant,
				     CORBA_Environment * ev);
static void
impl_GMF_FilterGraph__set_syncSource(impl_POA_GMF_FilterGraph * servant,
				     GMF_TimeReference value,
				     CORBA_Environment * ev);
static GMF_Filter
impl_GMF_FilterGraph_FindFilterByName(impl_POA_GMF_FilterGraph * servant,
				      CORBA_char * name,
				      CORBA_Environment * ev);
static GMF_Filter_RecordList *
impl_GMF_FilterGraph__get_graphFilters(impl_POA_GMF_FilterGraph * servant,
				       CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_BuildGraph(impl_POA_GMF_FilterGraph * servant,
				GMF_GraphPoint *renderFrom,
				GMF_GraphPoint *renderTo,
				CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_AddFilter(impl_POA_GMF_FilterGraph * servant,
			       CORBA_char * name,
			       GMF_Filter filter,
			       CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_RemoveFilter(impl_POA_GMF_FilterGraph * servant,
				  CORBA_char * name,
				  CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_Stop(impl_POA_GMF_FilterGraph * servant,
				 CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_Pause(impl_POA_GMF_FilterGraph * servant,
				 CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_Run(impl_POA_GMF_FilterGraph * servant,
			 CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_SetTimeBase(impl_POA_GMF_FilterGraph * servant,
				 GMF_TimeVal *timeBase,
				 CORBA_Environment * ev);
static void
impl_GMF_FilterGraph_SetApplicationObject(impl_POA_GMF_FilterGraph * servant,
					  Bonobo_Unknown appobj,
					  CORBA_Environment * ev);

static void 	gmf_filter_graph_class_init 	(GMFFilterGraphClass 		*klass);
static void 	gmf_filter_graph_init       	(GMFFilterGraph 		*obj);
static void 	gmf_filter_graph_destroy    	(GMFFilterGraph 		*obj);
static gboolean fg_remove_filter	  	(const char 			*key, 
						 FG_FilterInfo 			*fi, 
						 impl_POA_GMF_FilterGraph 	*servant);

enum {
  STOP,
  PAUSE,
  RUN,
  DELIVER_EVENT,
  LAST_SIGNAL
};

static guint fg_signals[LAST_SIGNAL] = { 0 };

static PortableServer_ServantBase__epv impl_GMF_FilterGraph_base_epv =
{
   NULL,			/* _private data */
   NULL,                	/* finalize routine...? */
   NULL,			/* default_POA routine */
};

static POA_GMF_StateChange__epv impl_GMF_FilterGraph_GMF_StateChange_epv = {
   NULL,			/* _private */
   (gpointer) & impl_GMF_FilterGraph_Stop,
   (gpointer) & impl_GMF_FilterGraph_Pause,
   (gpointer) & impl_GMF_FilterGraph_Run,
   (gpointer) & impl_GMF_FilterGraph_SetTimeBase
};

static POA_GMF_FilterGraph__epv impl_GMF_FilterGraph_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_FilterGraph__get_syncSource,
   (gpointer) & impl_GMF_FilterGraph__set_syncSource,
   (gpointer) & impl_GMF_FilterGraph_FindFilterByName,
   (gpointer) & impl_GMF_FilterGraph__get_graphFilters,
   (gpointer) & impl_GMF_FilterGraph_BuildGraph,
   (gpointer) & impl_GMF_FilterGraph_AddFilter,
   (gpointer) & impl_GMF_FilterGraph_RemoveFilter,
   (gpointer) & impl_GMF_FilterGraph_SetApplicationObject
};

static POA_GMF_EventReceiver__epv impl_GMF_FilterGraph_GMF_EventReceiver_epv =
{
  NULL, /* _private */
  (gpointer) &impl_GMF_EventReceiver_deliver
};

static POA_GMF_FilterGraph__vepv impl_GMF_FilterGraph_vepv =
{
   &impl_GMF_FilterGraph_base_epv,
   NULL,
   &impl_GMF_FilterGraph_GMF_StateChange_epv,
   &impl_GMF_FilterGraph_GMF_EventReceiver_epv,
   &impl_GMF_FilterGraph_epv
};

static void
impl_GMF_EventReceiver_deliver(impl_POA_GMF_FilterGraph * servant, GMF_TimeVal *timestamp, CORBA_char *evName,
			       CORBA_any *evData, CORBA_Environment *ev)
{
  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), fg_signals[DELIVER_EVENT], timestamp, evName, evData);
}


static void
impl_GMF_FilterGraph__destroy (BonoboObject *obj, impl_POA_GMF_FilterGraph *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	void (*servant_destroy_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	CORBA_exception_init(&ev);

	servant = BONOBO_OBJECT(obj)->servant;
  	g_list_free(servant->source_filters); 
  	servant->source_filters = NULL;

  	g_list_free(servant->render_filters); 
  	servant->render_filters = NULL;

  	g_hash_table_foreach_remove (servant->filters_byname, (GHRFunc)fg_remove_filter, servant);
  	g_hash_table_destroy (servant->filters_byname); servant->filters_byname = NULL;

	servant_destroy_func = GMF_FILTER_GRAPH_CLASS (GTK_OBJECT (servant->gtk_object)->klass)->servant_destroy_func;
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	servant_destroy_func ((PortableServer_Servant) servant, &ev);
	g_free (servant);
	CORBA_exception_free(&ev);
}

static GMF_FilterGraph
impl_GMF_FilterGraph__create (GMFFilterGraph *graph, CORBA_Environment * ev)
{
  	GMF_FilterGraph retval;
	impl_POA_GMF_FilterGraph *servant;
	void (*servant_init_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	GMFFilterGraphClass *filter_class = GMF_FILTER_GRAPH_CLASS (GTK_OBJECT(graph)->klass);
	
	servant_init_func = filter_class->servant_init_func;
	servant = g_new0 (impl_POA_GMF_FilterGraph, 1);
	servant->servant.vepv = filter_class->vepv;
	if (!servant->servant.vepv->Bonobo_Unknown_epv)
		servant->servant.vepv->Bonobo_Unknown_epv = bonobo_object_get_epv ();
	servant_init_func ((PortableServer_Servant) servant, ev);
	
	servant->gtk_object = graph;
	
	retval = bonobo_object_activate_servant (BONOBO_OBJECT (graph), servant);
	
	gtk_signal_connect (GTK_OBJECT (graph), "destroy", GTK_SIGNAL_FUNC (impl_GMF_FilterGraph__destroy), servant);

	servant->filters_byname = g_hash_table_new (g_str_hash, g_str_equal);
	
	return retval;
}

static GMF_TimeReference
impl_GMF_FilterGraph__get_syncSource(impl_POA_GMF_FilterGraph * servant,
				     CORBA_Environment * ev)
{
   GMF_TimeReference retval = CORBA_OBJECT_NIL;

   g_warning("get_syncSource NYI");

   return retval;
}

static void
impl_GMF_FilterGraph__set_syncSource(impl_POA_GMF_FilterGraph * servant,
				     GMF_TimeReference value,
				     CORBA_Environment * ev)
{
   g_warning("set_syncSource NYI");
}

static GMF_Filter
impl_GMF_FilterGraph_FindFilterByName(impl_POA_GMF_FilterGraph * servant,
				      CORBA_char * name,
				      CORBA_Environment * ev)
{
   GMF_Filter retval = CORBA_OBJECT_NIL;
   FG_FilterInfo *finfo;

   finfo = g_hash_table_lookup(servant->filters_byname, name);
   if(finfo)
     retval = CORBA_Object_duplicate(finfo->obj, ev);

   return retval;
}

static void
put_fginfo_on_list(char *name, FG_FilterInfo *fi,
		   GMF_Filter_RecordList *retval)
{
	int i;
	CORBA_Environment ev;

	CORBA_exception_init(&ev);

  	i = retval->_maximum;
  	retval->_buffer[i].name = CORBA_string_dup(name);
  	retval->_buffer[i].filterType = fi->ftype;
  	retval->_buffer[i].obj = CORBA_Object_duplicate(fi->obj, &ev);
  	retval->_maximum++;

  	CORBA_exception_free(&ev);
}

static GMF_Filter_RecordList *
impl_GMF_FilterGraph__get_graphFilters(impl_POA_GMF_FilterGraph * servant,
				       CORBA_Environment * ev)
{
   GMF_Filter_RecordList *retval;

   retval = GMF_Filter_RecordList__alloc();
   retval->_length = g_hash_table_size(servant->filters_byname);
   retval->_maximum = 0;
   retval->_buffer = CORBA_sequence_GMF_Filter_Record_allocbuf(retval->_length);
   CORBA_sequence_set_release(retval, CORBA_TRUE);
   g_hash_table_foreach(servant->filters_byname,
			(GHFunc)put_fginfo_on_list, retval);

   return retval;
}

static GMF_Pipe
gmf_filter_graph_create_source(GMFFilterGraph *fg,
			       GMF_GraphPoint *renderFrom,
			       CORBA_Environment *ev,
			       GList *slist)
{
  GMF_Filter source_filter = CORBA_OBJECT_NIL;
  GMF_Pipe retval = CORBA_OBJECT_NIL;
  GMF_PipeList *outpipes = NULL;
  int i;

  /* First, get a filter objref for whatever point we want to go from */

  switch(renderFrom->_d) {
	case GMF_POINT_URL:
	{
		const char *mimetype;

		mimetype = gnome_mime_type_or_default_of_file (renderFrom->_u.url, "application/data");

		if(!strcmp(mimetype, "application/data")) 
			goto onerror;

		source_filter = gmf_filter_activate_for_types(NULL, mimetype);
	}
		break;
	
	case GMF_POINT_FILTER:
    		source_filter = CORBA_Object_duplicate(renderFrom->_u.rFilter, ev);
		break;
		
	case GMF_POINT_FILTER_BYNAME:
    	{
		source_filter = impl_GMF_FilterGraph_FindFilterByName(BONOBO_OBJECT(fg)->servant, renderFrom->_u.filterName, ev);
		source_filter = CORBA_Object_duplicate(source_filter, ev);
	}
		break;
	
	default:
		g_assert_not_reached();
    		break;
  }

  if(CORBA_Object_is_nil(source_filter, ev)) goto onerror;

  /* Now, we try to get a pipe from it */
  outpipes = GMF_Filter__get_outputPipes(source_filter, ev);
  if(ev->_major != CORBA_NO_EXCEPTION)
    goto onerror;

  for(i = 0; i < outpipes->_length; i++) {
    CORBA_boolean iscnx;

    iscnx = GMF_Pipe__get_isConnected(outpipes->_buffer[i], ev);
    if(ev->_major != CORBA_NO_EXCEPTION) goto onerror;

    iscnx = iscnx && !GMF_OutputPipe__get_autoRender(outpipes->_buffer[i], ev);
    if(ev->_major != CORBA_NO_EXCEPTION) goto onerror;

    if(!iscnx) {
      retval = CORBA_Object_duplicate(outpipes->_buffer[i], ev);
      break;
    }
  }

  if(CORBA_Object_is_nil(retval, ev))
    retval = GMF_Filter_GetPipe(source_filter, GMF_OUT, ev);

 onerror:
  CORBA_Object_release(source_filter, ev);

  CORBA_free(outpipes);

  return retval;
}

static GMF_Pipe
gmf_filter_graph_create_render(GMFFilterGraph *fg,
			       GMF_GraphPoint *renderTo,
			       CORBA_Environment *ev,
			       GList *slist,
			       GMF_Pipe from)
{
  GMF_Filter render_filter = CORBA_OBJECT_NIL;
  GMF_Pipe retval = CORBA_OBJECT_NIL;
  GMF_PipeList *inpipes = NULL;
  int i;

  /* First, get a filter objref for whatever point we want to go from */

  switch(renderTo->_d) {
  case GMF_POINT_URL:
    {
      const char *mimetype = "application/data";
      GMF_MediaTypeInfoList *ptypes;

      ptypes = GMF_Pipe__get_processableTypes(from, ev);
      if(ev->_major) goto onerror;

      for(i = 0; i < ptypes->_length; i++) {
	mimetype = ptypes->_buffer[i].minorType;
	break;
      }

      CORBA_free(ptypes);

      render_filter = gmf_filter_activate_for_types(mimetype, NULL);
    }
    break;
  case GMF_POINT_FILTER:
    render_filter = CORBA_Object_duplicate(renderTo->_u.rFilter, ev);
    break;
  case GMF_POINT_FILTER_BYNAME:
    {
      render_filter = impl_GMF_FilterGraph_FindFilterByName(BONOBO_OBJECT(fg)->servant, renderTo->_u.filterName, ev);
      render_filter = CORBA_Object_duplicate(render_filter, ev);
    }
    break;
  default:
    g_assert_not_reached();
    break;
  }

  if(CORBA_Object_is_nil(render_filter, ev)) goto onerror;

  /* Now, we try to get a pipe from it */
  inpipes = GMF_Filter__get_inputPipes(render_filter, ev);
  if(ev->_major != CORBA_NO_EXCEPTION)
    goto onerror;

  for(i = 0; i < inpipes->_length; i++) {
    CORBA_boolean iscnx;

    iscnx = GMF_Pipe__get_isConnected(inpipes->_buffer[i], ev);
    if(ev->_major != CORBA_NO_EXCEPTION) goto onerror;

    if(!iscnx) {
      retval = CORBA_Object_duplicate(inpipes->_buffer[i], ev);
      break;
    }
  }

  if(CORBA_Object_is_nil(retval, ev))
    retval = GMF_Filter_GetPipe(render_filter, GMF_IN, ev);

 onerror:
  CORBA_Object_release(render_filter, ev);

  CORBA_free(inpipes);

  return retval;
}

/* This routine isn't as bright as it possibly could be. For example,
the "right" way to connect two filters is to handle the autoRender
pipes.  */

static void
impl_GMF_FilterGraph_BuildGraph(impl_POA_GMF_FilterGraph * servant,
				GMF_GraphPoint *renderFrom,
				GMF_GraphPoint *renderTo,
				CORBA_Environment * ev)
{
  GMF_Pipe from = CORBA_OBJECT_NIL, to = CORBA_OBJECT_NIL;
  GList *slist = NULL;

  static const char *required_interfaces[] = {"IDL:GMF/Filter:1.0"};

  /* Get the output pipe of the FROM point. */

  switch(renderFrom->_d) {
  case GMF_POINT_URL:
    slist = od_get_server_list (required_interfaces);
  case GMF_POINT_FILTER_BYNAME:
  case GMF_POINT_FILTER:
    from = gmf_filter_graph_create_source(servant->gtk_object, renderFrom, ev, slist);
    break;
  case GMF_POINT_PIPE:
    from = renderFrom->_u.rPipe;
    break;
  default:
    g_assert_not_reached();
    break;
  }

  /* Get the input pipe of the TO thing. */
  switch(renderTo->_d) {
  case GMF_POINT_URL:
    if(!slist) {
      slist = od_get_server_list(required_interfaces);
    }
  case GMF_POINT_FILTER_BYNAME:
  case GMF_POINT_FILTER:
    to = gmf_filter_graph_create_render(servant->gtk_object, renderTo, ev, slist, from);
    break;
  case GMF_POINT_PIPE:
    to = renderTo->_u.rPipe;
    break;
  default:
    g_assert_not_reached();
    break;
  }

  /* Now, try and make a path between those two pipes */
  GMF_Pipe_ConnectTo(from, to, ev);
  if(ev->_major) goto onerror;

 onerror:
  od_server_list_free(slist);
}

static void
impl_GMF_FilterGraph_AddFilter(impl_POA_GMF_FilterGraph * servant,
			       CORBA_char * name,
			       GMF_Filter filter,
			       CORBA_Environment * ev)
{
	FG_FilterInfo *fi;

	fi = g_hash_table_lookup(servant->filters_byname, name);

	if (fi) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
				     ex_GMF_Filter_JoinRefused, NULL);
		return;
	}

	Bonobo_Unknown_ref(filter, ev);
	if(ev->_major != CORBA_NO_EXCEPTION) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
				     ex_GMF_Filter_JoinRefused, NULL);
		return;
	}

	GMF_Filter_JoinFilterGraph(filter, name, bonobo_object_corba_objref(BONOBO_OBJECT(servant->gtk_object)), ev);
	if(ev->_major != CORBA_NO_EXCEPTION) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
				     ex_GMF_Filter_JoinRefused, NULL);
		return;
	}

	fi = g_new0(FG_FilterInfo, 1);
	fi->ftype = GMF_Filter__get_filterType(filter, ev);

	fi->name = g_strdup(name);

	fi->obj = CORBA_Object_duplicate(filter, ev);

	if(fi->ftype == GMF_Filter_RENDERER)
		servant->render_filters = g_list_prepend(servant->render_filters, fi);
	else if(fi->ftype == GMF_Filter_SOURCE)
		servant->source_filters = g_list_prepend(servant->source_filters, fi);

	g_hash_table_insert(servant->filters_byname, fi->name, fi);
 }

static void
impl_GMF_FilterGraph_RemoveFilter(impl_POA_GMF_FilterGraph * servant,
				  CORBA_char * name,
				  CORBA_Environment * ev)
{
  FG_FilterInfo *fi;

  fi = g_hash_table_lookup(servant->filters_byname, name);

  if(!fi)
    return;

  GMF_Filter_JoinFilterGraph(fi->obj, "", CORBA_OBJECT_NIL, ev);
  Bonobo_Unknown_unref(fi->obj, ev);

  g_hash_table_remove(servant->filters_byname, name);

  CORBA_Object_release(fi->obj, ev);
  g_free(fi->name);
  g_free(fi);
}

static void
fg_stop_to_all(guchar *key, FG_FilterInfo *fi,
	       CORBA_Environment *ev)
{
  GMF_Filter_Stop(fi->obj, ev);
}

static void
impl_GMF_FilterGraph_Stop(impl_POA_GMF_FilterGraph * servant,
			  CORBA_Environment * ev)
{
  if(servant->gtk_object->state != GMF_Filter_STOPPED) {
    servant->gtk_object->state = GMF_Filter_STOPPED;
    g_hash_table_foreach(servant->filters_byname, (GHFunc)fg_stop_to_all, ev);
    gtk_signal_emit(GTK_OBJECT(servant->gtk_object), fg_signals[STOP]);
  }
}

static void
fg_pause_to_all(guchar *key, FG_FilterInfo *fi,
		CORBA_Environment *ev)
{
  GMF_Filter_Pause(fi->obj, ev);
}

static void
impl_GMF_FilterGraph_Pause(impl_POA_GMF_FilterGraph * servant,
			   CORBA_Environment * ev)
{
  if(servant->gtk_object->state != GMF_Filter_PAUSED) {
    servant->gtk_object->state = GMF_Filter_PAUSED;
    g_hash_table_foreach(servant->filters_byname, (GHFunc)fg_pause_to_all, ev);
    gtk_signal_emit(GTK_OBJECT(servant->gtk_object), fg_signals[PAUSE]);
  }
}

static void
fg_run_to_all(guchar *key, FG_FilterInfo *fi)
{
	CORBA_Environment ev;
	CORBA_exception_init(&ev);

	GMF_Filter_Run(fi->obj, &ev);

	CORBA_exception_free (&ev);
}

static void
impl_GMF_FilterGraph_Run(impl_POA_GMF_FilterGraph * servant,
			 CORBA_Environment * ev)
{
  if(servant->gtk_object->state != GMF_Filter_RUNNING) {
    servant->gtk_object->state = GMF_Filter_RUNNING;
    g_hash_table_foreach(servant->filters_byname, (GHFunc)fg_run_to_all, NULL);
    gtk_signal_emit(GTK_OBJECT(servant->gtk_object), fg_signals[RUN]);
  }
}

static void
fg_stb_to_all(guchar *key, FG_FilterInfo *fi, GMF_TimeVal *timeBase)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	GMF_Filter_SetTimeBase(fi->obj, timeBase, &ev);

	CORBA_exception_free (&ev);

}

static void
impl_GMF_FilterGraph_SetTimeBase(impl_POA_GMF_FilterGraph * servant,
				 GMF_TimeVal *timeBase,
				 CORBA_Environment * ev)
{
  servant->gtk_object->timeBase = *timeBase;
  g_hash_table_foreach(servant->filters_byname, (GHFunc)fg_stb_to_all, timeBase);
}

static void
impl_GMF_FilterGraph_SetApplicationObject(impl_POA_GMF_FilterGraph * servant,
					  Bonobo_Unknown appobj,
					  CORBA_Environment * ev)
{
  if(!CORBA_Object_is_nil(servant->gtk_object->app_object, ev))
    CORBA_Object_release(servant->gtk_object->app_object, ev);

  servant->gtk_object->app_object = CORBA_Object_duplicate(appobj, ev);
}

GtkObjectClass *parent_class;

GtkType
gmf_filter_graph_get_type (void)
{
  static GtkType gmf_filter_graph_type = 0;
  if (!gmf_filter_graph_type)
    {
      static const GtkTypeInfo gmf_filter_graph_info =
      {
	"GMFFilterGraph",
	sizeof (GMFFilterGraph),
	sizeof (GMFFilterGraphClass),
	(GtkClassInitFunc) gmf_filter_graph_class_init,
	(GtkObjectInitFunc) gmf_filter_graph_init,
	NULL, NULL,		/* reserved 1 & 2 */
	NULL
      };

      gmf_filter_graph_type = gtk_type_unique (bonobo_object_get_type(), &gmf_filter_graph_info);
      parent_class = gtk_type_class(gtk_type_parent(gmf_filter_graph_type));
    }

  return gmf_filter_graph_type;
}

static void
gmf_filter_graph_query_interface(BonoboObject *object, const char *repo_id, CORBA_Object *retval)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	if(!CORBA_Object_is_nil(GMF_FILTER_GRAPH(object)->app_object, &ev))
		*retval = Bonobo_Unknown_queryInterface (GMF_FILTER_GRAPH(object)->app_object, (CORBA_char *)repo_id, &ev);

	if(CORBA_Object_is_nil(*retval, &ev)
		&& BONOBO_OBJECT_CLASS(parent_class)->query_interface)
    		BONOBO_OBJECT_CLASS(parent_class)->query_interface(object, repo_id, retval);

	CORBA_exception_free (&ev);
}

static void 
gmf_filter_graph_class_init (GMFFilterGraphClass * klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	object_class->destroy = (void (*)(GtkObject *))gmf_filter_graph_destroy;
	klass->klass.query_interface = gmf_filter_graph_query_interface;

	klass->servant_init_func = POA_GMF_FilterGraph__init;
	klass->servant_destroy_func = POA_GMF_FilterGraph__fini;
	klass->vepv = &impl_GMF_FilterGraph_vepv;

	fg_signals[STOP] =
		gtk_signal_new("stop", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterGraphClass, stop),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0);
  	fg_signals[PAUSE] =
    		gtk_signal_new("pause", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterGraphClass, pause),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0);
  	fg_signals[RUN] =
    		gtk_signal_new("run", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterGraphClass, run),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  	fg_signals[DELIVER_EVENT] =
    		gtk_signal_new("deliver_event", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterGraphClass, deliver_event),
		   gtk_marshal_NONE__POINTER_POINTER_POINTER,
		   GTK_TYPE_NONE, 3, GTK_TYPE_POINTER, GTK_TYPE_POINTER,
		   GTK_TYPE_POINTER);

  	gtk_object_class_add_signals(object_class, fg_signals, LAST_SIGNAL);

	CORBA_exception_free (&ev);
}

static void 
gmf_filter_graph_init (GMFFilterGraph * obj)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

  	bonobo_object_construct (BONOBO_OBJECT(obj), impl_GMF_FilterGraph__create(obj, &ev));
  	obj->state = GMF_Filter_STOPPED;
  	obj->app_object = CORBA_OBJECT_NIL;
  	obj->timeref = GMF_TIME_REFERENCE(gmf_time_reference_new());
  	gtk_object_ref(GTK_OBJECT(obj->timeref));
  	gtk_object_sink(GTK_OBJECT(obj->timeref));

	CORBA_exception_free (&ev);
}

static gboolean
fg_remove_filter(const char *key, FG_FilterInfo *fi, impl_POA_GMF_FilterGraph *servant)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

  	GMF_Filter_JoinFilterGraph(fi->obj, "", CORBA_OBJECT_NIL, &ev);
  	Bonobo_Unknown_unref(fi->obj, &ev);
  	CORBA_Object_release(fi->obj, &ev);
  	g_free(fi->name);

  	g_free(fi);

	CORBA_exception_free (&ev);

  	return TRUE;
}

static void
gmf_filter_graph_destroy (GMFFilterGraph *obj)
{
  	gtk_object_unref(GTK_OBJECT(obj->timeref));
    	
  	GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, destroy, GTK_OBJECT (obj));
}

GtkObject *
gmf_filter_graph_new (void)
{
  return gtk_type_new(gmf_filter_graph_get_type());
}

void
gmf_filter_graph_render_file (GMFFilterGraph *filter_graph,
			      const char *filename)
{
	GMF_GraphPoint p1;
	GMF_GraphPoint p2;
	char *curdir = NULL;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	p1._d = GMF_POINT_URL;

	if(*filename != '/') {
		curdir = g_get_current_dir();
		p1._u.url = alloca(strlen(filename) + sizeof("file:///") + strlen(curdir) + 1);
		sprintf(p1._u.url, "file:///%s/%s", curdir, filename);
  	} else {
    		p1._u.url = alloca(strlen(filename) + sizeof("file://"));
    		sprintf(p1._u.url, "file:///%s", filename);
  	}

	p2._d = GMF_POINT_AUTO;

	GMF_FilterGraph_BuildGraph(bonobo_object_corba_objref(BONOBO_OBJECT(filter_graph)), &p1, &p2, &ev);

	g_free(curdir);
	CORBA_exception_free (&ev);
}
