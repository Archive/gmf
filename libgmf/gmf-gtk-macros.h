
#ifndef GMF_GTK_MACROS_H
#define GMF_GTK_MACROS_H

/* Define a parent_class global and a get_type function for a GTK class.
   Since this is boilerplate, it's better not to repeat it over and over again.
   Called like this:

       GMF_DEFINE_CLASS_BOILERPLATE (NautilusBookmark, nautilus_bookmark, GTK_TYPE_OBJECT)

   The parent_class_type parameter is guaranteed to be evaluated only once
   so it can be an expression, even an expression that contains a function call.
*/

#define GMF_DEFINE_CLASS_BOILERPLATE(class_name, class_name_in_function_format, parent_class_type) \
\
static gpointer parent_class; \
\
GtkType \
class_name_in_function_format##_get_type (void) \
{ \
	GtkType parent_type; \
	static GtkType type; \
        \
	if (type == 0) { \
		static GtkTypeInfo info = { \
			#class_name, \
			sizeof (class_name), \
			sizeof (class_name##Class), \
			(GtkClassInitFunc)class_name_in_function_format##_initialize_class, \
			(GtkObjectInitFunc)class_name_in_function_format##_initialize, \
			NULL, \
			NULL, \
			NULL \
		}; \
		\
		parent_type = (parent_class_type); \
		type = gtk_type_unique (parent_type, &info); \
		parent_class = gtk_type_class (parent_type); \
	} \
        \
	return type; \
}


/* Call a parent class version of a signal.
   Nice because it documents what it's doing and there is less chance for
   a typo. Depends on the parent class pointer having the conventional
   name "parent_class".
*/

#define GMF_CALL_PARENT_CLASS(parent_class_cast_macro, signal, parameters) \
\
(parent_class_cast_macro (parent_class)->signal == NULL) \
	? 0 \
	: ((* parent_class_cast_macro (parent_class)->signal) parameters)

#endif