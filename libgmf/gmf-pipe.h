/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_PIPE_H
#define GMF_PIPE_H 1

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-main.h>


/* GtkObject GMFPipe */
#define GMF_TYPE_PIPE	(gmf_pipe_get_type())
#define GMF_PIPE(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_PIPE, GMFPipe))
#define GMF_PIPE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_PIPE, GMFPipeClass))
#define GMF_IS_PIPE(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_PIPE))
#define GMF_IS_PIPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_PIPE))

struct _GMFPipe {
	BonoboObject object;

	GtkObject *filter_object;

	GMF_Pipe_Info attr_pipeInfo;
	GMF_Pipe_ConnectionInfo *attr_pConnectionInfo;
	GMF_Transport_Type acceptable_transports;
	gchar *name;
	GMFTimeReference *timeref;
};

typedef struct {
	BonoboObjectClass klass;

	void (*can_process_type)	(GMFPipe 		   *apipe, 
					 GMF_MediaTypeInfo 	   *type_info,
			   		 gboolean 		   *retval);
	void (*get_processable_types)	(GMFPipe 		   *apipe,
					 GMF_MediaTypeInfoList 	   **out_typelist);
	void (*deliver_event)		(GMFPipe 		   *apipe, 
					 GMF_TimeVal 		   *timestamp, 
					 const char 		   *evName, 
					 CORBA_any 		   *evData);
	void (*connect)			(GMFPipe 		   *apipe, 
					 GMF_Pipe_ConnectionInfo   *cnxinfo);
	void (*disconnect)		(GMFPipe 		   *apipe);
	void (*fixup_media_format)	(GMFPipe 		   *apipe, 
					 GMF_MediaTypeInfo 	   *typeinfo, 
					 GMF_Pipe_FormatAcceptance other_end,
			     		 GMF_Pipe_FormatAcceptance *retval);
} GMFPipeClass;

GtkType gmf_pipe_get_type (void);
gint gmf_pipe_connect (GMFPipe *apipe, GMF_Pipe remote_end); /* Returns 0 if connection succeeded */

/* GtkObject GMFInputPipe */
#define GMF_TYPE_INPUT_PIPE	(gmf_input_pipe_get_type())
#define GMF_INPUT_PIPE(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_INPUT_PIPE, GMFInputPipe))
#define GMF_INPUT_PIPE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_INPUT_PIPE, GMFInputPipeClass))
#define GMF_IS_INPUT_PIPE(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_INPUT_PIPE))
#define GMF_IS_INPUT_PIPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_INPUT_PIPE))

typedef struct {
  GMFPipe parent;
  gboolean in_flush;
} GMFInputPipe;

typedef struct {
  GMFPipeClass parent;

  void (*receive_sample)(GMFInputPipe *pipe, GMF_Sample *sample);
  void (*begin_flush)(GMFInputPipe *pipe);
  void (*end_flush)(GMFInputPipe *pipe);
  void (*new_segment)(GMFPipe *pipe, GMF_TimeVal *tStart, GMF_TimeVal *tStop,
		      gdouble playRate);
} GMFInputPipeClass;

GtkType gmf_input_pipe_get_type (void);

/* GtkObject GMFOutputPipe */
#define GMF_TYPE_OUTPUT_PIPE	(gmf_output_pipe_get_type())
#define GMF_OUTPUT_PIPE(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_OUTPUT_PIPE, GMFOutputPipe))
#define GMF_OUTPUT_PIPE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_OUTPUT_PIPE, GMFOutputPipeClass))
#define GMF_IS_OUTPUT_PIPE(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_OUTPUT_PIPE))
#define GMF_IS_OUTPUT_PIPE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_OUTPUT_PIPE))
typedef struct {
  GMFPipe parent;

  gboolean autorender;
  GMF_QualityControl_Quality last_qosinfo;
} GMFOutputPipe;

typedef struct {
  GMFPipeClass parent;

  void (*quality_notify)(GMFOutputPipe *pipe, GMF_QualityControl_Quality *qosinfo);
} GMFOutputPipeClass;

GtkType gmf_output_pipe_get_type (void);
void gmf_output_pipe_send_sample(GMFOutputPipe *apipe, GMF_Sample *sample);
void gmf_output_pipe_deliver_begin_flush(GMFOutputPipe *apipe);
void gmf_output_pipe_deliver_end_flush(GMFOutputPipe *apipe);
void gmf_output_pipe_deliver_end_of_stream(GMFOutputPipe *apipe, GMF_TimeVal *timestamp);

/* 
 *  GtkObject *gmf_pipe_new (GMFFilter *filter, GMF_Direction pipedir,
 *  GMF_Transport_Type acceptable_transports);
 */
#endif /* GMF_PIPE_H */
