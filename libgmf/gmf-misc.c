#include "libgmf.h"

#include <libgnomevfs/gnome-vfs-init.h>

void
gmf_init(CORBA_ORB orb)
{
	bonobo_init(orb, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL);
	gnome_vfs_init ();
}
