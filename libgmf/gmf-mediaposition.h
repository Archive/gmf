/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_MEDIA_POSITION_H
#define GMF_MEDIA_POSITION_H 1

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-main.h>
#include <libgmf/gmf-corba.h>

/* GtkObject GMFMediaPosition */
#define GMF_TYPE_MEDIA_POSITION	(gmf_media_position_get_type())
#define GMF_MEDIA_POSITION(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_MEDIA_POSITION, GMFMediaPosition))
#define GMF_MEDIA_POSITION_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_MEDIA_POSITION, GMFMediaPositionClass))
#define GMF_IS_MEDIA_POSITION(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_MEDIA_POSITION))
#define GMF_IS_MEDIA_POSITION_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_MEDIA_POSITION))

typedef struct {
	BonoboObject object;
} GMFMediaPosition;

typedef struct {
	BonoboObjectClass klass;

	void (*get_playback_rate) 	(GMFMediaPosition 	 *media_position, 
					 gdouble 		 *retval);
	void (*set_playback_rate) 	(GMFMediaPosition 	 *media_position, 
					 gdouble 		 newrate);
	void (*can_seek_forward)  	(GMFMediaPosition 	 *media_position, 
					 gboolean 		 *retval);
	void (*can_seek_backward) 	(GMFMediaPosition 	 *media_position, 
					 gboolean 		 *retval);
	void (*get_duration)      	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Type  posType, 
					 GMF_MediaPosition_Value *retval);
	void (*get_position)      	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Type  posType, 
					 GMF_MediaPosition_Value *retval);
	void (*set_position)      	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Value *value);
	void (*get_stop_time)     	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Type   posType, 
					 GMF_MediaPosition_Value *retval);
	void (*set_stop_time)     	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Value *value);
	void (*get_preroll_time)  	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Type  posType, 
					 GMF_MediaPosition_Value *retval);
	void (*set_preroll_time)  	(GMFMediaPosition 	 *media_position, 
					 GMF_MediaPosition_Value *value);
	gpointer servant_init_func, servant_destroy_func, vepv;
} GMFMediaPositionClass;

GtkType 	gmf_media_position_get_type 	(void);
GtkObject 	*gmf_media_position_new 	(void);

#endif
