/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_FILTERREGISTRY_H
#define GMF_FILTERREGISTRY_H 1

#include <libgnomevfs/gnome-vfs.h>

GMF_Filter gmf_filter_activate			(const char *oaf_id);
GMF_Filter gmf_filter_activate_for_types	(const char *intype,
					 	 const char *outtype);
/* returns a source filter for the specified file */
GMF_Filter gmf_filter_activate_for_uri		(GnomeVFSURI *uri);

#endif /* GMF_FILTERREGISTRY_H */
