/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include <gdk/gdkx.h>
#include "gmf-videoarea.h"

typedef struct {
  POA_GMF_VideoArea servant;
  GtkObject *gtk_object;

} impl_POA_GMF_VideoArea;

static CORBA_unsigned_long
impl_GMF_VideoArea__get_socket_id(impl_POA_GMF_VideoArea * servant,
				  CORBA_Environment *ev);

static POA_GMF_VideoArea__epv impl_GMF_VideoArea_epv =
{
   NULL,			/* _private */
   (gpointer) &impl_GMF_VideoArea__get_socket_id
};

static PortableServer_ServantBase__epv base_epv = { NULL, NULL, NULL };
static POA_GMF_VideoArea__vepv impl_GMF_VideoArea_vepv =
{
  &base_epv,
  NULL,
  &impl_GMF_VideoArea_epv
};

static void
impl_GMF_VideoArea__destroy(BonoboObject *obj, impl_POA_GMF_VideoArea *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	void (*servant_destroy_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	CORBA_exception_init(&ev);
	
	servant_destroy_func = GMF_VIDEO_AREA_CLASS (GTK_OBJECT (servant->gtk_object)->klass)->servant_destroy_func;
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	servant_destroy_func ((PortableServer_Servant) servant, &ev);
	g_free (servant);
	CORBA_exception_free(&ev);
}

static GMF_VideoArea
impl_GMF_VideoArea__create (GMFVideoArea *area, CORBA_Environment * ev)
{
  	GMF_VideoArea retval;
	impl_POA_GMF_VideoArea *servant;
	void (*servant_init_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	GMFVideoAreaClass *area_class = GMF_VIDEO_AREA_CLASS (GTK_OBJECT(area)->klass);
	
	servant_init_func = area_class->servant_init_func;
	servant = g_new0 (impl_POA_GMF_VideoArea, 1);
	servant->servant.vepv = area_class->vepv;
	if (!servant->servant.vepv->Bonobo_Unknown_epv)
		servant->servant.vepv->Bonobo_Unknown_epv = bonobo_object_get_epv ();
	servant_init_func ((PortableServer_Servant) servant, ev);
	
	servant->gtk_object = GTK_OBJECT (area);
	
	retval = bonobo_object_activate_servant (BONOBO_OBJECT (area), servant);
	
	gtk_signal_connect (GTK_OBJECT (area), "destroy", GTK_SIGNAL_FUNC (impl_GMF_VideoArea__destroy), servant);
	
	return retval;
}

static CORBA_unsigned_long
impl_GMF_VideoArea__get_socket_id(impl_POA_GMF_VideoArea * servant,
				  CORBA_Environment *ev)
{
  GMFVideoArea *va;

  va = (GMFVideoArea *)servant->gtk_object;

  if(GTK_WIDGET_REALIZED(va->socket)) {
    return GDK_WINDOW_XWINDOW(va->socket->window);
  } else
    return 0;
}

static void gmf_video_area_class_init(GMFVideoAreaClass *klass);
static void gmf_video_area_init(GMFVideoArea *obj);
static void gmf_video_area_destroy(GMFVideoArea *obj);

static GtkObjectClass *video_area_parent_class = NULL;


GtkType
gmf_video_area_get_type (void)
{
  static GtkType gmf_video_area_type = 0;
  if (!gmf_video_area_type)
    {
      static const GtkTypeInfo gmf_video_area_info =
      {
	"GMFVideoArea",
	sizeof (GMFVideoArea),
	sizeof (GMFVideoAreaClass),
	(GtkClassInitFunc) gmf_video_area_class_init,
	(GtkObjectInitFunc) gmf_video_area_init,
	NULL, NULL,		/* reserved 1 & 2 */
	NULL
      };

      gmf_video_area_type = gtk_type_unique (bonobo_object_get_type(), &gmf_video_area_info);
      video_area_parent_class = gtk_type_class(gtk_type_parent(gmf_video_area_type));
    }

  return gmf_video_area_type;
}

static void
gmf_video_area_class_init(GMFVideoAreaClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);
	
	object_class->destroy = (gpointer)&gmf_video_area_destroy;

  	klass->servant_init_func = POA_GMF_VideoArea__init;
	klass->servant_destroy_func = POA_GMF_VideoArea__fini;
	klass->vepv = &impl_GMF_VideoArea_vepv;
}

static void
gmf_video_area_init(GMFVideoArea *obj)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	bonobo_object_construct(BONOBO_OBJECT(obj), impl_GMF_VideoArea__create(obj, &ev));
	obj->socket = gtk_socket_new();
	gtk_object_ref (GTK_OBJECT(obj->socket));
	gtk_object_sink (GTK_OBJECT(obj->socket));

	CORBA_exception_free (&ev);
}

static void
gmf_video_area_destroy(GMFVideoArea *obj)
{
  	gtk_object_unref(GTK_OBJECT(obj->socket));

  	if(video_area_parent_class->destroy) {
    		video_area_parent_class->destroy ((GtkObject *)obj);
    	}
}

GtkObject *
gmf_video_area_new (void)
{
	return gtk_type_new(gmf_video_area_get_type());
}

GtkWidget *
gmf_video_area_get_socket (GMFVideoArea *obj)
{
	g_return_val_if_fail(GMF_IS_VIDEO_AREA(obj), NULL);

	return obj->socket;
}
