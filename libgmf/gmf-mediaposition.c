/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include <gtk/gtk.h>

#include "gmf-mediaposition.h"

typedef struct {
	POA_GMF_MediaPosition servant;
	GtkObject *gtk_object;
} impl_POA_GMF_MediaPosition;

static CORBA_double
impl_GMF_MediaPosition__get_playbackRate(impl_POA_GMF_MediaPosition * servant,
					 CORBA_Environment * ev);
static void
impl_GMF_MediaPosition__set_playbackRate(impl_POA_GMF_MediaPosition * servant,
					 CORBA_double value,
					 CORBA_Environment * ev);
static CORBA_boolean
impl_GMF_MediaPosition__get_can_seek_forward(impl_POA_GMF_MediaPosition * servant,
					     CORBA_Environment * ev);
static CORBA_boolean
impl_GMF_MediaPosition__get_can_seek_backward(impl_POA_GMF_MediaPosition * servant,
					      CORBA_Environment * ev);
static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_duration(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Type posType,
				     CORBA_Environment * ev);
static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_position(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Type posType,
				     CORBA_Environment * ev);
static void
impl_GMF_MediaPosition__set_position(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Value * value,
				     CORBA_Environment * ev);
static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_stopTime(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Type posType,
				     CORBA_Environment * ev);
static void
impl_GMF_MediaPosition__set_stopTime(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Value * value,
				     CORBA_Environment * ev);
static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_prerollTime(impl_POA_GMF_MediaPosition * servant,
					GMF_MediaPosition_Type posType,
					CORBA_Environment * ev);
static void
impl_GMF_MediaPosition__set_prerollTime(impl_POA_GMF_MediaPosition * servant,
					GMF_MediaPosition_Value * value,
					CORBA_Environment * ev);
enum {
  GET_PLAYBACK_RATE,
  SET_PLAYBACK_RATE,
  CAN_SEEK_FORWARD,
  CAN_SEEK_BACKWARD,
  GET_DURATION,
  GET_POSITION,
  SET_POSITION,
  GET_STOPTIME,
  SET_STOPTIME,
  GET_PREROLLTIME,
  SET_PREROLLTIME,
  LAST_SIGNAL
};
static guint mp_signals[LAST_SIGNAL] = { 0 };

static POA_GMF_MediaPosition__epv impl_GMF_MediaPosition_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_MediaPosition__get_playbackRate,
   (gpointer) & impl_GMF_MediaPosition__set_playbackRate,
   (gpointer) & impl_GMF_MediaPosition__get_can_seek_forward,
   (gpointer) & impl_GMF_MediaPosition__get_can_seek_backward,
   (gpointer) & impl_GMF_MediaPosition__get_duration,
   (gpointer) & impl_GMF_MediaPosition__get_position,
   (gpointer) & impl_GMF_MediaPosition__set_position,
   (gpointer) & impl_GMF_MediaPosition__get_stopTime,
   (gpointer) & impl_GMF_MediaPosition__set_stopTime,
   (gpointer) & impl_GMF_MediaPosition__get_prerollTime,
   (gpointer) & impl_GMF_MediaPosition__set_prerollTime
};

static PortableServer_ServantBase__epv base_epv = { NULL, NULL, NULL };
static POA_GMF_MediaPosition__vepv impl_GMF_MediaPosition_vepv =
{
  &base_epv,
  NULL,
  &impl_GMF_MediaPosition_epv
};


static void
impl_GMF_MediaPosition__destroy(BonoboObject *obj, impl_POA_GMF_MediaPosition *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	void (*servant_destroy_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	CORBA_exception_init(&ev);
	
	servant_destroy_func = GMF_MEDIA_POSITION_CLASS (GTK_OBJECT (servant->gtk_object)->klass)->servant_destroy_func;
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	servant_destroy_func ((PortableServer_Servant) servant, &ev);
	g_free (servant);
	CORBA_exception_free(&ev);
}

static GMF_MediaPosition
impl_GMF_MediaPosition__create (GMFMediaPosition *position, CORBA_Environment * ev)
{
  	GMF_MediaPosition retval;
	impl_POA_GMF_MediaPosition *servant;
	void (*servant_init_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	GMFMediaPositionClass *position_class = GMF_MEDIA_POSITION_CLASS (GTK_OBJECT(position)->klass);
	
	servant_init_func = position_class->servant_init_func;
	servant = g_new0 (impl_POA_GMF_MediaPosition, 1);
	servant->servant.vepv = position_class->vepv;
	if (!servant->servant.vepv->Bonobo_Unknown_epv)
		servant->servant.vepv->Bonobo_Unknown_epv = bonobo_object_get_epv ();
	servant_init_func ((PortableServer_Servant) servant, ev);
	
	servant->gtk_object = GTK_OBJECT (position);
	
	retval = bonobo_object_activate_servant (BONOBO_OBJECT (position), servant);
	
	gtk_signal_connect (GTK_OBJECT (position), "destroy", GTK_SIGNAL_FUNC (impl_GMF_MediaPosition__destroy), servant);
	
	return retval;
}

static CORBA_double
impl_GMF_MediaPosition__get_playbackRate(impl_POA_GMF_MediaPosition * servant,
					 CORBA_Environment * ev)
{
   CORBA_double retval = 1.0;

   gtk_signal_emit(servant->gtk_object, mp_signals[GET_PLAYBACK_RATE], &retval);

   return retval;
}

static void
impl_GMF_MediaPosition__set_playbackRate(impl_POA_GMF_MediaPosition * servant,
					 CORBA_double value,
					 CORBA_Environment * ev)
{
  gtk_signal_emit(servant->gtk_object, mp_signals[SET_PLAYBACK_RATE], value);
}

static CORBA_boolean
impl_GMF_MediaPosition__get_can_seek_forward(impl_POA_GMF_MediaPosition * servant,
					     CORBA_Environment * ev)
{
   gboolean retval = FALSE;

   gtk_signal_emit(servant->gtk_object, mp_signals[CAN_SEEK_FORWARD], &retval);

   return retval && CORBA_TRUE;
}

static CORBA_boolean
impl_GMF_MediaPosition__get_can_seek_backward(impl_POA_GMF_MediaPosition * servant,
					      CORBA_Environment * ev)
{
   gboolean retval = FALSE;

   gtk_signal_emit(servant->gtk_object, mp_signals[CAN_SEEK_BACKWARD], &retval);

   return retval && CORBA_TRUE;
}

static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_duration(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Type posType,
				     CORBA_Environment * ev)
{
   GMF_MediaPosition_Value retval;

   retval._d = GMF_MediaPosition_TIME;

   retval._u.value_time.tvSec =
     retval._u.value_time.tvUsec = 0;

   gtk_signal_emit(servant->gtk_object, mp_signals[GET_DURATION], posType, &retval);

   return retval;
}

static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_position(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Type posType,
				     CORBA_Environment * ev)
{
   GMF_MediaPosition_Value retval;

   retval._d = GMF_MediaPosition_TIME;
   retval._u.value_time.tvSec =
     retval._u.value_time.tvUsec = 0;

   gtk_signal_emit(servant->gtk_object, mp_signals[GET_POSITION], posType, &retval);

   return retval;
}

static void
impl_GMF_MediaPosition__set_position(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Value * value,
				     CORBA_Environment * ev)
{
  gtk_signal_emit(servant->gtk_object, mp_signals[SET_POSITION], value);
}

static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_stopTime(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Type posType,
				     CORBA_Environment * ev)
{
   GMF_MediaPosition_Value retval;

   retval._d = GMF_MediaPosition_TIME;
   retval._u.value_time.tvSec =
     retval._u.value_time.tvUsec = 0;

   gtk_signal_emit(servant->gtk_object, mp_signals[GET_STOPTIME], posType, &retval);

   return retval;
}

static void
impl_GMF_MediaPosition__set_stopTime(impl_POA_GMF_MediaPosition * servant,
				     GMF_MediaPosition_Value * value,
				     CORBA_Environment * ev)
{
   gtk_signal_emit(servant->gtk_object, mp_signals[SET_STOPTIME], value);
}

static GMF_MediaPosition_Value
impl_GMF_MediaPosition__get_prerollTime(impl_POA_GMF_MediaPosition * servant,
					GMF_MediaPosition_Type posType,
					CORBA_Environment * ev)
{
   GMF_MediaPosition_Value retval;

   retval._d = GMF_MediaPosition_TIME;
   retval._u.value_time.tvSec =
     retval._u.value_time.tvUsec = 0;

   gtk_signal_emit(servant->gtk_object, mp_signals[GET_PREROLLTIME], posType, &retval);

   return retval;
}

static void
impl_GMF_MediaPosition__set_prerollTime(impl_POA_GMF_MediaPosition * servant,
					GMF_MediaPosition_Value * value,
					CORBA_Environment * ev)
{
   gtk_signal_emit(servant->gtk_object, mp_signals[SET_PREROLLTIME], value);
}

static void gmf_media_position_class_init(GMFMediaPositionClass *klass);
static void gmf_media_position_init(GMFMediaPosition *obj);
static void gmf_media_position_destroy(GMFMediaPosition *obj);

static GtkObjectClass *media_position_parent_class = NULL;

typedef void (*GtkSignal_NONE__DOUBLE) (GtkObject *object, 
					gdouble arg1,
					gpointer user_data);

static void
gtk_marshal_NONE__DOUBLE (GtkObject    *object, 
			  GtkSignalFunc func, 
			  gpointer      func_data, 
			  GtkArg       *args)
{
  GtkSignal_NONE__DOUBLE rfunc;

  rfunc = (GtkSignal_NONE__DOUBLE) func;
  (* rfunc) (object,
	     GTK_VALUE_DOUBLE(args[0]),
	     func_data);
}

typedef void (*GtkSignal_NONE__ENUM_POINTER) (GtkObject *object, 
					      gint arg1,
					      gpointer arg2,
					      gpointer user_data);

static void
gtk_marshal_NONE__ENUM_POINTER (GtkObject    *object, 
				GtkSignalFunc func, 
				gpointer      func_data, 
				GtkArg       *args)
{
  GtkSignal_NONE__ENUM_POINTER rfunc;

  rfunc = (GtkSignal_NONE__ENUM_POINTER) func;
  (* rfunc) (object,
	     GTK_VALUE_ENUM(args[0]),
	     GTK_VALUE_POINTER(args[1]),
	     func_data);
}

GtkType
gmf_media_position_get_type (void)
{
  static GtkType gmf_media_position_type = 0;
  if (!gmf_media_position_type)
    {
      static const GtkTypeInfo gmf_media_position_info =
      {
	"GMFMediaPosition",
	sizeof (GMFMediaPosition),
	sizeof (GMFMediaPositionClass),
	(GtkClassInitFunc) gmf_media_position_class_init,
	(GtkObjectInitFunc) gmf_media_position_init,
	NULL, NULL,		/* reserved 1 & 2 */
	NULL
      };

      gmf_media_position_type = gtk_type_unique (bonobo_object_get_type(), &gmf_media_position_info);
      media_position_parent_class = gtk_type_class(gtk_type_parent(gmf_media_position_type));
    }

  return gmf_media_position_type;
}

static void
gmf_media_position_class_init(GMFMediaPositionClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = (gpointer)&gmf_media_position_destroy;

	klass->servant_init_func = POA_GMF_MediaPosition__init;
	klass->servant_destroy_func = POA_GMF_MediaPosition__fini;
	klass->vepv = &impl_GMF_MediaPosition_vepv;

  mp_signals[GET_PLAYBACK_RATE] =
    gtk_signal_new("get_playback_rate", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, get_playback_rate),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  mp_signals[SET_PLAYBACK_RATE] =
    gtk_signal_new("set_playback_rate", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, set_playback_rate),
		   gtk_marshal_NONE__DOUBLE,
		   GTK_TYPE_NONE, 1, GTK_TYPE_DOUBLE);

  mp_signals[CAN_SEEK_FORWARD] =
    gtk_signal_new("can_seek_forward", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, can_seek_forward),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  mp_signals[CAN_SEEK_BACKWARD] =
    gtk_signal_new("can_seek_backward", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, can_seek_backward),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  mp_signals[GET_DURATION] =
    gtk_signal_new("get_duration", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, get_duration),
		   gtk_marshal_NONE__ENUM_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_ENUM, GTK_TYPE_POINTER);

  mp_signals[GET_POSITION] =
    gtk_signal_new("get_position", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, get_position),
		   gtk_marshal_NONE__ENUM_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_ENUM, GTK_TYPE_POINTER);

  mp_signals[SET_POSITION] =
    gtk_signal_new("set_position", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, set_position),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  mp_signals[GET_STOPTIME] =
    gtk_signal_new("get_stop_time", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, get_stop_time),
		   gtk_marshal_NONE__ENUM_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_ENUM, GTK_TYPE_POINTER);

  mp_signals[SET_STOPTIME] =
    gtk_signal_new("set_stop_time", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, set_stop_time),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  mp_signals[GET_PREROLLTIME] =
    gtk_signal_new("get_preroll_time", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, get_preroll_time),
		   gtk_marshal_NONE__ENUM_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_ENUM, GTK_TYPE_POINTER);

  mp_signals[SET_PREROLLTIME] =
    gtk_signal_new("set_preroll_time", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaPositionClass, set_preroll_time),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals(object_class, mp_signals, LAST_SIGNAL);
}

static void
gmf_media_position_init(GMFMediaPosition *obj)
{
	CORBA_Environment ev;
	CORBA_exception_init(&ev);

	bonobo_object_construct (BONOBO_OBJECT(obj), impl_GMF_MediaPosition__create (obj, &ev));

	CORBA_exception_free (&ev);
}

static void
gmf_media_position_destroy(GMFMediaPosition *obj)
{
	if(media_position_parent_class->destroy)
    		media_position_parent_class->destroy((GtkObject *)obj);
}

GtkObject *gmf_media_position_new (void)
{
  return gtk_type_new(gmf_media_position_get_type());
}
