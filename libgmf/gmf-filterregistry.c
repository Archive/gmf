/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include <config.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

#include "libgmf.h"
#include "gmf-filterregistry.h"

#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>

GMF_Filter
gmf_filter_activate (const char *iid)
{
	CORBA_Environment ev;
	GMF_Filter retval;

	retval = NULL;
	
	CORBA_exception_init (&ev);
			
	retval = oaf_activate_from_id ((char *)iid, 0, NULL, &ev);

	if (ev._major != CORBA_NO_EXCEPTION) {
		if (ev._major == CORBA_USER_EXCEPTION) {
			CORBA_char *buffer;
			buffer = CORBA_exception_id (&ev);
			g_warning ("%s", buffer);
		} else {
			g_warning ("System excpetion: %s",
				CORBA_exception_id (&ev));
		}
	}

	CORBA_exception_free (&ev);

	return retval;
}

#if 0
static gboolean string_in_array(const char *string, const char **array)
{
	int i;
	for(i = 0; array[i]; i++) {
		if(!strcmp(string, array[i])) {
			return TRUE;
		}
  	}
	return FALSE;
}
#endif

static GSList *
get_lang_list (void)
{
        GSList *retval;
        char *lang;
        char * equal_char;

        retval = NULL;

        lang = g_getenv ("LANGUAGE");

        if (!lang) {
                lang = g_getenv ("LANG");
        }


        if (lang) {
                equal_char = strchr (lang, '=');
                if (equal_char != NULL) {
                        lang = equal_char + 1;
                }

                retval = g_slist_prepend (retval, lang);
        }
        
        return retval;
}

/**
 * gmf_filter_activate_for_uri:
 * 
 */
GMF_Filter
gmf_filter_activate_for_uri (GnomeVFSURI *uri)
{
	char *proto, *query, *iid, *tmp_uri;
	char **tmp_uri_val;
	GMF_Filter retval = CORBA_OBJECT_NIL;
	CORBA_Environment ev;
	GMF_AttributeAccess setfn;
	CORBA_any setme;	
        OAF_ServerInfoList *oaf_result;
	int index;
	const char *mime_type, *scheme, *uri_string;
	
	iid = NULL;
	
	/* Find the protocol to use for 'url' */
	scheme = gnome_vfs_uri_get_scheme (uri);
	proto = strdup (scheme);
	
	/* Get mime type */
	mime_type = gnome_vfs_get_mime_type_from_uri (uri);
	
	CORBA_exception_init(&ev);

#if 0
	/* This is a really ugly hard-coded hack to figure out what filter to activate for different devices... */
	if (strcmp (scheme, "file") == 0) {
		struct stat sbuf;

		if (stat (filename, &sbuf) == 0) {
			if (S_ISBLK (sbuf.st_mode) && strstr (filename, "cdrom")) {
				/* What could it be? Only answer right now is CD-ROM. */
				g_free (proto);
				proto = g_strdup ("device:cdrom");
      			} else if (S_ISCHR (sbuf.st_mode)) {
#if defined(linux)
				if (strstr (filename, "/bttv") || strstr (filename, "/video")) {
					g_free (proto); 
					proto = g_strdup ("device:video4linux");
					} else if (strstr (filename, "/audio") || strstr (filename, "/dsp")) {
					g_free (proto); 
					proto = g_strdup ("device:oss-dsp");
				} else if (strstr (filename, "/sequencer") || strstr (filename, "/music")) {
					g_free (proto); 
					proto = g_strdup ("device:oss-sequencer");
				} else if (strstr (filename, "/midi")) {
					g_free (proto); 
					proto = g_strdup ("device:oss-midi");
				} else if (strstr (filename, "/fb")) {
					g_free (proto); 
					proto = g_strdup ("device:linux-framebuffer");
				}
#endif
			} /* else try to just open it like a normal file */
		}
	}
#endif
	
	/* Query OAF for all filters that can handle our protocol and mime type */
	query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')"
				 "AND gmf:protocols.has ('%s')"
				 , proto);

	oaf_result = oaf_query (query, NULL, &ev);	
	if (oaf_result == NULL) {
		g_warning ("gmf_filter_activate_for_url: oaf_query no results");
	}

	/* Check and see if we found a protocol handler */
 	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL) {
		for (index = 0; index < oaf_result->_length; index++) {			
			OAF_ServerInfo *server;

			/* We need to check the quality here */
                        server = &oaf_result->_buffer[index];
			iid = g_strdup (server->iid);
			index = oaf_result->_length;
		}
	} 

	/* Clean up query and query result */
	g_free (query);
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}

	if (iid != NULL) {
		retval = gmf_filter_activate (iid);
	}
	
	if (!CORBA_Object_is_nil (retval, &ev)) {
		setfn = Bonobo_Unknown_queryInterface (retval, "IDL:GMF/AttributeAccess:1.0", &ev);

    		if (ev._major != CORBA_NO_EXCEPTION || CORBA_Object_is_nil (setfn, &ev)) {
			Bonobo_Unknown_ref (retval, &ev);
			Bonobo_Unknown_unref (retval, &ev); /* Kill it off */
			CORBA_Object_release (retval, &ev); 
			retval = CORBA_OBJECT_NIL;

			CORBA_exception_free (&ev);
			g_free (proto);
			return retval;

		}

		uri_string = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
		setme._type = (CORBA_TypeCode)TC_string;
		tmp_uri = g_strdup (uri_string);
		tmp_uri_val = &tmp_uri;
		setme._value = tmp_uri_val;
		CORBA_any_set_release (&setme, CORBA_FALSE);
		GMF_AttributeAccess_set_attribute (setfn, "URL", &setme, &ev);

		if(ev._major != CORBA_NO_EXCEPTION) {
			Bonobo_Unknown_ref (retval, &ev);
			Bonobo_Unknown_unref (retval, &ev); /* Kill it off */
			CORBA_Object_release (retval, &ev); 
			retval = CORBA_OBJECT_NIL;
		}

		/* XXX unref setfn */
		CORBA_Object_release(setfn, &ev);
	}

	CORBA_exception_free (&ev);
	g_free (proto);
	return retval;
}


GMF_Filter
gmf_filter_activate_for_types (const char *intype, const char *outtype)
{
	GMF_Filter retval = CORBA_OBJECT_NIL;
	int index;
	CORBA_Environment ev;
	OAF_ServerInfoList *oaf_result;
	char *query;
		
	g_return_val_if_fail(intype || outtype, CORBA_OBJECT_NIL);
		
	CORBA_exception_init(&ev);

	/* Query OAF for all filters that can handle the requested input and output type */
	if (intype == NULL) {
		query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')"
				 	 "AND gmf:output_types.has ('%s')"
				 	 , outtype);
	} else if (outtype == NULL) {
		query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')"
				 	 "AND gmf:input_types.has ('%s')"
				 	 , intype);
	} else {
		/* Handle wildcards. A '*' means we don't care. We will take anyone
		 * that handles a type first, followed by any that have no
		 * type defined.
		 */
		if (strcmp (intype, "*") == 0) {
			query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')"
					 "AND gmf:input_types.defined ()"
				 	 "AND gmf:output_types.has ('%s')"
				 	 "OR repo_ids.has ('IDL:GMF/Filter:1.0')"
				 	 "AND gmf:output_types.has ('%s')"
				 	 ,outtype, outtype);

		} else if (strcmp (outtype, "*") == 0) {
			query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')"
					 "AND gmf:input_types.has ('%s')"
				 	 "AND gmf:output_types.defined ()"				 	 
				 	 "OR repo_ids.has ('IDL:GMF/Filter:1.0')"
				 	 "AND gmf:input_types.has ('%s')"
				 	 ,intype, intype);
		} else {
			query = g_strdup_printf ("repo_ids.has ('IDL:GMF/Filter:1.0')"
					 "AND gmf:input_types.has ('%s')"
				 	 "AND gmf:output_types.has ('%s')"				 	 
				 	 ,intype, outtype);
		}
	}

	oaf_result = oaf_query (query, NULL, &ev);	
	if (oaf_result == NULL) {
		g_warning ("gmf_filter_activate_for_url: oaf_query no results");
	}

	g_free (query);
	/* FIXME: We are now just activating the first one we find.  We need a better solution. */
	/* Check and see if we found a filter that can handle are requested types */
 	if (ev._major == CORBA_NO_EXCEPTION && oaf_result != NULL) {
		GSList *langs;
		
 	        langs = get_lang_list ();
		
		for (index = 0; index < oaf_result->_length; index++) {			
			OAF_ServerInfo *server;

                        server = &oaf_result->_buffer[index];
			retval = gmf_filter_activate(server->iid);
			break;
		}

		g_slist_free (langs);
	} 

	/* Clean up query result */
	if (oaf_result != NULL) {
		CORBA_free (oaf_result);
	}

	CORBA_exception_free(&ev);

	return retval;
}
