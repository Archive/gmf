/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_TIMEREFERENCE_H
#define GMF_TIMEREFERENCE_H 1

/* GtkObject GMFTimeReference.
 *  In the future this can sync over CORBA. For now, just use local clock.
 */

#define GMF_TYPE_TIME_REFERENCE	(gmf_time_reference_get_type())
#define GMF_TIME_REFERENCE(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_TIME_REFERENCE, GMFTimeReference))
#define GMF_TIME_REFERENCE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_TIME_REFERENCE, GMFTimeReferenceClass))
#define GMF_IS_TIME_REFERENCE(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_TIME_REFERENCE))
#define GMF_IS_TIME_REFERENCE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_TIME_REFERENCE))

struct _GMFTimeReference {
  GtkObject object;
  GMF_TimeReference trobj;
  gpointer servant; /* Only if this is an implementation */

  GHashTable *alarmids;
};

typedef struct {
  GtkObjectClass klass;
} GMFTimeReferenceClass;

GtkType gmf_time_reference_get_type (void);
GtkObject *gmf_time_reference_new (void);
GtkObject *gmf_time_reference_new_from_corba (GMF_TimeReference obj);

#ifdef G_CAN_INLINE
#include <time.h>
#include <sys/time.h>

G_INLINE_FUNC GMF_TimeVal
gmf_time_reference_current_time (GMFTimeReference *timeref)
{
  struct timeval tod;
  GMF_TimeVal retval;

  gettimeofday(&tod, NULL);
  retval.tvSec = tod.tv_sec;
  retval.tvUsec = tod.tv_usec;

  return retval;
}


G_INLINE_FUNC GMF_TimeVal
gmf_time_add (GMF_TimeVal t1, GMF_TimeVal t2)
{
  GMF_TimeVal retval;

  if(t1.tvSec < 0)
    t1.tvUsec *= -1;
  if(t2.tvSec < 0)
    t2.tvUsec *= -1;

  retval.tvSec = t1.tvSec + t2.tvSec;
  retval.tvUsec = t1.tvUsec + t2.tvUsec;

  /* normalize */
  retval.tvSec += (retval.tvUsec / 1000000);
  retval.tvUsec = (retval.tvUsec % 1000000);

  if((retval.tvUsec < 0) && retval.tvSec) {
    retval.tvSec--;
    retval.tvUsec += 1000000;
  }

  return retval;
}

G_INLINE_FUNC GMF_TimeVal
gmf_time_subtract(GMF_TimeVal t1, GMF_TimeVal t2)
{
  GMF_TimeVal retval;

  retval.tvSec = t1.tvSec - t2.tvSec;
  retval.tvUsec = t1.tvUsec - t2.tvUsec;

  /* normalize */
  retval.tvSec += (retval.tvUsec / 1000000);
  retval.tvUsec = (retval.tvUsec % 1000000);

  if((retval.tvUsec < 0) && retval.tvSec) {
    retval.tvSec--;
    retval.tvUsec += 1000000;
  }

  return retval;
}

G_INLINE_FUNC gint
gmf_time_compare(GMF_TimeVal t1, GMF_TimeVal t2)
{
  GMF_TimeVal t;

  t = gmf_time_subtract(t2, t1);
  if(t.tvSec > 0)
    return 1;
  else if(t.tvSec < 0)
    return -1;
  else {
    if(t.tvUsec > 0)
      return 1;
    else if(t.tvUsec < 0)
      return -1;
  }

  return 0;
}
#else
GMF_TimeVal gmf_time_reference_current_time(GMFTimeReference *timeref);

GMF_TimeVal gmf_time_subtract(GMF_TimeVal t1, GMF_TimeVal t2);
GMF_TimeVal gmf_time_add(GMF_TimeVal t1, GMF_TimeVal t2);
gint gmf_time_compare(GMF_TimeVal t1, GMF_TimeVal t2);
GMF_TimeVal gmf_time_multiply(GMF_TimeVal t, gint multiple);
gint gmf_time_divide(GMF_TimeVal t1, GMF_TimeVal t2, GMF_TimeVal *remainder);

#endif

/* To make life easier on everyone, alarms should always return FALSE */
GMF_Callback_CallbackID
gmf_time_reference_request_alarm(GMFTimeReference *timeref,
				 GMF_TimeVal *base_time,
				 GMF_TimeVal *stream_time,
				 GSourceFunc cbfunc,
				 gpointer cbdata);

/* metronomes should return TRUE if they want to stay informed,
   FALSE if not */
GMF_Callback_CallbackID
gmf_time_reference_request_metronome(GMFTimeReference *timeref,
				     GMF_TimeVal *base_time,
				     GMF_TimeVal *frequency_time,
				     GSourceFunc cbfunc,
				     gpointer cbdata);

void
gmf_time_reference_remove_request(GMFTimeReference *timeref,
				  GMF_Callback_CallbackID cbid);

#endif /* GMF_TIMEREFERENCE_H */
