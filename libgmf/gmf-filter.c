/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "config.h"
#include <gmf.h>
#include <stdlib.h>
#include <sched.h>

#include "gmf-gtk-macros.h"

typedef struct {
  /* This is a seekable filter for both GMFFilter and
     GMFSeekableFilter classes. */
	POA_GMF_Filter servant;
	gpointer bonobo_object;
	
	GMF_Filter_State attr_filterState;

	GMF_FilterGraph attr_graph; CORBA_char *attr_graphName;
	GMF_Filter_Type attr_filterType;

	GMFFilter *gtk_object;

} impl_POA_GMF_Filter;

#define FILTER_DATA(x) ((impl_POA_GMF_Filter *)(BONOBO_OBJECT(x)->servant))

static void
impl_GMF_EventReceiver_deliver(impl_POA_GMF_Filter * servant,
			       GMF_TimeVal *timestamp,
			       CORBA_char *evName,
			       CORBA_any *evData,
			       CORBA_Environment *ev);

static GMF_Filter_State
impl_GMF_Filter__get_filterState(impl_POA_GMF_Filter * servant,
				 CORBA_Environment * ev);

static void
impl_GMF_Filter_Stop(impl_POA_GMF_Filter * servant,
		     CORBA_Environment * ev);

static void
impl_GMF_Filter_Pause(impl_POA_GMF_Filter * servant,
		      CORBA_Environment * ev);

static void
impl_GMF_Filter_Run(impl_POA_GMF_Filter * servant,
		    CORBA_Environment * ev);

static void
impl_GMF_Filter_SetTimeBase(impl_POA_GMF_Filter * servant,
			    GMF_TimeVal *timeBase,
			    CORBA_Environment * ev);

static GMF_PipeList *
impl_GMF_Filter__get_inputPipes(impl_POA_GMF_Filter * servant,
				CORBA_Environment * ev);

static GMF_PipeList *
impl_GMF_Filter__get_outputPipes(impl_POA_GMF_Filter * servant,
				 CORBA_Environment * ev);

static GMF_Pipe
impl_GMF_Filter_GetPipe(impl_POA_GMF_Filter * servant,
			GMF_Direction pDirection,
			CORBA_Environment * ev);

static void
impl_GMF_Filter_JoinFilterGraph(impl_POA_GMF_Filter * servant,
				CORBA_char * name,
				GMF_FilterGraph fg,
				CORBA_Environment * ev);
static GMF_FilterGraph
impl_GMF_Filter__get_graph(impl_POA_GMF_Filter * servant,
			   CORBA_Environment * ev);
static CORBA_char *
impl_GMF_Filter__get_graphName(impl_POA_GMF_Filter *servant,
			       CORBA_Environment *ev);

static GMF_Filter_Type
impl_GMF_Filter__get_filterType(impl_POA_GMF_Filter *servant,
			       CORBA_Environment *ev);

static GMF_TimeReference
impl_GMF_Filter__get_syncSource(impl_POA_GMF_Filter *servant,
				CORBA_Environment *ev);

static void
impl_GMF_Filter__set_syncSource(impl_POA_GMF_Filter *servant,
				GMF_TimeReference value,
				CORBA_Environment *ev);

static void
impl_GMF_Filter_invoke(impl_POA_GMF_Filter * servant,
		       CORBA_Environment * ev);
static GNOME_stringlist *impl_GMF_Filter__get_attributes(impl_POA_GMF_Filter * servant,
							 CORBA_Environment *ev);
static CORBA_any *impl_GMF_Filter_get_attribute(impl_POA_GMF_Filter * servant,
						CORBA_char * attr_name,
						CORBA_Environment *ev);
static void impl_GMF_Filter_set_attribute(impl_POA_GMF_Filter * servant,
					  CORBA_char * attr_name,
					  CORBA_any * attr_val,
					  CORBA_Environment *ev);

enum { STOP, PAUSE,
       RUN,
       GET_PIPE,
       GET_ATTRIBUTES,
       GET_ATTRIBUTE,
       SET_ATTRIBUTE,
       DELIVER_EVENT,
       LAST_SIGNAL };
static guint filter_signals[LAST_SIGNAL] = { 0 };

static POA_GMF_AttributeAccess__epv impl_GMF_Filter_GMF_AttributeAccess_epv = {
   NULL,			/* _private */
   (gpointer) & impl_GMF_Filter__get_attributes,
   (gpointer) & impl_GMF_Filter_get_attribute,
   (gpointer) & impl_GMF_Filter_set_attribute
};

static POA_GMF_StateChange__epv impl_GMF_Filter_GMF_StateChange_epv = 
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_Filter_Stop,
   (gpointer) & impl_GMF_Filter_Pause,
   (gpointer) & impl_GMF_Filter_Run,
   (gpointer) & impl_GMF_Filter_SetTimeBase   
};

static POA_GMF_Filter__epv impl_GMF_Filter_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_Filter__get_inputPipes,
   (gpointer) & impl_GMF_Filter__get_outputPipes,
   (gpointer) & impl_GMF_Filter_GetPipe,
   (gpointer) & impl_GMF_Filter_JoinFilterGraph,
   (gpointer) & impl_GMF_Filter__get_graph,
   (gpointer) & impl_GMF_Filter__get_graphName,
   (gpointer) & impl_GMF_Filter__get_filterState,
   (gpointer) & impl_GMF_Filter__get_filterType,
   (gpointer) & impl_GMF_Filter__get_syncSource,
   (gpointer) & impl_GMF_Filter__set_syncSource
};

static POA_GMF_Callback__epv impl_GMF_Filter_GMF_Callback_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_Filter_invoke
};

static POA_GMF_EventReceiver__epv impl_GMF_Filter_GMF_EventReceiver_epv =
{
  NULL, /* _private */
  (gpointer) &impl_GMF_EventReceiver_deliver
};

static PortableServer_ServantBase__epv impl_GMF_Filter_base_epv =
{
   NULL,			/* _private data */
   NULL,                        /* finalize routine */
   NULL,			/* default_POA routine */
};

static POA_GMF_Filter__vepv impl_GMF_Filter_vepv =
{
   &impl_GMF_Filter_base_epv,
   NULL,
   &impl_GMF_Filter_GMF_Callback_epv,
   &impl_GMF_Filter_GMF_AttributeAccess_epv,
   &impl_GMF_Filter_GMF_StateChange_epv,
   &impl_GMF_Filter_GMF_EventReceiver_epv,
   &impl_GMF_Filter_epv
};

static void gmf_filter_class_init (GMFFilterClass * klass);
static void gmf_filter_init (GMFFilter * obj);
static void gmf_filter_destroy (GMFFilter *filter);
static void gmf_filter_relay_begin_flush(GMFPipe *pipe, GMFFilter *filter);
static void gmf_filter_relay_end_flush(GMFPipe *pipe, GMFFilter *filter);
static void gmf_filter_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void gmf_filter_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void gmf_filter_deliver_event(GMFFilter *filter, GMF_TimeVal *timeval, const char *evName, CORBA_any *evData);

static void
impl_GMF_EventReceiver_deliver(impl_POA_GMF_Filter * servant, GMF_TimeVal *timestamp, CORBA_char *evName,
			       CORBA_any *evData, CORBA_Environment *ev)
{
	gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[DELIVER_EVENT], timestamp, evName, evData);
}


static void
impl_GMF_Filter__destroy(BonoboObject *obj, impl_POA_GMF_Filter *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	void (*servant_destroy_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	CORBA_exception_init(&ev);

	if(servant->attr_graphName) {
    		GMF_FilterGraph_RemoveFilter(servant->attr_graph,
				 servant->attr_graphName,
				 &ev);
		CORBA_free(servant->attr_graphName);    	
  	}
	
	servant_destroy_func = GMF_FILTER_CLASS (GTK_OBJECT (servant->gtk_object)->klass)->servant_destroy_func;
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	servant_destroy_func ((PortableServer_Servant) servant, &ev);
	g_free (servant);
	CORBA_exception_free(&ev);
}

static GMF_Filter
impl_GMF_Filter__create (GMFFilter *filter, CORBA_Environment * ev)
{
  	GMF_Filter retval;
	impl_POA_GMF_Filter *servant;
	void (*servant_init_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	GMFFilterClass *filter_class = GMF_FILTER_CLASS (GTK_OBJECT(filter)->klass);
	
	servant_init_func = filter_class->servant_init_func;
	servant = g_new0 (impl_POA_GMF_Filter, 1);
	servant->servant.vepv = filter_class->vepv;
	if (!servant->servant.vepv->Bonobo_Unknown_epv)
		servant->servant.vepv->Bonobo_Unknown_epv = bonobo_object_get_epv ();
	servant_init_func ((PortableServer_Servant) servant, ev);
	
	servant->gtk_object = filter;
	
	retval = bonobo_object_activate_servant (BONOBO_OBJECT (filter), servant);
	
	gtk_signal_connect (GTK_OBJECT (filter), "destroy", GTK_SIGNAL_FUNC (impl_GMF_Filter__destroy), servant);
	
	return retval;
}

static GMF_Filter_State
impl_GMF_Filter__get_filterState(impl_POA_GMF_Filter * servant,
				 CORBA_Environment * ev)
{
   return servant->attr_filterState;
}

static void
impl_GMF_Filter_Stop(impl_POA_GMF_Filter * servant,
		     CORBA_Environment * ev)
{
  if(servant->attr_filterState != GMF_Filter_STOPPED) {
    servant->gtk_object->state = 
      servant->attr_filterState = GMF_Filter_STOPPED;
    gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[STOP]);
  }
}

static void
impl_GMF_Filter_Pause(impl_POA_GMF_Filter * servant,
		      CORBA_Environment * ev)
{
  if(servant->attr_filterState != GMF_Filter_PAUSED) {
    servant->gtk_object->state = 
      servant->attr_filterState = GMF_Filter_PAUSED;
    gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[PAUSE]);
  }
}

static void
impl_GMF_Filter_Run(impl_POA_GMF_Filter * servant,
		    CORBA_Environment * ev)
{
  if(servant->attr_filterState != GMF_Filter_RUNNING) {
    servant->gtk_object->state = 
      servant->attr_filterState = GMF_Filter_RUNNING;

    gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[RUN]);
  }
}

static void
impl_GMF_Filter_SetTimeBase(impl_POA_GMF_Filter * servant,
			    GMF_TimeVal *timeBase,
			    CORBA_Environment * ev)
{
  servant->gtk_object->timeBase = *timeBase;
}

static GMF_PipeList *
impl_GMF_Filter__get_inputPipes(impl_POA_GMF_Filter * servant,
				CORBA_Environment * ev)
{
   GMF_PipeList *retval;
   int i;
   GList *ltmp;

   retval = GMF_PipeList__alloc();
   retval->_length = g_list_length(servant->gtk_object->inputPipes);
   retval->_buffer = CORBA_sequence_GMF_Pipe_allocbuf(retval->_length);
   for(ltmp = servant->gtk_object->inputPipes, i = 0; ltmp; ltmp = g_list_next(ltmp), i++) {
     retval->_buffer[i] =
       CORBA_Object_duplicate(bonobo_object_corba_objref(ltmp->data), ev);
   }

   CORBA_sequence_set_release(retval, CORBA_TRUE);

   return retval;
}

static GMF_PipeList *
impl_GMF_Filter__get_outputPipes(impl_POA_GMF_Filter * servant,
				 CORBA_Environment * ev)
{
	GMF_PipeList *retval;
	int i;
	GList *ltmp;

	retval = GMF_PipeList__alloc();
	retval->_length = g_list_length (servant->gtk_object->outputPipes);
	retval->_buffer = CORBA_sequence_GMF_Pipe_allocbuf(retval->_length);
	for(ltmp = servant->gtk_object->outputPipes, i = 0; ltmp; ltmp = g_list_next(ltmp), i++) {
		retval->_buffer[i] =
			CORBA_Object_duplicate (bonobo_object_corba_objref (ltmp->data), ev);
	}

	CORBA_sequence_set_release(retval, CORBA_TRUE);

	return retval;
}

GMF_Pipe
impl_GMF_Filter_GetPipe(impl_POA_GMF_Filter * servant,
			GMF_Direction pDirection,
			CORBA_Environment * ev)
{
	GtkArg targ;
	GMF_Pipe retval = CORBA_OBJECT_NIL;
	GList *list;
	GMFPipe *ptmp = NULL;

	if(pDirection == GMF_IN)
		list = servant->gtk_object->inputPipes;
	else
		list = servant->gtk_object->outputPipes;

	for(; list; list = g_list_next(list)) {
		targ.type = GTK_TYPE_BOOL;
		targ.name = "inuse";
		gtk_object_getv(list->data, 1, &targ);

    		if(!GTK_VALUE_BOOL(targ)) {
			retval = CORBA_Object_duplicate(bonobo_object_corba_objref(list->data), ev);
      		break;
		}
	}

	if(CORBA_Object_is_nil(retval, ev)) {
		gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[GET_PIPE],
				pDirection, &ptmp);

		if(ptmp) {
			retval = CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(ptmp)), ev);
		}
	}

	return retval;
}

static void
impl_GMF_Filter_JoinFilterGraph(impl_POA_GMF_Filter * servant,
				CORBA_char * name,
				GMF_FilterGraph fg,
				CORBA_Environment * ev)
{
  if(servant->attr_graphName) {
    if(CORBA_Object_is_nil(fg, ev)) {
      CORBA_Object_release(servant->attr_graph, ev);
      servant->attr_graph = CORBA_OBJECT_NIL;
      CORBA_free(servant->attr_graphName); servant->attr_graphName = NULL;
    } else
      CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			  ex_GMF_Filter_JoinRefused, NULL);
  } else {
    servant->attr_graph = CORBA_Object_duplicate(fg, ev);
    servant->attr_graphName = CORBA_string_dup(name);
  }
}

static GMF_FilterGraph
impl_GMF_Filter__get_graph(impl_POA_GMF_Filter * servant,
			   CORBA_Environment * ev)
{
  return CORBA_Object_duplicate(servant->attr_graph, ev);
}

static CORBA_char *
impl_GMF_Filter__get_graphName(impl_POA_GMF_Filter *servant,
			       CORBA_Environment *ev)
{
  return CORBA_string_dup(servant->attr_graphName);
}

static GMF_Filter_Type
impl_GMF_Filter__get_filterType(impl_POA_GMF_Filter *servant,
				CORBA_Environment *ev)
{
  return servant->attr_filterType;
}

static GMF_TimeReference
impl_GMF_Filter__get_syncSource(impl_POA_GMF_Filter *servant,
				CORBA_Environment *ev)
{
  return CORBA_OBJECT_NIL;
}

static void
impl_GMF_Filter__set_syncSource(impl_POA_GMF_Filter *servant,
				GMF_TimeReference value,
				CORBA_Environment *ev)
{
}

static void
impl_GMF_Filter_invoke(impl_POA_GMF_Filter * servant,
		       CORBA_Environment * ev)
{
  g_error("GMF_Filter_invoke NYI - fix the time reference workings.");
}

static GNOME_stringlist *
impl_GMF_Filter__get_attributes(impl_POA_GMF_Filter * servant,
				CORBA_Environment *ev)
{
  GNOME_stringlist *retval;

  retval = GNOME_stringlist__alloc();
  retval->_length = 0;
  retval->_buffer = NULL;

  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[GET_ATTRIBUTES],
		  retval);

  return retval;
}

static CORBA_any *
impl_GMF_Filter_get_attribute(impl_POA_GMF_Filter * servant,
			      CORBA_char * attr_name,
			      CORBA_Environment *ev)
{
  CORBA_any *retval;

  retval = CORBA_any_alloc();

  retval->_type = (CORBA_TypeCode)TC_void;
  retval->_value = NULL;
  CORBA_any_set_release(retval, CORBA_TRUE);

  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[GET_ATTRIBUTE],
		  attr_name, retval);

  return retval;
}

static void
impl_GMF_Filter_set_attribute(impl_POA_GMF_Filter * servant,
			      CORBA_char * attr_name,
			      CORBA_any * attr_val,
			      CORBA_Environment *ev)
{
  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), filter_signals[SET_ATTRIBUTE],
		  attr_name, attr_val, ev);

}

GtkObjectClass *parent_class;

GtkType
gmf_filter_get_type (void)
{
  static GtkType gmf_filter_type = 0;
  if (!gmf_filter_type)
    {
      static const GtkTypeInfo gmf_filter_info =
      {
	"GMFFilter",
	sizeof (GMFFilter),
	sizeof (GMFFilterClass),
	(GtkClassInitFunc) gmf_filter_class_init,
	(GtkObjectInitFunc) gmf_filter_init,
	NULL, NULL,		/* reserved 1 & 2 */
	NULL
      };

	gmf_filter_type = gtk_type_unique (bonobo_object_get_type(), &gmf_filter_info);
     	parent_class = gtk_type_class(gtk_type_parent(gmf_filter_type));
    }

  return gmf_filter_type;
}

enum { ARG_0, ARG_FILTER_TYPE };

static void 
gmf_filter_class_init (GMFFilterClass * klass)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (klass);
  	object_class->destroy = (gpointer)&gmf_filter_destroy;
  	object_class->set_arg = &gmf_filter_set_arg;
  	object_class->get_arg = &gmf_filter_get_arg;

  	klass->deliver_event = &gmf_filter_deliver_event;

	klass->servant_init_func = POA_GMF_Filter__init;
	klass->servant_destroy_func = POA_GMF_Filter__fini;
	klass->vepv = &impl_GMF_Filter_vepv;

  	filter_signals[STOP] = gtk_signal_new("stop", GTK_RUN_FIRST,
		   				object_class->type,
		   				GTK_SIGNAL_OFFSET(GMFFilterClass, stop),
		   				gtk_marshal_NONE__NONE,
		   				GTK_TYPE_NONE, 0);
  	filter_signals[PAUSE] =
    		gtk_signal_new("pause", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, pause),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0);
  	filter_signals[RUN] =
    		gtk_signal_new("run", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, run),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0);
  	filter_signals[GET_PIPE] =
    		gtk_signal_new("get_pipe", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, get_pipe),
		   gtk_marshal_NONE__INT_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_ENUM, GTK_TYPE_POINTER);
  	filter_signals[GET_ATTRIBUTES] =
    		gtk_signal_new("get_attributes", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, get_attributes),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  	filter_signals[GET_ATTRIBUTE] =
    		gtk_signal_new("get_attribute", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, get_attribute),
		   gtk_marshal_NONE__POINTER_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_POINTER, GTK_TYPE_POINTER);
  	filter_signals[SET_ATTRIBUTE] =
    		gtk_signal_new("set_attribute", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, set_attribute),
		   gtk_marshal_NONE__POINTER_POINTER_POINTER,
		   GTK_TYPE_NONE, 3, GTK_TYPE_POINTER, GTK_TYPE_POINTER, GTK_TYPE_POINTER);
  	filter_signals[DELIVER_EVENT] =
    		gtk_signal_new("deliver_event", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFFilterClass, deliver_event),
		   gtk_marshal_NONE__POINTER_POINTER_POINTER,
		   GTK_TYPE_NONE, 3, GTK_TYPE_POINTER, GTK_TYPE_POINTER, GTK_TYPE_POINTER);

  	gtk_object_class_add_signals(object_class, filter_signals, LAST_SIGNAL);

  	gtk_object_add_arg_type("GMFFilter::filter_type", GTK_TYPE_ENUM,
                          GTK_ARG_READWRITE|GTK_ARG_CONSTRUCT_ONLY, ARG_FILTER_TYPE);
}

static void
gmf_filter_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
	switch(arg_id) {
  		case ARG_FILTER_TYPE:
			GTK_VALUE_ENUM(*arg) = FILTER_DATA(obj)->attr_filterType;
    			break;
    			
  		default:
			GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, get_arg, (obj, arg, arg_id));	    		
			break;
  	}
}

static void
gmf_filter_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
	switch(arg_id) {
		case ARG_FILTER_TYPE:
			FILTER_DATA(obj)->attr_filterType = GTK_VALUE_ENUM(*arg);
			break;
			
		default:
			GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, set_arg, (obj, arg, arg_id));	    		    
			break;
  }
}

static void 
gmf_filter_init (GMFFilter *filter)
{
	CORBA_Environment ev;
	CORBA_exception_init(&ev);

	filter->state = GMF_Filter_STOPPED;

	filter->timeref = GMF_TIME_REFERENCE (gmf_time_reference_new());
	gtk_object_ref(GTK_OBJECT (filter->timeref));
	gtk_object_sink(GTK_OBJECT (filter->timeref));
	
	bonobo_object_construct (BONOBO_OBJECT (filter), impl_GMF_Filter__create (filter, &ev));

	CORBA_exception_free(&ev);
}

static void
gmf_filter_destroy (GMFFilter *filter)
{

	gtk_object_unref(GTK_OBJECT(filter->timeref));

  	g_list_foreach(filter->inputPipes, (GFunc)gtk_object_unref, NULL);
  	g_list_free(filter->inputPipes);
  	g_list_foreach(filter->outputPipes, (GFunc)gtk_object_unref, NULL);
  	g_list_free(filter->outputPipes);

	GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, destroy, GTK_OBJECT (filter));
}

void
gmf_filter_remove_pipe(GMFFilter *filter, GMFPipe *apipe)
{
	GtkArg targ;
	impl_POA_GMF_Filter *servant;

	servant = FILTER_DATA(filter);

	targ.type = GTK_TYPE_ENUM;
	targ.name = "direction";

	gtk_object_getv(GTK_OBJECT(apipe), 1, &targ);
	if(GTK_VALUE_ENUM(targ) == GMF_IN) {
		guint sigid;

		sigid = GPOINTER_TO_UINT(gtk_object_get_data(GTK_OBJECT(apipe),
						 "begin_flush_id"));
		gtk_signal_disconnect(GTK_OBJECT(apipe), sigid);

		sigid = GPOINTER_TO_UINT(gtk_object_get_data(GTK_OBJECT(apipe),
						 "end_flush_id"));
		gtk_signal_disconnect(GTK_OBJECT(apipe), sigid);

		if(!GTK_OBJECT_DESTROYED(filter))
			servant->gtk_object->inputPipes = g_list_remove(servant->gtk_object->inputPipes, apipe);
		} else if(GTK_VALUE_ENUM(targ) == GMF_OUT) {
			if(!GTK_OBJECT_DESTROYED(filter))
				servant->gtk_object->outputPipes = g_list_remove(servant->gtk_object->outputPipes, apipe);
  	} else
		g_error("invalid pipe dir");

	gtk_object_unref(GTK_OBJECT(apipe));
}

void
gmf_filter_add_pipe(GMFFilter *filter, GMFPipe *apipe)
{
	GtkArg targ;
	impl_POA_GMF_Filter *servant;

	servant = FILTER_DATA(filter);

	targ.type = GTK_TYPE_ENUM;
	targ.name = "direction";

	gtk_object_getv(GTK_OBJECT(apipe), 1, &targ);
	if(GTK_VALUE_ENUM(targ) == GMF_IN) {
		guint sigid;

		g_return_if_fail(servant->attr_filterType != GMF_Filter_SOURCE);

		g_return_if_fail(!g_list_find(servant->gtk_object->inputPipes, apipe));

		servant->gtk_object->inputPipes = g_list_prepend(servant->gtk_object->inputPipes, apipe);

		/* set up downstream relaying of flushes */
		sigid = gtk_signal_connect(GTK_OBJECT(apipe), "begin_flush",
			       GTK_SIGNAL_FUNC(gmf_filter_relay_begin_flush),
			       filter);
    		gtk_object_set_data(GTK_OBJECT(apipe), "begin_flush_id", GUINT_TO_POINTER(sigid));
    		gtk_signal_connect (GTK_OBJECT(apipe), "end_flush",
		       		    GTK_SIGNAL_FUNC(gmf_filter_relay_end_flush),
		       		    filter);
    		gtk_object_set_data(GTK_OBJECT(apipe), "end_flush_id",
				    GUINT_TO_POINTER(sigid));
  	} else if(GTK_VALUE_ENUM(targ) == GMF_OUT) {
    		g_return_if_fail(servant->attr_filterType != GMF_Filter_RENDERER);

    		g_return_if_fail(!g_list_find(servant->gtk_object->outputPipes, apipe));

    		servant->gtk_object->outputPipes = g_list_prepend(servant->gtk_object->outputPipes, apipe);
  	} else
    		g_error("invalid pipe dir");

  	gtk_object_ref (GTK_OBJECT(apipe));
  	gtk_object_sink (GTK_OBJECT(apipe));
}

static void
gmf_filter_relay_begin_flush(GMFPipe *pipe,
			     GMFFilter *filter)
{
	GList *ltmp;

	for(ltmp = FILTER_DATA(filter)->gtk_object->outputPipes; ltmp; ltmp = g_list_next(ltmp))
		gmf_output_pipe_deliver_begin_flush(((GMFOutputPipe *)ltmp->data));
}

static void
gmf_filter_relay_end_flush(GMFPipe *apipe,
			   GMFFilter *filter)
{
	GList *ltmp;

	for(ltmp = FILTER_DATA(filter)->gtk_object->outputPipes; ltmp; ltmp = g_list_next(ltmp))
		gmf_output_pipe_deliver_end_flush(((GMFOutputPipe *)ltmp->data));
}

static void
gmf_filter_deliver_event(GMFFilter *filter, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData)
{
	GList *ltmp;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	if(!strcmp(evName, "EndOfStream")) {

		/* This event goes to any output pipes we have. If we're at the
		end of the line, then it goes to the graph upstairs. */

		if(filter->outputPipes) {
       			for(ltmp = filter->outputPipes; ltmp; ltmp = g_list_next(ltmp))
	 			gtk_signal_emit_by_name(GTK_OBJECT(ltmp->data), "deliver_event", timestamp, evName, evData);
     		} else if(!CORBA_Object_is_nil(FILTER_DATA(filter)->attr_graph, &ev)
	       		&& FILTER_DATA(filter)->attr_filterType == GMF_Filter_RENDERER) {
       			GMF_EventReceiver_deliver(FILTER_DATA(filter)->attr_graph, timestamp, (CORBA_char *)evName, evData, &ev);
		}
	}

	CORBA_exception_free (&ev);
}
