/* Gnome-Streamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GMF_CLOCK_H
#define GMF_CLOCK_H

//#include <gst/gst.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef guint64 GmfClockTime;
typedef gint64 GmfClockTimeDiff;

#define gmf_clock_DIFF(s, e) (GmfClockTimeDiff)((s)-(e))

typedef struct _GmfClock GmfClock;

struct _GmfClock {
	gchar *name;
	GmfClockTime start_time;
	GmfClockTime current_time;
	GmfClockTimeDiff adjust;
	gboolean locking;
	GList *sinkobjects;
	gint num, num_locked;
	GMutex *sinkmutex;
	GMutex *lock;
};

GmfClock 	 *gmf_clock_new 	(gchar 		*name);
GmfClock 	 *gmf_clock_get_system	(void);
//void 		 gmf_clock_register	(GmfClock 	*clock, 
//					 GstObject	*obj);
void 		 gmf_clock_set		(GmfClock 	*clock, 
					 GmfClockTime 	time);
void 		 gmf_clock_reset	(GmfClock 	*clock);
void 		 gmf_clock_wait		(GmfClock 	*clock, 
					 GmfClockTime 	time);
GmfClockTimeDiff gmf_clock_current_diff	(GmfClock 	*clock, 
					 GmfClockTime 	time);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* GMF_CLOCK_H */
