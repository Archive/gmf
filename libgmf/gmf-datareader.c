/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "config.h"

#include <gtk/gtk.h>

#include "gmf-datareader.h"

typedef struct {
  POA_GMF_DataReader servant;
  GMFDataReader *gtk_object;

} impl_POA_GMF_DataReader;

enum {
  READ,
  GET_POSITION,
  SET_POSITION,
  GET_LENGTH,
  LAST_SIGNAL
};
static guint dr_signals[LAST_SIGNAL] = { 0 };

static void
impl_GMF_DataReader_read(impl_POA_GMF_DataReader * servant,
			 CORBA_long nbytes,
			 GMF_Data **rv,
			 CORBA_Environment * ev);
static CORBA_long
impl_GMF_DataReader_get_position(impl_POA_GMF_DataReader * servant,
				 CORBA_Environment *ev);
static CORBA_long
impl_GMF_DataReader_set_position(impl_POA_GMF_DataReader * servant,
				 CORBA_long new_pos,
				 GMF_DataReader_PositionRelation prel,
				 CORBA_Environment *ev);
static CORBA_long
impl_GMF_DataReader__get_length(impl_POA_GMF_DataReader * servant,
				CORBA_Environment * ev);

static POA_GMF_DataReader__epv impl_GMF_DataReader_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_DataReader_read,
   (gpointer) & impl_GMF_DataReader_get_position,
   (gpointer) & impl_GMF_DataReader_set_position,
   (gpointer) & impl_GMF_DataReader__get_length
};

static PortableServer_ServantBase__epv base_epv = { NULL, NULL, NULL };
static POA_GMF_DataReader__vepv impl_GMF_DataReader_vepv =
{
	&base_epv,
	NULL,
	&impl_GMF_DataReader_epv
};


static void
impl_GMF_DataReader__destroy(BonoboObject *obj, impl_POA_GMF_DataReader *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	void (*servant_destroy_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	CORBA_exception_init(&ev);
	
	servant_destroy_func = GMF_DATA_READER_CLASS (GTK_OBJECT (servant->gtk_object)->klass)->servant_destroy_func;
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	servant_destroy_func ((PortableServer_Servant) servant, &ev);
	g_free (servant);
	CORBA_exception_free(&ev);
}


static GMF_DataReader
impl_GMF_DataReader__create (GMFDataReader *reader, CORBA_Environment * ev)
{
     	GMF_DataReader retval;
	impl_POA_GMF_DataReader *servant;
	void (*servant_init_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	GMFDataReaderClass *reader_class = GMF_DATA_READER_CLASS (GTK_OBJECT(reader)->klass);
	
	servant_init_func = reader_class->servant_init_func;
	servant = g_new0 (impl_POA_GMF_DataReader, 1);
	servant->servant.vepv = reader_class->vepv;
	if (!servant->servant.vepv->Bonobo_Unknown_epv)
		servant->servant.vepv->Bonobo_Unknown_epv = bonobo_object_get_epv ();
	servant_init_func ((PortableServer_Servant) servant, ev);
	
	servant->gtk_object = reader;
	
	retval = bonobo_object_activate_servant (BONOBO_OBJECT (reader), servant);
	
	gtk_signal_connect (GTK_OBJECT (reader), "destroy", GTK_SIGNAL_FUNC (impl_GMF_DataReader__destroy), servant);
	
	return retval;

}

static CORBA_long
impl_GMF_DataReader__get_length(impl_POA_GMF_DataReader * servant,
				CORBA_Environment * ev)
{
  CORBA_long rv;

  gtk_signal_emit((GtkObject *)servant->gtk_object, dr_signals[GET_LENGTH], &rv);

  return rv;
}

static CORBA_long
impl_GMF_DataReader_get_position(impl_POA_GMF_DataReader * servant,
				 CORBA_Environment *ev)
{
  CORBA_long retval = 0;

  gtk_signal_emit((GtkObject *)servant->gtk_object, dr_signals[GET_POSITION], &retval);

  return retval;
}

static CORBA_long
impl_GMF_DataReader_set_position(impl_POA_GMF_DataReader * servant,
				 CORBA_long new_pos,
				 GMF_DataReader_PositionRelation prel,
				 CORBA_Environment *ev)
{
  CORBA_long retval = 0;

  gtk_signal_emit((GtkObject *)servant->gtk_object, dr_signals[SET_POSITION], new_pos, prel, &retval);

  return retval;
}

static void
impl_GMF_DataReader_read(impl_POA_GMF_DataReader * servant,
			 CORBA_long nbytes,
			 GMF_Data **rv,
			 CORBA_Environment * ev)
{
  gtk_signal_emit((GtkObject *)servant->gtk_object, dr_signals[READ], nbytes, rv);
}

static void gmf_data_reader_class_init(GMFDataReaderClass *klass);
static void gmf_data_reader_init(GMFDataReader *obj);
static void gmf_data_reader_destroy(GMFDataReader *obj);

static GtkObjectClass *data_reader_parent_class = NULL;

GtkType
gmf_data_reader_get_type (void)
{
  static GtkType gmf_data_reader_type = 0;
  if (!gmf_data_reader_type)
    {
      static const GtkTypeInfo gmf_data_reader_info =
      {
	"GMFDataReader",
	sizeof (GMFDataReader),
	sizeof (GMFDataReaderClass),
	(GtkClassInitFunc) gmf_data_reader_class_init,
	(GtkObjectInitFunc) gmf_data_reader_init,
	NULL, NULL,		/* reserved 1 & 2 */
	NULL
      };

      gmf_data_reader_type = gtk_type_unique (bonobo_object_get_type(), &gmf_data_reader_info);
      data_reader_parent_class = gtk_type_class(gtk_type_parent(gmf_data_reader_type));
    }

  return gmf_data_reader_type;
}

static void
gmf_data_reader_class_init(GMFDataReaderClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = (gpointer)&gmf_data_reader_destroy;

  	klass->servant_init_func = POA_GMF_DataReader__init;
	klass->servant_destroy_func = POA_GMF_DataReader__fini;
	klass->vepv = &impl_GMF_DataReader_vepv;


	dr_signals[READ] =
		gtk_signal_new("read", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFDataReaderClass, read),
		   gtk_marshal_NONE__INT_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_POINTER);

	dr_signals[GET_POSITION] =
    		gtk_signal_new("get_position", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFDataReaderClass, get_position),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	dr_signals[SET_POSITION] =
		gtk_signal_new("set_position", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFDataReaderClass, set_position),
		   gtk_marshal_NONE__INT_INT_POINTER,
		   GTK_TYPE_NONE, 3, GTK_TYPE_INT, GTK_TYPE_INT, GTK_TYPE_POINTER);

	dr_signals[GET_LENGTH] =
		gtk_signal_new("get_length", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFDataReaderClass, get_length),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	gtk_object_class_add_signals(object_class, dr_signals, LAST_SIGNAL);
}

static void
gmf_data_reader_init(GMFDataReader *reader)
{
	CORBA_Environment ev;
	CORBA_exception_init(&ev);

	bonobo_object_construct (BONOBO_OBJECT (reader), impl_GMF_DataReader__create (reader, &ev));

	CORBA_exception_free(&ev);
						       
}

static void
gmf_data_reader_destroy(GMFDataReader *obj)
{
	if(data_reader_parent_class->destroy) {
    		data_reader_parent_class->destroy((GtkObject *)obj);
    	}
}

GtkObject *gmf_data_reader_new (void)
{
  return gtk_type_new(gmf_data_reader_get_type());
}
