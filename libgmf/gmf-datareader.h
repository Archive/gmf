/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_DATA_READER_H
#define GMF_DATA_READER_H 1

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-object.h>
#include <libgmf/gmf-corba.h>

/* GtkObject GMFDataReader */
#define GMF_TYPE_DATA_READER	(gmf_data_reader_get_type())
#define GMF_DATA_READER(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_DATA_READER, GMFDataReader))
#define GMF_DATA_READER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_DATA_READER, GMFDataReaderClass))
#define GMF_IS_DATA_READER(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_DATA_READER))
#define GMF_IS_DATA_READER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_DATA_READER))

typedef struct {
	BonoboObject object;
} GMFDataReader;

typedef struct {
	BonoboObjectClass klass;

	void (*read)		(GMFDataReader 			 *data_reader, 
				 CORBA_long 			 nbytes, 
				 GMF_Data 			 **retval);
	void (*get_position)	(GMFDataReader 			 *data_reader, 
				 CORBA_long 			 *retval);
	void (*set_position)	(GMFDataReader 			 *data_reader, 
				 CORBA_long 			 new_pos, 
				 GMF_DataReader_PositionRelation prel, 
				 CORBA_long 			 *retval);

	void (*get_length)	(GMFDataReader 			 *data_reader, 
				CORBA_long 			 *retval);

	gpointer servant_init_func, servant_destroy_func, vepv;
} GMFDataReaderClass;

GtkType 	gmf_data_reader_get_type 	(void);
GtkObject 	*gmf_data_reader_new 		(void);

#endif
