/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_VIDEO_AREA_H
#define GMF_VIDEO_AREA_H 1

#include <gtk/gtk.h>
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-main.h>
#include <libgmf/gmf-corba.h>

/* GtkObject GMFVideoArea */
#define GMF_TYPE_VIDEO_AREA	(gmf_video_area_get_type())
#define GMF_VIDEO_AREA(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_VIDEO_AREA, GMFVideoArea))
#define GMF_VIDEO_AREA_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_VIDEO_AREA, GMFVideoAreaClass))
#define GMF_IS_VIDEO_AREA(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_VIDEO_AREA))
#define GMF_IS_VIDEO_AREA_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_VIDEO_AREA))

typedef struct {
	BonoboObject object;

	GtkWidget *socket;
} GMFVideoArea;

typedef struct {
	BonoboObjectClass klass;
	gpointer servant_init_func, servant_destroy_func, vepv;
} GMFVideoAreaClass;

GtkType 	gmf_video_area_get_type   (void);
GtkObject 	*gmf_video_area_new 	  (void);
GtkWidget *	gmf_video_area_get_socket (GMFVideoArea *obj);

#endif
