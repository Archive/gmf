/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_FILTER_GRAPH_H
#define GMF_FILTER_GRAPH_H 1

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>

/* GtkObject GMFFilterGraph */

#define GMF_TYPE_FILTER_GRAPH	(gmf_filter_graph_get_type())
#define GMF_FILTER_GRAPH(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_FILTER_GRAPH, GMFFilterGraph))
#define GMF_FILTER_GRAPH_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_FILTER_GRAPH, GMFFilterGraphClass))
#define GMF_IS_FILTER_GRAPH(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_FILTER_GRAPH))
#define GMF_IS_FILTER_GRAPH_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_FILTER_GRAPH))

struct _GMFFilterGraph {
  BonoboObject object;

  GMF_TimeVal timeBase;
  GMF_Filter_State state;
  Bonobo_Unknown app_object;
  GMFTimeReference *timeref;
};

typedef struct {
  	BonoboObjectClass klass;

  	void (*stop)		(GMFFilterGraph *filter_graph);
  	void (*pause)		(GMFFilterGraph *filter_graph);
  	void (*run)		(GMFFilterGraph *filter_graph);

  	void (*deliver_event)	(GMFFilterGraph *filter_graph, 
  				 GMF_TimeVal 	*timestamp, 
  				 const char 	*evName, 
  				 CORBA_any 	*evData);
	gpointer servant_init_func, servant_destroy_func, vepv;
} GMFFilterGraphClass;

GtkType gmf_filter_graph_get_type (void);
GtkObject *gmf_filter_graph_new (void);
void gmf_filter_graph_render_file (GMFFilterGraph *filter_graph,
				   const char *filename);

#endif /* GMF_FILTER_GRAPH_H */
