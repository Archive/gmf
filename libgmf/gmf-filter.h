/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_FILTER_H
#define GMF_FILTER_H 1

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <libgmf/gmf-corba.h>
#include "gmf-timereference.h"

/* GtkObject GMFFilter */
#define GMF_TYPE_FILTER	(gmf_filter_get_type())
#define GMF_FILTER(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_FILTER, GMFFilter))
#define GMF_FILTER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_FILTER, GMFFilterClass))
#define GMF_IS_FILTER(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_FILTER))
#define GMF_IS_FILTER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_FILTER))

struct _GMFFilter {
	BonoboObject object;

	GList *inputPipes, *outputPipes;
	GMF_TimeVal timeBase;
	GMF_Filter_State state;
	GMFTimeReference *timeref;
};

typedef struct {
  	BonoboObjectClass 		klass;

  	void (*stop)			(GMFFilter 		*filter);
  	void (*pause)			(GMFFilter 		*filter);
  	void (*run)			(GMFFilter 		*filter);
  	void (*get_pipe)		(GMFFilter 		*filter, 
  				 	 GMF_Direction 		pDirection, 
  				 	 GMFPipe 		**pipe);
  	void (*get_attributes)		(GMFFilter 		*filter, 
  				 	 GNOME_stringlist 	*retval);
  	void (*get_attribute)		(GMFFilter 		*filter, 
  				 	 const char 		*attr_name, 
  				 	 CORBA_any 		*ret_attr_val);
  	void (*set_attribute)		(GMFFilter 		*filter, 
  				 	 const char 		*attr_name, 
  				 	 CORBA_any 		*attr_val, 
  				 	 CORBA_Environment 	*ev);
  	void (*deliver_event)		(GMFFilter 		*filter, 
  				 	 GMF_TimeVal 		*timestamp, 
  				 	 const char 		*evName, 
  				 	 CORBA_any 		*evData);
	gpointer servant_init_func, servant_destroy_func, vepv;
} GMFFilterClass;

GtkType gmf_filter_get_type 	(void);
GtkObject *gmf_filter_new 	(GMF_Filter_Type	filter_type,
			   	 const char 		*oaf_id);
void gmf_filter_add_pipe	(GMFFilter 		*filter, 
				 GMFPipe 		*pipe);
void gmf_filter_remove_pipe	(GMFFilter 		*filter, 
				 GMFPipe 		*pipe);

#endif /* GMF_FILTER_H */
