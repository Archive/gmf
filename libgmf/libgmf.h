#ifndef LIBGMF_H
#define LIBGMF_H 1

#include <bonobo.h>
#include <gnome.h>
#include <gtk/gtk.h>
#include <liboaf/liboaf.h>

#include "gmf-corba.h"

typedef struct _GMFPipe GMFPipe;
typedef struct _GMFFilter GMFFilter;
typedef struct _GMFFilterGraph GMFFilterGraph;
typedef struct _GMFTimeReference GMFTimeReference;

#include <libgmf/gmf-pipe.h>
#include <libgmf/gmf-filter.h>
#include <libgmf/gmf-filtergraph.h>
#include <libgmf/gmf-timereference.h>
#include <libgmf/gmf-filterregistry.h>
#include <libgmf/gmf-datareader.h>
#include <libgmf/gmf-mediaposition.h>
#include <libgmf/gmf-videoarea.h>
#include <libgmf/gmf-mediainfo.h>

void gmf_init(CORBA_ORB orb);

#endif /* LIBGMF_H */
