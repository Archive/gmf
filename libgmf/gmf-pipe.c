/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */
/* TODO:
   Separation between input & output pipes is not very good - some leftovers from when there was just GMFPipe
 */

#ifdef DEBUG
#define d_message g_message
#else
#define dump_message(x...)
#define d_message dump_message
#endif

#include "config.h"
#include <gmf.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/uio.h>

#include "gmf-gtk-macros.h"

static void GMF_Pipe_Info__copy			(GMF_Pipe_Info 			*out,
						 GMF_Pipe_Info 			*in);
static void GMF_Pipe_ConnectionInfo__copy	(GMF_Pipe_ConnectionInfo 	*out,
					  	 GMF_Pipe_ConnectionInfo 	*in);
static void GMF_Transport_Info__copy		(GMF_Transport_Info 		*out,
				     		 GMF_Transport_Info 		*in);
static void GMF_MediaTypeInfo__copy		(GMF_MediaTypeInfo 		*out,
				    		 GMF_MediaTypeInfo 		*in);

static void gmf_pipe_initialize_class 		(GMFPipeClass 			*klass);
static void gmf_pipe_initialize 		(GMFPipe 			*obj);
static void gmf_pipe_destroy 			(GtkObject 			*obj);
static void gmf_pipe_get_arg 			(GtkObject 			*obj, 
						 GtkArg 			*arg, 
						 guint 				arg_id);
static void gmf_pipe_set_arg 			(GtkObject 			*obj, 
						 GtkArg 			*arg, 
						 guint 				arg_id);
static CORBA_char *gmf_pipe_get_address		(GMFPipe 			*apipe, 
						 GMF_Transport_Type 		ttype);
static gint gmf_pipe_connect_transport		(GMFPipe 			*apipe);
static gint gmf_pipe_setup_fd			(GMFPipe 			*apipe, 
						 gint 				fd, 
						 gboolean 			is_server);
static void gmf_pipe_setup_server		(GMFPipe 			*apipe);

typedef struct {
  POA_GMF_Pipe servant;
  GMFPipe *bonobo_object;
  
  /* These are to hold info on the active connection, for UNIX_SOCKET & TCPIP transports */
  int fd, fd_id;

  int usock_serv_fd, usock_serv_fd_id;

  int tcp_serv_fd, tcp_serv_fd_id;
  struct sockaddr_un usock_addr; /* Our local address */
  struct sockaddr_in tcpsock_addr; /* Our local address */

  long shmem_key;
} impl_POA_GMF_Pipe;

typedef struct {
 	POA_GMF_InputPipe servant;
 	GMFInputPipe *bonobo_object;
} impl_POA_GMF_InputPipe;

typedef struct {
  	POA_GMF_OutputPipe servant;
	GMFOutputPipe *bonobo_object;

  	GMF_Callback_CallbackID repeat_id;
  	gulong repeat_counter;
} impl_POA_GMF_OutputPipe;

#define GTK_TO_PIPE_SERVANT(x) ((impl_POA_GMF_Pipe *)(((BonoboObject *)(x))->servant))
#define PIPE_SERVANT_TO_GTK(x) ((impl_POA_GMF_Pipe *)(x))->bonobo_object

GMF_DEFINE_CLASS_BOILERPLATE (GMFPipe, gmf_pipe, BONOBO_OBJECT_TYPE)

/*******************************************
 * GMFPipe class                           *
 *******************************************/

static void
impl_GMF_EventReceiver_deliver(impl_POA_GMF_Pipe * servant,
			       GMF_TimeVal *timestamp,
			       CORBA_char *evName,
			       CORBA_any *evData,
			       CORBA_Environment *ev);

static GMF_Pipe_Info *
impl_GMF_Pipe__get_pipeInfo(impl_POA_GMF_Pipe * servant,
			    CORBA_Environment * ev);

static GMF_Pipe_ConnectionInfo *
impl_GMF_Pipe__get_pConnectionInfo(impl_POA_GMF_Pipe * servant,
				   CORBA_Environment * ev);

static CORBA_boolean
impl_GMF_Pipe__get_isConnected(impl_POA_GMF_Pipe * servant,
			       CORBA_Environment * ev);

static CORBA_boolean
impl_GMF_Pipe_CanProcessType(impl_POA_GMF_Pipe * servant,
			     GMF_MediaTypeInfo * typeInfo,
			     CORBA_Environment * ev);

static GMF_MediaTypeInfoList *
impl_GMF_Pipe__get_processableTypes(impl_POA_GMF_Pipe * servant,
				    CORBA_Environment * ev);

static void
impl_GMF_Pipe_ConnectTo(impl_POA_GMF_Pipe * servant,
			GMF_Pipe other_end,
			CORBA_Environment *ev);

static void
impl_GMF_Pipe_AcceptConnection(impl_POA_GMF_Pipe * servant,
			       GMF_Pipe_ConnectionInfo * cinfo,
			       CORBA_Environment * ev);

static void
impl_GMF_Pipe_CloseConnection(impl_POA_GMF_Pipe * servant,
			      CORBA_Environment * ev);

static GMF_Pipe_FormatAcceptance
impl_GMF_Pipe_FixupMediaFormat(impl_POA_GMF_Pipe * servant,
			       GMF_MediaTypeInfo * typeinfo,
			       GMF_Pipe_FormatAcceptance other_end,
			       CORBA_Environment * ev);

static CORBA_char *
impl_GMF_Pipe__get_pipeName(impl_POA_GMF_Pipe * servant,
			    CORBA_Environment * ev);

/*****************************************************
 * GMFInputPipe class                                *
 *****************************************************/
static void
impl_GMF_InputPipe_NewSegment(impl_POA_GMF_Pipe * servant,
			      GMF_TimeVal * tStart,
			      GMF_TimeVal * tStop,
			      CORBA_double playRate,
			      CORBA_Environment * ev);

static void
impl_GMF_InputPipe_BeginFlush(impl_POA_GMF_Pipe * servant,
			      CORBA_Environment * ev);

static void
impl_GMF_InputPipe_EndFlush(impl_POA_GMF_Pipe * servant,
			    CORBA_Environment * ev);

static void
impl_GMF_InputPipe_HandleSample(impl_POA_GMF_Pipe * servant,
				GMF_Sample * sdata,
				CORBA_Environment * ev);

static void
impl_GMF_InputPipe_NotifySample(impl_POA_GMF_Pipe * servant,
				CORBA_Environment * ev);

/************************************************************
 * GMFOutputPipe class                                      *
 ************************************************************/
static void
impl_GMF_OutputPipe_quality_notify(impl_POA_GMF_OutputPipe * servant,
				   GMF_QualityControl_Quality * qosinfo,
				   CORBA_Environment * ev);

static CORBA_boolean
impl_GMF_OutputPipe__get_autoRender(impl_POA_GMF_OutputPipe * servant,
				    CORBA_Environment * ev);

/* module vars */

enum {
  CONNECT,
  DISCONNECT,
  CAN_PROCESS_TYPE,
  GET_PROCESSABLE_TYPES,
  DELIVER_EVENT,
  FIXUP_MEDA_FORMAT,
  PIPE_LAST_SIGNAL
};

static guint pipe_signals[PIPE_LAST_SIGNAL] = { 0 };


/* The CORBA entry point vectors */
POA_GMF_Pipe__vepv gmf_pipe_vepv;
POA_GMF_InputPipe__vepv gmf_input_pipe_vepv;
POA_GMF_OutputPipe__vepv gmf_output_pipe_vepv;
POA_GMF_QualityControl__epv gmf_quality_control_vepv;


/**************** Utility routines ******************/

static POA_GMF_QualityControl__epv *
gmf_quality_control_get_epv (void)
{
	POA_GMF_QualityControl__epv *epv;

	epv = g_new0 (POA_GMF_QualityControl__epv, 1);

	epv->quality_notify = (gpointer) impl_GMF_OutputPipe_quality_notify;
	
	return epv;
}


static void
GMF_Pipe_Info__copy(GMF_Pipe_Info *out, GMF_Pipe_Info *in)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

  	out->pDirection = in->pDirection;
  	out->pFilter = CORBA_Object_duplicate(in->pFilter, &ev);
  	out->acceptedTransports = in->acceptedTransports;

  	CORBA_exception_free (&ev);
}

static void
GMF_Pipe_ConnectionInfo__copy(GMF_Pipe_ConnectionInfo *out,
			      GMF_Pipe_ConnectionInfo *in)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	GMF_MediaTypeInfo__copy(&out->connectionMediaInfo, &in->connectionMediaInfo);
	out->connectedTo = CORBA_Object_duplicate(in->connectedTo, &ev);
	GMF_Transport_Info__copy(&out->connectionInfo, &in->connectionInfo);

	CORBA_exception_free (&ev);
}

static void
GMF_Transport_Info__copy(GMF_Transport_Info *out, GMF_Transport_Info *in)
{
  out->_d = in->_d;
  switch(out->_d) {
  case GMF_Transport_UNIX_SOCKETS:
  case GMF_Transport_TCPIP:
    out->_u.address = CORBA_string_dup(in->_u.address);
    break;
  case GMF_Transport_CUSTOM:
    CORBA_any__copy(&out->_u.tdata, &in->_u.tdata);
    break;
  case GMF_Transport_SHARED_MEM:
    out->_u.shmem_key = in->_u.shmem_key;
    break;
  default:
    break;
  }
}

static void
GMF_MediaTypeInfo__copy(GMF_MediaTypeInfo *out, GMF_MediaTypeInfo *in)
{
  out->majorType = in->majorType;
  out->minorType = CORBA_string_dup(in->minorType);
  out->fixedSizeSamples = in->fixedSizeSamples;
  out->temporalCompression = in->temporalCompression;
  out->sampleSize = in->sampleSize;
  CORBA_any__copy(&out->typeData, &in->typeData);
  
  CORBA_any__copy(&out->formatData, &in->formatData);
}


typedef void (*GtkSignal_NONE__POINTER_POINTER) (GtkObject *object, 
						 gpointer arg1,
						 gpointer arg2,
						 gpointer user_data);
typedef void (*GtkSignal_NONE__POINTER_ENUM_POINTER)(GtkObject *object,
						       gpointer arg1,
						       guint arg2,
						       gpointer arg3,
						       gpointer user_data);

#if 0
typedef void (*GtkSignal_NONE__POINTER_BOOL) (GtkObject *object, 
					      gpointer arg1,
					      gboolean arg2,
					      gpointer user_data);

static void
gtk_marshal_NONE__POINTER_BOOL (GtkObject    *object, 
				GtkSignalFunc func, 
				gpointer      func_data, 
				GtkArg       *args)
{
  GtkSignal_NONE__POINTER_BOOL rfunc;

  rfunc = (GtkSignal_NONE__POINTER_BOOL) func;
  (* rfunc) (object,
	     GTK_VALUE_POINTER(args[0]),
	     GTK_VALUE_BOOL(args[1]),
	     func_data);
}
#endif

static void
gtk_marshal_NONE__POINTER_ENUM_POINTER (GtkObject    *object, 
					GtkSignalFunc func, 
					gpointer      func_data, 
					GtkArg       *args)
{
  GtkSignal_NONE__POINTER_ENUM_POINTER rfunc;

  rfunc = (GtkSignal_NONE__POINTER_ENUM_POINTER) func;
  (* rfunc) (object,
	     GTK_VALUE_POINTER(args[0]),
	     GTK_VALUE_ENUM(args[1]),
	     GTK_VALUE_POINTER(args[2]),
	     func_data);
}

/****************** GMFPipe routines ***************/

enum { ARG_0, ARG_DIRECTION, ARG_INUSE, ARG_TRANSPORTS_ACCEPTED, ARG_FILTER, ARG_NAME };

static void
destroy_gmf_pipe (BonoboObject *obj, impl_POA_GMF_Pipe *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	
	CORBA_exception_init(&ev);
	
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	POA_GMF_Pipe__fini ((PortableServer_Servant) servant, &ev);
	
	g_free (servant);
	
	CORBA_exception_free(&ev);
}

static POA_GMF_EventReceiver__epv *
gmf_event_receiver_epv (void)
{
	POA_GMF_EventReceiver__epv *epv;

	epv = g_new0 (POA_GMF_EventReceiver__epv, 1);

	epv->deliver = (gpointer) impl_GMF_EventReceiver_deliver;

	return epv;
}

static PortableServer_ServantBase__epv *
gmf_pipe_get_servant_base_epv (void)
{
	PortableServer_ServantBase__epv *epv;

	epv = g_new0 (PortableServer_ServantBase__epv, 1);

	epv->default_POA = PortableServer_ServantBase__default_POA;
	epv->finalize    = PortableServer_ServantBase__fini;

	return epv;
}

static POA_GMF_Pipe__epv *
gmf_pipe_get_epv (void)
{
	POA_GMF_Pipe__epv *epv;

	epv = g_new0 (POA_GMF_Pipe__epv, 1);

	epv->_get_pipeInfo		= (gpointer) impl_GMF_Pipe__get_pipeInfo;
	epv->_get_pConnectionInfo	= (gpointer) impl_GMF_Pipe__get_pConnectionInfo;
	epv->_get_isConnected		= (gpointer) impl_GMF_Pipe__get_isConnected;
	epv->CanProcessType		= (gpointer) impl_GMF_Pipe_CanProcessType;
	epv->_get_processableTypes	= (gpointer) impl_GMF_Pipe__get_processableTypes;
	epv->ConnectTo			= (gpointer) impl_GMF_Pipe_ConnectTo;
	epv->AcceptConnection		= (gpointer) impl_GMF_Pipe_AcceptConnection;
	epv->CloseConnection		= (gpointer) impl_GMF_Pipe_CloseConnection;
	epv->FixupMediaFormat		= (gpointer) impl_GMF_Pipe_FixupMediaFormat;
	epv->_get_pipeName		= (gpointer) impl_GMF_Pipe__get_pipeName;
	
	return epv;
}

static void
init_pipe_corba_class (void)
{
	gmf_pipe_vepv._base_epv 	 	= gmf_pipe_get_servant_base_epv ();
	gmf_pipe_vepv.Bonobo_Unknown_epv 	= bonobo_object_get_epv ();
	gmf_pipe_vepv.GMF_EventReceiver_epv 	= gmf_event_receiver_epv ();
	gmf_pipe_vepv.GMF_Pipe_epv 		= gmf_pipe_get_epv ();
}

/* create_gmf_pipe
 *
 * Create and setup bonobo object 	
 */
static GMF_Pipe
create_gmf_pipe (GMFPipe *pipe)
{
	impl_POA_GMF_Pipe *servant;
	CORBA_Environment ev;

	servant = g_new0 (impl_POA_GMF_Pipe, 1);
	servant->servant.vepv = &gmf_pipe_vepv;

	CORBA_exception_init (&ev);
	
	POA_GMF_Pipe__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);	
			
	gtk_signal_connect (GTK_OBJECT (pipe), "destroy", GTK_SIGNAL_FUNC (destroy_gmf_pipe), servant);

	servant->bonobo_object = pipe;

	servant->usock_serv_fd =
	servant->usock_serv_fd_id =
	servant->tcp_serv_fd =
	servant->tcp_serv_fd_id =
	servant->fd =
	servant->fd_id = -1;

	return bonobo_object_activate_servant (BONOBO_OBJECT (pipe), servant);
}


static void 
gmf_pipe_initialize_class (GMFPipeClass * klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = &gmf_pipe_destroy;
	object_class->get_arg = &gmf_pipe_get_arg;
	object_class->set_arg = &gmf_pipe_set_arg;
	
	init_pipe_corba_class ();

	pipe_signals[CAN_PROCESS_TYPE] =
		gtk_signal_new("can_process_type", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFPipeClass, can_process_type),
		   gtk_marshal_NONE__POINTER_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
		   GTK_TYPE_POINTER);

	pipe_signals[GET_PROCESSABLE_TYPES] =
		gtk_signal_new("get_processable_types", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFPipeClass, get_processable_types),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	pipe_signals[DELIVER_EVENT] =
		gtk_signal_new("deliver_event", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFPipeClass, deliver_event),
		   gtk_marshal_NONE__POINTER_POINTER_POINTER,
		   GTK_TYPE_NONE, 3, GTK_TYPE_POINTER, GTK_TYPE_POINTER,
		   GTK_TYPE_POINTER);

	pipe_signals[CONNECT] =
		gtk_signal_new("connect", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFPipeClass, connect),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	pipe_signals[DISCONNECT] =
		gtk_signal_new("disconnect", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFPipeClass, disconnect),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0);

	pipe_signals[FIXUP_MEDA_FORMAT] =
		gtk_signal_new("fixup_media_format", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFPipeClass, fixup_media_format),
		   gtk_marshal_NONE__POINTER_ENUM_POINTER,
		   GTK_TYPE_NONE, 3, GTK_TYPE_POINTER, GTK_TYPE_ENUM, GTK_TYPE_POINTER);

	gtk_object_class_add_signals(object_class, pipe_signals, PIPE_LAST_SIGNAL);

  	gtk_object_add_arg_type("GMFPipe::acceptable_transports", GTK_TYPE_ENUM,
                          GTK_ARG_READWRITE|GTK_ARG_CONSTRUCT_ONLY, ARG_TRANSPORTS_ACCEPTED);
  	gtk_object_add_arg_type("GMFPipe::inuse", GTK_TYPE_BOOL,
                          GTK_ARG_READABLE, ARG_INUSE);
  	gtk_object_add_arg_type("GMFPipe::filter", GTK_TYPE_POINTER,
                          GTK_ARG_READWRITE|GTK_ARG_CONSTRUCT_ONLY, ARG_FILTER);
  	gtk_object_add_arg_type("GMFPipe::name", GTK_TYPE_STRING,
                          GTK_ARG_READWRITE|GTK_ARG_CONSTRUCT_ONLY, ARG_NAME);
  	gtk_object_add_arg_type("GMFPipe::direction", GTK_TYPE_ENUM,
                          GTK_ARG_READABLE, ARG_DIRECTION);
}

static void
gmf_pipe_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
	GMFPipe *apipe;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	apipe = ((GMFPipe *)(obj));

  switch(arg_id) {
	case ARG_TRANSPORTS_ACCEPTED:
		apipe->acceptable_transports = GTK_VALUE_ENUM(*arg);
		apipe->acceptable_transports &= (GMF_Transport_CORBA|GMF_Transport_UNIX_SOCKETS);
		if(apipe->acceptable_transports != GTK_VALUE_ENUM(*arg))
			g_warning("Trimming your choice of acceptable transports down to CORBA & UNIX sockets - the rest NYI.");
    		gmf_pipe_setup_server(apipe);
		apipe->attr_pipeInfo.acceptedTransports = apipe->acceptable_transports;
		break;
		
	case ARG_FILTER:
		apipe->filter_object = GTK_VALUE_POINTER(*arg);
		apipe->attr_pipeInfo.pFilter = CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(apipe->filter_object)), &ev);
		break;
		
	case ARG_NAME:
		if(apipe->name)
			g_free(apipe->name);
		apipe->name = g_strdup(GTK_VALUE_STRING(*arg));
		break;

	default:
		GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, set_arg, (obj, arg, arg_id));
		break;
	}

	CORBA_exception_free (&ev);
}

static void
gmf_pipe_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
  GMFPipe *apipe;

  apipe = ((GMFPipe *)(obj));

  switch(arg_id) {
  case ARG_INUSE:
    GTK_VALUE_BOOL(*arg) = apipe->attr_pConnectionInfo?TRUE:FALSE;
    break;
  case ARG_TRANSPORTS_ACCEPTED:
    GTK_VALUE_ENUM(*arg) = apipe->acceptable_transports;
    break;
  case ARG_FILTER:
    GTK_VALUE_POINTER(*arg) = apipe->filter_object;
    break;
  case ARG_NAME:
    GTK_VALUE_STRING(*arg) = apipe->name;
    break;
  case ARG_DIRECTION:
    GTK_VALUE_ENUM(*arg) = apipe->attr_pipeInfo.pDirection;
    break;
  default:
	GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, get_arg, (obj, arg, arg_id));
    break;
  }
}

static void 
gmf_pipe_initialize (GMFPipe *pipe)
{
	bonobo_object_construct (BONOBO_OBJECT (pipe), create_gmf_pipe (pipe));
}

static void
gmf_pipe_destroy (GtkObject * obj)
{
	GMFPipe *apipe;
	impl_POA_GMF_Pipe *servant;
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);

	apipe = ((GMFPipe *)(obj));
	servant = BONOBO_OBJECT (obj)->servant;

	if(servant->fd >= 0) {
		close(servant->fd);
		gdk_input_remove(servant->fd_id);
	}

	if(servant->usock_serv_fd) {
		close(servant->usock_serv_fd);
		unlink(servant->usock_addr.sun_path);
		gdk_input_remove(servant->usock_serv_fd_id);
	}

	if(servant->tcp_serv_fd) {
		close(servant->tcp_serv_fd);
		gdk_input_remove(servant->tcp_serv_fd_id);
	}

	CORBA_Object_release(apipe->attr_pipeInfo.pFilter, &ev);
	g_free(apipe->name);


	CORBA_exception_free (&ev);

	GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, destroy, (obj));
}

static void
impl_GMF_EventReceiver_deliver(impl_POA_GMF_Pipe * servant, GMF_TimeVal *timestamp, CORBA_char *evName,
			       CORBA_any *evData, CORBA_Environment *ev)
{
	gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), pipe_signals[DELIVER_EVENT], timestamp, evName, evData);  
}

static GMF_Pipe_Info *
impl_GMF_Pipe__get_pipeInfo(impl_POA_GMF_Pipe * servant,
			    CORBA_Environment * ev)
{
	GMF_Pipe_Info *retval;

	retval = GMF_Pipe_Info__alloc();
	GMF_Pipe_Info__copy(retval, &servant->bonobo_object->attr_pipeInfo);

	return retval;
}

static GMF_Pipe_ConnectionInfo *
impl_GMF_Pipe__get_pConnectionInfo(impl_POA_GMF_Pipe * servant,
				   CORBA_Environment * ev)
{
  GMF_Pipe_ConnectionInfo *retval;

  retval = GMF_Pipe_ConnectionInfo__alloc();

  if(servant->bonobo_object->attr_pConnectionInfo) {
    GMF_Pipe_ConnectionInfo__copy(retval, servant->bonobo_object->attr_pConnectionInfo);
  } else {
    d_message("Sending back an empty pConnectionInfo");
    retval->connectedTo = CORBA_Object_duplicate(CORBA_OBJECT_NIL, ev);
    retval->connectionInfo._d = GMF_Transport_NONE; 
    retval->connectionMediaInfo.majorType = GMF_MEDIA_NONE;
    retval->connectionMediaInfo.minorType = CORBA_string_dup("");
    retval->connectionMediaInfo.typeData._type = (CORBA_TypeCode)TC_void;
    retval->connectionMediaInfo.typeData._value = NULL;
    CORBA_any_set_release(&retval->connectionMediaInfo.typeData, CORBA_FALSE);
    retval->connectionMediaInfo.formatData._type = (CORBA_TypeCode)TC_void;
    retval->connectionMediaInfo.formatData._value = NULL;
    CORBA_any_set_release(&retval->connectionMediaInfo.formatData, CORBA_FALSE);
  }

  return retval;
}

static CORBA_boolean
impl_GMF_Pipe__get_isConnected(impl_POA_GMF_Pipe * servant,
			       CORBA_Environment * ev)
{
  return servant->bonobo_object->attr_pConnectionInfo?CORBA_TRUE:CORBA_FALSE;
}


static CORBA_boolean
impl_GMF_Pipe_CanProcessType(impl_POA_GMF_Pipe * servant,
			     GMF_MediaTypeInfo * typeInfo,
			     CORBA_Environment * ev)
{
  gboolean retval = FALSE;

  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), pipe_signals[CAN_PROCESS_TYPE],
		  typeInfo, &retval);

  return (CORBA_boolean)retval;
}

static GMF_MediaTypeInfoList *
impl_GMF_Pipe__get_processableTypes(impl_POA_GMF_Pipe * servant,
				    CORBA_Environment * ev)
{
  GMF_MediaTypeInfoList *retval = NULL;

  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object),
		  pipe_signals[GET_PROCESSABLE_TYPES],
		  &retval);

  if(!retval) {
    retval = GMF_MediaTypeInfoList__alloc();
    retval->_length = 0;
    retval->_buffer = NULL;
    CORBA_sequence_set_release(retval, CORBA_FALSE);
  }

  return retval;
}


static gint
gmf_pipe_connect_transport(GMFPipe *apipe)
{
  GMF_Pipe_ConnectionInfo *pcnx;
  struct sockaddr_un remote;
  impl_POA_GMF_Pipe *servant;

  servant = GTK_TO_PIPE_SERVANT(apipe);
  pcnx = apipe->attr_pConnectionInfo;
  
  switch(pcnx->connectionInfo._d) {
  case GMF_Transport_UNIX_SOCKETS:
    servant->fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if(servant->fd < 0) return -1;
    remote.sun_family = AF_UNIX;
    strcpy(remote.sun_path, pcnx->connectionInfo._u.address);
    if(connect(servant->fd, &remote, SUN_LEN(&remote)) < 0) {
      close(servant->fd); servant->fd = -1;
      return -1;
    }
    if(apipe->attr_pipeInfo.pDirection == GMF_IN)
      servant->fd_id = gmf_pipe_setup_fd(apipe, servant->fd, FALSE);
    break;

  case GMF_Transport_CORBA:
    break;

  default:
    g_error("Transports beside UNIX sockets & CORBA NYI");
    break;
  }

  return 0;
}

static void
impl_GMF_Pipe_ConnectTo(impl_POA_GMF_Pipe * servant,
			GMF_Pipe other_end,
			CORBA_Environment *ev)
{
	/* Wherein the magic occurs */

	GMF_MediaTypeInfoList *their_types = NULL;
	GMF_Pipe_Info *remote_pinfo = NULL;
	GMF_Pipe_ConnectionInfo *pcnx = NULL;
	GMF_Transport_Type negotiated_transport;
	int i, n;

	if(CORBA_Object_is_nil(other_end, ev))
		goto out;

	/* steps to negotiating a connection:
	 *  1. (a) Get pipe info from the remote end.
	 *     (b) Figure out a transport to use.
   	 *  2. Figure out a media type.
   	 *  3. Set up our pConnectionInfo structure.
   	 *  3.5. Format negotiation.
   	 *  4. Tell the remote pipe to connect.
  	 */

  	/* 1. */
  	remote_pinfo = GMF_Pipe__get_pipeInfo(other_end, ev);
  	if(ev->_major != CORBA_NO_EXCEPTION) {
    		remote_pinfo = NULL;
    		goto out;
  	}

  	/* 1. (a) */
  	/* For now, just check for UNIX sockets, and if we don't have that use CORBA */
  	/* XXX fixme - we need to allow fallbacks if the other end isn't running on
     	   the same system 
     	*/
  	if(other_end->servant /* cheat to find out if it is local */)
    		negotiated_transport = GMF_Transport_CORBA;
  	else if(remote_pinfo->acceptedTransports & GMF_Transport_UNIX_SOCKETS)
    		negotiated_transport = GMF_Transport_UNIX_SOCKETS;
  	else
    		negotiated_transport = GMF_Transport_CORBA;

  	/* 2. */
  	their_types = GMF_Pipe__get_processableTypes(other_end, ev);
  	if(!their_types || ev->_major != CORBA_NO_EXCEPTION) {
    		their_types = NULL;
    		goto out;
  	}

  	for(i = 0, n = their_types->_length; i < n; i++) {
    		CORBA_boolean rv;

    		rv = impl_GMF_Pipe_CanProcessType(servant, &their_types->_buffer[i], ev);

    		if(ev->_major != CORBA_NO_EXCEPTION) 
    			goto out;

    		if(rv) 
    			break; /* Go with this type */
  	}

  	if(i == n) /* nothing found */
    		goto out;

  	/* 3. */
  	pcnx = GMF_Pipe_ConnectionInfo__alloc();
  	pcnx->connectionInfo._d = negotiated_transport;
  	
  	switch(negotiated_transport) {
  		case GMF_Transport_UNIX_SOCKETS:
  		case GMF_Transport_TCPIP:
    			pcnx->connectionInfo._u.address = gmf_pipe_get_address(servant->bonobo_object,
							   negotiated_transport);
   			 break;
   			 
  		case GMF_Transport_SHARED_MEM:
    			pcnx->connectionInfo._u.shmem_key = servant->shmem_key;
    			break;
    			
  		case GMF_Transport_CUSTOM:
    			g_error("Custom transport NYI");
    			break;

  		default:
    			break;
  	}

  	GMF_MediaTypeInfo__copy(&pcnx->connectionMediaInfo, &their_types->_buffer[i]);
  	CORBA_free(their_types); their_types = NULL;

  	pcnx->connectedTo = CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(servant->bonobo_object)), ev);
  	d_message("our connectedTo is %p", pcnx->connectedTo);

  	/* 3.5 */ /* Have the app fix up the format on the media type as needed */
  	{
    		GMF_Pipe_FormatAcceptance resp;

    		resp = impl_GMF_Pipe_FixupMediaFormat(servant, &pcnx->connectionMediaInfo, GMF_Pipe_UNKNOWN, ev);
    		if(resp != GMF_Pipe_ACCEPTED)
      			goto out;

    		resp = GMF_Pipe_FixupMediaFormat(other_end, &pcnx->connectionMediaInfo, GMF_Pipe_ACCEPTED, ev);
    		
    		switch(resp) {
    			case GMF_Pipe_TRY_MINE:
      				resp = impl_GMF_Pipe_FixupMediaFormat(servant, &pcnx->connectionMediaInfo, resp, ev);
      				if(resp != GMF_Pipe_ACCEPTED)
				goto out;
      				break;
      				
    			case GMF_Pipe_CANNOT_HANDLE:
      				resp = impl_GMF_Pipe_FixupMediaFormat(servant, &pcnx->connectionMediaInfo, resp, ev);
      				switch(resp) {
      					case GMF_Pipe_TRY_MINE:
						if(GMF_Pipe_FixupMediaFormat(other_end, &pcnx->connectionMediaInfo, resp, ev) != GMF_Pipe_ACCEPTED)
	  						goto out;
							break;

      					default:
						goto out;
						break;
      				}
      				break;
      				
    		case GMF_Pipe_ACCEPTED:
      			/* All is well! */
      			break;
      			
    		default:
      			g_assert_not_reached();
      			break;
    		}
  	}

  	/* 4. */
  	d_message("accept a connection connectedTo %p", pcnx->connectedTo);
  	GMF_Pipe_AcceptConnection(other_end, pcnx, ev);
  	if(ev->_major != CORBA_NO_EXCEPTION) 
  		goto out;

  	/* 5. */ /* We are now connected - do misc maintainance & clean up */

  	/* this has been modified by the remote end to something we can use */
  	servant->bonobo_object->attr_pConnectionInfo = pcnx;

  	if(gmf_pipe_connect_transport(servant->bonobo_object)) {
    		GMF_Pipe_CloseConnection(other_end, ev);

    		CORBA_free(servant->bonobo_object->attr_pConnectionInfo);
    		servant->bonobo_object->attr_pConnectionInfo = NULL;

    		goto out;
  	}

  	d_message("connection OK connectedTo %p", pcnx->connectedTo);
  	gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), pipe_signals[CONNECT], pcnx, TRUE);

  	return;

	out:
  		if(their_types)
    			CORBA_free(their_types);

  		if(remote_pinfo)
    			CORBA_free(remote_pinfo);

  		if(pcnx)
    			CORBA_free(pcnx);

  		CORBA_exception_set(ev, CORBA_USER_EXCEPTION, ex_GMF_Pipe_ConnectionRefused, NULL);
}

static void
impl_GMF_Pipe_AcceptConnection(impl_POA_GMF_Pipe * servant,
			       GMF_Pipe_ConnectionInfo * cinfo,
			       CORBA_Environment * ev)
{
  GMF_Pipe_ConnectionInfo *my_cinfo;
  GMFPipe *apipe;

  apipe = servant->bonobo_object;

  if(apipe->attr_pConnectionInfo) {
    CORBA_exception_set(ev, CORBA_USER_EXCEPTION,
			ex_GMF_Pipe_ConnectionRefused, NULL);
    return;
  }

  my_cinfo = GMF_Pipe_ConnectionInfo__alloc();
  GMF_Pipe_ConnectionInfo__copy(my_cinfo, cinfo);

  CORBA_Object_release(cinfo->connectedTo, ev);
  cinfo->connectedTo = CORBA_Object_duplicate(bonobo_object_corba_objref(BONOBO_OBJECT(servant->bonobo_object)), ev);
  switch(cinfo->connectionInfo._d) {
  case GMF_Transport_CUSTOM:
    if(CORBA_any_get_release(&cinfo->connectionInfo._u.tdata))
      CORBA_free(cinfo->connectionInfo._u.tdata._value);
    CORBA_Object_release((CORBA_Object)cinfo->connectionInfo._u.tdata._type, ev);
    g_error("CUSTOM transport not yet fully implemented");
    break;
  case GMF_Transport_UNIX_SOCKETS:
  case GMF_Transport_TCPIP:
    CORBA_free(cinfo->connectionInfo._u.address);
    cinfo->connectionInfo._u.address =
      gmf_pipe_get_address(servant->bonobo_object, cinfo->connectionInfo._d);
    break;
  }

  apipe->attr_pConnectionInfo = my_cinfo;

  /* Connection accepted... */
  gtk_signal_emit(GTK_OBJECT(apipe), pipe_signals[CONNECT], apipe->attr_pConnectionInfo);
}

static void
impl_GMF_Pipe_CloseConnection(impl_POA_GMF_Pipe * servant,
			      CORBA_Environment * ev)
{
  if(servant->bonobo_object->attr_pConnectionInfo) {
    CORBA_free(servant->bonobo_object->attr_pConnectionInfo);
    servant->bonobo_object->attr_pConnectionInfo = NULL;
    gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), pipe_signals[DISCONNECT]);
  } else
    g_warning("How can they close the connection when we're not connected? Explain this to me!");
}

static GMF_Pipe_FormatAcceptance
impl_GMF_Pipe_FixupMediaFormat(impl_POA_GMF_Pipe * servant,
			       GMF_MediaTypeInfo * typeinfo,
			       GMF_Pipe_FormatAcceptance other_end,
			       CORBA_Environment * ev)
{
  GMF_Pipe_FormatAcceptance retval = GMF_Pipe_ACCEPTED;

  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), pipe_signals[FIXUP_MEDA_FORMAT], typeinfo, other_end, &retval);

  return retval;
}

static CORBA_char *
impl_GMF_Pipe__get_pipeName(impl_POA_GMF_Pipe * servant,
			    CORBA_Environment * ev)
{
  if(!servant->bonobo_object->name) {
    /* Just give it some sort of unique-for-this-filter value */
    servant->bonobo_object->name = g_strdup_printf("%p", servant);
  }
  
  return CORBA_string_dup(servant->bonobo_object->name);
}

static gboolean
gmf_pipe_handle_fd(GMFPipe *apipe, gint fd, GdkInputCondition condition)
{
	GMF_Sample samp;
	int nread, ntoread, n;

	impl_POA_GMF_Pipe *servant;
	CORBA_Environment ev;
	
	servant = GTK_TO_PIPE_SERVANT(apipe);

	if(fd != servant->fd) {
		g_warning("fd is %d, servant->fd is %d", fd, servant->fd);
		return FALSE;
  	}

  	CORBA_exception_init (&ev);

  	if(condition & GDK_INPUT_EXCEPTION) 
  		goto errout;

	ntoread = sizeof(GMF_SampleInfo);
	for(nread = 0, n = 1; (n > 0) && (nread < ntoread); nread += n)
	n = read(fd, &samp.sInfo, ntoread - nread);
    
	if(n <= 0) 
		goto errout;

	CORBA_sequence_set_release(&samp.sData, CORBA_FALSE);
	samp.sData._length = samp.sInfo.actualDataLength;
	ntoread = samp.sData._length;
	samp.sData._buffer = alloca(ntoread);
	for(nread = 0, n = 1; (n > 0) && (nread < ntoread); nread += n)
		n = read(fd, samp.sData._buffer, ntoread - nread);

	if(n <= 0) 
		goto errout;

	g_return_val_if_fail(samp.sData._length && samp.sData._buffer && samp.sInfo.actualDataLength, TRUE);

	  impl_GMF_InputPipe_HandleSample(servant, &samp, &ev);

	CORBA_exception_free (&ev);

	return TRUE;

	errout:
		close(fd);
		servant->fd = servant->fd_id = -1;
		CORBA_exception_free (&ev);
		return FALSE;
}

static gboolean
gmf_pipe_handle_server_fd(GMFPipe *apipe, gint fd, GdkInputCondition condition)
{
  impl_POA_GMF_Pipe *servant;
  struct sockaddr_un usock_addr;
  struct sockaddr_in tcp_addr;
  int n;
  int newfd;
  gpointer addr_ptr;

  g_return_val_if_fail(condition & GDK_INPUT_READ, FALSE);

  servant = GTK_TO_PIPE_SERVANT(apipe);

  if(fd == servant->usock_serv_fd) { /* server thing */
    usock_addr.sun_family = AF_UNIX;
    n = sizeof(usock_addr);
    addr_ptr = &usock_addr;

  } else if(fd == servant->tcp_serv_fd) {

    tcp_addr.sin_family = AF_INET;
    n = sizeof(tcp_addr);
    addr_ptr = &tcp_addr;
  } else
    addr_ptr = NULL;

  newfd = accept(fd, addr_ptr, &n);

  if(servant->fd >= 0) {
    /* Instead of doing this, learn how to reject a connection */
    close(newfd);
    g_warning("Double connection");
  } else {
    servant->fd = newfd;
    servant->fd_id = gmf_pipe_setup_fd(apipe, newfd, FALSE);
  }

  return TRUE;
}

static gint
gmf_pipe_setup_fd(GMFPipe *apipe, gint fd, gboolean is_server)
{
  impl_POA_GMF_Pipe *servant;
  gpointer routine = NULL;

  servant = GTK_TO_PIPE_SERVANT(apipe);

  if(is_server)
    routine = gmf_pipe_handle_server_fd;
  else if(servant->bonobo_object->attr_pipeInfo.pDirection == GMF_IN)
    routine = gmf_pipe_handle_fd;

  if(routine)
    return gdk_input_add(fd, GDK_INPUT_READ|GDK_INPUT_EXCEPTION, routine, apipe);
  else
    return -1;
}

static void
gmf_pipe_setup_usock_server(GMFPipe *apipe)
{
  impl_POA_GMF_Pipe *servant;
  int n;
  static int didrand = 0;

  servant = GTK_TO_PIPE_SERVANT(apipe);
  servant->usock_serv_fd = socket(AF_UNIX, SOCK_STREAM, 0);

  g_return_if_fail(servant->usock_serv_fd > 0);

  if(!didrand) {
    srand(time(NULL) % getpid());
    didrand = 1;
  }

  snprintf(servant->usock_addr.sun_path,
	   sizeof(servant->usock_addr.sun_path),
	   "/tmp/orbit-%s/gmf-%d%d", g_get_user_name(), rand(), rand());

  servant->usock_addr.sun_family = AF_UNIX;

  n = bind(servant->usock_serv_fd, &servant->usock_addr,
	   SUN_LEN(&servant->usock_addr));

  if(n != 0) {
    g_warning("bind failed: ");
  }
  g_return_if_fail(n == 0);

  n = listen(servant->usock_serv_fd, 5);

  g_return_if_fail(n == 0);

  servant->usock_serv_fd_id = gmf_pipe_setup_fd(apipe,
						servant->usock_serv_fd, TRUE);
}

static void
gmf_pipe_setup_tcpip_server(GMFPipe *apipe)
{
  g_error("TCP/IP transport NYI");
}

static void
gmf_pipe_setup_shmem_server(GMFPipe *apipe)
{
  g_error("Shmem transport NYI");
}

static void
gmf_pipe_setup_custom_server(GMFPipe *apipe)
{
  g_error("Custom transport NYI");
}

static void
gmf_pipe_setup_server (GMFPipe *apipe)
{
	impl_POA_GMF_Pipe *servant;

	servant = GTK_TO_PIPE_SERVANT(apipe);

	if(apipe->acceptable_transports & GMF_Transport_UNIX_SOCKETS)
		gmf_pipe_setup_usock_server (apipe);

	if(apipe->acceptable_transports & GMF_Transport_TCPIP)
		gmf_pipe_setup_tcpip_server (apipe);

	if(apipe->acceptable_transports & GMF_Transport_SHARED_MEM)
		gmf_pipe_setup_shmem_server (apipe);

	if(apipe->acceptable_transports & GMF_Transport_CUSTOM) 
		gmf_pipe_setup_custom_server (apipe);

  /* CORBA requires no extra work */
}

static CORBA_char *
gmf_pipe_get_address(GMFPipe *apipe, GMF_Transport_Type ttype)
{
  CORBA_char *retval = NULL;
  char *ctmp;

  switch(ttype) {
  case GMF_Transport_UNIX_SOCKETS:
    retval = CORBA_string_dup(GTK_TO_PIPE_SERVANT(apipe)->usock_addr.sun_path);
    break;
  case GMF_Transport_TCPIP:
    ctmp = inet_ntoa(GTK_TO_PIPE_SERVANT(apipe)->tcpsock_addr.sin_addr);
    retval = CORBA_string_alloc(strlen(ctmp)
				+ 1 /* ':' */ +  5 /* port */ + 1 /* '\0' */);
    sprintf(retval, "%s:%d", ctmp,
	    ntohs(GTK_TO_PIPE_SERVANT(apipe)->tcpsock_addr.sin_port));
    break;
  default:
    g_error("gmf_pipe_get_address(): This should not happen.");
    break;
  }

  return retval;
}

/*********************************************** end GMFPipe */

/********************************************* GMFInputPipe class */

static void gmf_input_pipe_initialize_class (GMFInputPipeClass * klass);
static void gmf_input_pipe_initialize (GMFInputPipe * obj);
static void gmf_input_pipe_destroy (GtkObject * obj);
static void gmf_input_pipe_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void gmf_input_pipe_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void gmf_input_pipe_deliver_event (GMFPipe *apipe, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData);

enum {
  RECEIVE_SAMPLE,
  NEW_SEGMENT,
  BEGIN_FLUSH,
  END_FLUSH,
  INPIPE_LAST_SIGNAL
};

static guint inpipe_signals[INPIPE_LAST_SIGNAL] = { 0 };

GMF_DEFINE_CLASS_BOILERPLATE (GMFInputPipe, gmf_input_pipe, GMF_TYPE_PIPE)

/* enum { ARG_0, ARG_DIRECTION, ARG_INUSE, ARG_TRANSPORTS_ACCEPTED, ARG_FILTER, ARG_NAME }; */

/* gmf_input_pipe_get_epv
 *
 * Allocate and return GMF_InputPipe entry point vector
 */
static POA_GMF_InputPipe__epv *
gmf_input_pipe_get_epv (void)
{
	POA_GMF_InputPipe__epv *epv;

	epv = g_new0 (POA_GMF_InputPipe__epv, 1);

	epv->HandleSample	= (gpointer) impl_GMF_InputPipe_HandleSample;
	epv->NotifySample	= (gpointer) impl_GMF_InputPipe_NotifySample;
	epv->NewSegment		= (gpointer) impl_GMF_InputPipe_NewSegment;
	epv->BeginFlush		= (gpointer) impl_GMF_InputPipe_BeginFlush;
	epv->EndFlush		= (gpointer) impl_GMF_InputPipe_EndFlush;
	
	return epv;
}

static void
init_input_pipe_corba_class (void)
{
	gmf_pipe_vepv._base_epv 		  = gmf_pipe_get_servant_base_epv ();
	gmf_input_pipe_vepv.Bonobo_Unknown_epv    = bonobo_object_get_epv ();
	gmf_input_pipe_vepv.GMF_EventReceiver_epv = gmf_event_receiver_epv ();
	gmf_input_pipe_vepv.GMF_Pipe_epv 	  = gmf_pipe_get_epv ();
	gmf_input_pipe_vepv.GMF_InputPipe_epv  	  = gmf_input_pipe_get_epv ();
}

static void 
gmf_input_pipe_initialize_class (GMFInputPipeClass * klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = &gmf_input_pipe_destroy;
	object_class->get_arg = &gmf_input_pipe_get_arg;
	object_class->set_arg = &gmf_input_pipe_set_arg;

	init_input_pipe_corba_class ();

	GMF_PIPE_CLASS(klass)->deliver_event = &gmf_input_pipe_deliver_event;

	inpipe_signals[RECEIVE_SAMPLE] =
		gtk_signal_new("receive_sample", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFInputPipeClass, receive_sample),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	inpipe_signals[BEGIN_FLUSH] =
		gtk_signal_new("begin_flush", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFInputPipeClass, begin_flush),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0, GTK_TYPE_NONE);

	inpipe_signals[END_FLUSH] =
		gtk_signal_new("end_flush", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFInputPipeClass, end_flush),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 0, GTK_TYPE_NONE);

	inpipe_signals[NEW_SEGMENT] =
		gtk_signal_new("new_segment", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFInputPipeClass, new_segment),
		   gtk_marshal_NONE__NONE,
		   GTK_TYPE_NONE, 3, GTK_TYPE_POINTER, GTK_TYPE_POINTER, GTK_TYPE_DOUBLE);

	gtk_object_class_add_signals(object_class, inpipe_signals, INPIPE_LAST_SIGNAL);
}

static void
gmf_input_pipe_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
	switch(arg_id) {
		default:
			GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, set_arg, (obj, arg, arg_id));
			break;
  	}
}

static void
gmf_input_pipe_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
  	switch(arg_id) {
  		default:
			GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, get_arg, (obj, arg, arg_id));
 		   	break;
  }
}


/* destroy_gmf_input_pipe
 *
 * Destroy bonobo object 	
 */
static void
destroy_gmf_input_pipe (BonoboObject *obj, impl_POA_GMF_InputPipe *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	
	CORBA_exception_init(&ev);
	
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	POA_GMF_InputPipe__fini ((PortableServer_Servant) servant, &ev);
	
	g_free (servant);
	
	CORBA_exception_free(&ev);
}

/* create_gmf_input_pipe
 *
 * Create and setup bonobo object 	
 */
static GMF_InputPipe
create_gmf_input_pipe (GMFInputPipe *pipe)
{
	impl_POA_GMF_InputPipe *servant;
	CORBA_Environment ev;

	servant = g_new0 (impl_POA_GMF_InputPipe, 1);
	servant->servant.vepv = &gmf_input_pipe_vepv;

	CORBA_exception_init (&ev);
	
	POA_GMF_InputPipe__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);	
		
	gtk_signal_connect (GTK_OBJECT (pipe), "destroy", GTK_SIGNAL_FUNC (destroy_gmf_input_pipe), servant);

	servant->bonobo_object = pipe;
	
	return bonobo_object_activate_servant (BONOBO_OBJECT (pipe), servant);
}

static void 
gmf_input_pipe_initialize (GMFInputPipe * pipe)
{
	((GMFPipe *)(pipe))->attr_pipeInfo.pDirection = GMF_IN;

	bonobo_object_construct (BONOBO_OBJECT (pipe), create_gmf_input_pipe (pipe));
}

static void
gmf_input_pipe_destroy (GtkObject * obj)
{
	GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, destroy, (obj));
}

/* XXX maybe we should emit these on the filter instead of the pipe...? */
static void
impl_GMF_InputPipe_NewSegment(impl_POA_GMF_Pipe * servant,
			      GMF_TimeVal * tStart,
			      GMF_TimeVal * tStop,
			      CORBA_double playRate,
			      CORBA_Environment * ev)
{
  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object),
		  inpipe_signals[NEW_SEGMENT], tStart, tStop,
		  playRate);
}

static void
impl_GMF_InputPipe_BeginFlush(impl_POA_GMF_Pipe * servant,
			      CORBA_Environment * ev)
{
  g_return_if_fail(!GMF_INPUT_PIPE(servant->bonobo_object)->in_flush);

  GMF_INPUT_PIPE(servant->bonobo_object)->in_flush = TRUE;
  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), inpipe_signals[BEGIN_FLUSH]);
}

static void
impl_GMF_InputPipe_EndFlush(impl_POA_GMF_Pipe * servant,
			    CORBA_Environment * ev)
{
  g_return_if_fail(GMF_INPUT_PIPE(servant->bonobo_object)->in_flush);

  GMF_INPUT_PIPE(servant->bonobo_object)->in_flush = FALSE;

  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), inpipe_signals[END_FLUSH]);
}

/* CORBA data transport only. */
static void
impl_GMF_InputPipe_HandleSample(impl_POA_GMF_Pipe * servant,
				GMF_Sample * sdata,
				CORBA_Environment * ev)
{
  gtk_signal_emit(GTK_OBJECT(servant->bonobo_object), inpipe_signals[RECEIVE_SAMPLE], sdata);
}

static void
impl_GMF_InputPipe_NotifySample(impl_POA_GMF_Pipe * servant,
				CORBA_Environment * ev)
{
  GMFPipe *apipe;

  apipe = servant->bonobo_object;

  g_return_if_fail(apipe->attr_pConnectionInfo);

  switch(apipe->attr_pConnectionInfo->connectionInfo._d)
    {
    case GMF_Transport_SHARED_MEM:
    case GMF_Transport_CUSTOM:
      gtk_signal_emit(GTK_OBJECT(servant->bonobo_object),
		      inpipe_signals[RECEIVE_SAMPLE], NULL);
      break;
    default:
      g_error("Transport %d does not support NotifySample",
	      apipe->attr_pConnectionInfo->connectionInfo._d);
      break;
    }
}

static void
gmf_input_pipe_deliver_event (GMFPipe *apipe, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData)
{
	GMF_CALL_PARENT_CLASS (GMF_PIPE_CLASS, deliver_event, (apipe, timestamp, evName, evData));
  	gtk_signal_emit_by_name(GTK_OBJECT(apipe->filter_object), "deliver_event", timestamp, evName, evData);
}

/***************************************************** end GMFInputPipe */

/***************************************************** GMFOutputPipe class */

static void gmf_output_pipe_shmem_send_sample(GMFPipe *apipe, GMF_Sample *sample);
static void gmf_output_pipe_notify_send_sample(GMFPipe *apipe, GMF_Sample *sample);
static void gmf_output_pipe_fd_send_sample(GMFPipe *apipe, GMF_Sample *sample);
static void gmf_output_pipe_corba_send_sample(GMFPipe *apipe, GMF_Sample *sample);
static void gmf_output_pipe_initialize_class (GMFOutputPipeClass * klass);
static void gmf_output_pipe_initialize (GMFOutputPipe * obj);
static void gmf_output_pipe_destroy (GtkObject * obj);
static void gmf_output_pipe_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void gmf_output_pipe_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void gmf_output_pipe_deliver_event (GMFPipe *apipe, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData);

enum {
  QUALITY_NOTIFY,
  OUTPIPE_LAST_SIGNAL
};
static guint outpipe_signals[OUTPIPE_LAST_SIGNAL] = { 0 };

enum { ARG_0_2, ARG_AUTORENDER };

GMF_DEFINE_CLASS_BOILERPLATE (GMFOutputPipe, gmf_output_pipe, GMF_TYPE_PIPE)

/* gmf_output_pipe_get_epv
 *
 * Allocate and return GMF_InputPipe entry point vector
 */
static POA_GMF_OutputPipe__epv *
gmf_output_pipe_get_epv (void)
{
	POA_GMF_OutputPipe__epv *epv;

	epv = g_new0 (POA_GMF_OutputPipe__epv, 1);

	epv->_get_autoRender	= (gpointer) impl_GMF_OutputPipe__get_autoRender;

	return epv;
	
}

static void
init_output_pipe_corba_class (void)
{
	gmf_pipe_vepv._base_epv 			= gmf_pipe_get_servant_base_epv ();
	gmf_output_pipe_vepv.Bonobo_Unknown_epv    	= bonobo_object_get_epv ();
	gmf_output_pipe_vepv.GMF_EventReceiver_epv 	= gmf_event_receiver_epv ();
	gmf_output_pipe_vepv.GMF_Pipe_epv 	   	= gmf_pipe_get_epv ();
	gmf_output_pipe_vepv.GMF_QualityControl_epv	= gmf_quality_control_get_epv ();
	gmf_output_pipe_vepv.GMF_OutputPipe_epv    	= gmf_output_pipe_get_epv ();
}

static void 
gmf_output_pipe_initialize_class (GMFOutputPipeClass * klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = &gmf_output_pipe_destroy;
	object_class->get_arg = &gmf_output_pipe_get_arg;
	object_class->set_arg = &gmf_output_pipe_set_arg;

	init_output_pipe_corba_class ();

	GMF_PIPE_CLASS(klass)->deliver_event = &gmf_output_pipe_deliver_event;

	outpipe_signals[QUALITY_NOTIFY] =
		gtk_signal_new("quality_notify", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFOutputPipeClass, quality_notify),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

	gtk_object_class_add_signals(object_class, outpipe_signals, OUTPIPE_LAST_SIGNAL);

	gtk_object_add_arg_type("GMFOutputPipe::autorender", GTK_TYPE_BOOL,
				 GTK_ARG_READWRITE, ARG_AUTORENDER);
}

static void
gmf_output_pipe_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
	switch(arg_id) {
		case ARG_AUTORENDER:
			((GMFOutputPipe *)(obj))->autorender = GTK_VALUE_BOOL(*arg);
			break;

		default:
			GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, set_arg, (obj, arg, arg_id));
			break;
	}
}

static void
gmf_output_pipe_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
	GMFOutputPipe *apipe;

	apipe = ((GMFOutputPipe *)(obj));

	switch(arg_id) {
		case ARG_AUTORENDER:
			GTK_VALUE_BOOL(*arg) = apipe->autorender;
			break;
		default:
			GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, get_arg, (obj, arg, arg_id));
			break;
	}
}


/* destroy_gmf_output_pipe
 *
 * Destroy bonobo object 	
 */
static void
destroy_gmf_output_pipe (BonoboObject *obj, impl_POA_GMF_OutputPipe *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	
	CORBA_exception_init(&ev);
	
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	POA_GMF_OutputPipe__fini ((PortableServer_Servant) servant, &ev);
	
	g_free (servant);
	
	CORBA_exception_free(&ev);
}


/* create_gmf_output_pipe
 *
 * Create and setup bonobo object 	
 */
static GMF_OutputPipe
create_gmf_output_pipe (GMFOutputPipe *pipe)
{
	impl_POA_GMF_OutputPipe *servant;
	CORBA_Environment ev;

	servant = g_new0 (impl_POA_GMF_OutputPipe, 1);
	servant->servant.vepv = &gmf_output_pipe_vepv;

	CORBA_exception_init (&ev);
	
	POA_GMF_OutputPipe__init ((PortableServer_Servant) servant, &ev);
	if (ev._major != CORBA_NO_EXCEPTION){
		g_free (servant);
		CORBA_exception_free (&ev);
		return CORBA_OBJECT_NIL;
	}
	CORBA_exception_free (&ev);	
		
	gtk_signal_connect (GTK_OBJECT (pipe), "destroy", GTK_SIGNAL_FUNC (destroy_gmf_output_pipe), servant);

	servant->bonobo_object = pipe;
	servant->repeat_id = -1;
	
	return bonobo_object_activate_servant (BONOBO_OBJECT (pipe), servant);
}

static void 
gmf_output_pipe_initialize (GMFOutputPipe *pipe)
{
	((GMFPipe *)(pipe))->attr_pipeInfo.pDirection = GMF_OUT;
	
  	pipe->last_qosinfo.lag.tvSec = pipe->last_qosinfo.lag.tvUsec = 0;
  	pipe->last_qosinfo.dlag.tvSec = pipe->last_qosinfo.dlag.tvUsec = 0;

  	bonobo_object_construct (BONOBO_OBJECT (pipe), create_gmf_output_pipe (pipe));
}

static void
gmf_output_pipe_destroy (GtkObject * obj)
{
	GMF_CALL_PARENT_CLASS (GTK_OBJECT_CLASS, destroy, (obj));
}

static void
gmf_output_pipe_deliver_event (GMFPipe *apipe, GMF_TimeVal *timestamp, const char *evName, CORBA_any *evData)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	GMF_CALL_PARENT_CLASS (GMF_PIPE_CLASS, deliver_event, (apipe, timestamp, evName, evData));

	if(!apipe->attr_pConnectionInfo)
		return;

	GMF_EventReceiver_deliver(apipe->attr_pConnectionInfo->connectedTo, timestamp, (CORBA_char *)evName, evData, &ev);

	CORBA_exception_free (&ev);
}

void
gmf_output_pipe_send_sample(GMFOutputPipe *apipe, GMF_Sample *sample)
{
  g_return_if_fail(apipe);
  g_return_if_fail(GMF_IS_OUTPUT_PIPE(apipe));

  if(!((GMFPipe *)(apipe))->attr_pConnectionInfo)
    return;

  g_return_if_fail(sample->sInfo.actualDataLength && sample->sData._length);

  switch(((GMFPipe *)(apipe))->attr_pConnectionInfo->connectionInfo._d) {
  case GMF_Transport_SHARED_MEM:
    gmf_output_pipe_shmem_send_sample(((GMFPipe *)(apipe)), sample);
    gmf_output_pipe_notify_send_sample(((GMFPipe *)(apipe)), sample);
    break;

  case GMF_Transport_UNIX_SOCKETS:
  case GMF_Transport_TCPIP:
    gmf_output_pipe_fd_send_sample(((GMFPipe *)(apipe)), sample);
    break;

  case GMF_Transport_CORBA:
    gmf_output_pipe_corba_send_sample(((GMFPipe *)(apipe)), sample);
    break;

  case GMF_Transport_CUSTOM:
    gmf_output_pipe_notify_send_sample(((GMFPipe *)(apipe)), sample);
    break;
  }
}

static void
gmf_output_pipe_corba_send_sample(GMFPipe *apipe, GMF_Sample *sample)
{
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

  	GMF_InputPipe_HandleSample(apipe->attr_pConnectionInfo->connectedTo,
			     	   sample, &ev);

	CORBA_exception_free (&ev);			     	   
}

static void
gmf_output_pipe_shmem_send_sample(GMFPipe *apipe, GMF_Sample *sample)
{
  g_error("SHMEM transport NYI");
}	

static void
gmf_output_pipe_notify_send_sample(GMFPipe *apipe, GMF_Sample *sample)
{
	impl_POA_GMF_Pipe *servant;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

  	servant = GTK_TO_PIPE_SERVANT(apipe);

  	GMF_InputPipe_NotifySample(apipe->attr_pConnectionInfo->connectedTo, &ev);

	CORBA_exception_free (&ev);
}

typedef struct {
  GMFPipe *apipe;
  GMF_Sample *sample;
  enum { SINFO, SDATA, SDONE } sendpos;
} SendMe;

static gboolean
gmf_output_pipe_fd_write(SendMe *delivery, gint fd, GdkInputCondition condition)
{
  gboolean retval = TRUE;
  GMF_Sample *sample;
  impl_POA_GMF_Pipe *servant;

  servant = GTK_TO_PIPE_SERVANT(delivery->apipe);
  sample = delivery->sample;

  switch(delivery->sendpos) {
  case SINFO:
    if(write(servant->fd, &sample->sInfo, sizeof(sample->sInfo)) < sizeof(sample->sInfo)) {
      delivery->sendpos = SDONE;
      retval = FALSE;
      gtk_main_quit();
    } else
      delivery->sendpos = SDATA;
    break;
  case SDATA:
    write(servant->fd, sample->sData._buffer, sample->sData._length);
    delivery->sendpos = SDONE;
    retval = FALSE;
    gtk_main_quit();
    break;
  default:
    retval = FALSE;
    break;
  }

  return retval;
}

static void
gmf_output_pipe_fd_send_sample(GMFPipe *apipe, GMF_Sample *sample)
{
  SendMe delivery;
  gint tag;
  impl_POA_GMF_Pipe *servant;

  servant = GTK_TO_PIPE_SERVANT(apipe);

  if(apipe->attr_pConnectionInfo->connectionMediaInfo.fixedSizeSamples)
    {
      g_assert(sample->sData._length == apipe->attr_pConnectionInfo->connectionMediaInfo.sampleSize);
    }

  g_assert(sample->sData._length == sample->sInfo.actualDataLength);

  if(apipe->attr_pConnectionInfo->connectionInfo._d == GMF_Transport_TCPIP)
    g_error("TCP/IP transport sucks at present (no packing/endianness considerations). Use CORBA transport instead.");

  delivery.apipe = apipe;
  delivery.sample = sample;
  delivery.sendpos = SINFO;

  tag = gdk_input_add(servant->fd, GDK_INPUT_WRITE, (GdkInputFunction)gmf_output_pipe_fd_write, &delivery);
  while(delivery.sendpos != SDONE)
    gtk_main();

  gdk_input_remove(tag);
}

static void
impl_GMF_OutputPipe_quality_notify(impl_POA_GMF_OutputPipe * servant,
				   GMF_QualityControl_Quality * qosinfo,
				   CORBA_Environment * ev)
{
	((GMFOutputPipe *)(servant->bonobo_object))->last_qosinfo = *qosinfo;
	gtk_signal_emit(GTK_OBJECT (servant->bonobo_object), outpipe_signals[QUALITY_NOTIFY], qosinfo);
}

static CORBA_boolean
impl_GMF_OutputPipe__get_autoRender(impl_POA_GMF_OutputPipe * servant,
				    CORBA_Environment * ev)
{
	return ((GMFOutputPipe *)(PIPE_SERVANT_TO_GTK(servant)))->autorender?CORBA_TRUE:CORBA_FALSE;
}

void
gmf_output_pipe_deliver_end_of_stream(GMFOutputPipe *apipe, GMF_TimeVal *timestamp)
{
	GMF_InputPipe other_end;
	GMFPipe *p;
	CORBA_any dummy = {(CORBA_TypeCode)TC_null, NULL};	
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);

	p = ((GMFPipe *)(apipe));

	g_return_if_fail(p->attr_pConnectionInfo);

	other_end = p->attr_pConnectionInfo->connectedTo;

	GMF_EventReceiver_deliver(other_end, timestamp, "EndOfStream", &dummy, &ev);

	CORBA_exception_free (&ev);
}

void
gmf_output_pipe_deliver_begin_flush(GMFOutputPipe *apipe)
{
	GMF_InputPipe other_end;
	GMFPipe *p;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	p = ((GMFPipe *)(apipe));

	g_return_if_fail(p->attr_pConnectionInfo);

	other_end = p->attr_pConnectionInfo->connectedTo;

	GMF_InputPipe_BeginFlush(other_end, &ev);

	CORBA_exception_free (&ev);
}

void
gmf_output_pipe_deliver_end_flush(GMFOutputPipe *apipe)
{
	GMF_InputPipe other_end;
	GMFPipe *p;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	p = ((GMFPipe *)(apipe));

	g_return_if_fail(p->attr_pConnectionInfo);

	other_end = p->attr_pConnectionInfo->connectedTo;

	GMF_InputPipe_EndFlush(other_end, &ev);

	CORBA_exception_free (&ev);
}
