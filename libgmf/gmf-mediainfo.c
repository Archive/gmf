#include <gtk/gtk.h>

#include "gmf-mediainfo.h"

typedef struct {
	POA_GMF_MediaInfo servant;
  	GMFMediaInfo *gtk_object;
} impl_POA_GMF_MediaInfo;

static GMF_MediaInfo_Titles *
impl_GMF_MediaInfo__get_mediaTitles(impl_POA_GMF_MediaInfo * servant,
				    CORBA_Environment * ev);
static CORBA_long
impl_GMF_MediaInfo__get_currentTitle(impl_POA_GMF_MediaInfo * servant,
				     CORBA_Environment * ev);
static void
impl_GMF_MediaInfo__set_currentTitle(impl_POA_GMF_MediaInfo * servant,
				     CORBA_long value,
				     CORBA_Environment * ev);
static CORBA_long
impl_GMF_MediaInfo_get_currentDimension(impl_POA_GMF_MediaInfo * servant,
					CORBA_long stream,
					CORBA_Environment * ev);
static void
impl_GMF_MediaInfo_set_currentDimension(impl_POA_GMF_MediaInfo * servant,
					CORBA_long stream,
					CORBA_long dimension,
					CORBA_Environment * ev);

enum {
  GET_MEDIA_TITLES,
  GET_CURRENT_TITLE,
  SET_CURRENT_TITLE,
  GET_CURRENT_DIMENSION,
  SET_CURRENT_DIMENSION,
  LAST_SIGNAL
};
static guint mi_signals[LAST_SIGNAL] = { 0 };

static POA_GMF_MediaInfo__epv impl_GMF_MediaInfo_epv =
{
   NULL,			/* _private */
   (gpointer) & impl_GMF_MediaInfo__get_mediaTitles,
   (gpointer) & impl_GMF_MediaInfo__get_currentTitle,
   (gpointer) & impl_GMF_MediaInfo__set_currentTitle,
   (gpointer) & impl_GMF_MediaInfo_get_currentDimension,
   (gpointer) & impl_GMF_MediaInfo_set_currentDimension
};

static PortableServer_ServantBase__epv base_epv = { NULL, NULL, NULL };
static POA_GMF_MediaInfo__vepv impl_GMF_MediaInfo_vepv =
{
  &base_epv,
  NULL,
  &impl_GMF_MediaInfo_epv
};

static void
impl_GMF_MediaInfo__destroy(BonoboObject *obj, impl_POA_GMF_MediaInfo *servant)
{
	PortableServer_ObjectId *objid;
	CORBA_Environment ev;
	void (*servant_destroy_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	CORBA_exception_init(&ev);
	
	servant_destroy_func = GMF_MEDIA_INFO_CLASS (GTK_OBJECT (servant->gtk_object)->klass)->servant_destroy_func;
	objid = PortableServer_POA_servant_to_id (bonobo_poa (), servant, &ev);
	PortableServer_POA_deactivate_object (bonobo_poa (), objid, &ev);
	CORBA_free (objid);
	obj->servant = NULL;
	
	servant_destroy_func ((PortableServer_Servant) servant, &ev);
	g_free (servant);
	CORBA_exception_free(&ev);
}


static GMF_MediaInfo
impl_GMF_MediaInfo__create (GMFMediaInfo *info, CORBA_Environment * ev)
{
  	GMF_MediaInfo retval;
	impl_POA_GMF_MediaInfo *servant;
	void (*servant_init_func) (PortableServer_Servant servant, CORBA_Environment *ev);
	
	GMFMediaInfoClass *info_class = GMF_MEDIA_INFO_CLASS (GTK_OBJECT(info)->klass);
	
	servant_init_func = info_class->servant_init_func;
	servant = g_new0 (impl_POA_GMF_MediaInfo, 1);
	servant->servant.vepv = info_class->vepv;
	if (!servant->servant.vepv->Bonobo_Unknown_epv)
		servant->servant.vepv->Bonobo_Unknown_epv = bonobo_object_get_epv ();
	servant_init_func ((PortableServer_Servant) servant, ev);
	
	servant->gtk_object = info;
	
	retval = bonobo_object_activate_servant (BONOBO_OBJECT (info), servant);
	
	gtk_signal_connect (GTK_OBJECT (info), "destroy", GTK_SIGNAL_FUNC (impl_GMF_MediaInfo__destroy), servant);
	
	return retval;
}

static GMF_MediaInfo_Titles *
impl_GMF_MediaInfo__get_mediaTitles(impl_POA_GMF_MediaInfo * servant,
				    CORBA_Environment * ev)
{
  GMF_MediaInfo_Titles *retval;

  retval = GMF_MediaInfo_Titles__alloc();
  retval->_length = 0;
  retval->_buffer = NULL;

  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), mi_signals[GET_MEDIA_TITLES], retval);

  return retval;
}

static CORBA_long
impl_GMF_MediaInfo__get_currentTitle(impl_POA_GMF_MediaInfo * servant,
				     CORBA_Environment * ev)
{
  CORBA_long retval = 0;

  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), mi_signals[GET_CURRENT_TITLE], &retval);

  return retval;
}

static void
impl_GMF_MediaInfo__set_currentTitle(impl_POA_GMF_MediaInfo * servant,
				     CORBA_long value,
				     CORBA_Environment * ev)
{
  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), mi_signals[SET_CURRENT_TITLE], value);
}

static CORBA_long
impl_GMF_MediaInfo_get_currentDimension(impl_POA_GMF_MediaInfo * servant,
					CORBA_long stream,
					CORBA_Environment * ev)
{
  CORBA_long retval = 0;

  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), mi_signals[GET_CURRENT_DIMENSION], stream, &retval);

  return retval;
}

static void
impl_GMF_MediaInfo_set_currentDimension(impl_POA_GMF_MediaInfo * servant,
					CORBA_long stream,
					CORBA_long dimension,
					CORBA_Environment * ev)
{
  gtk_signal_emit(GTK_OBJECT(servant->gtk_object), mi_signals[SET_CURRENT_DIMENSION], stream, dimension);
}

static void gmf_media_info_class_init(GMFMediaInfoClass *klass);
static void gmf_media_info_init(GMFMediaInfo *obj);
static void gmf_media_info_destroy(GMFMediaInfo *obj);

static GtkObjectClass *media_info_parent_class = NULL;

GtkType
gmf_media_info_get_type (void)
{
  static GtkType gmf_media_info_type = 0;
  if (!gmf_media_info_type)
    {
      static const GtkTypeInfo gmf_media_info_info =
      {
	"GMFMediaInfo",
	sizeof (GMFMediaInfo),
	sizeof (GMFMediaInfoClass),
	(GtkClassInitFunc) gmf_media_info_class_init,
	(GtkObjectInitFunc) gmf_media_info_init,
	NULL, NULL,		/* reserved 1 & 2 */
	NULL
      };

      gmf_media_info_type = gtk_type_unique (bonobo_object_get_type(), &gmf_media_info_info);
      media_info_parent_class = gtk_type_class(gtk_type_parent(gmf_media_info_type));
    }

  return gmf_media_info_type;
}

static void
gmf_media_info_class_init(GMFMediaInfoClass *klass)
{
	GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass);

	object_class->destroy = (gpointer)&gmf_media_info_destroy;

  	klass->servant_init_func = POA_GMF_MediaInfo__init;
	klass->servant_destroy_func = POA_GMF_MediaInfo__fini;
	klass->vepv = &impl_GMF_MediaInfo_vepv;

  	mi_signals[GET_MEDIA_TITLES] =
    		gtk_signal_new("get_media_titles", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaInfoClass, get_media_titles),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  	mi_signals[GET_CURRENT_DIMENSION] =
    		gtk_signal_new("get_current_dimension", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaInfoClass, get_current_dimension),
		   gtk_marshal_NONE__INT_POINTER,
		   GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_POINTER);
  	mi_signals[SET_CURRENT_DIMENSION] =
    		gtk_signal_new("set_current_dimension", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaInfoClass, set_current_dimension),
		   gtk_marshal_NONE__INT_INT,
		   GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_INT);
  	mi_signals[GET_CURRENT_TITLE] =
    		gtk_signal_new("get_current_title", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaInfoClass, get_current_title),
		   gtk_marshal_NONE__POINTER,
		   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
  	mi_signals[SET_CURRENT_TITLE] =
    		gtk_signal_new("set_current_title", GTK_RUN_FIRST,
		   object_class->type,
		   GTK_SIGNAL_OFFSET(GMFMediaInfoClass, set_current_title),
		   gtk_marshal_NONE__INT,
		   GTK_TYPE_NONE, 1, GTK_TYPE_INT);

  	gtk_object_class_add_signals(object_class, mi_signals, LAST_SIGNAL);
}

static void
gmf_media_info_init(GMFMediaInfo *obj)
{
	CORBA_Environment ev;
	CORBA_exception_init(&ev);

	bonobo_object_construct (BONOBO_OBJECT (obj), impl_GMF_MediaInfo__create (obj, &ev));

	CORBA_exception_free(&ev);
}

static void
gmf_media_info_destroy(GMFMediaInfo *obj)
{
	CORBA_Environment ev;

	CORBA_exception_init(&ev);

  	if(media_info_parent_class->destroy)
    		media_info_parent_class->destroy((GtkObject *)obj);

	CORBA_exception_free (&ev);
}

GtkObject *gmf_media_info_new (void)
{
  return gtk_type_new(gmf_media_info_get_type());
}
