/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#ifndef GMF_MEDIA_INFO_H
#define GMF_MEDIA_INFO_H 1

#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-main.h>

#include <libgmf/gmf-corba.h>

/* GtkObject GMFMediaInfo */
#define GMF_TYPE_MEDIA_INFO	(gmf_media_info_get_type())
#define GMF_MEDIA_INFO(obj)	(GTK_CHECK_CAST((obj), GMF_TYPE_MEDIA_INFO, GMFMediaInfo))
#define GMF_MEDIA_INFO_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GMF_TYPE_MEDIA_INFO, GMFMediaInfoClass))
#define GMF_IS_MEDIA_INFO(obj) (GTK_CHECK_TYPE((obj), GMF_TYPE_MEDIA_INFO))
#define GMF_IS_MEDIA_INFO_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GMF_TYPE_MEDIA_INFO))

typedef struct {
  BonoboObject object;
} GMFMediaInfo;

typedef struct {
  BonoboObjectClass klass;

  void (* get_media_titles) (GMFMediaInfo *media_info, GMF_MediaInfo_Titles *retval);
  void (* get_current_title) (GMFMediaInfo *media_info, CORBA_long *retval);
  void (* set_current_title) (GMFMediaInfo *media_info, CORBA_long value);
  void (* get_current_dimension) (GMFMediaInfo *media_info, CORBA_long stream, CORBA_long *retval);
  void (* set_current_dimension) (GMFMediaInfo *media_info, CORBA_long stream, CORBA_long dimension);
  gpointer servant_init_func, servant_destroy_func, vepv;
} GMFMediaInfoClass;

GtkType gmf_media_info_get_type (void);
GtkObject *gmf_media_info_new (void);

#endif
