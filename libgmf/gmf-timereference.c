/*
 *  GMF: The GNOME Media Framework
 *
 *  Copyright (C) 1999 Elliot Lee, Red Hat Software.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Elliot Lee <sopwith@redhat.com>
 *
 */

#include "config.h"
#include <gmf.h>

#include <time.h>
#include <sys/time.h>

static void gmf_time_reference_class_init 	(GMFTimeReferenceClass	*klass);
static void gmf_time_reference_init    		(GMFTimeReference 	*obj);

GtkType
gmf_time_reference_get_type (void)
{
	static GtkType gmf_time_reference_type = 0;
	if (!gmf_time_reference_type) {
		static const GtkTypeInfo gmf_time_reference_info = {
			"GMFTimeReference",
			sizeof (GMFTimeReference),
			sizeof (GMFTimeReferenceClass),
			(GtkClassInitFunc) gmf_time_reference_class_init,
			(GtkObjectInitFunc) gmf_time_reference_init,
			NULL, NULL,		/* reserved 1 & 2 */
			NULL
		};

		gmf_time_reference_type = gtk_type_unique (gtk_object_get_type(), &gmf_time_reference_info);
	}

	return gmf_time_reference_type;
}

static void 
gmf_time_reference_class_init (GMFTimeReferenceClass * klass)
{
	/* GtkObjectClass *object_class = GTK_OBJECT_CLASS (klass); */
}

static void 
gmf_time_reference_init (GMFTimeReference * obj)
{
	obj->alarmids = g_hash_table_new (g_direct_hash, g_direct_equal);
}

GtkObject *
gmf_time_reference_new (void)
{
	return gtk_type_new(gmf_time_reference_get_type());
}

GtkObject *
gmf_time_reference_new_from_corba (GMF_TimeReference obj)
{
	g_warning("gmf_time_reference_new_from_corba() NYI - calling gmf_time_reference_new()");
	return gmf_time_reference_new();
}

GMF_TimeVal
gmf_time_reference_current_time(GMFTimeReference *timeref)
{
	struct timeval tod;
	GMF_TimeVal retval;

	gettimeofday(&tod, NULL);
	retval.tvSec = tod.tv_sec;
	retval.tvUsec = tod.tv_usec;

	return retval;
}

static GMF_Callback_CallbackID cbid_assignment = 1;

GMF_Callback_CallbackID
gmf_time_reference_request_alarm(GMFTimeReference *timeref,
				 GMF_TimeVal *base_time,
				 GMF_TimeVal *stream_time,
				 GSourceFunc cbfunc,
				 gpointer cbdata)
{
	struct timeval tod;
	GMF_TimeVal alarm_time;
	GMF_Callback_CallbackID retval = 0;

	gettimeofday(&tod, NULL);

	alarm_time = gmf_time_add (*base_time, *stream_time);

	if (alarm_time.tvSec < tod.tv_sec || (alarm_time.tvSec == tod.tv_sec 
	    && alarm_time.tvUsec < tod.tv_usec)) {
		retval = g_idle_add (cbfunc, cbdata);
	} else {
		struct timeval tleft;

		tleft.tv_sec = alarm_time.tvSec - tod.tv_sec;
		tleft.tv_usec = alarm_time.tvUsec - tod.tv_usec;

		/* we know when now is, and we know when we want that alarm. Do it */
		retval = g_timeout_add (tleft.tv_sec * 1000 + tleft.tv_usec/1000, cbfunc, cbdata);
	}

	return retval;
}

typedef struct {
	GSourceFunc cbfunc;
	gpointer cbdata;
	guint interval;
	GMFTimeReference *timeref;
	GMF_Callback_CallbackID cbid;
} metronome_info;

static gboolean
gmf_timeref_metronome_start (metronome_info *mi)
{
	g_timeout_add (mi->interval, mi->cbfunc, mi->cbdata);
	return FALSE;
}

GMF_Callback_CallbackID
gmf_time_reference_request_metronome (GMFTimeReference *timeref,
				      GMF_TimeVal *base_time,
				      GMF_TimeVal *frequency_time,
				      GSourceFunc cbfunc,
				      gpointer cbdata)
{
	struct timeval tod;
	metronome_info *newmi, mi;

	gettimeofday (&tod, NULL);

	g_warning("Metronomes are broken - remove_request etc.");

	mi.cbfunc = cbfunc;
	mi.cbdata = cbdata;
	mi.interval = (frequency_time->tvSec*1000 + frequency_time->tvUsec/1000);
	mi.timeref = timeref; /* XXX in the future should refcount it */
	mi.cbid = cbid_assignment++;

	if (base_time->tvSec < tod.tv_sec || (base_time->tvSec == tod.tv_sec
	    && base_time->tvUsec < tod.tv_usec)) {
		gmf_timeref_metronome_start(&mi);
	} else {
		struct timeval tleft;

		tleft.tv_sec = base_time->tvSec - tod.tv_sec;
		tleft.tv_usec = base_time->tvUsec - tod.tv_usec;

		newmi = g_new(metronome_info, 1);
		mi.cbid = g_timeout_add_full (G_PRIORITY_DEFAULT, tleft.tv_sec * 1000 + tleft.tv_usec/1000,
				 	      (GSourceFunc)gmf_timeref_metronome_start, 
				 	      newmi, (GDestroyNotify) g_free);
		*newmi = mi;
	}

	return mi.cbid;
}

void
gmf_time_reference_remove_request (GMFTimeReference *timeref,
				   GMF_Callback_CallbackID cbid)
{
	g_source_remove(cbid);
}

GMF_TimeVal
gmf_time_subtract (GMF_TimeVal t1, GMF_TimeVal t2)
{
	GMF_TimeVal retval;

	retval.tvSec = t1.tvSec - t2.tvSec;
	retval.tvUsec = t1.tvUsec - t2.tvUsec;

	/* normalize */
	retval.tvSec += (retval.tvUsec / 1000000);
	retval.tvUsec = (retval.tvUsec % 1000000);

	if ((retval.tvUsec < 0) && retval.tvSec) {
		retval.tvSec--;
		retval.tvUsec += 1000000;
	}

	return retval;
}

GMF_TimeVal
gmf_time_add(GMF_TimeVal t1, GMF_TimeVal t2)
{
	GMF_TimeVal retval;

	if(t1.tvSec < 0) {
		t1.tvUsec *= -1;
	}
	
	if(t2.tvSec < 0) {
		t2.tvUsec *= -1;
	}

	retval.tvSec = t1.tvSec + t2.tvSec;
	retval.tvUsec = t1.tvUsec + t2.tvUsec;

	/* normalize */
	retval.tvSec += (retval.tvUsec / 1000000);
	retval.tvUsec = (retval.tvUsec % 1000000);

	if ((retval.tvUsec < 0) && retval.tvSec) {
		retval.tvSec--;
		retval.tvUsec += 1000000;
	}

	return retval;
}

gint
gmf_time_compare(GMF_TimeVal t1, GMF_TimeVal t2)
{
	GMF_TimeVal t;

	t = gmf_time_subtract (t2, t1);
	if (t.tvSec > 0) {
		return 1;
	} else if (t.tvSec < 0) {
		return -1;
  	} else {
		if (t.tvUsec > 0) {
			return 1;
		} else if (t.tvUsec < 0) {
			return -1;
  		}
	}
	
	return 0;
}

GMF_TimeVal
gmf_time_multiply (GMF_TimeVal t, gint multiple)
{
	GMF_TimeVal retval;

	retval.tvSec = t.tvSec * multiple;
	retval.tvUsec = t.tvUsec * multiple;

	/* normalize */
	retval.tvSec += (retval.tvUsec / 1000000);
	retval.tvUsec = (retval.tvUsec % 1000000);

	return retval;
}

/* Slooooooooow */
gint
gmf_time_divide (GMF_TimeVal t1, GMF_TimeVal t2, GMF_TimeVal *remainder)
{
	gint retval = 0;

	while ((t1.tvSec > t2.tvSec) || ((t1.tvSec == t2.tvSec)
	        && (t1.tvUsec > t2.tvUsec))) {
		retval++;
		t1 = gmf_time_subtract(t1, t2);
	}

	if (remainder) {
		*remainder = t1;
	}

	return retval;
}
